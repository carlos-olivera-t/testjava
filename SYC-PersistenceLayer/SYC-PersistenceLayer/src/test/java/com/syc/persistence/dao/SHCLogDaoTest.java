package com.syc.persistence.dao;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.syc.persistence.dto.SHCLogDto;
import com.syc.persistence.dao.SHCLogDao;
import com.syc.persistence.dao.impl.GeneralUtil;


import java.sql.Date;

public class SHCLogDaoTest extends BaseTest{
	
	
	@Autowired
	private SHCLogDao shclogDao;
	
	
	
	private static final transient Log log = LogFactory.getLog(SHCLogDao.class);
	
	
	@Test
	public void getInfoLogDetails(){
		
		
		String pan = "5453250100087008";
		int msj =210;
		String trans = "118423";
		String fecha = "29-09-2011";
		String formato = "dd-MM-yyyy";
		Date tranDate = GeneralUtil.fechaToString(fecha, formato);
		
		
	
		SHCLogDto shclogDto= (SHCLogDto) shclogDao.getLogByCardNumberDetails(pan, tranDate, msj, trans);
		if( shclogDto != null ){
			log.debug( shclogDto.toString() );	
		}else{
			log.debug("Informacion No encontrada\n");
		}
	}
	
	
	
	
	
	

}
