package com.syc.persistence.dao;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.syc.persistence.dto.CatBinDto;
import com.syc.persistence.dto.CatBinDto.CatBinPk;

public class CatBinDaoTest extends BaseTest {

	@Autowired
	CatBinDao catBinDao;
	
	/* Hay que poner una imagen en el server
	 * para que funcione el test
	 * */
	@Ignore
	@Test
	@Rollback(false)
	public void updateImage001() {

		BufferedImage img = null;
		try {
			//File f = new File("/home/recf/imagenes/tarjetas/visa-credit-card.jpg");
			File f = new File("/home/recf/imagenes/tarjetas/Credit_cards_Classic.jpg");

			if (f.exists()) {
				img = ImageIO.read(f);
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write( img, "jpg", baos );
				baos.flush();
				byte[] imageInByte = baos.toByteArray();
				baos.close();
				
				catBinDao.updateImage("477130","01",imageInByte);
				
				System.out.println(imageInByte);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void read001() {
		CatBinPk catBinPk = new CatBinDto().getCatBinPk();
		
		catBinPk.setBin("477130");
		catBinPk.setProducto("01");
		
		CatBinDto res = catBinDao.read(catBinPk);
		
		System.out.println(res);
	}
	
	@Test
	public void getBinesByOwner(){
		List<String> bines = catBinDao.findBinByOwner(1);
		
		System.out.println(bines);
	}

}
