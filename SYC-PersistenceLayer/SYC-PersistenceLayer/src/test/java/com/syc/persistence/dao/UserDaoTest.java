package com.syc.persistence.dao;

import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;


import com.syc.persistence.dto.UserDto;

public class UserDaoTest extends BaseTest{

	@Autowired
	UserDao userDao;
	
	@Test
	public void findUser001(){
		UserDto listUsers = userDao.findUserByName("master");
		
		Assert.notNull(listUsers);
	}
	
//	@Test
//	public void findUser002(){
//		UserDto user = userDao.findUserByName("ninguno");				
//	}
	
	@Test
	public void countAllUsers001(){
		long count = userDao.countAllUsers(1);
		
		Assert.isTrue(count > 0);
	}
	
	@Test
	public void findAllUsers001(){
		List<UserDto> usuarios = userDao.findAllUsers(1,1);
		
		Assert.isTrue(usuarios.size() > 0);
	}
	
}
