package com.syc.persistence.dao;

import java.sql.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.syc.persistence.dto.AccountFullDto;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.CardFullDto;


public class CardDaoTest extends BaseTest{
	
	private static final transient Log log = LogFactory.getLog(CardDaoTest.class);
	
	@Autowired
	private CardDao cardDao;
	
	@Autowired
	private AccountDao accountDao;
	
	@Test
	public void isCardDaoNull(){
		Assert.notNull(cardDao);
	}
	
	@Test
	public void getInfoCardByPan(){
		CardDto cardDto = cardDao.findByNumberCard("5887711000110889735");
		if( cardDto != null ){
			log.debug( cardDto.toString() );	
		}else{
			log.debug("Informacion No encontrada");
		}
	}
	
	@Test
	public void updateInactiveStatus(){
		CardDto cardDto = cardDao.findByNumberCard("5887711000110889735");
		if( cardDto != null ){
			log.debug( cardDto.toString() );
			
			/**
			 * Actualizando Estatus
			 */
			
			cardDto.setStatus(51);
			cardDao.sqlUpdate(cardDto);
			
		}else{
			log.debug("Informacion No encontrada");
		}
	}
	
	
	@Test
	public void getaInfoByAccount(){
		String account = "0220000001133111111A";
	
		List<CardDto> list =cardDao.findByAccount(account);
		log.debug( "El tamano de la lista es: " + list.size() );
		
	}
	
	@Test
	public void getaTitInfoByAccount(){
		String account = "1150000000000980345";
	
		CardDto cardDto =cardDao.getTitCardByAccount(account);
		log.debug( cardDto.toString() );
		
	}
	
	
	@Test
	public void insertTarjeta(){
		CardFullDto cardFullDto = new CardFullDto();
		
		cardFullDto.setPan("5555444433332222");
		cardFullDto.setBase("1930950");
		cardFullDto.setCardType("0040");
		cardFullDto.setExpiryDate(new Date(1111111));
		cardFullDto.setPrimaryAcct("1150000000000580340");
		cardFullDto.setOldExpDate(new Date(1111111));
						
		cardDao.saveNewCard(cardFullDto);		
		
		log.info("Tarjeta insertada con exito:" + cardFullDto);
	}
	
	@Test
	public void insertCuenta(){
		AccountFullDto cuenta = new AccountFullDto();
		
		cuenta.setBin("0058877110");
		cuenta.setAccount("5555555555444444444");
		
		accountDao.saveNewAccount(cuenta);		
	}
	
}
