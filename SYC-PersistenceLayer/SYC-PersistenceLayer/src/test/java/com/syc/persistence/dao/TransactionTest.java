package com.syc.persistence.dao;

import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.syc.persistence.dto.mapper.TransactionDto;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/persistence-app-TEST-ctx.xml"})
public class TransactionTest {

private static final transient Log log = LogFactory.getLog(TransactionTest.class);
	
	@Autowired
	private TransactionDao transactionDao;
	
	@Test
	public void isTransactionDaoNull(){
		Assert.notNull(transactionDao);
	}
	
	@Test
	public void getTransactionsList(){
		List<TransactionDto> list = transactionDao.getTransactionsList("5339870000000056", 20, 3, false, 99);
		
		log.debug("El tamano de la Lista es: " + list.size());
		for( TransactionDto transaction: list ){
			log.debug(ReflectionToStringBuilder.toString(transaction));
		}
		
	}
	
}
