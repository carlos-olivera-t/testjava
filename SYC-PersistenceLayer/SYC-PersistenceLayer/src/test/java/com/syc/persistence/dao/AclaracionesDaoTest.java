package com.syc.persistence.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.syc.persistence.dao.impl.GeneralUtil;
import com.syc.persistence.dto.AclaracionDto;

public class AclaracionesDaoTest extends BaseTest{
	
private static final transient Log log = LogFactory.getLog(AclaracionesDaoTest.class);
	
	@Autowired
	private AclaracionesDao aclaracionesDao;
	
	
	@Test
	public void isCardDaoNull(){
		Assert.notNull(aclaracionesDao);
	}
	
	@Test
	public void create(){
		
		AclaracionDto aclaracion = new AclaracionDto();
		aclaracion.setPan("5453250100574963");
		aclaracion.setPcode("2000");
		aclaracion.setMonto(3500);
		aclaracion.setFecha(GeneralUtil.getCurrentSqlDate());
		aclaracion.setAutorizacion("078139");
		aclaracion.setComercio("7097781");
		aclaracion.setStatus(1);
		aclaracion.setFechaAlta(GeneralUtil.getCurrentSqlDate());
		aclaracion.setEmpleadora("COMEX");
		
		aclaracionesDao.create(aclaracion);
		
	}
	
	@Ignore
	@Test
	public void findByFolio(){
		
		AclaracionDto aclaracionDto = aclaracionesDao.findByFolio(200);
		log.debug(aclaracionDto.toString());
		
	}
	
	@Test
	public void findBySHCKey(){
		
		AclaracionDto aclaracion = aclaracionesDao.findBySHCKey("5453250100574963", "078139", GeneralUtil.getCurrentSqlDate());
		log.debug(aclaracion.toString());
		
	}
	
	
	@Test
	public void findByPan(){
		
		List<AclaracionDto> aclaracionList = aclaracionesDao.findByPan("5453250100574963");
		
		for(AclaracionDto aclaracion: aclaracionList)
		
		log.debug(aclaracion.toString());
		
	}


}
