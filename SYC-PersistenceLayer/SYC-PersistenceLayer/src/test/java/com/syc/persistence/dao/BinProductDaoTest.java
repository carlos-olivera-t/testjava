package com.syc.persistence.dao;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.syc.persistence.dto.BinProductDto;
import com.syc.persistence.dto.BinProductId;
import com.syc.persistence.dto.ClientDto;

public class BinProductDaoTest extends BaseTest{
	
	@Autowired
	BinProductDao binProductDao;
	
	@Test
	public void readBinProduct(){
		BinProductDto binProductDto = binProductDao.read(new BinProductId("498588", "02", 1));
		
		System.out.println(binProductDto);
		
		ClientDto clientDto = binProductDto.getClientDto();
		
		System.out.println(clientDto);
	}
	
	@Test
	public void readByBin(){
		List<BinProductDto> binProductDto = binProductDao.getByBin("498588");
		
		System.out.println(binProductDto);
		
		ClientDto clientDto = binProductDto.get(0).getClientDto();
		
		System.out.println(clientDto);
	}

}
