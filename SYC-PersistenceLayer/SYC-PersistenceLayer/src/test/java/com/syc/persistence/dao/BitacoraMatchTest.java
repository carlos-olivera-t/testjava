package com.syc.persistence.dao;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.syc.persistence.dto.BitacoraMatchDto;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/persistence-app-TEST-ctx.xml"})
public class BitacoraMatchTest {
	
	private static final transient Log log = LogFactory.getLog(BitacoraMatchTest.class);
	
	@Autowired
	private BitacoraMatchDao bitacoraDao;
	
	@Test
	public void isBitacoraMatchDaoNull(){
		Assert.notNull(bitacoraDao);
	}
	
	@Test
	public void findInfoByAccount(){
		BitacoraMatchDto bitacoraDto = bitacoraDao.findByAccount("00000140007", "SNICOLAS");
		bitacoraDto.setStatus(21);
		bitacoraDao.update(bitacoraDto);
		log.debug( ReflectionToStringBuilder.toString(bitacoraDto) );
		
//		log.debug(ReflectionToStringBuilder.toString(bitacoraDto));
//		bitacoraDto.setNombreLargo("ANGEL CONTRERAS MARTINEZ");
//		bitacoraDto.setCuenta("00000140008");
//		bitacoraDao.createOrUpdate(bitacoraDto);
		
	}
	
//	@Test
	public void createAccount(){
		BitacoraMatchDto bitacoraDto = bitacoraDao.findByAccount("00000140007", "SNICOLAS");
		bitacoraDto.setStatusCuenta("B");
		bitacoraDao.create(bitacoraDto);
	}

}
