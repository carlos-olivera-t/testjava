package com.syc.persistence.dao;

import java.sql.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.syc.persistence.dao.impl.GeneralUtil;
import com.syc.persistence.dto.CardLimitDto;


public class CardLimitDaoTest extends BaseTest{
	
	
	
	@Autowired
	private CardLimitDao cardLimitDao;
	
	String pan = "4607660000012617";
	int msj =210;
	String trans = "118423";
	String fecha = "29-09-2011";
	String formato = "dd-MM-yyyy";
	Date tranDate = GeneralUtil.fechaToString(fecha, formato);
	
	private static final transient Log log = LogFactory.getLog(CardLimitDao.class);
	@Ignore
	@Test
	
	public void getlimitsDetails(){
	CardLimitDto cardLimitDto = (CardLimitDto) cardLimitDao.getLimitsDetail(pan, "bin");
	if( cardLimitDto != null ){
		log.debug( cardLimitDto.toString());	
	}else{
		log.debug("Informacion No encontrada\n");
	}

	}
	
	@Test
	
	public void getlimitsPan(){
	CardLimitDto cardLimitDto = (CardLimitDto) cardLimitDao.findBypan(pan, "460766");
	if( cardLimitDto != null ){
		log.debug( cardLimitDto.toString());	
	}else{
		log.debug("Informacion No encontrada\n");
	}

	}
}
