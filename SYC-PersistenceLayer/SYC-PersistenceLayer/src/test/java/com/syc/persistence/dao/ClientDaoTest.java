package com.syc.persistence.dao;

import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.syc.persistence.dto.BinProductDto;
import com.syc.persistence.dto.ClientDto;

public class ClientDaoTest extends BaseTest{

	@Autowired
	ClientDao clientDao;
	
	@Test
	public void getClient(){
		ClientDto clientDto = clientDao.read(1);
		
		Set<BinProductDto> bines = clientDto.getIstBinProducts();
		
		
		System.out.println(bines);
	}
}
