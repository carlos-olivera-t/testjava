package com.syc.persistence.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.syc.persistence.dto.EmisorDto;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/persistence-app-TEST-ctx.xml"})
public class EmisorDaoTest {
	
	@Autowired
	EmisorDao emisorDao;
	
	@Ignore
	@Test
	public void test(){
		
		@SuppressWarnings({ "rawtypes", "unused" })
		List lista = emisorDao.findAll();
		EmisorDto emisorDto = emisorDao.findByBin("0000460068");
		
		System.out.println ("EmisorDto:" + emisorDto);

	}
	
	@Test
	public void create(){
		
		EmisorDto emisor = new EmisorDto();
		emisor.setBin("1234");
		emisor.setCuenta("789");
		emisor.setCveEmisor(123455);
		emisor.setPrefijo("123");
		emisor.setEmisor("ci");
		
		emisorDao.create(emisor);
		
		System.out.println("Hola");
		
	}

}
