package com.syc.persistence.dao;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.syc.persistence.dto.IstCardTypeDto;

public class IstCardTypeDaoTest extends BaseTest{

	@Autowired
	IstCardTypeDao istCardTypeDao;
	
	
	@Test
	public void findByBin001(){
		List<IstCardTypeDto> istCardTypeDto =  istCardTypeDao.findByBin("0000506202");
		System.out.println(istCardTypeDto);
	}
	
}
