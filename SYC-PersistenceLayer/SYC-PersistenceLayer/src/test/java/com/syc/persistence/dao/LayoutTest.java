package com.syc.persistence.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.syc.persistence.dto.LayoutDto;


@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/persistence-app-TEST-ctx.xml"})
public class LayoutTest {
	
	private static final transient Log log = LogFactory.getLog(LayoutTest.class);
	
	@Autowired
	private LayoutDao layoutDao;
	
	@Test
	public void isLayoutDaoNull(){
		Assert.notNull(layoutDao);
	}
	
	@Test
	public void getInfoCardByPan(){
		List<LayoutDto> list = layoutDao.getLayoutByIdEmbosser(1);
		
		for( LayoutDto layout: list ){
			log.debug( layout.toString() );
		}
		
	}
	
	
}
