package com.syc.persistence.dao;

import java.sql.Date;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.syc.persistence.dto.EmbosserPropertiesDto;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/persistence-app-TEST-ctx.xml"})
public class EmbosserPropertiesDaoTest {
	
	@Autowired
	EmbosserPropertiesDao embosserPropertiesDao;
	
	@Test
	public void embosserInsert(){
		EmbosserPropertiesDto oEmbosserPropertiesDto = new EmbosserPropertiesDto();
		
		
		oEmbosserPropertiesDto.setCardNumber("1111222233334444");
		oEmbosserPropertiesDto.setExpiryDate(new Date(0));
		oEmbosserPropertiesDto.setNewExpiryDate(new Date(0));
		oEmbosserPropertiesDto.setShortName("shortName");
		oEmbosserPropertiesDto.setName2("Nombre");
		oEmbosserPropertiesDto.setMagstripname("tracks");
		oEmbosserPropertiesDto.setCardIndicator("1");
		oEmbosserPropertiesDto.setIssueDate(new Date(0));
		
		
		embosserPropertiesDao.create(oEmbosserPropertiesDto);		
	}

}
