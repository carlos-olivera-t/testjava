package com.syc.persistence.dao;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.syc.persistence.dto.EmployeeDto;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/persistence-app-TEST-ctx.xml"})
public class EmployeeTest {
	
	private static final transient Log log = LogFactory.getLog(EmployeeTest.class);
	
	@Autowired
	private EmployeeDao employeeDao;
	
	private String ACCOUNT = "92807001";
	
	
	@Test
	public void isEmployeeNull(){
		Assert.notNull(employeeDao);
	}
	
	@Test
	public void findEmployeeByAccount(){
		EmployeeDto employeeDto = employeeDao.findEmployeeByAccount(ACCOUNT);
		log.debug("**********************************");
		log.debug(ReflectionToStringBuilder.toString(employeeDto));
		log.debug("**********************************");
		
	}

}
