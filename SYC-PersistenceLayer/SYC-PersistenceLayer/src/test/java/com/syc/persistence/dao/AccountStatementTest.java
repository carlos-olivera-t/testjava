package com.syc.persistence.dao;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TreeMap;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.syc.persistence.dto.mapper.AccountStatementDto;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
						"/persistence-app-TEST-ctx.xml"})

public class AccountStatementTest {
	
	@Autowired
	AccountStatementDao accountStatement;
	
	@Test
	public void isNull(){
		Assert.notNull(accountStatement);
	}
	
	@Test
	public void listSqlFiles(){
		String employer = "0104700001";
		String product  = "";
		
		List<AccountStatementDto> list = accountStatement.findSqlServerAccountStatement(employer, product, getAccountStatementCutDates(6));
		
		for( AccountStatementDto row: list ){
			System.out.println( row.toString() );
		}
	}
	
	
	/*************************************************************
	 * Genera Fechas Inicial (Fecha de corte mes Anterior) y 
	 * Final (Fecha de corte Inicial - Numero de Meses)
	 * con  formato DD/MM/YYYY  
	 **************************************************************/
    @SuppressWarnings("static-access")
	public static TreeMap<Integer,String> getAccountStatementCutDates( int numberOfPeriods ) {  
    	 TreeMap<Integer,String> map = new TreeMap<Integer,String>();
    	 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    	 String currentDate = null;
    	 String startDate   = null;
    	 String endDate     = null;
    	 
         Calendar calendar = new GregorianCalendar();
         
         currentDate = sdf.format(calendar.getTime());
         
         /** Obteniendo la Fecha de Corte Inicial **/
         calendar.add(Calendar.MONTH,-1);
         calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(calendar.DAY_OF_MONTH));
         startDate= sdf.format(calendar.getTime());

         /** Obteniendo la Fecha de Corte Final **/
         calendar.add(Calendar.MONTH, (numberOfPeriods-1)*-1);
         calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(calendar.DAY_OF_MONTH));
         endDate = sdf.format(calendar.getTime());
         
         map.put(1, currentDate);
         map.put(2, startDate);
         map.put(3, endDate);
         
        return map;
         
     }

}
