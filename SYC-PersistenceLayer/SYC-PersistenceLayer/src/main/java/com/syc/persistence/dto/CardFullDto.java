package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ISTCARD")
public class CardFullDto implements Serializable {

	private static final long serialVersionUID = -2321401983939228026L;

	@Id
	@Column(name = "PAN", length = 19, nullable = false)
	private String pan;

	@Column(name = "BASE", length = 10, nullable = false)
	private String base;

	@Column(name = "REGION", length = 1)
	private String region;

	@Column(name = "BRANCH", length = 3)
	private String branch;

	@Column(name = "MISID", length = 8)
	private String misid;

	@Column(name = "GROUPCODE", length = 1)
	private String groupCode;

	@Column(name = "CARDTYPE", length = 4)
	private String cardType;

	@Column(name = "STATUS", nullable = false)
	private int status;

	@Column(name = "EXPIRYDATE", nullable = false)
	private Date expiryDate;

	@Column(name = "PRIMARYACCT", length = 32, nullable = false)
	private String primaryAcct;

	@Column(name = "PRIMARYACCTTYPE", length = 2, nullable = false)
	private String primaryAcctType;

	@Column(name = "SECONDARYACCT", length = 32)
	private String secondaryAcct;

	@Column(name = "SECONDARYACCTTYPE", length = 2)
	private String secondaryAcctType;

	@Column(name = "RELATIONINDICATOR", length = 1)
	private String relationIndicator;

	@Column(name = "LIMITINDICATOR", length = 1)
	private String limitiIndicator;

	@Column(name = "LANGUAGECODE", length = 1)
	private String languageCode;

	@Column(name = "TXNCODE", length = 4)
	private String txncode;

	@Column(name = "LAST_ACTIVITY_DATE")
	private Date lastActivityDate;

	@Column(name = "LAST_ACTIVITY_TIME")
	private int lastActivityTime;

	@Column(name = "PKVI", length = 1)
	private String pkvi;

	@Column(name = "PVV", length = 4)
	private String pvv;

	@Column(name = "CASH_WD_AMT_USED")
	private double cashWdAmtUsed;

	@Column(name = "CASH_WD_COUNT")
	private int cashWdCount;

	@Column(name = "CASH_WD_DATE")
	private Date cashWdDate;

	@Column(name = "CASH_WD_TIME")
	private int cashWdTime;

	@Column(name = "TC_WD_AMT_USED")
	private double tcWdAmtUused;

	@Column(name = "TC_WD_COUNT")
	private int tcWdCount;

	@Column(name = "TC_WD_DATE")
	private Date tcWdDate;

	@Column(name = "TC_WD_TIME")
	private int tcWdTime;

	@Column(name = "CASH_TC_AMT_USED")
	private double cashTcAmtUsed;

	@Column(name = "CASH_TC_DATE")
	private Date cashTcDate;

	@Column(name = "CASH_TC_TIME")
	private int cashTcTime;

	@Column(name = "PURCH_AMT_USED")
	private double purchAmtUsed;

	@Column(name = "PURCH_COUNT")
	private int purchCcount;

	@Column(name = "PURCH_DATE")
	private Date purchDate;

	@Column(name = "PURCH_TIME")
	private int purchTime;

	@Column(name = "TSF_BASE_CUR_USED")
	private double tsfBaseCurUsed;

	@Column(name = "TSF_BASE_CUR_DATE")
	private Date tsfBaseCurDate;

	@Column(name = "TSF_BASE_CUR_TIME")
	private int tsdBaseCurTime;

	@Column(name = "TSF_MULTI_CUR_USED")
	private double tsfMultiCurUsed;

	@Column(name = "TSF_MULTI_CUR_DATE")
	private Date tsfMultiCurDate;

	@Column(name = "TSF_MULTI_CUR_TIME")
	private int tsdMultiCurTime;

	@Column(name = "PINRETRY")
	private int pinretry;

	@Column(name = "BLOCKCODE", length = 1)
	private String blockCode;

	@Column(name = "ALTBLOCKCODE", length = 1)
	private String altBlockCode;

	@Column(name = "CREDIT_LIMIT")
	private double creditLimit;

	@Column(name = "CREDIT_LIMIT_USED")
	private double creditLimitUsed;

	@Column(name = "CASH_ADVANCE_LIMIT")
	private double cashAdvanceLimit;

	@Column(name = "CASH_ADVANCE_USED")
	private double cashAdvanceUsed;

	@Column(name = "DECLINE_COUNT")
	private int declineCount;

	@Column(name = "DECLINE_DATE")
	private Date declineDate;

	@Column(name = "DECLINE_TIME")
	private int declineTime;

	@Column(name = "TEMP_PKVI", length = 1)
	private String temp_pkvi;

	@Column(name = "TEMP_PVV", length = 4)
	private String temp_pvv;

	@Column(name = "TPKVI", length = 1)
	private String tpkvi;

	@Column(name = "TPVV", length = 4)
	private String tpvv;

	@Column(name = "TPINRETRY")
	private int tpinrentry;

	@Column(name = "TEMP_TPKVI", length = 1)
	private String tempTpkvi;

	@Column(name = "TEMP_TPVV", length = 4)
	private String tempTpvv;

	@Column(name = "CONSENT_DATE")
	private Date consentDate;

	@Column(name = "SEQ_NO")
	private int seqNo;

	@Column(name = "OLD_EXP_DATE")
	private Date oldExpDate;

	@Column(name = "CASH_DEP_USED")
	private double cashDepUsed;

	@Column(name = "CASH_DEP_DATE")
	private Date cashDepDate;

	@Column(name = "PINOFFSET", length = 12)
	private String pinOffset;

	@Column(name = "TEMP_PINOFFSET", length = 12)
	private String tempPinOffset;

	@Column(name = "TPINOFFSET", length = 12)
	private String tpinOffset;

	@Column(name = "TEMP_TPINOFFSET", length = 12)
	private String tempTpinOffset;

	@Column(name = "BALANCE_INDICATOR", length = 1)
	private String balanceIndicator;

	@Column(name = "CASH_WD_AMT_U_INT")
	private double cashWdAmtUInt;

	@Column(name = "CASH_WD_COUNT_INT")
	private int cashWdCountInt;

	@Column(name = "CASH_WD_DATE_INT")
	private Date cashWdDateInt;

	@Column(name = "CASH_WD_TIME_INT")
	private int cashWdTimeInt;

	public CardFullDto(){
		Date fecha1 = null;
		Date fecha2 = null;
		
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		
		try {			
			fecha1 = new Date(formatoFecha.parse("01/01/1970").getTime());			
			fecha2 = new Date(formatoFecha.parse("01/01/1980").getTime());
		} catch (ParseException e) {
			//Si no genera las fechas la manda como nula
		}
		
		this.region = "1";
		this.groupCode = "R";
		this.status = 51;
		this.primaryAcctType = "04";
		this.relationIndicator = "0";
		this.limitiIndicator = "0";
		this.languageCode = "1";
		this.txncode = "0001";
		this.lastActivityDate = fecha1;
		this.pkvi = "2";
		this.cashWdDate = fecha1;
		this.tcWdDate = fecha1;
		this.cashTcDate = fecha1;
		this.purchDate = fecha1;
		this.tsfBaseCurDate = fecha1;
		this.tsfMultiCurDate = fecha1;
		this.blockCode = " ";
		this.altBlockCode = " ";
		this.declineDate = fecha1;
		this.consentDate = fecha2;
		this.cashDepDate = fecha2;
		this.balanceIndicator = "1";
		this.cashWdDateInt = fecha2;
	}
	
	
	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getMisid() {
		return misid;
	}

	public void setMisid(String misid) {
		this.misid = misid;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getPrimaryAcct() {
		return primaryAcct;
	}

	public void setPrimaryAcct(String primaryAcct) {
		this.primaryAcct = primaryAcct;
	}

	public String getPrimaryAcctType() {
		return primaryAcctType;
	}

	public void setPrimaryAcctType(String primaryAcctType) {
		this.primaryAcctType = primaryAcctType;
	}

	public String getSecondaryAcct() {
		return secondaryAcct;
	}

	public void setSecondaryAcct(String secondaryAcct) {
		this.secondaryAcct = secondaryAcct;
	}

	public String getSecondaryAcctType() {
		return secondaryAcctType;
	}

	public void setSecondaryAcctType(String secondaryAcctType) {
		this.secondaryAcctType = secondaryAcctType;
	}

	public String getRelationIndicator() {
		return relationIndicator;
	}

	public void setRelationIndicator(String relationIndicator) {
		this.relationIndicator = relationIndicator;
	}

	public String getLimitiIndicator() {
		return limitiIndicator;
	}

	public void setLimitiIndicator(String limitiIndicator) {
		this.limitiIndicator = limitiIndicator;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getTxncode() {
		return txncode;
	}

	public void setTxncode(String txncode) {
		this.txncode = txncode;
	}

	public Date getLastActivityDate() {
		return lastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}

	public int getLastActivityTime() {
		return lastActivityTime;
	}

	public void setLastActivityTime(int lastActivityTime) {
		this.lastActivityTime = lastActivityTime;
	}

	public String getPkvi() {
		return pkvi;
	}

	public void setPkvi(String pkvi) {
		this.pkvi = pkvi;
	}

	public String getPvv() {
		return pvv;
	}

	public void setPvv(String pvv) {
		this.pvv = pvv;
	}

	public double getCashWdAmtUsed() {
		return cashWdAmtUsed;
	}

	public void setCashWdAmtUsed(double cashWdAmtUsed) {
		this.cashWdAmtUsed = cashWdAmtUsed;
	}

	public int getCashWdCount() {
		return cashWdCount;
	}

	public void setCashWdCount(int cashWdCount) {
		this.cashWdCount = cashWdCount;
	}

	public Date getCashWdDate() {
		return cashWdDate;
	}

	public void setCashWdDate(Date cashWdDate) {
		this.cashWdDate = cashWdDate;
	}

	public int getCashWdTime() {
		return cashWdTime;
	}

	public void setCashWdTime(int cashWdTime) {
		this.cashWdTime = cashWdTime;
	}

	public double getTcWdAmtUused() {
		return tcWdAmtUused;
	}

	public void setTcWdAmtUused(double tcWdAmtUused) {
		this.tcWdAmtUused = tcWdAmtUused;
	}

	public int getTcWdCount() {
		return tcWdCount;
	}

	public void setTcWdCount(int tcWdCount) {
		this.tcWdCount = tcWdCount;
	}

	public Date getTcWdDate() {
		return tcWdDate;
	}

	public void setTcWdDate(Date tcWdDate) {
		this.tcWdDate = tcWdDate;
	}

	public int getTcWdTime() {
		return tcWdTime;
	}

	public void setTcWdTime(int tcWdTime) {
		this.tcWdTime = tcWdTime;
	}

	public double getCashTcAmtUsed() {
		return cashTcAmtUsed;
	}

	public void setCashTcAmtUsed(double cashTcAmtUsed) {
		this.cashTcAmtUsed = cashTcAmtUsed;
	}

	public Date getCashTcDate() {
		return cashTcDate;
	}

	public void setCashTcDate(Date cashTcDate) {
		this.cashTcDate = cashTcDate;
	}

	public int getCashTcTime() {
		return cashTcTime;
	}

	public void setCashTcTime(int cashTcTime) {
		this.cashTcTime = cashTcTime;
	}

	public double getPurchAmtUsed() {
		return purchAmtUsed;
	}

	public void setPurchAmtUsed(double purchAmtUsed) {
		this.purchAmtUsed = purchAmtUsed;
	}

	public int getPurchCcount() {
		return purchCcount;
	}

	public void setPurchCcount(int purchCcount) {
		this.purchCcount = purchCcount;
	}

	public Date getPurchDate() {
		return purchDate;
	}

	public void setPurchDate(Date purchDate) {
		this.purchDate = purchDate;
	}

	public int getPurchTime() {
		return purchTime;
	}

	public void setPurchTime(int purchTime) {
		this.purchTime = purchTime;
	}

	public double getTsfBaseCurUsed() {
		return tsfBaseCurUsed;
	}

	public void setTsfBaseCurUsed(double tsfBaseCurUsed) {
		this.tsfBaseCurUsed = tsfBaseCurUsed;
	}

	public Date getTsfBaseCurDate() {
		return tsfBaseCurDate;
	}

	public void setTsfBaseCurDate(Date tsfBaseCurDate) {
		this.tsfBaseCurDate = tsfBaseCurDate;
	}

	public int getTsdBaseCurTime() {
		return tsdBaseCurTime;
	}

	public void setTsdBaseCurTime(int tsdBaseCurTime) {
		this.tsdBaseCurTime = tsdBaseCurTime;
	}

	public double getTsfMultiCurUsed() {
		return tsfMultiCurUsed;
	}

	public void setTsfMultiCurUsed(double tsfMultiCurUsed) {
		this.tsfMultiCurUsed = tsfMultiCurUsed;
	}

	public Date getTsfMultiCurDate() {
		return tsfMultiCurDate;
	}

	public void setTsfMultiCurDate(Date tsfMultiCurDate) {
		this.tsfMultiCurDate = tsfMultiCurDate;
	}

	public int getTsdMultiCurTime() {
		return tsdMultiCurTime;
	}

	public void setTsdMultiCurTime(int tsdMultiCurTime) {
		this.tsdMultiCurTime = tsdMultiCurTime;
	}

	public int getPinretry() {
		return pinretry;
	}

	public void setPinretry(int pinretry) {
		this.pinretry = pinretry;
	}

	public String getBlockCode() {
		return blockCode;
	}

	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}

	public String getAltBlockCode() {
		return altBlockCode;
	}

	public void setAltBlockCode(String altBlockCode) {
		this.altBlockCode = altBlockCode;
	}

	public double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public double getCreditLimitUsed() {
		return creditLimitUsed;
	}

	public void setCreditLimitUsed(double creditLimitUsed) {
		this.creditLimitUsed = creditLimitUsed;
	}

	public double getCashAdvanceLimit() {
		return cashAdvanceLimit;
	}

	public void setCashAdvanceLimit(double cashAdvanceLimit) {
		this.cashAdvanceLimit = cashAdvanceLimit;
	}

	public double getCashAdvanceUsed() {
		return cashAdvanceUsed;
	}

	public void setCashAdvanceUsed(double cashAdvanceUsed) {
		this.cashAdvanceUsed = cashAdvanceUsed;
	}

	public int getDeclineCount() {
		return declineCount;
	}

	public void setDeclineCount(int declineCount) {
		this.declineCount = declineCount;
	}

	public Date getDeclineDate() {
		return declineDate;
	}

	public void setDeclineDate(Date declineDate) {
		this.declineDate = declineDate;
	}

	public int getDeclineTime() {
		return declineTime;
	}

	public void setDeclineTime(int declineTime) {
		this.declineTime = declineTime;
	}

	public String getTemp_pkvi() {
		return temp_pkvi;
	}

	public void setTemp_pkvi(String temp_pkvi) {
		this.temp_pkvi = temp_pkvi;
	}

	public String getTemp_pvv() {
		return temp_pvv;
	}

	public void setTemp_pvv(String temp_pvv) {
		this.temp_pvv = temp_pvv;
	}

	public String getTpkvi() {
		return tpkvi;
	}

	public void setTpkvi(String tpkvi) {
		this.tpkvi = tpkvi;
	}

	public String getTpvv() {
		return tpvv;
	}

	public void setTpvv(String tpvv) {
		this.tpvv = tpvv;
	}

	public int getTpinrentry() {
		return tpinrentry;
	}

	public void setTpinrentry(int tpinrentry) {
		this.tpinrentry = tpinrentry;
	}

	public String getTempTpkvi() {
		return tempTpkvi;
	}

	public void setTempTpkvi(String tempTpkvi) {
		this.tempTpkvi = tempTpkvi;
	}

	public String getTempTpvv() {
		return tempTpvv;
	}

	public void setTempTpvv(String tempTpvv) {
		this.tempTpvv = tempTpvv;
	}

	public Date getConsentDate() {
		return consentDate;
	}

	public void setConsentDate(Date consentDate) {
		this.consentDate = consentDate;
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public Date getOldExpDate() {
		return oldExpDate;
	}

	public void setOldExpDate(Date oldExpDate) {
		this.oldExpDate = oldExpDate;
	}

	public double getCashDepUsed() {
		return cashDepUsed;
	}

	public void setCashDepUsed(double cashDepUsed) {
		this.cashDepUsed = cashDepUsed;
	}

	public Date getCashDepDate() {
		return cashDepDate;
	}

	public void setCashDepDate(Date cashDepDate) {
		this.cashDepDate = cashDepDate;
	}

	public String getPinOffset() {
		return pinOffset;
	}

	public void setPinOffset(String pinOffset) {
		this.pinOffset = pinOffset;
	}

	public String getTempPinOffset() {
		return tempPinOffset;
	}

	public void setTempPinOffset(String tempPinOffset) {
		this.tempPinOffset = tempPinOffset;
	}

	public String getTpinOffset() {
		return tpinOffset;
	}

	public void setTpinOffset(String tpinOffset) {
		this.tpinOffset = tpinOffset;
	}

	public String getTempTpinOffset() {
		return tempTpinOffset;
	}

	public void setTempTpinOffset(String tempTpinOffset) {
		this.tempTpinOffset = tempTpinOffset;
	}

	public String getBalanceIndicator() {
		return balanceIndicator;
	}

	public void setBalanceIndicator(String balanceIndicator) {
		this.balanceIndicator = balanceIndicator;
	}

	public double getCashWdAmtUInt() {
		return cashWdAmtUInt;
	}

	public void setCashWdAmtUInt(double cashWdAmtUInt) {
		this.cashWdAmtUInt = cashWdAmtUInt;
	}

	public int getCashWdCountInt() {
		return cashWdCountInt;
	}

	public void setCashWdCountInt(int cashWdCountInt) {
		this.cashWdCountInt = cashWdCountInt;
	}

	public Date getCashWdDateInt() {
		return cashWdDateInt;
	}

	public void setCashWdDateInt(Date cashWdDateInt) {
		this.cashWdDateInt = cashWdDateInt;
	}

	public int getCashWdTimeInt() {
		return cashWdTimeInt;
	}

	public void setCashWdTimeInt(int cashWdTimeInt) {
		this.cashWdTimeInt = cashWdTimeInt;
	}

	public String toString() {
		StringBuilder buffer = new StringBuilder();
		buffer.append(" pan[ " + pan + " ]  ");
		buffer.append(" base[ " + base + " ]  ");
		buffer.append(" region[ " + region + " ]  ");
		buffer.append(" branch[ " + branch + " ]  ");
		buffer.append(" misid[ " + misid + " ]  ");
		buffer.append(" groupCode[ " + groupCode + " ]  ");
		buffer.append(" cardType[ " + cardType + " ]  ");
		buffer.append(" status[ " + status + " ]  ");
		buffer.append(" expiryDate[ " + expiryDate + " ]  ");
		buffer.append(" primaryAcct[ " + primaryAcct + " ]  ");
		buffer.append("\n");
		buffer.append(" primaryAcctType[ " + primaryAcctType + " ]  ");
		buffer.append(" secondaryAcct[ " + secondaryAcct + " ]  ");
		buffer.append(" secondaryAcctType[ " + secondaryAcctType + " ]  ");
		buffer.append(" relationIndicator[ " + relationIndicator + " ]  ");
		buffer.append(" limitiIndicator[ " + limitiIndicator + " ]  ");
		buffer.append(" languageCode[ " + languageCode + " ]  ");
		buffer.append(" txncode[ " + txncode + " ]  ");
		buffer.append(" lastActivityDate[ " + lastActivityDate + " ]  ");
		buffer.append(" lastActivityTime[ " + lastActivityTime + " ]  ");
		buffer.append(" pkvi[ " + pkvi + " ]  ");
		buffer.append("\n");
		buffer.append(" pvv[ " + pvv + " ]  ");
		buffer.append(" cashWdAmtUsed[ " + cashWdAmtUsed + " ]  ");
		buffer.append(" cashWdCount [ " + cashWdCount + " ]  ");
		buffer.append(" cashWdDate[ " + cashWdDate + " ]  ");
		buffer.append(" cashWdTime[ " + cashWdTime + " ]  ");
		buffer.append(" tcWdAmtUused[ " + tcWdAmtUused + " ]  ");
		buffer.append(" tcWdCount[ " + tcWdCount + " ]  ");
		buffer.append(" tcWdDate[ " + tcWdDate + " ]  ");
		buffer.append(" tcWdTime[ " + tcWdTime + " ]  ");
		buffer.append(" cashTcAmtUsed[ " + cashTcAmtUsed + " ]  ");
		buffer.append("\n");
		buffer.append(" cashTcDate[ " + cashTcDate + " ]  ");
		buffer.append(" cashTcTime[ " + cashTcTime + " ]  ");
		buffer.append(" purchAmtUsed[ " + purchAmtUsed + " ]  ");
		buffer.append(" purchCcount[ " + purchCcount + " ]  ");
		buffer.append(" purchDate[ " + purchDate + " ]  ");
		buffer.append(" purchTime[ " + purchTime + " ]  ");
		buffer.append(" tsfBaseCurUsed[ " + tsfBaseCurUsed + " ]  ");
		buffer.append(" tsfBaseCurDate[ " + tsfBaseCurDate + " ]  ");
		buffer.append(" tsdBaseCurTime[ " + tsdBaseCurTime + " ]  ");
		buffer.append(" tsfMultiCurUsed [ " + tsfMultiCurUsed + " ]  ");
		buffer.append("\n");
		buffer.append(" tsfMultiCurDate[ " + tsfMultiCurDate + " ]  ");
		buffer.append(" tsdMultiCurTime[ " + tsdMultiCurTime + " ]  ");
		buffer.append(" pinretry[ " + pinretry + " ]  ");
		buffer.append(" blockCode[ " + blockCode + " ]  ");
		buffer.append(" altBlockCode[ " + altBlockCode + " ]  ");
		buffer.append(" creditLimit[ " + creditLimit + " ]  ");
		buffer.append(" creditLimitUsed[ " + creditLimitUsed + " ]  ");
		buffer.append(" cashAdvanceLimit[ " + cashAdvanceLimit + " ]  ");
		buffer.append(" cashAdvanceUsed[ " + cashAdvanceUsed + " ]  ");
		buffer.append(" declineCount[ " + declineCount + " ]  ");
		buffer.append("\n");
		buffer.append(" declineDate[ " + declineDate + " ]  ");
		buffer.append(" declineTime[ " + declineTime + " ]  ");
		buffer.append(" temp_pkvi[ " + temp_pkvi + " ]  ");
		buffer.append(" temp_pvv[ " + temp_pvv + " ]  ");
		buffer.append(" tpkvi[ " + tpkvi + " ]  ");
		buffer.append(" tpvv[ " + tpvv + " ]  ");
		buffer.append(" tpinrentry[ " + tpinrentry + " ]  ");
		buffer.append(" tempTpkvi[ " + tempTpkvi + " ]  ");
		buffer.append(" tempTpvv[ " + tempTpvv + " ]  ");
		buffer.append(" consentDate[ " + consentDate + " ]  ");
		buffer.append("\n");
		buffer.append(" seqNo[ " + seqNo + " ]  ");
		buffer.append(" oldExpDate[ " + oldExpDate + " ]  ");
		buffer.append(" cashDepUsed[ " + cashDepUsed + " ]  ");
		buffer.append(" cashDepDate[ " + cashDepDate + " ]  ");
		buffer.append(" pinOffset[ " + pinOffset + " ]  ");
		buffer.append(" tempPinOffset[ " + tempPinOffset + " ]  ");
		buffer.append(" tpinOffset[ " + tpinOffset + " ]  ");
		buffer.append(" tempTpinOffset[ " + tempTpinOffset + " ]  ");
		buffer.append(" balanceIndicator[ " + balanceIndicator + " ]  ");
		buffer.append(" cashWdAmtUInt[ " + cashWdAmtUInt + " ]  ");
		buffer.append("\n");
		buffer.append(" cashWdCountInt[ " + cashWdCountInt + " ]  ");
		buffer.append(" cashWdDateInt[ " + cashWdDateInt + " ]  ");
		buffer.append(" cashWdTimeInt[ " + cashWdTimeInt + " ]  ");

		return buffer.toString();
	}

}