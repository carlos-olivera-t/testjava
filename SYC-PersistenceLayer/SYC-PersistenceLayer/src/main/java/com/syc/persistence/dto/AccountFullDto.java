package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ISTDBACCT")
public class AccountFullDto implements Serializable {

	private static final long serialVersionUID = 5048986644733845055L;

	@Id
	@Column(name = "ACCOUNT", length = 32)
	private String account;

	@Column(name = "BIN", length = 10)
	private String bin;

	@Column(name = "SHORTNAME", length = 25)
	private String shortName;

	@Column(name = "LANGUAGECODE", length = 1)
	private String languageCode;

	@Column(name = "PRODUCTCODE", length = 2)
	private String productCode;

	@Column(name = "STATUS", length = 2)
	private String status;

	@Column(name = "GROUPCODE", length = 1)
	private String groupCode;

	@Column(name = "REGION", length = 1)
	private String region;

	@Column(name = "CUSTOMER_TYPE", length = 1)
	private String customerType;

	@Column(name = "BRANCH", length = 3)
	private String branch;

	@Column(name = "MISID", length = 8)
	private String misid;

	@Column(name = "CURRENCY")
	private int currency;

	@Column(name = "LEDGER_BALANCE")
	private double ledgerBalance;

	@Column(name = "AVAILABLE_BALANCE")
	private double availableBalance;

	@Column(name = "ADVANCE_LINE")
	private double advanceLine;

	@Column(name = "ADVANCE_LINE_USED")
	private double advanceLineUsed;

	@Column(name = "TXNCODE", length = 4)
	private String txncode;

	@Column(name = "LAST_ACTIVITY_DATE")
	private Date lastActivityDate;

	@Column(name = "LAST_ACTIVITY_TIME")
	private int lastActivityTime;

	@Column(name = "CASH_WD_AMT_USED")
	private double cashWdAmtUsed;

	@Column(name = "CASH_WD_COUNT")
	private int cashWdCount;

	@Column(name = "CASH_WD_DATE")
	private Date cashWdDate;

	@Column(name = "CASH_WD_TIME")
	private int cashWdTime;

	@Column(name = "TC_WD_AMT_USED")
	private double tcWdAmtUsed;

	@Column(name = "TC_WD_COUNT")
	private int tcWdCount;

	@Column(name = "TC_WD_DATE")
	private Date tcWdDate;

	@Column(name = "TC_WD_TIME")
	private int tcWdTime;

	@Column(name = "CASH_TC_AMT_USED")
	private double cashTcAmtUsed;

	@Column(name = "CASH_TC_DATE")
	private Date cashTcDate;

	@Column(name = "CASH_TC_TIME")
	private int cashTcTime;

	@Column(name = "PURCH_AMT_USED")
	private double purchAmtUsed;

	@Column(name = "PURCH_COUNT")
	private int purchCount;

	@Column(name = "PURCH_DATE")
	private Date purchDate;

	@Column(name = "PURCH_TIME")
	private int purchTime;

	@Column(name = "TSF_BASE_CUR_USED")
	private double tsfBaseCurUsed;

	@Column(name = "TSF_BASE_CUR_DATE")
	private Date tsfBaseCurDate;

	@Column(name = "TSF_BASE_CUR_TIME")
	private int tsfBaseCurTime;

	@Column(name = "TSF_MULTI_CUR_USED")
	private double tsfMultiCurUsed;

	@Column(name = "TSF_MULTI_CUR_DATE")
	private Date tsfMultiCurDate;

	@Column(name = "TSF_MULTI_CUR_TIME")
	private int tsfMultiCurTime;

	@Column(name = "AGGREGATE_INDICATOR", length = 1)
	private String aggredateIndicator;

	@Column(name = "LAST_UPDATE_DATE")
	private Date lastUpdateDate;

	@Column(name = "LAST_UPDATE_TIME")
	private int lastUpdateTime;

	@Column(name = "NET_ACTIVITY")
	private double netActivity;

	@Column(name = "CASH_DEP_USED")
	private double cashDepUsed;

	@Column(name = "CASH_DEP_DATE")
	private Date cashDepDate;

	@Column(name = "DECLINE_COUNT_NAC")
	private int declineCountNac;

	@Column(name = "DECLINE_DATE_NAC")
	private Date declineDateNac;

	@Column(name = "DECLINE_COUNT_INT")
	private int declineCountInt;

	@Column(name = "DECLINE_DATE_INT")
	private Date declineDateInt;

	@Column(name = "CASH_WD_AMT_U_INT")
	private double cashWdAmtUInt;

	@Column(name = "CASH_WD_COUNT_INT")
	private int cashWdCountInt;

	@Column(name = "CASH_WD_DATE_INT")
	private Date cashWdDateInt;

	@Column(name = "CASH_WD_TIME_INT")
	private int cashWdTimeInt;

	@Column(name = "PURCH_AMT_U_INT")
	private double purchAmtUInt;

	@Column(name = "PURCH_COUNT_INT")
	private int purchCountInt;

	@Column(name = "PURCH_DATE_INT")
	private Date purchDateInt;

	@Column(name = "PURCH_TIME_INT")
	private int purchTimeInt;

	@Column(name = "CASHBACK_AMT_USED")
	private double cashBackAmtUsed;

	@Column(name = "CASHBACK_COUNT")
	private int cashBackCount;

	@Column(name = "CASHBACK_DATE")
	private Date cashBackDate;

	@Column(name = "CASHBACK_TIME")
	private int cashBackTime;
	
	public AccountFullDto(){
		Date fecha = null;
				
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		
		try {			
			fecha = new Date(formatoFecha.parse("01/01/1970").getTime());						
		} catch (ParseException e) {
			//Si no genera las fechas la manda como nula
		}
				
		this.shortName = "9999999";
		this.languageCode = "1";
		this.productCode = "04";
		this.status = "00";
		this.groupCode = "R";
		this.region = "1";
		this.customerType = "0";
		this.branch = "001";
		this.currency = 484;
		this.txncode = "0001";
		this.lastActivityDate = fecha;
		this.cashWdDate = fecha;
		this.tcWdDate = fecha;
		this.cashTcDate = fecha;
		this.purchDate = fecha;
		this.tsfBaseCurDate = fecha;
		this.tsfMultiCurDate = fecha;
		this.lastUpdateDate = fecha;
		this.cashDepDate = fecha;
		this.declineDateNac = fecha;
		this.declineDateInt = fecha;
		this.cashWdDateInt = fecha;
		this.purchDateInt = fecha;
		
	}
	

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getMisid() {
		return misid;
	}

	public void setMisid(String misid) {
		this.misid = misid;
	}

	public int getCurrency() {
		return currency;
	}

	public void setCurrency(int currency) {
		this.currency = currency;
	}

	public double getLedgerBalance() {
		return ledgerBalance;
	}

	public void setLedgerBalance(double ledgerBalance) {
		this.ledgerBalance = ledgerBalance;
	}

	public double getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(double availableBalance) {
		this.availableBalance = availableBalance;
	}

	public double getAdvanceLine() {
		return advanceLine;
	}

	public void setAdvanceLine(double advanceLine) {
		this.advanceLine = advanceLine;
	}

	public double getAdvanceLineUsed() {
		return advanceLineUsed;
	}

	public void setAdvanceLineUsed(double advanceLineUsed) {
		this.advanceLineUsed = advanceLineUsed;
	}

	public String getTxncode() {
		return txncode;
	}

	public void setTxncode(String txncode) {
		this.txncode = txncode;
	}

	public Date getLastActivityDate() {
		return lastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}

	public int getLastActivityTime() {
		return lastActivityTime;
	}

	public void setLastActivityTime(int lastActivityTime) {
		this.lastActivityTime = lastActivityTime;
	}

	public double getCashWdAmtUsed() {
		return cashWdAmtUsed;
	}

	public void setCashWdAmtUsed(double cashWdAmtUsed) {
		this.cashWdAmtUsed = cashWdAmtUsed;
	}

	public int getCashWdCount() {
		return cashWdCount;
	}

	public void setCashWdCount(int cashWdCount) {
		this.cashWdCount = cashWdCount;
	}

	public Date getCashWdDate() {
		return cashWdDate;
	}

	public void setCashWdDate(Date cashWdDate) {
		this.cashWdDate = cashWdDate;
	}

	public int getCashWdTime() {
		return cashWdTime;
	}

	public void setCashWdTime(int cashWdTime) {
		this.cashWdTime = cashWdTime;
	}

	public double getTcWdAmtUsed() {
		return tcWdAmtUsed;
	}

	public void setTcWdAmtUsed(double tcWdAmtUsed) {
		this.tcWdAmtUsed = tcWdAmtUsed;
	}

	public int getTcWdCount() {
		return tcWdCount;
	}

	public void setTcWdCount(int tcWdCount) {
		this.tcWdCount = tcWdCount;
	}

	public Date getTcWdDate() {
		return tcWdDate;
	}

	public void setTcWdDate(Date tcWdDate) {
		this.tcWdDate = tcWdDate;
	}

	public int getTcWdTime() {
		return tcWdTime;
	}

	public void setTcWdTime(int tcWdTime) {
		this.tcWdTime = tcWdTime;
	}

	public double getCashTcAmtUsed() {
		return cashTcAmtUsed;
	}

	public void setCashTcAmtUsed(double cashTcAmtUsed) {
		this.cashTcAmtUsed = cashTcAmtUsed;
	}

	public Date getCashTcDate() {
		return cashTcDate;
	}

	public void setCashTcDate(Date cashTcDate) {
		this.cashTcDate = cashTcDate;
	}

	public int getCashTcTime() {
		return cashTcTime;
	}

	public void setCashTcTime(int cashTcTime) {
		this.cashTcTime = cashTcTime;
	}

	public double getPurchAmtUsed() {
		return purchAmtUsed;
	}

	public void setPurchAmtUsed(double purchAmtUsed) {
		this.purchAmtUsed = purchAmtUsed;
	}

	public int getPurchCount() {
		return purchCount;
	}

	public void setPurchCount(int purchCount) {
		this.purchCount = purchCount;
	}

	public Date getPurchDate() {
		return purchDate;
	}

	public void setPurchDate(Date purchDate) {
		this.purchDate = purchDate;
	}

	public int getPurchTime() {
		return purchTime;
	}

	public void setPurchTime(int purchTime) {
		this.purchTime = purchTime;
	}

	public double getTsfBaseCurUsed() {
		return tsfBaseCurUsed;
	}

	public void setTsfBaseCurUsed(double tsfBaseCurUsed) {
		this.tsfBaseCurUsed = tsfBaseCurUsed;
	}

	public Date getTsfBaseCurDate() {
		return tsfBaseCurDate;
	}

	public void setTsfBaseCurDate(Date tsfBaseCurDate) {
		this.tsfBaseCurDate = tsfBaseCurDate;
	}

	public int getTsfBaseCurTime() {
		return tsfBaseCurTime;
	}

	public void setTsfBaseCurTime(int tsfBaseCurTime) {
		this.tsfBaseCurTime = tsfBaseCurTime;
	}

	public double getTsfMultiCurUsed() {
		return tsfMultiCurUsed;
	}

	public void setTsfMultiCurUsed(double tsfMultiCurUsed) {
		this.tsfMultiCurUsed = tsfMultiCurUsed;
	}

	public Date getTsfMultiCurDate() {
		return tsfMultiCurDate;
	}

	public void setTsfMultiCurDate(Date tsfMultiCurDate) {
		this.tsfMultiCurDate = tsfMultiCurDate;
	}

	public int getTsfMultiCurTime() {
		return tsfMultiCurTime;
	}

	public void setTsfMultiCurTime(int tsfMultiCurTime) {
		this.tsfMultiCurTime = tsfMultiCurTime;
	}

	public String getAggredateIndicator() {
		return aggredateIndicator;
	}

	public void setAggredateIndicator(String aggredateIndicator) {
		this.aggredateIndicator = aggredateIndicator;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public int getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(int lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public double getNetActivity() {
		return netActivity;
	}

	public void setNetActivity(double netActivity) {
		this.netActivity = netActivity;
	}

	public double getCashDepUsed() {
		return cashDepUsed;
	}

	public void setCashDepUsed(double cashDepUsed) {
		this.cashDepUsed = cashDepUsed;
	}

	public Date getCashDepDate() {
		return cashDepDate;
	}

	public void setCashDepDate(Date cashDepDate) {
		this.cashDepDate = cashDepDate;
	}

	public int getDeclineCountNac() {
		return declineCountNac;
	}

	public void setDeclineCountNac(int declineCountNac) {
		this.declineCountNac = declineCountNac;
	}

	public Date getDeclineDateNac() {
		return declineDateNac;
	}

	public void setDeclineDateNac(Date declineDateNac) {
		this.declineDateNac = declineDateNac;
	}

	public int getDeclineCountInt() {
		return declineCountInt;
	}

	public void setDeclineCountInt(int declineCountInt) {
		this.declineCountInt = declineCountInt;
	}

	public Date getDeclineDateInt() {
		return declineDateInt;
	}

	public void setDeclineDateInt(Date declineDateInt) {
		this.declineDateInt = declineDateInt;
	}

	public double getCashWdAmtUInt() {
		return cashWdAmtUInt;
	}

	public void setCashWdAmtUInt(double cashWdAmtUInt) {
		this.cashWdAmtUInt = cashWdAmtUInt;
	}

	public int getCashWdCountInt() {
		return cashWdCountInt;
	}

	public void setCashWdCountInt(int cashWdCountInt) {
		this.cashWdCountInt = cashWdCountInt;
	}

	public Date getCashWdDateInt() {
		return cashWdDateInt;
	}

	public void setCashWdDateInt(Date cashWdDateInt) {
		this.cashWdDateInt = cashWdDateInt;
	}

	public int getCashWdTimeInt() {
		return cashWdTimeInt;
	}

	public void setCashWdTimeInt(int cashWdTimeInt) {
		this.cashWdTimeInt = cashWdTimeInt;
	}

	public double getPurchAmtUInt() {
		return purchAmtUInt;
	}

	public void setPurchAmtUInt(double purchAmtUInt) {
		this.purchAmtUInt = purchAmtUInt;
	}

	public int getPurchCountInt() {
		return purchCountInt;
	}

	public void setPurchCountInt(int purchCountInt) {
		this.purchCountInt = purchCountInt;
	}

	public Date getPurchDateInt() {
		return purchDateInt;
	}

	public void setPurchDateInt(Date purchDateInt) {
		this.purchDateInt = purchDateInt;
	}

	public int getPurchTimeInt() {
		return purchTimeInt;
	}

	public void setPurchTimeInt(int purchTimeInt) {
		this.purchTimeInt = purchTimeInt;
	}

	public double getCashBackAmtUsed() {
		return cashBackAmtUsed;
	}

	public void setCashBackAmtUsed(double cashBackAmtUsed) {
		this.cashBackAmtUsed = cashBackAmtUsed;
	}

	public int getCashBackCount() {
		return cashBackCount;
	}

	public void setCashBackCount(int cashBackCount) {
		this.cashBackCount = cashBackCount;
	}

	public Date getCashBackDate() {
		return cashBackDate;
	}

	public void setCashBackDate(Date cashBackDate) {
		this.cashBackDate = cashBackDate;
	}

	public int getCashBackTime() {
		return cashBackTime;
	}

	public void setCashBackTime(int cashBackTime) {
		this.cashBackTime = cashBackTime;
	}

}
