package com.syc.persistence.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.EmbosserPropertiesDao;
import com.syc.persistence.dto.EmbosserPropertiesDto;


@Repository
@SuppressWarnings("unchecked")
public class EmbosserPropertiesDaoImpl extends AbstractPersistenceDaoImpl<EmbosserPropertiesDto, Long> implements EmbosserPropertiesDao{
	
	private static final transient Log log = LogFactory.getLog(AbstractPersistenceDaoImpl.class);
	
	@Autowired
	public EmbosserPropertiesDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	public EmbosserPropertiesDto findEmbosserProperiesByPan( String pan ){
		log.debug("Buscando registro en ISTCMSCARD");
		String QUERY =   "FROM EmbosserPropertiesDto " +
						"WHERE cardNumber = '" + pan + "' ";
		return (EmbosserPropertiesDto) DataAccessUtils.uniqueResult(getHibernateTemplate().find(QUERY));
	}
	

}
