package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.syc.persistence.dao.impl.GeneralUtil;

@Entity
@Table(name = "ISTCMSACT")
@IdClass(ActivityPk.class)

@NamedQueries({
	@NamedQuery(name="activity.findByPan", query="SELECT c.txDate, c.txTime FROM CardActivityDto AS c WHERE c.pan = CAST(:pan as char) ORDER BY c.txDate DESC, c.txTime DESC")
})
public class CardActivityDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@AttributeOverrides({
		@AttributeOverride(name = "pan", column =@Column(name = "PAN") ),
		@AttributeOverride(name = "replReason", column =@Column(name = "REPL_REASON") )
	})
	@Column(name = "PAN")
	private String pan;
	@Column(name = "REPL_REASON")
	private String replReason;
	@Column(name = "PAN_REPL")
	private String panReplace;
	@Column(name = "REGION")
	private String region;
	@Column(name = "BRANCH")
	private String branch;
	@Column(name = "CARDTYPE")
	private String cardType;
	@Column(name = "CMS_FUNC")
	private String cmsFunc;
	@Column(name = "STATUS")
	private int    status;
	@Column(name = "CMS_MODE")
	private int    cmsMode;
	@Column(name = "USER_ID")
	private String userId;
	@Column(name = "SUPER_ID")
	private String superId;
	@Column(name = "TX_DATE")
	private Date   txDate;
	@Column(name = "TX_TIME")
	private String txTime;
	@Column(name = "RECNO")
	private int    recno;
	@Column(name = "ACT_COUNT")
	private int    actCount;
	@Column(name = "ACT_DATE")
	private Date   actDate;
	@Column(name = "ACT_TIME")
	private String actTime;
	@Column(name = "PM_STATUS")
	private int    pmStatus;
	@Column(name = "PM_USER_ID")
	private String pmUserId;
	@Column(name = "PM_SUPER_ID")
	private String pmSuperId;
	@Column(name = "PM_DATE")
	private Date   pmDate;
	@Column(name = "PM_TIME")
	private String pmTime;
	@Column(name = "SPARE_CHAR")
	private String spareChar;
	@Column(name = "SPARE_INT")
	private int    spareInt;
	@Column(name = "O_ROWID")
	private int    oRowId;
	@Column(name = "SEQ_NO")
	private int    seqNo;
	
	
	
	
	public CardActivityDto(){
		
		this.pan = null;
		this.panReplace = "0000000000000000000";
		this.region = "1";
		this.branch = "001";
		this.cardType = null;
		this.cmsFunc = null;
		this.status = 0;
		this.cmsMode = 0;
		this.userId = null;
		this.superId = null;
		this.txDate  = new Date(System.currentTimeMillis());
		this.txTime = GeneralUtil.getCurrentTime("HHmmss");
		this.recno = 0;
		this.actCount = 1;
		this.actDate = null;
		this.actTime = "0";
		this.pmStatus = 0;
		this.pmUserId = null;
		this.superId = null;
		this.pmDate = null;
		this.pmTime = null;
		this.spareChar = null;
		this.spareInt = 0;
		this.oRowId = 0;
		this.seqNo = 0;
		this.replReason = null;
		
	}
	/**
	 * Constructor para generar un nuevo registro
	 */
	public CardActivityDto(boolean newRow){
		java.util.Date currentDate = new java.util.Date();
		Date fecha = null;		
		
		String currentTime = null;		
		
		SimpleDateFormat formato1 = new SimpleDateFormat("hhmmss");
		SimpleDateFormat formato2 = new SimpleDateFormat("dd/MM/yyyy");
		
		currentTime = formato1.format(currentDate);
		
		try {			
			fecha = new Date(formato2.parse("01/01/1970").getTime());						
		} catch (ParseException e) {
			//Si no genera las fechas la manda como nula
		}
		
		this.panReplace = "0000000000000000000";
		this.region = "1";
		this.cmsFunc = "01";
		this.status = 64;
		this.userId = "staff1";
		this.txDate = new Date(currentDate.getTime());
		this.txTime = currentTime;
		this.actCount = 0;
		this.actDate = fecha;
		this.actTime = currentTime;
		this.pmUserId = "staff1";
		this.pmDate = fecha;
		this.spareChar = "M";
	}
	
	
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	
	public String getPanReplace() {
		return panReplace;
	}
	public void setPanReplace(String panReplace) {
		this.panReplace = panReplace;
	}

	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	public String getCmsFunc() {
		return cmsFunc;
	}
	public void setCmsFunc(String cmsFunc) {
		this.cmsFunc = cmsFunc;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getCmsMode() {
		return cmsMode;
	}
	public void setCmsMode(int cmsMode) {
		this.cmsMode = cmsMode;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getSuperId() {
		return superId;
	}
	public void setSuperId(String superId) {
		this.superId = superId;
	}
	
	
	public Date getTxDate() {
		return txDate;
	}
	public void setTxDate(Date txDate) {
		this.txDate = txDate;
	}
	
	
	public String getTxTime() {
		return txTime;
	}
	public void setTxTime(String txTime) {
		this.txTime = txTime;
	}
	
	
	public int getRecno() {
		return recno;
	}
	public void setRecno(int recno) {
		this.recno = recno;
	}
	
	
	public int getActCount() {
		return actCount;
	}
	public void setActCount(int actCount) {
		this.actCount = actCount;
	}
	
	
	public Date getActDate() {
		return actDate;
	}
	public void setActDate(Date actDate) {
		this.actDate = actDate;
	}
	
	
	public String getActTime() {
		return actTime;
	}
	public void setActTime(String actTime) {
		this.actTime = actTime;
	}
	
	
	public int getPmStatus() {
		return pmStatus;
	}
	public void setPmStatus(int pmStatus) {
		this.pmStatus = pmStatus;
	}
	
	
	public String getPmUserId() {
		return pmUserId;
	}
	public void setPmUserId(String pmUserId) {
		this.pmUserId = pmUserId;
	}
	
	
	public String getPmSuperId() {
		return pmSuperId;
	}
	public void setPmSuperId(String pmSuperId) {
		this.pmSuperId = pmSuperId;
	}
	
	
	public Date getPmDate() {
		return pmDate;
	}
	public void setPmDate(Date pmDate) {
		this.pmDate = pmDate;
	}
	
	
	public String getPmTime() {
		return pmTime;
	}
	public void setPmTime(String pmTime) {
		this.pmTime = pmTime;
	}
	
	
	public String getSpareChar() {
		return spareChar;
	}
	public void setSpareChar(String spareChar) {
		this.spareChar = spareChar;
	}
	
	
	public int getSpareInt() {
		return spareInt;
	}
	public void setSpareInt(int spareInt) {
		this.spareInt = spareInt;
	}
	
	
	public int getoRowId() {
		return oRowId;
	}
	public void setoRowId(int oRowId) {
		this.oRowId = oRowId;
	}
	
	
	public int getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
	
	
	public String getReplReason() {
		return replReason;
	}
	public void setReplReason(String replReason) {
		this.replReason = replReason;
	}
	
	
	
}

@Embeddable
class ActivityPk implements Serializable{

	private static final long serialVersionUID = 7326130925157166033L;
	
	public ActivityPk(String pan, String replReason){
		this.pan = pan;
		this.replReason = replReason;
	}
	
	public ActivityPk(){}
	
	@Column(name = "PAN")
	private String   pan;
	@Column(name = "REPL_REASON")
	private String   replReason;

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getReplReason() {
		return replReason;
	}

	public void setReplReason(String replReason) {
		this.replReason = replReason;
	}

	

}
