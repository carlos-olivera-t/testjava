package com.syc.persistence.dao;

import com.syc.persistence.dto.CardholderDto;


public interface CardholderDao extends PersistenceDao<CardholderDto, Long>{
	/**
	 * Llena el Objeto ClienteDto con los datos del Cliente
	 * @author Angel Contreras
	 * @param idTarjeta
	 * @return ClienteDto
	 */
	public CardholderDto getCardholderInfoByCardNumber( String pan );
	
	/**
	 * Obtiene los datos del cliente por numero de cuenta
	 * @author Hector Armenta
	 * @param bin
	 * @param account
	 * @return ClienteDto
	 */
	public CardholderDto getCardholderInfoByAccount( String bin, String account );
		
	/**
	 * Llena el Objeto ClienteDto con los datos del Cliente en base
	 * a la cuenta
	 * @author Angel Contreras
	 * @param idTarjeta
	 * @return ClienteDto
	 */

	public CardholderDto getCardholderInfoByAccountTBLEmployee( String account );
	
	/**
	 * Obtiene los datos del Cuentahabiente necesarios para el
	 *            proceso de Maquila 
	 *            
	 * @author Angel Contreras
	 * @param idTarjeta
	 * @return ClienteDto
	 */
	public CardholderDto getMaquilaCardholderInfo( String pan );

	
}
