package com.syc.persistence.dao.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.RolDao;
import com.syc.persistence.dto.RolDto;


@Repository("rolDao")
@SuppressWarnings("unchecked")
public class RolDaoImpl extends AbstractPersistenceDaoImpl<RolDto, Long> implements RolDao {

	
	
	@Autowired
	public RolDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);				
	}

	public List<RolDto> getRolesByList(List<Long> roles) {			
		return (List<RolDto>) getSession().getNamedQuery("rol.findList")
				 .setParameterList("roles", roles)
				 .list(); 			
	}
	
	public RolDto findRolByName(String authority) {		
		return (RolDto) getSession().getNamedQuery("rol.findByName")
				 .setParameter("authority", authority)
				 .uniqueResult(); 	
	}
	
	
	

}
