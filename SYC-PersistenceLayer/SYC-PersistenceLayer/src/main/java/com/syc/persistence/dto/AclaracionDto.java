package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "TBL_ACLARACION_SDOS")
@NamedQueries({
	@NamedQuery(name="aclaracion.findByFolio" , query="FROM AclaracionDto a WHERE a.folio = :folio"),
	@NamedQuery(name="aclaracion.findByPan"   , query="FROM AclaracionDto a WHERE a.pan = CAST(:pan as char)"),
	@NamedQuery(name="aclaracion.findBySHCKey", query="FROM AclaracionDto a WHERE a.pan = CAST(:pan as char) AND a.fecha = :fecha AND a.autorizacion = :autorizacion ")
})
public class AclaracionDto implements Serializable{
	
	private static final long serialVersionUID = -8910768942667739802L;
	
	@SequenceGenerator(name="SEQ_ACLARACIONES", sequenceName = "SEQ_ACLARACIONES")
	
	@Id
	@Column(name = "FOLIO")	
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "SEQ_ACLARACIONES")
	private long   folio;
	
	@Column(name = "TARJETA")
	private String pan;
	
	@Column(name = "PCODE")
	private String    pcode;
	
	@Column(name = "MONTO")
	private double monto;
	
	@Column(name = "FECHA")
	private Date   fecha;
	
	@Column(name = "AUTORIZACION")
	private String autorizacion;
	
	@Column(name = "COMERCIO")
	private String comercio;
	
	@Column(name = "EMPLEADORA")
	private String empleadora;
	
	@Column(name = "STATUS")
	private int    status;
	
	@Column(name = "FECHA_INGRESO")
	private Date   fechaAlta;
	
	@Column(name = "FECHA_MOD")
	private Date   fechaModificacion;
	
	public long getFolio() {
		return folio;
	}
//	public void setFolio(long folio) {
//		this.folio = folio;
//	}
	
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	
	public String getPcode() {
		return pcode;
	}
	public void setPcode(String pcode) {
		this.pcode = pcode;
	}
	
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	
	public String getComercio() {
		return comercio;
	}
	public void setComercio(String comercio) {
		this.comercio = comercio;
	}
	
	public String getEmpleadora() {
		return empleadora;
	}

	public void setEmpleadora(String empleadora) {
		this.empleadora = empleadora;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	public String toString() {
		StringBuilder buffer = new StringBuilder();
		
		buffer.append("folio["+folio+"],");
		buffer.append("pan["+pan+"],");
		buffer.append("pcode["+pcode+"],");
		buffer.append("monto["+monto+"],");
		buffer.append("fecha["+fecha+"],");
		buffer.append("autorizacion["+autorizacion+"],");
		buffer.append("comercio["+comercio+"],");
		buffer.append("status["+status+"],");
		buffer.append("fechaAlta["+fechaAlta+"],");
		buffer.append("fechaModificacion["+fechaModificacion+"],");
		
		return buffer.toString();
	}
	
}	
