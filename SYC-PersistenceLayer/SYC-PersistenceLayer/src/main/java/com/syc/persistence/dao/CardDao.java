package com.syc.persistence.dao;

import java.util.List;

import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.CardFullDto;
import com.syc.persistence.dto.mapper.PanDto;



public interface CardDao extends PersistenceDao<CardDto, Long>{
	
	/**
	 * Recupera la informacion de una Tarjeta
	 * @author Angel Contreras
	 * @param idTarjeta
	 * @return CardDto
	 */
	public CardDto findByNumberCard( String pan );
	
	/*************************************************	
	 * Servicio que devuelve la tarjeta Titular de una 
	 *            cuenta especifica 
	 * @param pan
	 * @return CardDto
	 *************************************************/
	public CardDto getTitCardByAccount( String account );
	
	public List<CardDto> findByAccount( String account );
	
	public List<CardDto> findByAccountMaxOneRow( String account );
	
	public CardDto findByAccountMonex( String bin, String account );
	
	/**
	 * Recupera la informacion de una Tarjeta
	 * en base a la cuenta
	 * @author Angel Contreras
	 * @param idTarjeta
	 * @return CardDto
	 */
	public CardDto findByAccountTblEmployeers(String account);
	
	/**
	 * Recupera una lista con las tarjetas Existentes
	 * @author Angel Contreras
	 * @param idTarjeta
	 * @return TarjetaDto
	 */
	public List<CardDto> findAll( String pan );
	
	
	public int sqlUpdate( CardDto card );
	
	public void runException();
	
	
	public void saveNewCard(CardFullDto card);
	
	/**
	 * Regresa una lista de tarjetas asociadas a una cuenta
	 * @param bin
	 * @param account
	 * @return
	 */
	List<Object[]> findCardsByAccount(String bin, String account);
	
	CardDto findPanByAccount(String bin, String account);
	
	List<CardDto> findIdClient(String bin, String base);
	
	List<CardDto> findIdClientMaxOneRow(String bin, String base);
	
	List<CardDto> findIdClientBus(String bin, String base);
}
