package com.syc.persistence.dao.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.ClientDao;
import com.syc.persistence.dto.ClientDto;

@SuppressWarnings("unchecked")
@Repository("clientDao")
public class ClientDaoImpl extends AbstractPersistenceDaoImpl<ClientDto, Integer> implements ClientDao {

	@Autowired
	public ClientDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}
	
}
