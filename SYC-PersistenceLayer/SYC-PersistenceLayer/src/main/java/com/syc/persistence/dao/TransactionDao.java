package com.syc.persistence.dao;

import java.util.Date;
import java.util.List;

import com.syc.persistence.dto.mapper.TransactionDto;
import com.syc.persistence.dto.mapper.TransactionModDto;

public interface TransactionDao {
	
	/** Metodo que devuelve un lista de Movimientos **/
	public List<TransactionDto> getTransactionsList( String pan, int numMaxOfRows, int operationType, boolean isapproved, int codigoEmisor );
	
	/** Metodo que devuelve un lista de Movimientos en un Periodo Determinado **/
	public List<TransactionDto> getTransactionsPeriodList( String pan, int period, int operationType, int codigoEmisor );
	
	/** Metodo que devuelve un lista de Movimientos en un Periodo Determinado **/
	public List<TransactionModDto> getTransactionsDateList( String pan, Date  initialDate, Date finalDate, int operationType, int codigoEmisor );
	/**
	 * Devuelve una lista de movimientos en un periodo de maximo 3 meses
	 * mandando fecha inicial y final
	 * @param pan
	 * @param initialDate
	 * @param finalDate
	 * @param operationType
	 * @param numMaxOfRows
	 * @return
	 */
	public List<TransactionDto> getTransactionsDateList( String pan, Date  initialDate, Date finalDate, int operationType, int numMaxOfRows, int codigoEmisor );

}
