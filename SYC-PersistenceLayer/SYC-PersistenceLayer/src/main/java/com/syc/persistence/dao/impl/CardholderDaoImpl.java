package com.syc.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.AccountDao;
import com.syc.persistence.dao.AddressDao;
import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dao.CommonFidndersDao;
import com.syc.persistence.dao.EmbosserPropertiesDao;
import com.syc.persistence.dao.IstrelDao;
import com.syc.persistence.dto.CardholderDto;

@Repository("cardHolderDao")
@SuppressWarnings("unchecked")
public class CardholderDaoImpl extends AbstractPersistenceDaoImpl<CardholderDto, Long> implements CardholderDao{
	
	@Autowired
	public CardholderDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	@Autowired
	CardDao    cardDao;
	
	@Autowired
	AccountDao accountDao;
	
	@Autowired
	IstrelDao istrelDao;
	
	@Autowired
	EmbosserPropertiesDao embosserDao;
	
	@Autowired
	CommonFidndersDao findersDao;
	
	@Autowired
	AddressDao addressDao;
	
	public CardholderDto getCardholderInfoByCardNumber( String numberCard ){
		
		CardholderDto cardholderDto = new CardholderDto();
		cardholderDto.setCard( cardDao.findByNumberCard( numberCard ) );
		if( cardholderDto.getCard() != null ){
			cardholderDto.setIstrel( istrelDao.findAccount(numberCard, cardholderDto.getCard().getClientId()) );
			if( cardholderDto.getIstrel()!=null ){
				cardholderDto.setAccount( accountDao.findInfoAccount( cardholderDto.getIstrel().getBin(), cardholderDto.getCard().getAccount()) );
			}
		}else
			cardholderDto = null;
		return cardholderDto;
	}
	
	public CardholderDto getCardholderInfoByAccount( String bin, String account ){
		
		CardholderDto cardholderDto = new CardholderDto();
		cardholderDto.setCard( cardDao.findByAccountMonex(bin, account) );
		if( cardholderDto.getCard() != null ){
			cardholderDto.setIstrel( istrelDao.findAccount(cardholderDto.getCard().getNumberCard(), cardholderDto.getCard().getClientId()) );
			if( cardholderDto.getIstrel()!=null ){
				cardholderDto.setAccount( accountDao.findInfoAccount( cardholderDto.getIstrel().getBin(), cardholderDto.getCard().getAccount()) );
			}
		}else
			cardholderDto = null;
		return cardholderDto;
	}
	
	public CardholderDto getCardholderInfoByAccountTBLEmployee( String account ){
		
		CardholderDto cardholderDto = new CardholderDto();
		cardholderDto.setCard( cardDao.findByAccountTblEmployeers(account) );
		if( cardholderDto.getCard() != null ){
			cardholderDto.setIstrel( istrelDao.findAccount(cardholderDto.getCard().getNumberCard(), cardholderDto.getCard().getClientId()) );
			if( cardholderDto.getIstrel()!=null ){
				cardholderDto.setAccount( accountDao.findInfoAccount( cardholderDto.getIstrel().getBin(), cardholderDto.getCard().getAccount()) );
			}
		}else
			cardholderDto = null;
		return cardholderDto;
	}
	
	public CardholderDto getMaquilaCardholderInfo( String pan ){
		
		CardholderDto cardholderDto = new CardholderDto();
		cardholderDto.setCard( cardDao.findByNumberCard( pan ) );
		if( cardholderDto.getCard() != null ){
			/** Obteniendo los datos de ISTREL **/
			cardholderDto.setIstrel( istrelDao.findAccount(pan, cardholderDto.getCard().getClientId()) );
			if( cardholderDto.getIstrel()!=null ){
				/** Obteniendo los datos de la Cuenta **/
				cardholderDto.setAccount( accountDao.findInfoAccount( cardholderDto.getIstrel().getBin(), cardholderDto.getCard().getAccount()) );
				
				/** Obteniendo las propiedades del Embosador **/
				cardholderDto.setEmbosserProperties( embosserDao.findEmbosserProperiesByPan( pan ) );
				
				/** Obteniendo el IstCardType **/
				cardholderDto.setIstcardType( findersDao.findIstCardTypeInfo( cardholderDto.getIstrel().getBin() ) );
				
				/** Obteniendo el IstBin **/
				cardholderDto.setIstBin( findersDao.findIstBinInfo( "00"+pan.substring(0,8) ) );
			}
		}else
			cardholderDto = null;
		
		return cardholderDto;
		
	}	

}
