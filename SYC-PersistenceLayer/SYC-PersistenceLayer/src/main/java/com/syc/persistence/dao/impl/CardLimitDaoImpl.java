package com.syc.persistence.dao.impl;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.CardLimitDao;
import com.syc.persistence.dto.CardLimitDto;

@Repository("cardLimitDao") 
@SuppressWarnings("unchecked")
public class CardLimitDaoImpl extends AbstractPersistenceDaoImpl<CardLimitDto, CardLimitDto.CardLimitPk> implements CardLimitDao {

	@SuppressWarnings("unused")
	private static final transient Log log = LogFactory.getLog(CardLimitDaoImpl.class);
	
	@Autowired
	public CardLimitDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}

	/** 
	 * @see com.syc.persistence.dao.CardLimitDao#findBypan(java.lang.String)
	 */	
	public CardLimitDto findBypan(String bin, String pan) {
		return (CardLimitDto) getSession().getNamedQuery("cardLimit.findByPk")
				.setString("pan", pan)
				.setString("bin", bin)
				.uniqueResult();				
	}
	
	public CardLimitDto getLimitsDetail(String pan, String bin){
		
		
		final String QUERY = "from CardLimitDto limit "+ 
				"where limit.pan = '" +pan+ "' "+
				"AND limit.bin = :bin ";
				
		Query query = getSession().createQuery(QUERY);
		query.setString("bin", bin);
		return (CardLimitDto) query.uniqueResult();
	}

}
