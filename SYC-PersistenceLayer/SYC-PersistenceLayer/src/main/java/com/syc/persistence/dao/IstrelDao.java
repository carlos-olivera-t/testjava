package com.syc.persistence.dao;

import com.syc.persistence.dto.IstrelDto;


public interface IstrelDao extends PersistenceDao<IstrelDto, Long>{
	
	public IstrelDto findAccount( String numberCard, String base );
	public int sqlUpdate( IstrelDto istrelDto );
	
	public int accountUpdate( IstrelDto istrelDto, String newAccount );

}
