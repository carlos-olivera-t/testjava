package com.syc.persistence.dao.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.TblEmployeeDao;
import com.syc.persistence.dto.TblEmployeeDto;


@SuppressWarnings("unchecked")
@Repository("tblEmployeeDao")
public class TblEmployeeDaoImpl  extends AbstractPersistenceDaoImpl<TblEmployeeDto, String> implements TblEmployeeDao{


	@Autowired
	public TblEmployeeDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}

	@Override
	public TblEmployeeDto findEmployee(String pan, int issCode, int status, String cardType) {
		
		return (TblEmployeeDto) getSession().getNamedQuery("tblemp.findEmployee")
				.setParameter("pan", pan)
				.setParameter("issCode", issCode)
				.setParameter("status", status)
				.setParameter("cardType", cardType)
				.uniqueResult();
	}
	
}
