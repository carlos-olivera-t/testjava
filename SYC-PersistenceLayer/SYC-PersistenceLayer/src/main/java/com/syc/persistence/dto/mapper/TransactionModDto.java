package com.syc.persistence.dto.mapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionModDto", propOrder = {
    "pan",
    "conceptDescription",
    "respCode",
    "respCodeDescription",
    "amount",
    "cash_back",
    "fee",
    "merchantType",
    "posEntryCode",
    "shcDataBuffer",
    "authorization",
    "trxCode",
    "trxDescription",
    "acceptorName",
    "transactionDate",
    "reference",
    "availableBalance"
})

public class TransactionModDto {
	
	private String pan;
	private String conceptDescription;
	private int    respCode;
	private String respCodeDescription;
	private double amount;
	private double cash_back;
	private double fee;
	private int    merchantType;
	private int    posEntryCode;
	private String shcDataBuffer;
	private  String authorization;
	private String trxCode;
	private String trxDescription;
	private String acceptorName;
	private String transactionDate;
	private String reference;
	private double availableBalance;
	
	
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getConceptDescription() {
		return conceptDescription;
	}
	public void setConceptDescription(String conceptDescription) {
		this.conceptDescription = conceptDescription;
	}
	public int getRespCode() {
		return respCode;
	}
	public void setRespCode(int respCode) {
		this.respCode = respCode;
	}
	public String getRespCodeDescription() {
		return respCodeDescription;
	}
	public void setRespCodeDescription(String respCodeDescription) {
		this.respCodeDescription = respCodeDescription;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getCash_back() {
		return cash_back;
	}
	public void setCash_back(double cash_back) {
		this.cash_back = cash_back;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public int getMerchantType() {
		return merchantType;
	}
	public void setMerchantType(int merchantType) {
		this.merchantType = merchantType;
	}
	public int getPosEntryCode() {
		return posEntryCode;
	}
	public void setPosEntryCode(int posEntryCode) {
		this.posEntryCode = posEntryCode;
	}
	public String getShcDataBuffer() {
		return shcDataBuffer;
	}
	public void setShcDataBuffer(String shcDataBuffer) {
		this.shcDataBuffer = shcDataBuffer;
	}
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	public String getTrxCode() {
		return trxCode;
	}
	public void setTrxCode(String trxCode) {
		this.trxCode = trxCode;
	}
	public String getTrxDescription() {
		return trxDescription;
	}
	public void setTrxDescription(String trxDescription) {
		this.trxDescription = trxDescription;
	}
	public String getAcceptorName() {
		return acceptorName;
	}
	public void setAcceptorName(String acceptorName) {
		this.acceptorName = acceptorName;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public double getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(double availableBalance) {
		this.availableBalance = availableBalance;
	}
	
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransactionModDto [pan=");
		builder.append(pan);
		builder.append(", conceptDescription=");
		builder.append(conceptDescription);
		builder.append(", respCode=");
		builder.append(respCode);
		builder.append(", respCodeDescription=");
		builder.append(respCodeDescription);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", cash_back=");
		builder.append(cash_back);
		builder.append(", fee=");
		builder.append(fee);
		builder.append(", merchantType=");
		builder.append(merchantType);
		builder.append(", posEntryCode=");
		builder.append(posEntryCode);
		builder.append(", shcDataBuffer=");
		builder.append(shcDataBuffer);
		builder.append(", authorization=");
		builder.append(authorization);
		builder.append(", trxCode=");
		builder.append(trxCode);
		builder.append(", trxDescription=");
		builder.append(trxDescription);
		builder.append(", acceptorName=");
		builder.append(acceptorName);
		builder.append(", transactionDate=");
		builder.append(transactionDate);
		builder.append(", reference=");
		builder.append(reference);
		builder.append(", availableBalance=");
		builder.append(availableBalance);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	

}
