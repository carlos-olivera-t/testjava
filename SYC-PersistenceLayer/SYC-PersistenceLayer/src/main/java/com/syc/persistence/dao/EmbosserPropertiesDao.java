package com.syc.persistence.dao;

import com.syc.persistence.dto.EmbosserPropertiesDto;

public interface EmbosserPropertiesDao extends PersistenceDao<EmbosserPropertiesDto, Long>{
	
	/**
	 * Recupera la informacion de personalizado de una tarjeta
	 * @author Angel Contreras
	 * @param  pan
	 * @return EmbosserPropertiesDto
	 */
	public EmbosserPropertiesDto findEmbosserProperiesByPan( String pan );
	

}
