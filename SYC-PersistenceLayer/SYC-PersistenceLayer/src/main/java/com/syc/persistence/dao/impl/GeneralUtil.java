package com.syc.persistence.dao.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TreeMap;

public class GeneralUtil {

	public static void main(String args[]){
		
		getPeriodTime();
	}
	
	/*************************************************************
	 * Regresa un objeto sqlDate con la fecha actual
	 **************************************************************/
	public static java.sql.Date getCurrentSqlDate() {
		return new java.sql.Date(System.currentTimeMillis());
	}
	
	
	/*************************************************************
	 * Devuelve la hora actual en un formato especificado
	 **************************************************************/
	public static String getCurrentTime( String format ){
		 return formatTimeStamp(format, new Timestamp(System.currentTimeMillis()));
	 }


	/*************************************************************
	 * Regresa una cadena con el formato especificado *
	 **************************************************************/
	public static String formatTimeStamp(String format, Timestamp date) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	/***********************************
	 * Valida si una Cadena esta Vacia
	 ***********************************/
	public static boolean isEmtyString(String field) {
		if (field != null && field.trim().length() > 0) {
			return false;
		}
		return true;
	}
	
	/***********************
     * Esta es una forma simple de poder convertir fecha
     * de formato String a formato java.sql.Date
     * @return 
     *
     *****************************/
	
	public static java.sql.Date fechaToString(String fecha, String formato){  
		 
		 
		 java.sql.Date fecFormatoDate = null;
		 try {
			 SimpleDateFormat sdf = new java.text.SimpleDateFormat(formato, new Locale("es", "ES"));
			 fecFormatoDate = new java.sql.Date(sdf.parse(fecha).getTime());
			
		 } catch (Exception ex) {
			
			 return null;
		 }
		 return fecFormatoDate;
	    
	 }
	
	/********************************************************
	  * Convertir un String a SqlDate
	  ********************************************************/
	 
	 public static java.sql.Date convertStringToSqlDate( String date, String format ){
		 java.sql.Date sqltDate = null;
		 try{
			 DateFormat formater = new SimpleDateFormat( format );  
			 java.util.Date parsedUtilDate = formater.parse(date);  
			 sqltDate= new java.sql.Date(parsedUtilDate.getTime());  
		 }catch(Exception e){e.printStackTrace();}
		return sqltDate;
	 }
	 
	 /********************************************************
	  * Obtener Periodos de Tiempo para Consulta de Movimientos
	  ********************************************************/
	 
	 public static TreeMap<String,Date> getPeriodTime( int period, String format ){
	
		 TreeMap<String,Date> map = new TreeMap<String,Date>();
			
			Calendar calendar = Calendar.getInstance();
			String startDate;
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String   currentDate = sdf.format(calendar.getTime());
	        String day = currentDate.substring(0, 2);
	        int dia = Integer.parseInt(day);
	         
	         /** Obteniendo la Fecha inicial **/
	         calendar.add(Calendar.DATE, - (dia-1));
	         calendar.add(Calendar.MONTH, - (period -1));
	         startDate= sdf.format(calendar.getTime());
	         
	         /**Obtiniendo fecha final**/
	         calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE)); 
			 String finalDate = sdf.format(calendar.getTime());	
			 /****convirtiendo los string a tipo de dato Date para su posterior busqueda en la bd***/
			Date startDateSql = convertStringToSqlDate(startDate, format);
			Date finalDateSql = convertStringToSqlDate(finalDate, format);
			
			map.put("FechaInicial", startDateSql);
	        map.put("FechaFinal", finalDateSql);
	     
	        return map;
			
						
			
			
	 }
	
	 /**
	  * Regresa un rango de fechas hasta 3 meses atras... contando el mes en curso
	  * @param finalDate
	  * @return
	  */
	 public static TreeMap<String,Date> getPeriodTime(){
		 
		 TreeMap<String,Date> map = new TreeMap<String,Date>();
			
			Calendar calendar = Calendar.getInstance();
			String startDate;
			String format = "dd/MM/yyyy";
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			String   currentDate = sdf.format(calendar.getTime());
	        String day = currentDate.substring(0, 2);
	        int dia = Integer.parseInt(day);
	         
	         /** Obteniendo la Fecha inicial **/
	         startDate= sdf.format(calendar.getTime());
	         
	         /** Obteniendo la Fecha final **/
	         calendar.add(Calendar.DATE, - (dia-1));
	         calendar.add(Calendar.MONTH, - 2);
			 String finalDate1 = sdf.format(calendar.getTime());	
			 /****convirtiendo los string a tipo de dato Date para su posterior busqueda en la bd***/
			Date startDateSql = convertStringToSqlDate(startDate, format);
			Date finalDateSql = convertStringToSqlDate(finalDate1, format);
			
			map.put("FechaInicial", startDateSql);
	        map.put("FechaFinal", finalDateSql);
	     

	        return map;
		 
	 }
	 /***************************************************************
	 * Rellena de Caracteres una cadena a una Longitud determinada
	 ***************************************************************/
	public static String rellenaCadena( String cadena, char caracter, int longitud, String posicion ){
		String cadenaFinal = null;
		String aux         = "";
		if( cadena!=null ){
			if( cadena.length() > longitud ){
				cadenaFinal = cadena.substring(0,longitud);
			}else{
				for( int i=0; i<(longitud-cadena.length()); i++ ){
					aux += caracter;				  
				}
				cadenaFinal = posicion.equals("R") ? cadena+aux : aux+cadena;
			}
		}
		return cadenaFinal;
	}
	 
}
