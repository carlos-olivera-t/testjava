package com.syc.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.LogAccountDao;
import com.syc.persistence.dto.LogAccountDto;

@SuppressWarnings("unchecked")
@Repository("logAccountDao")
public class LogAccountDaoImpl extends AbstractPersistenceDaoImpl<LogAccountDto, Long> implements LogAccountDao{

	@Autowired
	public LogAccountDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	@Override
	public String findLastAccount(String bin) {
		return   (String) getSession().getNamedQuery("loga.findLastAccount")
				 .setParameter("bin", bin)
				 .uniqueResult();
	}

}
