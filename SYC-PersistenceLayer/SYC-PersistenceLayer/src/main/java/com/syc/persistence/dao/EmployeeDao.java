package com.syc.persistence.dao;

import com.syc.persistence.dto.EmployeeDto;

public interface EmployeeDao extends PersistenceDao<EmployeeDto, Long>{
	
	/**
	 * @param       => Account
	 * @return      => EmployeeDto
	 * @description => Metodo que busca informacion de un empleado especificamente de Monex
	 */
	public EmployeeDto findEmployeeByAccount( String account );
	/**
	 * @param       => Account
	 * @param 		=> emisor    identificador de cliente
	 * @return      => EmployeeDto
	 * @description => Metodo que busca informacion de un empleado 
	 */
	public EmployeeDto findEmployeeByAccount( String account, int emisor);

}
