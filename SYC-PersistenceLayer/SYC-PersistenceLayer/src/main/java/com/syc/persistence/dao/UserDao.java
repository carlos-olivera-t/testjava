package com.syc.persistence.dao;


import java.util.List;

import com.syc.persistence.dto.UserDto;

public interface UserDao extends PersistenceDao<UserDto, Long>{

	/**
	 * Busca a un usuario por nombre
	 * @param name
	 * @return
	 */
	UserDto findUserByName(String name);
	
	/**
	 * Busca a un usuario por nombre y por estatus
	 * @param name
	 * @return
	 */
	List<UserDto> findUserByName(String name, int status);
	
	/**
	 * Hace un count del query que extrae los usuarios
	 * @param idOwner
	 * @return
	 */
	long countAllUsers(int idOwner);
	
	/**
	 * Query que extrae los usuarios
	 * @param pageNumber
	 * @return
	 */
	public List<UserDto> findAllUsers(int pageNumber, int idOwner); 
	
	
	int borraUsuario(long Id);
	
	/**
	 * Query que busca el usuario que pertenece a determinado perfil
	 * @param name, idOwner
	 * @return
	 */
	
	UserDto findUserByProfile(String name, Long idOwner);
	
	
}