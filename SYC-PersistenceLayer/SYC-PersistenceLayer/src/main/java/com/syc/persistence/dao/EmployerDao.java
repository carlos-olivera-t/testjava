package com.syc.persistence.dao;

import com.syc.persistence.dto.EmployerDto;

public interface EmployerDao extends PersistenceDao<EmployerDto, Long>{
	
	EmployerDto findEmployer(String employer, int issCode, String tiType);

}
