package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.syc.persistence.dao.impl.GeneralUtil;

@Entity
@Table(name="SYCLOGACCOUNT")
@NamedQueries({
	@NamedQuery(name="loga.findLastAccount", query="SELECT max(log.lastAccount +1) FROM LogAccountDto log WHERE log.bin =:bin")
	
})
public class LogAccountDto implements Serializable {
	

	private static final long serialVersionUID = -8526259183937947889L;
	
	@Column(name = "CONSEQ")
	private long conseq;
	@Id
	@Column(name = "BIN", length = 10, nullable = false)
	private String bin;
	@Column(name = "LASTACTIVITY", nullable = false)
	private Date lastActivity;
	@Column(name = "LASTACCOUNT", nullable = false)
	private String lastAccount;
	@Column(name = "PERSONAL", nullable = false)
	private String personal;
	@Column(name = "FILENAMECREATED",length = 60)
	private String fileNameCreated;
	@Column(name = "NOTES", nullable = true, length = 100)
	private String notes;
	@Column(name = "MAQ_FILE", nullable = true, length = 38)
	private String maq_file;
	
	
	
	public LogAccountDto() {
		Date currentDate   = GeneralUtil.getCurrentSqlDate();
		conseq = 0;
		lastActivity = currentDate;
		personal = "ws_user";
		fileNameCreated = null;
		notes = "se generó tarjeta vía WS";
		maq_file = null;
		
	}
	public long getConseq() {
		return conseq;
	}
	public void setConseq(long conseq) {
		this.conseq = conseq;
	}
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	public Date getLastActivity() {
		return lastActivity;
	}
	public void setLastActivity(Date lastActivity) {
		this.lastActivity = lastActivity;
	}
	public String getLastAccount() {
		return lastAccount;
	}
	public void setLastAccount(String lastAccount) {
		this.lastAccount = lastAccount;
	}
	public String getPersonal() {
		return personal;
	}
	public void setPersonal(String personal) {
		this.personal = personal;
	}
	public String getFileNameCreated() {
		return fileNameCreated;
	}
	public void setFileNameCreated(String fileNameCreated) {
		this.fileNameCreated = fileNameCreated;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getMaq_file() {
		return maq_file;
	}
	public void setMaq_file(String maq_file) {
		this.maq_file = maq_file;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LogAccountDto [conseq=");
		builder.append(conseq);
		builder.append(", bin=");
		builder.append(bin);
		builder.append(", LastActivity=");
		builder.append(lastActivity);
		builder.append(", lastAccount=");
		builder.append(lastAccount);
		builder.append(", personal=");
		builder.append(personal);
		builder.append(", fileNameCreated=");
		builder.append(fileNameCreated);
		builder.append(", notes=");
		builder.append(notes);
		builder.append(", maq_file=");
		builder.append(maq_file);
		builder.append("]");
		return builder.toString();
	}
	
	

}
