package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_EMPLEADORAS_DETAIL")
@IdClass(EmpleadoraDetailPk.class)
@NamedQueries({
	@NamedQuery(name="employerDetail.findByEmprLocCode" , query=" FROM EmployerDetailDto a WHERE a.emprLocCode = :emprLocCode AND a.issCode = :issCode AND a.tiType = :tiType")
	})
public class EmployerDetailDto implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@AttributeOverrides({
		@AttributeOverride(name = "issCode", column =@Column(name = "ISS_CODE") ),
		@AttributeOverride(name = "emprLocCode", column =@Column(name = "EMPR_LOC_CODE") ),
		@AttributeOverride(name = "tiType", column =@Column(name = "TI_TYPE") )
		
	})
	
	private String emprLocCode;
	private String tiType;
	private int issCode;
	
	
	@Column(name="EMPR_CODE")
	private String emprCode;
	@Column(name="EMPR_LOC_NAME")
	private String emprLocName;
	@Column(name="STREET_NUMBER")
	private String streetNumber;
	@Column(name="COUNTRY")
	private String country;
	@Column(name="CITY")
	private String city;
	@Column(name="STATE")
	private String state;
	@Column(name="ZIP_CODE")
	private String zopCode;
	@Column(name="EMPR_PHONE")
	private String emprPhone;
	@Column(name="CONTACT")
	private String contact;
	@Column(name="CONTACT_EMAIL")
	private String contactMail;
	@Column(name="FCH_INSERT")
	private Date insertDate;
	@Column(name="FCH_ALTA_SYC")
	private Date SycDate;
	@Column(name="NOM_ARCHIVO")
	private String fileName;
	@Column(name="ESTATUS")
	private String status;

	@Column(name="RFC")
	private String rfc;
	@Column(name="MUNICIPIO")
	private String municipality;
	
	
	public int getIssCode() {
		return issCode;
	}
	public void setIssCode(int issCode) {
		this.issCode = issCode;
	}
	public String getEmprCode() {
		return emprCode;
	}
	public void setEmprCode(String emprCode) {
		this.emprCode = emprCode;
	}
	public String getEmprLocCode() {
		return emprLocCode;
	}
	public void setEmprLocCode(String emprLocCode) {
		this.emprLocCode = emprLocCode;
	}
	public String getEmprLocName() {
		return emprLocName;
	}
	public void setEmprLocName(String emprLocName) {
		this.emprLocName = emprLocName;
	}
	public String getStreetNumber() {
		return streetNumber;
	}
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZopCode() {
		return zopCode;
	}
	public void setZopCode(String zopCode) {
		this.zopCode = zopCode;
	}
	public String getEmprPhone() {
		return emprPhone;
	}
	public void setEmprPhone(String emprPhone) {
		this.emprPhone = emprPhone;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getContactMail() {
		return contactMail;
	}
	public void setContactMail(String contactMail) {
		this.contactMail = contactMail;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public Date getSycDate() {
		return SycDate;
	}
	public void setSycDate(Date sycDate) {
		SycDate = sycDate;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTyType() {
		return tiType;
	}
	public void setTyType(String tiType) {
		this.tiType = tiType;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getMunicipality() {
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}	

}


@Embeddable
class EmpleadoraDetailPk implements Serializable{
	

	private static final long serialVersionUID = 1L;
	
	public EmpleadoraDetailPk(String emprLocCode, String tiType, int issCode) {
		super();
		this.emprLocCode = emprLocCode;
		this.tiType = tiType;
		this.issCode = issCode;
	}
	
	
	public EmpleadoraDetailPk() {}


	@Column(name = "EMPR_LOC_CODE")
	private String emprLocCode;
	@Column(name = "TI_TYPE")
	private String tiType;
	@Column(name = "ISS_CODE")
	private int issCode;

	public String getEmprLocCode() {
		return emprLocCode;
	}
	public void setEmprLocCode(String emprLocCode) {
		this.emprLocCode = emprLocCode;
	}
	public String getTiType() {
		return tiType;
	}
	public void setTiType(String tiType) {
		this.tiType = tiType;
	}
	public int getIssCode() {
		return issCode;
	}
	public void setIssCode(int issCode) {
		this.issCode = issCode;
	}
	@Override
	public String toString() {
		return "EmpleadoraDetailPk [emprLocCode=" + emprLocCode + ", tiType=" + tiType + ", issCode=" + issCode + "]";
	}
	
	
	
	
	
}
