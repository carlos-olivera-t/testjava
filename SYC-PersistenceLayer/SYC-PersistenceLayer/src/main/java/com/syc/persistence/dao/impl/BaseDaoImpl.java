package com.syc.persistence.dao.impl;


import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.BaseDao;
import com.syc.persistence.dto.BaseDto;

@SuppressWarnings("unchecked")
@Repository("baseDao")
public class BaseDaoImpl extends AbstractPersistenceDaoImpl<BaseDto,String> implements BaseDao {

	@Autowired
	public BaseDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	public long getMaxBase() {
		String sqlQuery = "select SEQ_ISTBASE.NEXTVAL as id from dual";
				
		return (Long) getSession().createSQLQuery(sqlQuery).addScalar("id", Hibernate.LONG).uniqueResult();		
	}
	
}
