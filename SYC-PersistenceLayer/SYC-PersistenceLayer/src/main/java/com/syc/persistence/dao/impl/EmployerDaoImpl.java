package com.syc.persistence.dao.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.EmployerDao;
import com.syc.persistence.dto.EmployerDto;


@SuppressWarnings("unchecked")
@Repository("employerDao")
public class EmployerDaoImpl extends AbstractPersistenceDaoImpl<EmployerDto, Long> implements EmployerDao{

	
	@Autowired
	public EmployerDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}

	@Override
	public EmployerDto findEmployer(String employer, int issCode, String tiType) {
		return (EmployerDto) getSession().getNamedQuery("employer.findByCode")
				 .setParameter("emprCode", employer)
				 .setParameter("issCode", issCode)
				 .setParameter("tiType", tiType)
				 .uniqueResult(); 	
	}
	
}
