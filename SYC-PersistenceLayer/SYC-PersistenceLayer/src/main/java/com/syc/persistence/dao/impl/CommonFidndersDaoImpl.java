package com.syc.persistence.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.CommonFidndersDao;
import com.syc.persistence.dto.IstBinDto;
import com.syc.persistence.dto.IstCardTypeDto;


@SuppressWarnings("unchecked")
@Repository
public class CommonFidndersDaoImpl extends AbstractPersistenceDaoImpl<Object, Long> implements CommonFidndersDao {
	
private static final transient Log log = LogFactory.getLog(CommonFidndersDaoImpl.class);
	
	@Autowired
	public CommonFidndersDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	public IstCardTypeDto findIstCardTypeInfo( String bin ){
		
		String QUERY =
					   "SELECT * "+
						  "FROM ( "+
						        "SELECT BIN, "+ 
						               "A_DESC, "+
						               "CASH_WD_LIMIT, "+
						               "CARDTYPE, "+        
						               "ICC_INDICATOR, "+ 
						               "SERVICE_CODE, "+ 
						               "ICC_CARD_PREFIX, "+     
						               "CARD_LIFE " +
						          "FROM ISTCARDTYPE "+ 
						         "WHERE BIN = '"+bin+"' "+
						         "ORDER BY CARDTYPE ASC "+
						       ") "+
						 "WHERE ROWNUM <=1 ";
		
		return (IstCardTypeDto) getSession().createSQLQuery(QUERY).addEntity("IstCardTypeDto",IstCardTypeDto.class ).uniqueResult();
	}
	
	public IstBinDto findIstBinInfo( String bin ){
		log.debug("Buscando registro en IST_BIN");
		final String QUERY = "FROM IstBinDto " +
				            "WHERE bin = '"+bin+"'";
		return (IstBinDto) DataAccessUtils.uniqueResult(getHibernateTemplate().find(QUERY));
	}
	
}
