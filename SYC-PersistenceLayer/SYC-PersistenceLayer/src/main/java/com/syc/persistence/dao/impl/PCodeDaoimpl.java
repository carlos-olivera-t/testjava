package com.syc.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.PCodeDao;
import com.syc.persistence.dto.PCodeDto;


@SuppressWarnings("unchecked")
@Repository("pCodeDao")
public class PCodeDaoimpl extends AbstractPersistenceDaoImpl<PCodeDto,String> implements PCodeDao{

	@Autowired
	public PCodeDaoimpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	@Override
	public PCodeDto findIdPCode(String id) {
		
		return (PCodeDto) getSession().getNamedQuery("pcodes.findId")
				 .setParameter("id", id)
				 .uniqueResult(); 	
	}

}
