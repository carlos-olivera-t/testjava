package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.syc.persistence.dao.impl.GeneralUtil;


@Entity
@Table(name = "SHCLOG")
@IdClass(WShcLogPK.class)
@NamedQueries({
@NamedQuery(name="shclog.findLimitsByPan", query="SELECT SUM(amount) FROM SHCLogDto shc WHERE shc.numberCard = CAST(:pan  as char) and shc.tranDate = :tranDate")
})
public class SHCLogDto {

	@Column(name = "MSGTYPE" )
	private int    msgType;
	@Column(name = "FLIPPED_MSGTYPE")
	private int    flippedMsgType;
	@Column( name = "TRANTIME" )
	private String tranTime;
	@Column(name = "TXNTYPE")
	private int    txtType; 
	@Column(name = "AMOUNT", nullable = false)
	private double amount;
	@Column(name = "AVAL_BALANCE")
	private double avalBalance;
	@Column(name = "AMOUNT_EQUIV")
	private double amountEquiv;
	@Column( name = "CASH_BACK" )
	private double cashBack;
	@Column( name = "ISS_CONV_RATE" )
	private double issConvRate;
	@Column( name = "ISS_CURRENCY_CODE" )
	private int    issCurrencyCode;
	@Column( name = "ISS_CONV_DATE" )
	private Date   issConvDate;
	@Column( name = "ACQ_CONV_RATE" )
	private double acqConvRate;
	@Column( name = "ACQ_CURRENCY_CODE" )
	private int    acqCurrencyCode;
	@Column( name = "ACQ_CONV_DATE" )
	private Date   acqConvDate;
	@Column( name = "TRA_AMOUNT" )
	private double traAmount;
	@Column( name = "TRA_CONV_RATE" )
	private double traConvRate;
	@Column( name = "TRA_CURRENCY_CODE" )
	private int    traCurrencyCode;
	@Column( name = "TRA_CONV_DATE" )
	private Date   traConvDate;
	@Column( name = "FEE" )
	private double fee;
	@Column( name = "NEW_FEE" )
	private double newFee;
	@Column( name = "NEW_AMOUNT" )
	private double newAmount;
	@Column( name = "NEW_SETL_AMOUNT" )
	private double newSetlAmount;
	@Column( name = "SETTLEMENT_FEE" )
	private double settlementFee;
	@Column( name = "SETTLEMENT_RATE" )
	private double settlementRate;
	@Column( name = "SETTLEMENT_CODE" )
	private int    settlementCode;
	@Column( name = "SETTLEMENT_AMOUNT" )
	private double settlementAmount;
	@Column( name = "TRANDATE" )
	private Date   tranDate;
	@Column( name = "TRACE" )
	private int    trace;
	@Column( name = "LOCAL_TIME" )
	private String localTime;
	@Column( name = "LOCAL_DATE" )
	private Date   localDate;
	@Column( name = "SETTLEMENT_DATE" )
	private Date   settlementDate;
	@Column( name = "CAP_DATE" )
	private Date   capDate;
	@Column( name = "POS_ENTRY_CODE" )
	private int    posEntryCode;
	@Column( name = "POS_CONDITION_CODE" )
	private int    posConditionCode;
	@Column( name = "POS_PIN_CAP_CODE" )
	private String posPinCapCode;
	@Column( name = "POS_CAP_CODE" )
	private int    posCapCode;
	@Column( name = "LIFE_CYCLE" )
	private int    lifeCycle;
	@Column( name = "ACQUIRER" )
	private String acquirer;
	@Column( name = "ISSUER" )
	private String issuer;
	@Column( name = "TRANSFEREE" )
	private String transferee;
	@Column( name = "ORIGINATOR" )
	private String originator;
	@Column( name = "MEMBER_ID" )
	private String memberId;
	@Column( name = "F_ID" )
	private String fId;
	@Column( name = "TXNSRC" )
	private String txnsrc;
	@Column( name = "TXNDEST" )
	private String txndest;
	@Column( name = "ALTERNATEACQUIRER" )
	private String alternateAcquirer;
	@Column( name = "ENTITYID" )
	private String entityId;
	@Column( name = "CARDPRODUCT" )
	private String cardProducti;
	@Column( name = "MVV" )
	private String mvv;
	@Column( name = "INVOICE_NUMBER" )
	private String invoiceNumber;
	@Column( name = "TRANS_ID" )
	private String transId;
	@Column( name = "FPI" )
	private String fpi;
	@Column( name = "TXN_START_TIME" )
	private int    txnStartTime;
	@Column( name = "TXN_END_TIME" )
	private int    txnEndTime;
	@Column( name = "DEVICE_CAP" )
	private int    deviceCap;
	@Column( name = "RESPCODE" )
	private int    respCode;
	@Column( name = "REASON_CODE" )
	private int    reasonCode;
	@Column( name = "REVCODE" )
	private int    revCode;
	@Column( name = "SHCERROR" )
	private int    shcError;
	@Column( name = "SAF" )
	private int    saf;
	@Column( name = "ORIGMSG" )
	private int    origMsg;
	@Column( name = "ORIGFLIPPEDMSG" )
	private int    origFlippedMsg;
	@Column( name = "ORIGTRACE" )
	private int    origTrace;
	@Column( name = "ORIGDATE" )
	private Date   origDate;
	@Column( name = "ORIGTIME" )
	private int    origTime;
	@Column( name = "MERCHANT_TYPE" )
	private int    merchantType;
	@Column( name = "ACQ_COUNTRY" )
	private int    acqCountry;
	@Column( name = "REFNUM" )
	private String refNum;
	@Column( name = "TERMID" )
	private String termId;
	@Column( name = "ACCEPTORNAME" )
	private String acceptorName;
	@Column( name = "TERMLOC" )
	private String termLoc;
	@Column( name = "ADDRESPONSE" )
	private String addResponse;
	@Column( name = "ACCTNUM" )
	private String acctNum;
	@Column( name = "BRANCH" )
	private int    branch;
	@Column( name = "SERIAL_1" )
	private int    serial1;
	@Column( name = "SERIAL_2" )
	private int    serial2;
	@Column( name = "STOREID" )
	private int    storeId;
	@Column( name = "LANE" )
	private int    lane;
	@Column( name = "TERMINAL_TRACE" )
	private int    terminalTrace;
	@Column( name = "CHECKER_ID" )
	private int    checkerId;
	@Column( name = "SUPERVISOR" )
	private int    supervisor;
	@Column( name = "SHIFT_NUMBER" )
	private int    shiftNumber;
	@Column( name = "BATCH_ID" )
	private int    batchId;
	@Column( name = "DEVICE_DEVCAP" )
	private int    deviceDevCap;
	@Column( name = "SHC_DEVCAP" )
	private int    shcDevCap;
	@Column( name = "FORMATTER_DEVCAP" )
	private int    formatterDevCap;
	@Column( name = "AUTH_DEVCAP" )
	private int    authDevcap;
	@Column( name = "FILLER1" )
	private String filler1;
	@Column( name = "FILLER2" )
	private String filler2;
	@Column( name = "FILLER3" )
	private String filler3;
	@Column( name = "FILLER4" )
	private String filler4;
	@Column( name = "ISSUER_DATA" )
	private String issuerData;
	@Column( name = "ACQUIRER_DATA" )
	private String acquirerData;
	@Column( name = "NEW_AMOUNT_EQUIV" )
	private double newAmountEquiv;
	@Column( name = "ACQ_AVAL_BALANCE" )
	private double acqAvalBalance;
	@Column( name = "ACQ_LEDGER_BALANCE" )
	private double acqLedgerBalance;
	@Column( name = "SETL_AVAL_BALANCE" )
	private double setlAvalBalance;
	@Column( name = "SETL_LEDGER_BALANCE" )
	private double setlLedgerBalance;
	@Column( name = "AVAL_BALANCE_TYPE" )
	private int    avalBalanceType;
	@Column( name = "LEDGER_BALANCE_TYPE" )
	private int    ledgerBalanceType;
	@Column( name = "NEW_SETL_FEE" )
	private double newSetlFee;
	@Column( name = "TXN_AMOUNT" )
	private double txnAmount;
	@Column( name = "TXN_NEW_AMOUNT" )
	private double txnNewAmount;
	@Column( name = "TXN_CURRENCY_CODE" )
	private int    txnCurrencyCode;
	@Column( name = "TXN_CONV_RATE" )
	private double txnConvRate;
	@Column( name = "TXN_CONV_DATE" )
	private Date   txnConvDate;
	@Column( name = "CH_AMOUNT" )
	private double chAmount;
	@Column( name = "CH_NEW_AMOUNT" )
	private double chNewAmount;
	@Column( name = "CH_CURRENCY_CODE" )
	private int    chCurrencyCode;
	@Column( name = "CH_CONV_RATE" )
	private double chConvRate;
	@Column( name = "CH_CONV_DATE" )
	private Date   chConvDate;
	@Column( name = "LEDGER_BALANCE" )
	private double ledgerBalance;
	@Column( name = "SLOT_NUM" )
	private int    slotNum;
	@Column( name = "DEVICE_FEE" )
	private double deviceFee;
	@Column( name = "SHC_DATA_BUFFER" )
	private String shcDataBuffer;
	@Column( name = "CARD_SEQNO" )
	private int    cardSeqNo;
	@Column( name = "ALPHARESPONSECODE" )
	private String alpharesponseCode;
	@Column( name = "CHIP_TYPE" )
	private String chipType;
	@Column( name = "CHIP_INDEX" )
	private String chipIndex;
	
	@Id
    @AttributeOverrides({
          @AttributeOverride(name = "numberCard", column = @Column(name="PAN")),           
          @AttributeOverride(name = "pCode",    column = @Column(name="PCODE")),
          @AttributeOverride(name = "authNum",    column = @Column(name="AUTHNUM"))
    })
	private String authNum;
	private String numberCard;
	private int    pCode;
	
	public SHCLogDto(){}

	
	public int getMsgType() {
		return msgType;
	}
	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}
	
	public int getFlippedMsgType() {
		return flippedMsgType;
	}
	public void setFlippedMsgType(int flippedMsgType) {
		this.flippedMsgType = flippedMsgType;
	}
	
	public String getNumberCard() {
		return numberCard;
	}
	public void setNumberCard(String numberCard) {
		this.numberCard = numberCard;
	}
	
	public int getpCode() {
		return pCode;
	}
	public void setpCode(int pCode) {
		this.pCode = pCode;
	}
	
	public String getTranTime() {
		return tranTime;
	}
	public void setTranTime(String tranTime) {
		this.tranTime = tranTime;
	}
	
	public int getTxtType() {
		return txtType;
	}
	public void setTxtType(int txtType) {
		this.txtType = txtType;
	}
	
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
	public double getAvalBalance() {
		return avalBalance;
	}
	public void setAvalBalance(double avalBalance) {
		this.avalBalance = avalBalance;
	}
	
	
	public double getAmountEquiv() {
		return amountEquiv;
	}
	public void setAmountEquiv(double amountEquiv) {
		this.amountEquiv = amountEquiv;
	}
	
	public double getCashBack() {
		return cashBack;
	}
	public void setCashBack(double cashBack) {
		this.cashBack = cashBack;
	}
	
	public double getIssConvRate() {
		return issConvRate;
	}
	public void setIssConvRate(double issConvRate) {
		this.issConvRate = issConvRate;
	}
	
	public int getIssCurrencyCode() {
		return issCurrencyCode;
	}
	public void setIssCurrencyCode(int issCurrencyCode) {
		this.issCurrencyCode = issCurrencyCode;
	}
	
	public Date getIssConvDate() {
		return issConvDate;
	}

	public void setIssConvDate(Date issConvDate) {
		this.issConvDate = issConvDate;
	}
	
	public double getAcqConvRate() {
		return acqConvRate;
	}
	public void setAcqConvRate(double acqConvRate) {
		this.acqConvRate = acqConvRate;
	}
	
	public int getAcqCurrencyCode() {
		return acqCurrencyCode;
	}
	public void setAcqCurrencyCode(int acqCurrencyCode) {
		this.acqCurrencyCode = acqCurrencyCode;
	}
	
	public Date getAcqConvDate() {
		return acqConvDate;
	}
	public void setAcqConvDate(Date acqConvDate) {
		this.acqConvDate = acqConvDate;
	}
	
	public double getTraAmount() {
		return traAmount;
	}
	public void setTraAmount(double traAmount) {
		this.traAmount = traAmount;
	}
	
	public double getTraConvRate() {
		return traConvRate;
	}
	public void setTraConvRate(double traConvRate) {
		this.traConvRate = traConvRate;
	}
	
	public int getTraCurrencyCode() {
		return traCurrencyCode;
	}
	public void setTraCurrencyCode(int traCurrencyCode) {
		this.traCurrencyCode = traCurrencyCode;
	}
	
	public Date getTraConvDate() {
		return traConvDate;
	}
	public void setTraConvDate(Date traConvDate) {
		this.traConvDate = traConvDate;
	}
	
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	
	public double getNewFee() {
		return newFee;
	}
	public void setNewFee(double newFee) {
		this.newFee = newFee;
	}
	
	public double getNewAmount() {
		return newAmount;
	}
	public void setNewAmount(double newAmount) {
		this.newAmount = newAmount;
	}
	
	public double getNewSetlAmount() {
		return newSetlAmount;
	}
	public void setNewSetlAmount(double newSetlAmount) {
		this.newSetlAmount = newSetlAmount;
	}
	
	public double getSettlementFee() {
		return settlementFee;
	}
	public void setSettlementFee(double settlementFee) {
		this.settlementFee = settlementFee;
	}
	
	public double getSettlementRate() {
		return settlementRate;
	}
	public void setSettlementRate(double settlementRate) {
		this.settlementRate = settlementRate;
	}
	
	public int getSettlementCode() {
		return settlementCode;
	}
	public void setSettlementCode(int settlementCode) {
		this.settlementCode = settlementCode;
	}
	
	
	public double getSettlementAmount() {
		return settlementAmount;
	}
	public void setSettlementAmount(double settlementAmount) {
		this.settlementAmount = settlementAmount;
	}
	
	public Date getTranDate() {
		return tranDate;
	}
	public void setTranDate(Date tranDate) {
		this.tranDate = tranDate;
	}
	
	public int getTrace() {
		return trace;
	}
	public void setTrace(int trace) {
		this.trace = trace;
	}
	
	public String getLocalTime() {
		return localTime;
	}
	public void setLocalTime(String localTime) {
		this.localTime = localTime;
	}
	
	public Date getLocalDate() {
		return localDate;
	}
	public void setLocalDate(Date localDate) {
		this.localDate = localDate;
	}
	
	public Date getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}

	public Date getCapDate() {
		return capDate;
	}
	public void setCapDate(Date capDate) {
		this.capDate = capDate;
	}
	
	public int getPosEntryCode() {
		return posEntryCode;
	}
	public void setPosEntryCode(int posEntryCode) {
		this.posEntryCode = posEntryCode;
	}
	
	public int getPosConditionCode() {
		return posConditionCode;
	}
	public void setPosConditionCode(int posConditionCode) {
		this.posConditionCode = posConditionCode;
	}
	
	public String getPosPinCapCode() {
		return posPinCapCode;
	}
	public void setPosPinCapCode(String posPinCapCode) {
		this.posPinCapCode = posPinCapCode;
	}
	
	public int getPosCapCode() {
		return posCapCode;
	}
	public void setPosCapCode(int posCapCode) {
		this.posCapCode = posCapCode;
	}
	
	public int getLifeCycle() {
		return lifeCycle;
	}
	public void setLifeCycle(int lifeCycle) {
		this.lifeCycle = lifeCycle;
	}
	
	public String getAcquirer() {
		return acquirer;
	}
	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}
	
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	public String getTransferee() {
		return transferee;
	}
	public void setTransferee(String transferee) {
		this.transferee = transferee;
	}
	
	public String getOriginator() {
		return originator;
	}
	public void setOriginator(String originator) {
		this.originator = originator;
	}
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	public String getfId() {
		return fId;
	}
	public void setfId(String fId) {
		this.fId = fId;
	}
	
	public String getTxnsrc() {
		return txnsrc;
	}
	
	public void setTxnsrc(String txnsrc) {
		this.txnsrc = txnsrc;
	}
	
	public String getTxndest() {
		return txndest;
	}
	public void setTxndest(String txndest) {
		this.txndest = txndest;
	}
	
	public String getAlternateAcquirer() {
		return alternateAcquirer;
	}
	public void setAlternateAcquirer(String alternateAcquirer) {
		this.alternateAcquirer = alternateAcquirer;
	}
	
	
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	
	public String getCardProducti() {
		return cardProducti;
	}
	public void setCardProducti(String cardProducti) {
		this.cardProducti = cardProducti;
	}
	
	public String getMvv() {
		return mvv;
	}
	public void setMvv(String mvv) {
		this.mvv = mvv;
	}
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	
	public String getFpi() {
		return fpi;
	}
	public void setFpi(String fpi) {
		this.fpi = fpi;
	}
	
	public int getTxnStartTime() {
		return txnStartTime;
	}
	public void setTxnStartTime(int txnStartTime) {
		this.txnStartTime = txnStartTime;
	}
	
	public int getTxnEndTime() {
		return txnEndTime;
	}
	public void setTxnEndTime(int txnEndTime) {
		this.txnEndTime = txnEndTime;
	}
	
	public int getDeviceCap() {
		return deviceCap;
	}
	public void setDeviceCap(int deviceCap) {
		this.deviceCap = deviceCap;
	}
	
	public int getRespCode() {
		return respCode;
	}
	public void setRespCode(int respCode) {
		this.respCode = respCode;
	}
	
	public int getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(int reasonCode) {
		this.reasonCode = reasonCode;
	}
	
	public int getRevCode() {
		return revCode;
	}
	public void setRevCode(int revCode) {
		this.revCode = revCode;
	}
	
	public int getShcError() {
		return shcError;
	}
	public void setShcError(int shcError) {
		this.shcError = shcError;
	}
	
	public int getSaf() {
		return saf;
	}
	public void setSaf(int saf) {
		this.saf = saf;
	}
	
	public int getOrigMsg() {
		return origMsg;
	}
	public void setOrigMsg(int origMsg) {
		this.origMsg = origMsg;
	}
	
	public int getOrigFlippedMsg() {
		return origFlippedMsg;
	}
	public void setOrigFlippedMsg(int origFlippedMsg) {
		this.origFlippedMsg = origFlippedMsg;
	}
	
	public int getOrigTrace() {
		return origTrace;
	}
	public void setOrigTrace(int origTrace) {
		this.origTrace = origTrace;
	}
	
	public Date getOrigDate() {
		return origDate;
	}
	public void setOrigDate(Date origDate) {
		this.origDate = origDate;
	}
	
	public int getOrigTime() {
		return origTime;
	}
	public void setOrigTime(int origTime) {
		this.origTime = origTime;
	}
	
	public int getMerchantType() {
		return merchantType;
	}
	public void setMerchantType(int merchantType) {
		this.merchantType = merchantType;
	}

	public int getAcqCountry() {
		return acqCountry;
	}
	public void setAcqCountry(int acqCountry) {
		this.acqCountry = acqCountry;
	}
	
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	
	public String getAuthNum() {
		return authNum;
	}
	public void setAuthNum(String authNum) {
		this.authNum = authNum;
	}
	
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	
	public String getAcceptorName() {
		return acceptorName;
	}
	public void setAcceptorName(String acceptorName) {
		this.acceptorName = acceptorName;
	}
	
	public String getTermLoc() {
		return termLoc;
	}
	public void setTermLoc(String termLoc) {
		this.termLoc = termLoc;
	}
	
	public String getAddResponse() {
		return addResponse;
	}
	public void setAddResponse(String addResponse) {
		this.addResponse = addResponse;
	}
	
	public String getAcctNum() {
		return acctNum;
	}
	public void setAcctNum(String acctNum) {
		this.acctNum = acctNum;
	}
	
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	
	public int getSerial1() {
		return serial1;
	}
	public void setSerial1(int serial1) {
		this.serial1 = serial1;
	}
	
	public int getSerial2() {
		return serial2;
	}
	public void setSerial2(int serial2) {
		this.serial2 = serial2;
	}
	
	public int getStoreId() {
		return storeId;
	}
	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}
	
	public int getLane() {
		return lane;
	}
	public void setLane(int lane) {
		this.lane = lane;
	}
	
	public int getTerminalTrace() {
		return terminalTrace;
	}
	public void setTerminalTrace(int terminalTrace) {
		this.terminalTrace = terminalTrace;
	}
	
	public int getCheckerId() {
		return checkerId;
	}
	public void setCheckerId(int checkerId) {
		this.checkerId = checkerId;
	}
	
	public int getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(int supervisor) {
		this.supervisor = supervisor;
	}
	
	public int getShiftNumber() {
		return shiftNumber;
	}
	public void setShiftNumber(int shiftNumber) {
		this.shiftNumber = shiftNumber;
	}
	
	public int getBatchId() {
		return batchId;
	}
	public void setBatchId(int batchId) {
		this.batchId = batchId;
	}
	
	public int getDeviceDevCap() {
		return deviceDevCap;
	}
	public void setDeviceDevCap(int deviceDevCap) {
		this.deviceDevCap = deviceDevCap;
	}
	
	public int getShcDevCap() {
		return shcDevCap;
	}
	public void setShcDevCap(int shcDevCap) {
		this.shcDevCap = shcDevCap;
	}
	
	public int getFormatterDevCap() {
		return formatterDevCap;
	}
	public void setFormatterDevCap(int formatterDevCap) {
		this.formatterDevCap = formatterDevCap;
	}
	
	public int getAuthDevcap() {
		return authDevcap;
	}
	public void setAuthDevcap(int authDevcap) {
		this.authDevcap = authDevcap;
	}
	
	public String getFiller1() {
		return filler1;
	}
	public void setFiller1(String filler1) {
		this.filler1 = filler1;
	}
	
	public String getFiller2() {
		return filler2;
	}
	public void setFiller2(String filler2) {
		this.filler2 = filler2;
	}
	
	public String getFiller3() {
		return filler3;
	}
	public void setFiller3(String filler3) {
		this.filler3 = filler3;
	}
	
	public String getFiller4() {
		return filler4;
	}
	public void setFiller4(String filler4) {
		this.filler4 = filler4;
	}
	
	public String getIssuerData() {
		return issuerData;
	}
	public void setIssuerData(String issuerData) {
		this.issuerData = issuerData;
	}
	
	public String getAcquirerData() {
		return acquirerData;
	}
	public void setAcquirerData(String acquirerData) {
		this.acquirerData = acquirerData;
	}
	
	public double getNewAmountEquiv() {
		return newAmountEquiv;
	}
	public void setNewAmountEquiv(double newAmountEquiv) {
		this.newAmountEquiv = newAmountEquiv;
	}
	
	public double getAcqAvalBalance() {
		return acqAvalBalance;
	}
	public void setAcqAvalBalance(double acqAvalBalance) {
		this.acqAvalBalance = acqAvalBalance;
	}
	
	public double getAcqLedgerBalance() {
		return acqLedgerBalance;
	}
	public void setAcqLedgerBalance(double acqLedgerBalance) {
		this.acqLedgerBalance = acqLedgerBalance;
	}
	
	public double getSetlAvalBalance() {
		return setlAvalBalance;
	}
	public void setSetlAvalBalance(double setlAvalBalance) {
		this.setlAvalBalance = setlAvalBalance;
	}
	
	public double getSetlLedgerBalance() {
		return setlLedgerBalance;
	}
	public void setSetlLedgerBalance(double setlLedgerBalance) {
		this.setlLedgerBalance = setlLedgerBalance;
	}
	
	public int getAvalBalanceType() {
		return avalBalanceType;
	}
	public void setAvalBalanceType(int avalBalanceType) {
		this.avalBalanceType = avalBalanceType;
	}
	
	public int getLedgerBalanceType() {
		return ledgerBalanceType;
	}
	public void setLedgerBalanceType(int ledgerBalanceType) {
		this.ledgerBalanceType = ledgerBalanceType;
	}
	
	public double getNewSetlFee() {
		return newSetlFee;
	}
	public void setNewSetlFee(double newSetlFee) {
		this.newSetlFee = newSetlFee;
	}
	
	public double getTxnAmount() {
		return txnAmount;
	}
	public void setTxnAmount(double txnAmount) {
		this.txnAmount = txnAmount;
	}
	
	public double getTxnNewAmount() {
		return txnNewAmount;
	}
	public void setTxnNewAmount(double txnNewAmount) {
		this.txnNewAmount = txnNewAmount;
	}
	
	public int getTxnCurrencyCode() {
		return txnCurrencyCode;
	}
	public void setTxnCurrencyCode(int txnCurrencyCode) {
		this.txnCurrencyCode = txnCurrencyCode;
	}
	
	public double getTxnConvRate() {
		return txnConvRate;
	}
	public void setTxnConvRate(double txnConvRate) {
		this.txnConvRate = txnConvRate;
	}
	
	public Date getTxnConvDate() {
		return txnConvDate;
	}
	public void setTxnConvDate(Date txnConvDate) {
		this.txnConvDate = txnConvDate;
	}
	
	public double getChAmount() {
		return chAmount;
	}
	public void setChAmount(double chAmount) {
		this.chAmount = chAmount;
	}
	
	public double getChNewAmount() {
		return chNewAmount;
	}
	public void setChNewAmount(double chNewAmount) {
		this.chNewAmount = chNewAmount;
	}
	
	public int getChCurrencyCode() {
		return chCurrencyCode;
	}
	public void setChCurrencyCode(int chCurrencyCode) {
		this.chCurrencyCode = chCurrencyCode;
	}
	
	public double getChConvRate() {
		return chConvRate;
	}
	public void setChConvRate(double chConvRate) {
		this.chConvRate = chConvRate;
	}
	
	public Date getChConvDate() {
		return chConvDate;
	}

	public void setChConvDate(Date chConvDate) {
		this.chConvDate = chConvDate;
	}
	
	public double getLedgerBalance() {
		return ledgerBalance;
	}
	public void setLedgerBalance(double ledgerBalance) {
		this.ledgerBalance = ledgerBalance;
	}
	
	public int getSlotNum() {
		return slotNum;
	}
	public void setSlotNum(int slotNum) {
		this.slotNum = slotNum;
	}
	
	public double getDeviceFee() {
		return deviceFee;
	}
	public void setDeviceFee(double deviceFee) {
		this.deviceFee = deviceFee;
	}
	
	public String getShcDataBuffer() {
		return shcDataBuffer;
	}
	public void setShcDataBuffer(String shcDataBuffer) {
		this.shcDataBuffer = shcDataBuffer;
	}
	
	public int getCardSeqNo() {
		return cardSeqNo;
	}
	public void setCardSeqNo(int cardSeqNo) {
		this.cardSeqNo = cardSeqNo;
	}
	
	public String getAlpharesponseCode() {
		return alpharesponseCode;
	}
	public void setAlpharesponseCode(String alpharesponseCode) {
		this.alpharesponseCode = alpharesponseCode;
	}
	
	public String getChipType() {
		return chipType;
	}
	public void setChipType(String chipType) {
		this.chipType = chipType;
	}
	public String getChipIndex() {
		return chipIndex;
	}
	public void setChipIndex(String chipIndex) {
		this.chipIndex = chipIndex;
	}
	
	public SHCLogDto( int init ){
		
		Date currentDate   = GeneralUtil.getCurrentSqlDate();
		String currentTime = GeneralUtil.getCurrentTime("HHmmss");
		
		msgType = 210;
		flippedMsgType = 0;
		numberCard = null;
		pCode = 0;
		txtType = 0;
		amount = 0;
		avalBalance = 0;
		amountEquiv = 0;
		cashBack = 0;
		issConvRate = 0;
		issCurrencyCode = 484;
		issConvDate = currentDate;
		acqConvRate = 0;
		acqCurrencyCode = 0;
		acqConvDate = currentDate;
		traAmount = 0;
		traConvRate = 0;
		traCurrencyCode = 0;
		traConvDate = currentDate;
		fee = 0;
		newFee = 0;
		newAmount = 0;
		newSetlAmount = 0;
		settlementFee = 0;
		settlementRate = 0;
		settlementCode = 0;
		settlementAmount = 0;
		tranDate = currentDate;
		tranTime = currentTime;
		trace = 0;
		localTime = currentTime;
		localDate = currentDate;
		settlementDate = currentDate;
		capDate = currentDate;
		posEntryCode = 0;
		posConditionCode = 0;
		posPinCapCode = null;
		posCapCode = 0;
		lifeCycle = 0;
		acquirer = null;
		issuer = null;
		transferee = null;
		originator = null;
		memberId = null;
		fId = null;
		txnsrc = null;
		txndest = null;
		alternateAcquirer = null;
		entityId = null;
		cardProducti = null;
		mvv = null;
		invoiceNumber = null;
		transId = null;
		fpi = null;
		txnStartTime = 0;
		txnEndTime = 0;
		deviceCap = 0;
		respCode = 0;
		reasonCode = 0;
		revCode = 0;
		shcError = 0;
		saf = 0;
		origMsg = 0;
		origFlippedMsg = 0;
		origTrace = 0;
		origDate = currentDate;
		origTime = 0;
		merchantType = 0;
		acqCountry = 0;
		refNum = null;
		authNum = null;
		termId = "111111";
		acceptorName = null;
		termLoc = null;
		addResponse = null;
		acctNum = null;
		branch = 0;
		serial1 = 0;
		serial2 = 0;
		storeId = 0;
		lane = 0;
		terminalTrace = 0;
		checkerId = 0;
		supervisor = 0;
		shiftNumber = 0;
		batchId = 0;
		deviceDevCap = 0;
		shcDevCap = 0;
		formatterDevCap = 0;
		authDevcap = 0;
		filler1 = "ws_user";
		filler2 = null;
		filler3 = "web_service";
		filler4 = null;
		issuerData = null;
		acquirerData = null;
		newAmountEquiv = 0;
		acqAvalBalance = 0;
		acqLedgerBalance = 0;
		setlAvalBalance = 0;
		setlLedgerBalance = 0;
		avalBalanceType = 0;
		ledgerBalanceType = 0;
		newSetlFee = 0;
		txnAmount = 0;
		txnNewAmount = 0;
		txnCurrencyCode = 0;
		txnConvRate = 0;
		txnConvDate = currentDate;
		chAmount = 0;
		chNewAmount = 0;
		chCurrencyCode = 0;
		chConvRate = 0;
		chConvDate = currentDate;
		ledgerBalance = 0;
		slotNum = 0;
		deviceFee = 0;
		shcDataBuffer = null;
		cardSeqNo = 0;
		alpharesponseCode = null;
		chipType = null;
		chipIndex = null;

	}
	
	
public SHCLogDto( WShcLogPK wShcLogPK ){
		
		Date currentDate   = GeneralUtil.getCurrentSqlDate();
		String currentTime = GeneralUtil.getCurrentTime("HHmmss");
		
		msgType = 210;
		flippedMsgType = 0;
		numberCard = wShcLogPK.getNumberCard();
		pCode = wShcLogPK.getPCode();
		txtType = 0;
		amount = 0;
		avalBalance = 0;
		amountEquiv = 0;
		cashBack = 0;
		issConvRate = 0;
		issCurrencyCode = 484;
		issConvDate = currentDate;
		acqConvRate = 0;
		acqCurrencyCode = 0;
		acqConvDate = currentDate;
		traAmount = 0;
		traConvRate = 0;
		traCurrencyCode = 0;
		traConvDate = currentDate;
		fee = 0;
		newFee = 0;
		newAmount = 0;
		newSetlAmount = 0;
		settlementFee = 0;
		settlementRate = 0;
		settlementCode = 0;
		settlementAmount = 0;
		tranDate = currentDate;
		tranTime = currentTime;
		trace = 0;
		localTime = currentTime;
		localDate = currentDate;
		settlementDate = currentDate;
		capDate = currentDate;
		posEntryCode = 0;
		posConditionCode = 0;
		posPinCapCode = null;
		posCapCode = 0;
		lifeCycle = 0;
		acquirer = null;
		issuer = null;
		transferee = null;
		originator = null;
		memberId = null;
		fId = null;
		txnsrc = null;
		txndest = null;
		alternateAcquirer = null;
		entityId = null;
		cardProducti = null;
		mvv = null;
		invoiceNumber = null;
		transId = null;
		fpi = null;
		txnStartTime = 0;
		txnEndTime = 0;
		deviceCap = 0;
		respCode = 0;
		reasonCode = 0;
		revCode = 0;
		shcError = 0;
		saf = 0;
		origMsg = 0;
		origFlippedMsg = 0;
		origTrace = 0;
		origDate = currentDate;
		origTime = 0;
		merchantType = 0;
		acqCountry = 0;
		refNum = null;
		authNum = wShcLogPK.getAuthNum();
		termId = "111111";
		acceptorName = null;
		termLoc = null;
		addResponse = null;
		acctNum = null;
		branch = 0;
		serial1 = 0;
		serial2 = 0;
		storeId = 0;
		lane = 0;
		terminalTrace = 0;
		checkerId = 0;
		supervisor = 0;
		shiftNumber = 0;
		batchId = 0;
		deviceDevCap = 0;
		shcDevCap = 0;
		formatterDevCap = 0;
		authDevcap = 0;
		filler1 = "ws_user";
		filler2 = null;
		filler3 = "web_service";
		filler4 = null;
		issuerData = null;
		acquirerData = null;
		newAmountEquiv = 0;
		acqAvalBalance = 0;
		acqLedgerBalance = 0;
		setlAvalBalance = 0;
		setlLedgerBalance = 0;
		avalBalanceType = 0;
		ledgerBalanceType = 0;
		newSetlFee = 0;
		txnAmount = 0;
		txnNewAmount = 0;
		txnCurrencyCode = 0;
		txnConvRate = 0;
		txnConvDate = currentDate;
		chAmount = 0;
		chNewAmount = 0;
		chCurrencyCode = 0;
		chConvRate = 0;
		chConvDate = currentDate;
		ledgerBalance = 0;
		slotNum = 0;
		deviceFee = 0;
		shcDataBuffer = null;
		cardSeqNo = 0;
		alpharesponseCode = null;
		chipType = null;
		chipIndex = null;

	}
	
	public String toString() {
		String respuesta = null;
		respuesta  = "msgType["+msgType+"],";
		respuesta += "flippedMsgType["+flippedMsgType+"],";
		respuesta += "numberCard["+numberCard+"],";
		respuesta += "pCode["+pCode+"],";
		respuesta += "txtType["+txtType+"],";
		respuesta += "amount["+amount+"],";
		respuesta += "avalBalance["+avalBalance+"],";
		respuesta += "amountEquiv["+amountEquiv+"],";
		respuesta += "cashBack["+cashBack+"],";
		respuesta += "issConvRate["+issConvRate+"],";
		respuesta += "issCurrencyCode["+issCurrencyCode+"],";
		respuesta += "issConvDate["+issConvDate+"],";
		respuesta += "acqConvRate["+acqConvRate+"],";
		respuesta += "acqCurrencyCode["+acqCurrencyCode+"],";
		respuesta += "acqConvDate["+acqConvDate+"],";
		respuesta += "traAmount["+traAmount+"],";
		respuesta += "traConvRate["+traConvRate+"],";
		respuesta += "traCurrencyCode["+traCurrencyCode+"],";
		respuesta += "traConvDate["+traConvDate+"],";
		respuesta += "fee["+fee+"],";
		respuesta += "newFee["+newFee+"],";
		respuesta += "newAmount["+newAmount+"],";
		respuesta += "newSetlAmount["+newSetlAmount+"],";
		respuesta += "settlementFee["+settlementFee+"],";
		respuesta += "settlementRate["+settlementRate+"],";
		respuesta += "settlementCode["+settlementCode+"],";
		respuesta += "settlementAmount["+settlementAmount+"],";
		respuesta += "tranDate["+tranDate+"],";
		respuesta += "tranTime["+tranTime+"],";
		respuesta += "trace["+trace+"],";
		respuesta += "localTime["+localTime+"],";
		respuesta += "localDate["+localDate+"],";
		respuesta += "settlementDate["+settlementDate+"],";
		respuesta += "capDate["+capDate+"],";
		respuesta += "posEntryCode["+posEntryCode+"],";
		respuesta += "posConditionCode["+posConditionCode+"],";
		respuesta += "posPinCapCode["+posPinCapCode+"],";
		respuesta += "posCapCode["+posCapCode+"],";
		respuesta += "lifeCycle["+lifeCycle+"],";
		respuesta += "acquirer["+acquirer+"],";
		respuesta += "issuer["+issuer+"],";
		respuesta += "transferee["+transferee+"],";
		respuesta += "originator["+originator+"],";
		respuesta += "memberId["+memberId+"],";
		respuesta += "fId["+fId+"],";
		respuesta += "txnsrc["+txnsrc+"],";
		respuesta += "txndest["+txndest+"],";
		respuesta += "alternateAcquirer["+alternateAcquirer+"],";
		respuesta += "entityId["+entityId+"],";
		respuesta += "cardProducti["+cardProducti+"],";
		respuesta += "mvv["+mvv+"],";
		respuesta += "invoiceNumber["+invoiceNumber+"],";
		respuesta += "transId["+transId+"],";
		respuesta += "fpi["+fpi+"],";
		respuesta += "txnStartTime["+txnStartTime+"],";
		respuesta += "txnEndTime["+txnEndTime+"],";
		respuesta += "deviceCap["+deviceCap+"],";
		respuesta += "respCode["+respCode+"],";
		respuesta += "reasonCode["+reasonCode+"],";
		respuesta += "revCode["+revCode+"],";
		respuesta += "shcError["+shcError+"],";
		respuesta += "saf["+saf+"],";
		respuesta += "origMsg["+origMsg+"],";
		respuesta += "origFlippedMsg["+origFlippedMsg+"],";
		respuesta += "origTrace["+origTrace+"],";
		respuesta += "origDate["+origDate+"],";
		respuesta += "origTime["+origTime+"],";
		respuesta += "merchantType["+merchantType+"],";
		respuesta += "acqCountry["+acqCountry+"],";
		respuesta += "refNum["+refNum+"],";
		respuesta += "authNum["+authNum+"],";
		respuesta += "termId["+termId+"],";
		respuesta += "acceptorName["+acceptorName+"],";
		respuesta += "termLoc["+termLoc+"],";
		respuesta += "addResponse["+addResponse+"],";
		respuesta += "acctNum["+acctNum+"],";
		respuesta += "branch["+branch+"],";
		respuesta += "serial1["+serial1+"],";
		respuesta += "serial2["+serial2+"],";
		respuesta += "storeId["+storeId+"],";
		respuesta += "lane["+lane+"],";
		respuesta += "terminalTrace["+terminalTrace+"],";
		respuesta += "checkerId["+checkerId+"],";
		respuesta += "supervisor["+supervisor+"],";
		respuesta += "shiftNumber["+shiftNumber+"],";
		respuesta += "batchId["+batchId+"],";
		respuesta += "deviceDevCap["+deviceDevCap+"],";
		respuesta += "shcDevCap["+shcDevCap+"],";
		respuesta += "formatterDevCap["+formatterDevCap+"],";
		respuesta += "authDevcap["+authDevcap+"],";
		respuesta += "filler1["+filler1+"],";
		respuesta += "filler2["+filler2+"],";
		respuesta += "filler3["+filler3+"],";
		respuesta += "filler4["+filler4+"],";
		respuesta += "issuerData["+issuerData+"],";
		respuesta += "acquirerData["+acquirerData+"],";
		respuesta += "newAmountEquiv["+newAmountEquiv+"],";
		respuesta += "acqAvalBalance["+acqAvalBalance+"],";
		respuesta += "acqLedgerBalance["+acqLedgerBalance+"],";
		respuesta += "setlAvalBalance["+setlAvalBalance+"],";
		respuesta += "setlLedgerBalance["+setlLedgerBalance+"],";
		respuesta += "avalBalanceType["+avalBalanceType+"],";
		respuesta += "ledgerBalanceType["+ledgerBalanceType+"],";
		respuesta += "newSetlFee["+newSetlFee+"],";
		respuesta += "txnAmount["+txnAmount+"],";
		respuesta += "txnNewAmount["+txnNewAmount+"],";
		respuesta += "txnCurrencyCode["+txnCurrencyCode+"],";
		respuesta += "txnConvRate["+txnConvRate+"],";
		respuesta += "txnConvDate["+txnConvDate+"],";
		respuesta += "chAmount["+chAmount+"],";
		respuesta += "chNewAmount["+chNewAmount+"],";
		respuesta += "chCurrencyCode["+chCurrencyCode+"],";
		respuesta += "chConvRate["+chConvRate+"],";
		respuesta += "chConvDate["+chConvDate+"],";
		respuesta += "ledgerBalance["+ledgerBalance+"],";
		respuesta += "slotNum["+slotNum+"],";
		respuesta += "deviceFee["+deviceFee+"],";
		respuesta += "shcDataBuffer["+shcDataBuffer+"],";
		respuesta += "cardSeqNo["+cardSeqNo+"],";
		respuesta += "alpharesponseCode["+alpharesponseCode+"],";
		respuesta += "chipType["+chipType+"],";
		respuesta += "chipIndex["+chipIndex+"]";
		return respuesta;
	}

}

@Embeddable
class WShcLogPK implements Serializable{

	private static final long serialVersionUID = 7326130925157166033L;
       
       public WShcLogPK(String numbercard, int pCode, String authNum){
    	   this.numberCard = numbercard;
           this.pCode = pCode;
           this.authNum = authNum;  
             
        
       }
       
       public WShcLogPK(){}
       
             
       @Column(name = "PAN", length = 15, nullable = false)
       private String   numberCard;
       @Column(name = "PCODE", length = 50, nullable = false)
       private int   pCode;
       @Column( name = "AUTHNUM" )
   	   private String authNum;
       
       public String getNumberCard() {
           return numberCard;
       }

       public void setNumberCard(String numberCard) {
             this.numberCard = numberCard;
       }

       public int getPCode() {
             return pCode;
       }

       public void setPCode(int pCode) {
             this.pCode = pCode;
       }

       public String getAuthNum() {
    	   return authNum;
       }

       public void setAuthNum(String authNum) {
    	   this.authNum = authNum;
       }
       
       
      
}
