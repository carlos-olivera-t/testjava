package com.syc.persistence.dao;

import java.util.List;

import com.syc.persistence.dto.RolDto;

public interface RolDao extends PersistenceDao<RolDto, Long>{

	List<RolDto> getRolesByList(List<Long> roles);
	
	public RolDto findRolByName(String authority);
	
}