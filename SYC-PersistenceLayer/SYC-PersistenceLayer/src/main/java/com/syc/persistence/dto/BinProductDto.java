package com.syc.persistence.dto;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "IST_BIN_PRODUCT", uniqueConstraints = @UniqueConstraint(columnNames = {"BIN", "PRODUCT" }))

@NamedQueries({
	@NamedQuery(name = "binProduct.findByBin", query = "FROM BinProductDto bp WHERE bp.id.bin = :bin")
})
public class BinProductDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4175699640984617915L;
	
	
	private BinProductId id;
	private ClientDto clientDto;
	private String productName;
	private String defaultCardtype;
	private String iccCardPrefix;
	private String iccAccountPrefix;
	private String cveProduct;
	private int embosserLayoutId;
	private int embosserNipFile;
	private String embosserFormatFile;
	private Boolean usingCustomAccount;
	private String brand;
	private byte[] image;
	
	public BinProductDto() {
	}

	public BinProductDto(BinProductId id, ClientDto istClient,
			String defaultCardtype, String iccCardPrefix,
			String iccAccountPrefix, int embosserLayoutId, int embosserNipFile) {
		this.id = id;
		this.clientDto = istClient;
		this.defaultCardtype = defaultCardtype;
		this.iccCardPrefix = iccCardPrefix;
		this.iccAccountPrefix = iccAccountPrefix;
		this.embosserLayoutId = embosserLayoutId;
		this.embosserNipFile = embosserNipFile;
	}
	
	public BinProductDto(BinProductId id, ClientDto istClient,
			String productName, String defaultCardtype, String iccCardPrefix,
			String iccAccountPrefix, String cveProduct, int embosserLayoutId,
			int embosserNipFile, String embosserFormatFile,
			Boolean usingCustomAccount, String brand, byte[] image) {
		this.id = id;
		this.clientDto = istClient;
		this.productName = productName;
		this.defaultCardtype = defaultCardtype;
		this.iccCardPrefix = iccCardPrefix;
		this.iccAccountPrefix = iccAccountPrefix;
		this.cveProduct = cveProduct;
		this.embosserLayoutId = embosserLayoutId;
		this.embosserNipFile = embosserNipFile;
		this.embosserFormatFile = embosserFormatFile;
		this.usingCustomAccount = usingCustomAccount;
		this.brand = brand;
		this.image = image;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "bin", column = @Column(name = "BIN", nullable = false, length = 6)),
			@AttributeOverride(name = "product", column = @Column(name = "PRODUCT", nullable = false, length = 4)),
			@AttributeOverride(name = "idOwner", column = @Column(name = "ID_OWNER", nullable = false)) })
	public BinProductId getId() {
		return this.id;
	}

	public void setId(BinProductId id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_OWNER", nullable = false, insertable = false, updatable = false)
	public ClientDto getClientDto() {
		return clientDto;
	}

	
	public void setClientDto(ClientDto clientDto) {
		this.clientDto = clientDto;
	}

	@Column(name = "PRODUCT_NAME", length = 100)
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "DEFAULT_CARDTYPE", nullable = false, length = 4)
	public String getDefaultCardtype() {
		return this.defaultCardtype;
	}

	public void setDefaultCardtype(String defaultCardtype) {
		this.defaultCardtype = defaultCardtype;
	}

	@Column(name = "ICC_CARD_PREFIX", nullable = false, length = 10)
	public String getIccCardPrefix() {
		return this.iccCardPrefix;
	}

	public void setIccCardPrefix(String iccCardPrefix) {
		this.iccCardPrefix = iccCardPrefix;
	}

	@Column(name = "ICC_ACCOUNT_PREFIX", nullable = false, length = 20)
	public String getIccAccountPrefix() {
		return this.iccAccountPrefix;
	}

	public void setIccAccountPrefix(String iccAccountPrefix) {
		this.iccAccountPrefix = iccAccountPrefix;
	}

	@Column(name = "CVE_PRODUCT", length = 20)
	public String getCveProduct() {
		return this.cveProduct;
	}

	public void setCveProduct(String cveProduct) {
		this.cveProduct = cveProduct;
	}

	@Column(name = "EMBOSSER_LAYOUT_ID", nullable = false)
	public int getEmbosserLayoutId() {
		return this.embosserLayoutId;
	}

	public void setEmbosserLayoutId(int embosserLayoutId) {
		this.embosserLayoutId = embosserLayoutId;
	}

	@Column(name = "EMBOSSER_NIP_FILE", nullable = false)
	public int getEmbosserNipFile() {
		return this.embosserNipFile;
	}

	public void setEmbosserNipFile(int embosserNipFile) {
		this.embosserNipFile = embosserNipFile;
	}

	@Column(name = "EMBOSSER_FORMAT_FILE", length = 20)
	public String getEmbosserFormatFile() {
		return this.embosserFormatFile;
	}

	public void setEmbosserFormatFile(String embosserFormatFile) {
		this.embosserFormatFile = embosserFormatFile;
	}

	@Column(name = "USING_CUSTOM_ACCOUNT", precision = 1, scale = 0)
	public Boolean getUsingCustomAccount() {
		return this.usingCustomAccount;
	}

	public void setUsingCustomAccount(Boolean usingCustomAccount) {
		this.usingCustomAccount = usingCustomAccount;
	}

	@Column(name = "BRAND", length = 50)
	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Column(name = "IMAGE")
	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}


	public String toString(){
		StringBuilder buffer = new StringBuilder();			
		
		buffer.append("BinProductId[ " + id + " ] ");
		buffer.append("productName[ " + productName + " ] ");
		buffer.append("defaultCardtype[ " + defaultCardtype + " ] ");
		buffer.append("iccCardPrefix[ " + iccCardPrefix + " ] ");		
		buffer.append("iccAccountPrefix[ " + iccAccountPrefix + " ] ");
		buffer.append("cveProduct[ " + cveProduct + " ] ");
		buffer.append("embosserLayoutId[ " + embosserLayoutId + " ] ");
		buffer.append("embosserNipFile[ " + embosserNipFile + " ] ");
		
		buffer.append("embosserFormatFile[ " + embosserFormatFile + " ] ");
		buffer.append("usingCustomAccount[ " + usingCustomAccount + " ] ");
		buffer.append("brand[ " + brand + " ] ");
		
		return buffer.toString();
	}


}
