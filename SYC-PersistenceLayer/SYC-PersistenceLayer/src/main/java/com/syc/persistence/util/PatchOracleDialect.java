package com.syc.persistence.util;


import java.sql.Types;

import org.hibernate.dialect.Oracle10gDialect;

public class PatchOracleDialect extends Oracle10gDialect{
	
	
	public PatchOracleDialect(){
		super();	
		//registerColumnType( Types.CHAR, "char(100 char)" );
		registerColumnType(Types.CHAR,  "char($l)" );
	}

	
}
