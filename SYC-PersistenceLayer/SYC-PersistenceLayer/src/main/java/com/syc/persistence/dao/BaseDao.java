package com.syc.persistence.dao;

import com.syc.persistence.dto.BaseDto;

public interface BaseDao extends PersistenceDao<BaseDto, String>{
	
	long getMaxBase();

}
