package com.syc.persistence.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IXESEQ")
public class CardSeqDto implements Serializable {

	private static final long serialVersionUID = -4683342589256715995L;

	@Id
	@Column(name = "BIN", length = 6, nullable = false)
	private String bin;

	@Column(name = "SEQNO")
	private long seqNo;

	@Column(name = "O_ROWID")
	private int oRowId;

	@Column(name = "SECUENCIA", length = 20)
	private String secuencia;

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public long getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(long seqNo) {
		this.seqNo = seqNo;
	}

	public int getoRowId() {
		return oRowId;
	}

	public void setoRowId(int oRowId) {
		this.oRowId = oRowId;
	}

	public String getSecuencia() {
		return secuencia;
	}

	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}

}
