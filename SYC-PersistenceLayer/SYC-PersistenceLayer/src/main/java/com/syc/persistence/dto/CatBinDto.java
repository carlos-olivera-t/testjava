package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * <p>
 * Clase de dominio que mapea la tabla <b>CAT_BINES</b>
 * <p>
 * @version 1.2-SNAPSHOT 30/04/2012.
 */
@Entity
@Table(name = "CAT_BINES")
@NamedQueries({
	@NamedQuery(name = "CatBinDto.updateImage", query = "UPDATE CatBinDto SET image = :imagen WHERE catBinPk.bin = :bin and catBinPk.producto = :producto"),
	@NamedQuery(name = "catBinDto.findBinByOwner", query = "SELECT DISTINCT(cb.catBinPk.bin) FROM CatBinDto cb WHERE cb.idOwner = :idOwner")
	})
public class CatBinDto implements Serializable {

	private static final long serialVersionUID = -3950628227930159669L;

	private CatBinPk catBinPk = new CatBinPk();
	private int idOwner;
	private String owner;
	private String productName;
	private int idEmbosser;
	private String formatEmbosserFile;
	private String brand;
	private int nipFile;
	private Blob image;

	@EmbeddedId
	public CatBinPk getCatBinPk() {
		return catBinPk;
	}

	public void setCatBinPk(CatBinPk catBinPk) {
		this.catBinPk = catBinPk;
	}

	@Column(name = "ID_OWNER")
	public int getIdOwner() {
		return idOwner;
	}

	public void setIdOwner(int idOwner) {
		this.idOwner = idOwner;
	}

	@Column(name = "OWNER")
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Column(name = "PRODUCT_NAME")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "ID_EMBOSSER")
	public int getIdEmbosser() {
		return idEmbosser;
	}

	public void setIdEmbosser(int idEmbosser) {
		this.idEmbosser = idEmbosser;
	}

	@Column(name = "FORMAT_EMBOSSER_FILE")
	public String getFormatEmbosserFile() {
		return formatEmbosserFile;
	}

	public void setFormatEmbosserFile(String formatEmbosserFile) {
		this.formatEmbosserFile = formatEmbosserFile;
	}

	@Column(name = "BRAND")
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	@Column(name = "NIP_FILE")
	public int getNipFile() {
		return nipFile;
	}

	public void setNipFile(int nipFile) {
		this.nipFile = nipFile;
	}

	@Column(name = "IMAGE")
	@Lob
	public Blob getImage() {
		return image;
	}

	public void setImage(Blob image) {
		this.image = image;
	}

	@Embeddable
	public class CatBinPk implements Serializable {

		private static final long serialVersionUID = 4019345795478641995L;

		private String bin;
		private String producto;

		@Column(name = "BIN", length = 6, columnDefinition = "VARCHAR2(6) NOT NULL")
		public String getBin() {
			return bin;
		}

		public void setBin(String bin) {
			this.bin = bin;
		}

		@Column(name = "PRODUCT", length = 6, columnDefinition = "VARCHAR2(4) NOT NULL")
		public String getProducto() {
			return producto;
		}

		public void setProducto(String producto) {
			this.producto = producto;
		}
	}

	public String toString() {
		StringBuilder buffer = new StringBuilder();

		buffer.append("catBinPk.bin[ " + catBinPk.getBin() + " ]  ");
		buffer.append("catBinPk.producto[ " + catBinPk.getProducto() + " ]  ");
		buffer.append("idOwner[ " + idOwner + " ]  ");
		buffer.append("owner[ " + owner + " ]  ");
		buffer.append("productName[ " + productName + " ]  ");
		buffer.append("idEmbosser[ " + idEmbosser + " ]  ");
		buffer.append("formatEmbosserFile[ " + formatEmbosserFile + " ]  ");
		buffer.append("brand[ " + brand + " ]  ");
		buffer.append("nipFile[ " + nipFile + " ]  ");

		return buffer.toString();
	}

}
