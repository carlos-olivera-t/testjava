package com.syc.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.EmisorDao;
import com.syc.persistence.dto.EmisorDto;

@Repository
@SuppressWarnings("unchecked")
public class EmisorDaoImpl extends AbstractPersistenceDaoImpl<EmisorDto, Long> implements EmisorDao {

	@Autowired
	public EmisorDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}
	
	public EmisorDto findByBin(String bin) {
		final String QUERY =  "FROM EmisorDto WHERE bin = '"+bin+"' " ;
		
		return (EmisorDto) DataAccessUtils.uniqueResult(getHibernateTemplate().find(QUERY));				
	}
	
	
	
}
