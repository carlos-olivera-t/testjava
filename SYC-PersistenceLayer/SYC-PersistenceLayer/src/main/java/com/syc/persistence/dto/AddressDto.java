package com.syc.persistence.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ISTBASEADDR")
public class AddressDto implements Serializable {

	private static final long serialVersionUID = 5061650351295242940L;

	@Column(name = "BIN", length = 10)
	private String bin;

	@Column(name = "BASE", length = 10)
	private String idCliente;

	@Id
	@Column(name = "PAN", length = 19)
	private String tarjeta;

	@Column(name = "E_NAME", length = 35)
	private String nombre;

	@Column(name = "E_ADDRESS_A", length = 45)
	private String direccion;

	@Column(name = "E_ADDRESS_B", length = 45)
	private String direccion2;

	@Column(name = "E_ADDRESS_C", length = 45)
	private String colonia;

	@Column(name = "E_ADDRESS_D", length = 45)
	private String localidad;

	@Column(name = "A_ADDRESS_A", length = 45)
	private String ciudad;

	@Column(name = "A_ADDRESS_B", length = 45)
	private String estado;

	@Column(name = "A_ADDRESS_C", length = 16)
	private String rfc;

	@Column(name = "A_ADDRESS_D", length = 45)
	private String email;

	@Column(name = "PO_BOX", length = 9)
	private String cp;
	
	@Column(name = "POSTAL_ZIP", length = 9)
	private String cp2;

	@Column(name = "PHONE_1", length = 10)
	private String telefono;

	@Column(name = "PHONE_2", length = 10)
	private String telefono2;

	@Column(name = "PHONE_3", length = 10)
	private String telefono3;

	@Column(name = "CORRESPOND_FLAG", length = 1)
	private String correspondFlag;

	@Column(name = "BRANCH", length = 3)
	private String branch;

	public AddressDto(){
		
	}
	
	public AddressDto(boolean newRow){
		this.direccion = " ";
		this.direccion2 = " ";
		this.colonia = " ";
		this.localidad = " ";
		this.nombre = " ";
		this.ciudad = " ";
		this.estado = " ";
		this.rfc = " ";
		this.email = " ";
		this.cp = " ";
		this.cp2 = this.cp; 
		this.telefono = " ";
		this.telefono2 = " ";
		this.telefono3 = " ";
		this.correspondFlag = "0";
		this.branch = "001";
	}
	
	
	
	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public String getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDireccion2() {
		return direccion2;
	}

	public void setDireccion2(String direccion2) {
		this.direccion2 = direccion2;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	
	public String getCp2() {
		return cp2;
	}

	public void setCp2(String cp2) {
		this.cp2 = cp2;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getTelefono3() {
		return telefono3;
	}

	public void setTelefono3(String telefono3) {
		this.telefono3 = telefono3;
	}

	public String getCorrespondFlag() {
		return correspondFlag;
	}

	public void setCorrespondFlag(String correspondFlag) {
		this.correspondFlag = correspondFlag;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String toString() {
		StringBuilder buffer = new StringBuilder();

		buffer.append("bin[ " + bin + " ]  ");
		buffer.append("idCliente[ " + idCliente + " ]  ");
		buffer.append("tarjeta[ " + tarjeta + " ]  ");
		buffer.append("nombre[ " + nombre + " ]  ");
		buffer.append("direccion[ " + direccion + " ]  ");
		buffer.append("direccio2[ " + direccion2 + " ]  ");
		buffer.append("colonia[ " + colonia + " ]  ");
		buffer.append("localidad[ " + localidad + " ]  ");
		buffer.append("estado[ " + estado + " ]  ");
		buffer.append("cp[ " + cp + " ]  ");
		buffer.append("cp2[ " + cp2 + " ]  ");
		buffer.append("telefono[ " + telefono + " ]  ");
		buffer.append("telefono2[ " + telefono2 + " ]  ");
		buffer.append("rfc[ " + rfc + " ]  ");
		return buffer.toString();
	}

}
