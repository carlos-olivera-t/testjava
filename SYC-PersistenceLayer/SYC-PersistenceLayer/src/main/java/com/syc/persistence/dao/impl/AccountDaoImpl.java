package com.syc.persistence.dao.impl;

import java.sql.Date;



import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.AccountDao;
import com.syc.persistence.dto.AccountDto;
import com.syc.persistence.dto.AccountFullDto;


@SuppressWarnings("unchecked")
@Repository("accountDao")
public class AccountDaoImpl extends AbstractPersistenceDaoImpl<AccountDto, Long> implements AccountDao{
	
	private static final transient Log log = LogFactory.getLog(AccountDaoImpl.class);
	
	@Autowired
	public AccountDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	public AccountDto findInfoAccount(String bin, String account){
		final String QUERY =  "FROM AccountDto account " +
				             "WHERE account.bin = '"+bin+"' " +
				               "AND account.account = '"+account+"'";
		return (AccountDto) DataAccessUtils.uniqueResult(getHibernateTemplate().find(QUERY));
	}
	
	public int sqlUpdate( AccountDto account ) throws StaleObjectStateException{	
		int updateEntity = 0;
		
			log.debug("usando el metodo 'update()' de la clase '" + this.getClass().getSimpleName() + " '");
			
			String hqlUpdate = "UPDATE AccountDto " +
					              "SET ledgerBalance    = :newAmount, "+
					                  "availableBalance = :newAmount "+
					            "WHERE bin = :bin "+
					              "AND account = :account";
			
			 updateEntity = getSession().createQuery( hqlUpdate )
							               .setDouble("newAmount" , account.getAvailableBalance())
							               .setString("bin"       , account.getBin())
							               .setString("account"   , account.getAccount())
							               .executeUpdate()   ;

		
		return updateEntity;
	}
	
	public int accountUpdate( AccountDto account, String newAccount ) {	
		String hqlUpdate = "UPDATE AccountDto " +
				              "SET account    = :newAccount "+
				            "WHERE bin        = :bin "+
				              "AND account    = :account";
		
		int updateEntity = getSession().createQuery( hqlUpdate )
						               .setString("newAccount" , newAccount)
						               .setString("bin"        , account.getBin())
						               .setString("account"    , account.getAccount())
						               .executeUpdate();
		
		return updateEntity;
	}
	

	public int accountUpdateFromDummy(AccountDto account, String newAccount){
		String hqlUpdate = "UPDATE AccountDto "   +
	              "SET account    = :newAccount ,"+
	              "  shortName    = :shortName " + 
	            "WHERE bin        = :bin "+
	              "AND account    = :account";

			int updateEntity = getSession().createQuery( hqlUpdate )
			               .setString("newAccount" , newAccount)
			               .setString("shortName"    , account.getAccount().trim())
			               .setString("bin"        , account.getBin())
			               .setString("account"    , account.getAccount())
			               .executeUpdate();

			return updateEntity;
	}
	
	public void saveNewAccount(AccountFullDto account){
		getHibernateTemplate().save(account);			
	}

	public int acumulateBalanceUpdate(double amount, Date trandate, String bin, String account){
		
		String hqlUpdate = "UPDATE AccountDto "   +
	              "SET cash_dep_used    = :amount ,"+
	              "  cash_dep_date    	= :trandate " + 
	            "WHERE bin        		= :bin "+
	              "AND account    		= :account";
			int updateEntity = getSession().createQuery( hqlUpdate )
			               .setDouble("amount" , amount)
			               .setDate("trandate"    , trandate)
			               .setString("bin"        , bin)
			               .setString("account"    , account)
			               .executeUpdate();

			return updateEntity;
		
	}
}
