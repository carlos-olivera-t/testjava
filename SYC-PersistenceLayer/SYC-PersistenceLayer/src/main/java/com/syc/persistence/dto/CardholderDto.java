package com.syc.persistence.dto;

public class CardholderDto {
	
	private CardDto    card;
	private IstrelDto  istrel;
	private AccountDto account;
	private IstCardTypeDto istcardType;
	private IstBinDto    istBin;
	private EmbosserPropertiesDto embosserProperties;
	
	
	public CardholderDto(){
		this.card        = null;
		this.account     = null;
		this.istrel      = null;
		this.istcardType = null;
		this.istBin      = null;
		this.embosserProperties  = null;
	}

	public CardDto getCard() {
		return card;
	}
	public void setCard(CardDto card) {
		this.card = card;
	}

	public AccountDto getAccount() {
		return account;
	}
	public void setAccount(AccountDto account) {
		this.account = account;
	}

	public IstrelDto getIstrel() {
		return istrel;
	}
	public void setIstrel(IstrelDto istrel) {
		this.istrel = istrel;
	}

	public EmbosserPropertiesDto getEmbosserProperties() {
		return embosserProperties;
	}
	public void setEmbosserProperties(EmbosserPropertiesDto embosserProperties) {
		this.embosserProperties = embosserProperties;
	}

	public IstCardTypeDto getIstcardType() {
		return istcardType;
	}
	public void setIstcardType(IstCardTypeDto istcardType) {
		this.istcardType = istcardType;
	}

	public IstBinDto getIstBin() {
		return istBin;
	}
	public void setIstBin(IstBinDto istBin) {
		this.istBin = istBin;
	}
	
}
