package com.syc.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="TBL_EMPLEADORAS")
@NamedQueries({
	@NamedQuery(name="employer.findByCode" , query="FROM EmployerDto a WHERE a.emprCode = :emprCode AND a.issCode = :issCode AND a.tiType =:tiType")
	})
public class EmployerDto {
	
	@Column(name="ISS_CODE")
	private int issCode;
	@Id
	@Column(name="EMPR_CODE")
	private String emprCode;
	@Column(name="EMPR_NAME")
	private String emprName;
	@Column(name="TI_TYPE")
	private String tiType;
	@Column(name="ESTATUS")
	private String status;
	
	
	public int getIssCode() {
		return issCode;
	}
	public void setIssCode(int issCode) {
		this.issCode = issCode;
	}
	public String getEmprCode() {
		return emprCode;
	}
	public void setEmprCode(String emprCode) {
		this.emprCode = emprCode;
	}
	public String getEmprName() {
		return emprName;
	}
	public void setEmprName(String emprName) {
		this.emprName = emprName;
	}
	public String getTiType() {
		return tiType;
	}
	public void setTiType(String tiType) {
		this.tiType = tiType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmpleadorasDto [issCode=");
		builder.append(issCode);
		builder.append(", emprCode=");
		builder.append(emprCode);
		builder.append(", emprName=");
		builder.append(emprName);
		builder.append(", tiType=");
		builder.append(tiType);
		builder.append(", status=");
		builder.append(status);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	

}
