package com.syc.persistence.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.AccountStatementDao;
import com.syc.persistence.dto.mapper.AccountStatementDto;

@Repository("accountStatementDao")
public class AccountStatementDaoImpl implements AccountStatementDao{
	
	@Autowired
	private JdbcTemplate sqlServerTemplate;
	
//	@Autowired
//	private JdbcTemplate postgresTemplate;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List <AccountStatementDto> findSqlServerAccountStatement(String employer, String product, TreeMap<Integer,String> mapOfDates){
		
		String QUERY = 
						"SELECT PUBLICA_EID, "+
					       "CONVERT(VARCHAR(10), PUBLICA_FCH_CORTE, 20) AS PUBLICA_FCH_CORTE, "+
					       "PUBLICA_WEB +'/'+ PUBLICA_FILE AS PDF_PATH, "+
					       "PUBLICA_PD, "+
					       "PUBLICA_FLGPDF, "+
					       "PUBLICA_FLGXML, "+
					       "PUBLICA_FLGEXCEL "+
					  "FROM PUBLICA "+
					 "WHERE PUBLICA_EID LIKE '"+employer+"%' "+
					   	(!GeneralUtil.isEmtyString(product) ? "AND PUBLICA_PD  = "+product+" " : "")+   
					   "AND PUBLICA_FCH_CORTE "+ 
						  		"BETWEEN CONVERT(DATETIME, '"+mapOfDates.get(3)+"') "+
					                "AND CONVERT(DATETIME, '"+mapOfDates.get(1)+"') "+
					   "ORDER BY PUBLICA_FCH_CORTE DESC";
		
		return sqlServerTemplate.query(QUERY, new RowMapper() {
		      public Object mapRow(ResultSet rs, int i) throws SQLException {
		    	  AccountStatementDto row = new AccountStatementDto();
		  		  row.setEmployer     ( rs.getString("PUBLICA_EID")       );
		  		  row.setPdfPath      ( rs.getString("PDF_PATH")          );
		  		  row.setCutDate      ( rs.getString("PUBLICA_FCH_CORTE") );
		  		  row.setIdProduct    ( rs.getString("PUBLICA_PD")        );
		  		  row.setXmlPath      ( rs.getString("PUBLICA_FLGXML")    );
		  		  row.setExcelPath    ( rs.getString("PUBLICA_FLGEXCEL")  );
		  		  row.setFinalShowDate( rs.getString("PUBLICA_FCH_CORTE") );
		  		  row.setStartShowDate( rs.getString("PUBLICA_FLGPDF")    );
		  		  return row;
		        }
		      }); 
	}

/*
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List <AccountStatementDto> findPostgresAccountStatement(String employer, String product, TreeMap<Integer,String> mapOfDates){
		
		String QUERY =    
					"SELECT VIS_TARJETA, "+
					       "VIS_FECHACORTE, "+
					       "VIS_VISIBLEWEBINICIO, "+
					       "VIS_VISIBLEWEBFINAL, "+   
					       "VIS_RUTA || VIS_NOMBREARCHIVO AS PDF_PATH, "+
					       "EXCEL_PATH, "+
					       "VIS_PRODUCTO "+
					  "FROM VISOR "+ 
					 "WHERE VIS_TARJETA LIKE '"+employer+"%' "+ 
					    (!GeneralUtil.isEmtyString(product) ?"AND VIS_PRODUCTO = '"+product+"' ":"")+
					   "AND VIS_FECHACORTE " +
					   		 	"BETWEEN TO_DATE('"+mapOfDates.get(3)+"', 'DD/MM/YYYY') "+
					   		 	    "AND TO_DATE('"+mapOfDates.get(2)+"', 'DD/MM/YYYY') "+
					   "ORDER BY VIS_FECHACORTE DESC ";
		
		return postgresTemplate.query(QUERY, new RowMapper() {
		      public Object mapRow(ResultSet rs, int i) throws SQLException {
		    	  AccountStatementDto row = new AccountStatementDto();
		  		  row.setEmployer     ( rs.getString("VIS_TARJETA")          );
		  		  row.setPdfPath      ( rs.getString("PDF_PATH")             );
		  		  row.setCutDate      ( rs.getString("VIS_FECHACORTE")       );
		  		  row.setIdProduct    ( rs.getString("VIS_PRODUCTO")         );
		  		  row.setExcelPath    ( rs.getString("EXCEL_PATH")           );
		  		  row.setFinalShowDate( rs.getString("VIS_VISIBLEWEBFINAL")  );
		  		  row.setStartShowDate( rs.getString("VIS_VISIBLEWEBINICIO") );
		  		  return row;
		        }
		      }); 
	}
*/	
}
