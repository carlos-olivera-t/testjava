package com.syc.persistence.dao.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.BitacoraMatchDao;
import com.syc.persistence.dto.BitacoraMatchDto;

@SuppressWarnings("unchecked")
@Repository("BitacoraMatchDao")
public class BitacoraMatchDaoImpl extends AbstractPersistenceDaoImpl<BitacoraMatchDto,Long> implements BitacoraMatchDao {
	
	@SuppressWarnings("unused")
	private static final transient Log log = LogFactory.getLog(BitacoraMatchDaoImpl.class);
	
	@Autowired
	public BitacoraMatchDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}
	
	public BitacoraMatchDto findByAccount(String account, String emisor){
		String QUERY =
						 "FROM BitacoraMatchDto "+
						"WHERE cuenta = '"+account+"' "+
						  "AND emisor = '"+emisor+"' "+
						  "AND statusCuenta IN ('A', 'B')";
		return (BitacoraMatchDto) DataAccessUtils.uniqueResult( getHibernateTemplate().find(QUERY) );
		
	}
	
}
