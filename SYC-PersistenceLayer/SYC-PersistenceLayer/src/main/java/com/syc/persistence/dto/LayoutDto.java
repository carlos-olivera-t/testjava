package com.syc.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LAYOUTS_MAQUILA")
public class LayoutDto {
	
	@Id
	@Column(name = "ID")
	private int    id;
	
	@Column(name = "ID_EMBOSSER")
	private int    idEmbosser;
	
	@Column(name = "FIELD")
	private String field;
	
	@Column(name = "DEFAULT_FIELD")
	private String defaultField;
	
	@Column(name = "MAP_FIELD")
	private String mapField;
	
	@Column(name = "FORMAT_FIELD")
	private String formatField;
	
	@Column(name = "LENGTH_FIELD")
	private int    lengthField;
	
	@Column(name = "FIELD_TYPE")
	private String fieldType;
	
	@Column(name = "CHAR_REFILL")
	private char   charRefill;
	
	@Column(name = "JUSTIFIED")
	private String  justified;
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getIdEmbosser() {
		return idEmbosser;
	}
	public void setIdEmbosser(int idEmbosser) {
		this.idEmbosser = idEmbosser;
	}

	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	
	public String getDefaultField() {
		return defaultField;
	}
	public void setDefaultField(String defaultField) {
		this.defaultField = defaultField;
	}
	
	public String getMapField() {
		return mapField;
	}
	public void setMapField(String mapField) {
		this.mapField = mapField;
	}
	
	public String getFormatField() {
		return formatField;
	}
	public void setFormatField(String formatField) {
		this.formatField = formatField;
	}
	
	public int getLengthField() {
		return lengthField;
	}
	public void setLengthField(int lengthField) {
		this.lengthField = lengthField;
	}

	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public char getCharRefill() {
		return charRefill;
	}
	public void setCharRefill(char charRefill) {
		this.charRefill = charRefill;
	}

	public String getJustified() {
		return justified;
	}
	public void setJustified(String justified) {
		this.justified = justified;
	}

	public String toString(){
		String buffer = null;
		buffer  = "id[ "+id+" ]  ";
		buffer += "idEmbosser[ "+idEmbosser+" ]  ";
		buffer += "field[ "+field+" ]  ";
		buffer += "defaultField[ "+defaultField+" ]  ";
		buffer += "mapField[ "+mapField+"]  ";
		buffer += "formatField[ "+formatField+"]  ";
		buffer += "lengthField[ "+lengthField+" ]  ";
		buffer += "fieldType[ "+fieldType+" ]  ";   
		buffer += "charRefill[ "+charRefill+" ]  ";
		buffer += "justified[ "+justified+" ]  ";
		return buffer;
	}
	
}
