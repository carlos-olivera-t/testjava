package com.syc.persistence.dao;

import java.sql.Date;
import java.util.List;

import com.syc.persistence.dto.SHCLogDto;


public interface SHCLogDao extends PersistenceDao<SHCLogDto, Long>{
	
	public List<SHCLogDto> getLogByCardNumber( String numberCard );	

	public SHCLogDto getLogByCardNumberDetails(String numberCard, Date tranDate, int msgType, String authNum );

	public Object getLimitsByCardNumber( String numberCard, Date fecha );	
}
