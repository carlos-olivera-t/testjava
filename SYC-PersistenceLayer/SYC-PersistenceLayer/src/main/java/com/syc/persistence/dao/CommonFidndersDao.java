package com.syc.persistence.dao;

import com.syc.persistence.dto.IstBinDto;
import com.syc.persistence.dto.IstCardTypeDto;


public interface CommonFidndersDao extends PersistenceDao<Object, Long>{
	
	
	/**
	 * Recupera la informacion del IstCardType
	 * @author Angel Contreras
	 * @param  bin
	 * @return IstCardTypeDto
	 */
	
	public IstCardTypeDto findIstCardTypeInfo( String bin );
	
	/**
	 * Recupera la informacion del IstBin
	 * @author Angel Contreras
	 * @param  bin
	 * @return IstBinDto
	 */
	
	public IstBinDto findIstBinInfo( String bin );

}
