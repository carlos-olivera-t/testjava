package com.syc.persistence.dao;


import com.syc.persistence.dto.PCodeDto;

public interface PCodeDao extends PersistenceDao<PCodeDto,String>{
	
	
	PCodeDto findIdPCode(String id);

}
