package com.syc.persistence.dao;


import com.syc.persistence.dto.CardActivityDto;

public interface CardActivityDao extends PersistenceDao<CardActivityDto, Long>{
	
	/***********************************************************
	 *   Los unicos metodos utilizados son el CRUD
	 ***********************************************************/

	public Object[] findByPan(String pan);
}
