package com.syc.persistence.dao;


import com.syc.persistence.dto.LogAccountDto;

public interface LogAccountDao extends PersistenceDao<LogAccountDto, Long>{
	
	@Deprecated
	String findLastAccount(String bin);


}
