package com.syc.persistence.dto.mapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionDto", propOrder = {
    "pan",
    "conceptDescription",
    "respCode",
    "respCodeDescription",
    "amount",
    "authorization",
    "trxCode",
    "trxDescription",
    "acceptorName",
    "transactionDate",
    "reference",
    "availableBalance"
})

public class TransactionDto{
	
	
	private String pan;
	private String conceptDescription;
	private int    respCode;
	private String respCodeDescription;
	private double amount;
	public  String authorization;
	private String trxCode;
	private String trxDescription;
	private String acceptorName;
	private String transactionDate;
	private String reference;
	private double availableBalance;
	
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getConceptDescription() {
		return conceptDescription;
	}
	public void setConceptDescription(String conceptDescription) {
		this.conceptDescription = conceptDescription;
	}

	public int getRespCode() {
		return respCode;
	}
	public void setRespCode(int respCode) {
		this.respCode = respCode;
	}

	public String getRespCodeDescription() {
		return respCodeDescription;
	}
	public void setRespCodeDescription(String respCodeDescription) {
		this.respCodeDescription = respCodeDescription;
	}

	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String getTrxCode() {
		return trxCode;
	}
	public void setTrxCode(String trxCode) {
		this.trxCode = trxCode;
	}

	public String getTrxDescription() {
		return trxDescription;
	}
	public void setTrxDescription(String trxDescription) {
		this.trxDescription = trxDescription;
	}

	public String getAcceptorName() {
		return acceptorName;
	}
	public void setAcceptorName(String acceptorName) {
		this.acceptorName = acceptorName;
	}

	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}	
	public double getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(double availableBalance) {
		this.availableBalance = availableBalance;
	}

}

