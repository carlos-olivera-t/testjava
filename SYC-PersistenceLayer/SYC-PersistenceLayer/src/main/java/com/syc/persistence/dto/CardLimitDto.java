package com.syc.persistence.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "ISTDBLIMIT")
@IdClass(CardLimitDto.CardLimitPk.class)

@NamedQueries({
@NamedQuery(name="cardLimit.findByPk", query="FROM CardLimitDto card WHERE card.pan = CAST(:pan  as char) and card.bin = CAST(:bin  as char)")
})
public class CardLimitDto implements Serializable{

	
	
	private static final long serialVersionUID = 8326110101062549853L;

	@Column(name = "BIN", length = 10)
	private String bin;

	@Id
	@Column(name = "PAN", length = 19)
	private String pan;

	@Column(name = "CASH_WD_LIMIT")
	private double cashWdLimit;

	@Column(name = "CASH_WD_COUNT")
	private int cashWdCount;

	@Column(name = "TC_LIMIT")
	private double tcLimit;

	@Column(name = "TC_COUNT")
	private int tcCount;

	@Column(name = "CASH_TC_LIMIT")
	private double cashTcLimit;

	@Column(name = "PURCH_LIMIT")
	private double purchLimit;

	@Column(name = "PURCH_COUNT")
	private int purchCount;

	@Column(name = "TSF_BASE_CURR_LIMIT")
	private double tsfBaseCurrLimit;

	@Column(name = "TSF_BASE_CURR_COUNT")
	private int tsfBaseCurrCount;

	@Column(name = "TSF_MULTI_LIMIT")
	private double tsfMultiLimit;

	@Column(name = "TSF_MULTI_COUNT")
	private int tsfMiltiCount;

	@Column(name = "TSF_BASE_TXN_LIM")
	private double tsfBaseTxnLim;

	@Column(name = "USD_TSF_TXN_LIM")
	private double usdTsfTxnLim;

	@Column(name = "CASH_DEP_LIMIT")
	private double cashDepLimit;

	@Column(name = "ICASH_WD_LIMIT_INT", nullable = true)
	private Double icashWdLimitInt;

	@Column(name = "ICASH_WD_COUNT_INT", nullable = true)
	private Integer icashWdCountInt;

	public CardLimitDto() {

	}

	public CardLimitDto(CardLimitDto limit) {
		super();
		this.bin 					= limit.getBin();
		this.pan 					= limit.getPan();
		this.cashWdLimit 			= limit.getCashWdLimit();
		this.cashWdCount 			= limit.getCashWdCount();
		this.tcLimit 				= limit.getTcLimit();
		this.tcCount 				= limit.getTcCount();
		this.cashTcLimit 			= limit.getCashTcLimit();
		this.purchLimit 			= limit.getPurchLimit();
		this.purchCount 			= limit.getPurchCount();
		this.tsfBaseCurrLimit 		= limit.getTsfBaseCurrLimit();
		this.tsfBaseCurrCount 		= limit.getTsfBaseCurrCount();
		this.tsfMultiLimit 			= limit.getTsfMultiLimit();
		this.tsfMiltiCount 			= limit.getTsfMiltiCount();
		this.tsfBaseTxnLim 			= limit.getTsfBaseTxnLim();
		this.usdTsfTxnLim 			= limit.getUsdTsfTxnLim();
		this.cashDepLimit 			= limit.getCashDepLimit();
		this.icashWdLimitInt 		= limit.getIcashWdLimitInt();
		this.icashWdCountInt 		= limit.getIcashWdCountInt();
	}

	/**
	 * @return the bin
	 */
	public String getBin() {
		return bin;
	}

	/**
	 * @param bin
	 *            the bin to set
	 */
	public void setBin(String bin) {
		this.bin = bin;
	}

	/**
	 * @return the pan
	 */
	public String getPan() {
		return pan;
	}

	/**
	 * @param pan
	 *            the pan to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}

	/**
	 * @return the cashWdLimit
	 */
	public double getCashWdLimit() {
		return cashWdLimit;
	}

	/**
	 * @param cashWdLimit
	 *            the cashWdLimit to set
	 */
	public void setCashWdLimit(double cashWdLimit) {
		this.cashWdLimit = cashWdLimit;
	}

	/**
	 * @return the cashWdCount
	 */
	public int getCashWdCount() {
		return cashWdCount;
	}

	/**
	 * @param cashWdCount
	 *            the cashWdCount to set
	 */
	public void setCashWdCount(int cashWdCount) {
		this.cashWdCount = cashWdCount;
	}

	/**
	 * @return the tcLimit
	 */
	public double getTcLimit() {
		return tcLimit;
	}

	/**
	 * @param tcLimit
	 *            the tcLimit to set
	 */
	public void setTcLimit(double tcLimit) {
		this.tcLimit = tcLimit;
	}

	/**
	 * @return the tcCount
	 */
	public int getTcCount() {
		return tcCount;
	}

	/**
	 * @param tcCount
	 *            the tcCount to set
	 */
	public void setTcCount(int tcCount) {
		this.tcCount = tcCount;
	}

	/**
	 * @return the cashTcLimit
	 */
	public double getCashTcLimit() {
		return cashTcLimit;
	}

	/**
	 * @param cashTcLimit
	 *            the cashTcLimit to set
	 */
	public void setCashTcLimit(double cashTcLimit) {
		this.cashTcLimit = cashTcLimit;
	}

	/**
	 * @return the purchLimit
	 */
	public double getPurchLimit() {
		return purchLimit;
	}

	/**
	 * @param purchLimit
	 *            the purchLimit to set
	 */
	public void setPurchLimit(double purchLimit) {
		this.purchLimit = purchLimit;
	}

	/**
	 * @return the purchCount
	 */
	public int getPurchCount() {
		return purchCount;
	}

	/**
	 * @param purchCount
	 *            the purchCount to set
	 */
	public void setPurchCount(int purchCount) {
		this.purchCount = purchCount;
	}

	/**
	 * @return the tsfBaseCurrLimit
	 */
	public double getTsfBaseCurrLimit() {
		return tsfBaseCurrLimit;
	}

	/**
	 * @param tsfBaseCurrLimit
	 *            the tsfBaseCurrLimit to set
	 */
	public void setTsfBaseCurrLimit(double tsfBaseCurrLimit) {
		this.tsfBaseCurrLimit = tsfBaseCurrLimit;
	}

	/**
	 * @return the tsfBaseCurrCount
	 */
	public int getTsfBaseCurrCount() {
		return tsfBaseCurrCount;
	}

	/**
	 * @param tsfBaseCurrCount
	 *            the tsfBaseCurrCount to set
	 */
	public void setTsfBaseCurrCount(int tsfBaseCurrCount) {
		this.tsfBaseCurrCount = tsfBaseCurrCount;
	}

	/**
	 * @return the tsfMultiLimit
	 */
	public double getTsfMultiLimit() {
		return tsfMultiLimit;
	}

	/**
	 * @param tsfMultiLimit
	 *            the tsfMultiLimit to set
	 */
	public void setTsfMultiLimit(double tsfMultiLimit) {
		this.tsfMultiLimit = tsfMultiLimit;
	}

	/**
	 * @return the tsfMiltiCount
	 */
	public int getTsfMiltiCount() {
		return tsfMiltiCount;
	}

	/**
	 * @param tsfMiltiCount
	 *            the tsfMiltiCount to set
	 */
	public void setTsfMiltiCount(int tsfMiltiCount) {
		this.tsfMiltiCount = tsfMiltiCount;
	}

	/**
	 * @return the tsfBaseTxnLim
	 */
	public double getTsfBaseTxnLim() {
		return tsfBaseTxnLim;
	}

	/**
	 * @param tsfBaseTxnLim
	 *            the tsfBaseTxnLim to set
	 */
	public void setTsfBaseTxnLim(double tsfBaseTxnLim) {
		this.tsfBaseTxnLim = tsfBaseTxnLim;
	}

	/**
	 * @return the usdTsfTxnLim
	 */
	public double getUsdTsfTxnLim() {
		return usdTsfTxnLim;
	}

	/**
	 * @param usdTsfTxnLim
	 *            the usdTsfTxnLim to set
	 */
	public void setUsdTsfTxnLim(double usdTsfTxnLim) {
		this.usdTsfTxnLim = usdTsfTxnLim;
	}

	/**
	 * @return the cashDepLimit
	 */
	public double getCashDepLimit() {
		return cashDepLimit;
	}

	/**
	 * @param cashDepLimit
	 *            the cashDepLimit to set
	 */
	public void setCashDepLimit(double cashDepLimit) {
		this.cashDepLimit = cashDepLimit;
	}

	/**
	 * @return the icashWdLimitInt
	 */
	public Double getIcashWdLimitInt() {
		return icashWdLimitInt;
	}

	/**
	 * @param icashWdLimitInt
	 *            the icashWdLimitInt to set
	 */
	public void setIcashWdLimitInt(Double icashWdLimitInt) {
		this.icashWdLimitInt = icashWdLimitInt;
	}

	/**
	 * @return the icashWdCountInt
	 */
	public Integer getIcashWdCountInt() {
		return icashWdCountInt;
	}

	/**
	 * @param icashWdCountInt
	 *            the icashWdCountInt to set
	 */
	public void setIcashWdCountInt(Integer icashWdCountInt) {
		this.icashWdCountInt = icashWdCountInt;
	}
	
	

	@Override
	public String toString() {
		return "CardLimitDto [bin=" + bin + ", pan=" + pan + ", cashWdLimit="
				+ cashWdLimit + ", cashWdCount=" + cashWdCount + ", tcLimit="
				+ tcLimit + ", tcCount=" + tcCount + ", cashTcLimit="
				+ cashTcLimit + ", purchLimit=" + purchLimit + ", purchCount="
				+ purchCount + ", tsfBaseCurrLimit=" + tsfBaseCurrLimit
				+ ", tsfBaseCurrCount=" + tsfBaseCurrCount + ", tsfMultiLimit="
				+ tsfMultiLimit + ", tsfMiltiCount=" + tsfMiltiCount
				+ ", tsfBaseTxnLim=" + tsfBaseTxnLim + ", usdTsfTxnLim="
				+ usdTsfTxnLim + ", cashDepLimit=" + cashDepLimit
				+ ", icashWdLimitInt=" + icashWdLimitInt + ", icashWdCountInt="
				+ icashWdCountInt + "]";
	}



	public static class CardLimitPk implements Serializable{
		
		
		private static final long serialVersionUID = -3387162575799103733L;

		public CardLimitPk(){
			
		}
		
		public CardLimitPk(String bin, String pan){
			this.bin = bin;
			this.pan = pan;
		}
		
		private String bin;
		private String pan;
		
		public String getBin() {
			return bin;
		}
		public void setBin(String bin) {
			this.bin = bin;
		}
		public String getPan() {
			return pan;
		}
		public void setPan(String pan) {
			this.pan = pan;
		}

		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((bin == null) ? 0 : bin.hashCode());
			result = prime * result + ((pan == null) ? 0 : pan.hashCode());
			return result;
		}

		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CardLimitPk other = (CardLimitPk) obj;
			if (bin == null) {
				if (other.bin != null)
					return false;
			} else if (!bin.equals(other.bin))
				return false;
			if (pan == null) {
				if (other.pan != null)
					return false;
			} else if (!pan.equals(other.pan))
				return false;
			return true;
		}
		
		
	}
}

