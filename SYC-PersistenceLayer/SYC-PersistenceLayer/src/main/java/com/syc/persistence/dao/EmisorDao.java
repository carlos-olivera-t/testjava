package com.syc.persistence.dao;

import com.syc.persistence.dto.EmisorDto;

public interface EmisorDao extends PersistenceDao<EmisorDto, Long>{

	EmisorDto findByBin(String bin);
}
