package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="CAT_PCODES")

@NamedQueries({
	@NamedQuery(name="pcodes.findId", query="FROM PCodeDto WHERE descPCode = :id")
	
})
public class PCodeDto implements Serializable{

	private static final long serialVersionUID = 5689894847799768043L;
	
	@Id
	@Column(name="PCODE")
	private int pCode;
	@Column(name="DESC_PCODE", unique = true)
	private String descPCode;
	@Column(name="CONCEPTO")
	private int concept;
	@Column(name="DESC_CONCEPTO")
	private String descConcept;
	@Column(name="ORIGEN")
	private String origin;
	@Column(name="APLICA_IVA")
	private long applyIVA;
	@Column(name="APLICA_EDO_CTA")
	private long applyEdoCta;
	@Column(name="DESC_EDO_CTA")
	private String descEdoCta;
	@Column(name="PRODUCTO")
	private String product;
	@Column(name="FECHA_CREACION")
	private Date createDate;
	@Column(name="MUESTRA_AJUSTE_WEB")
	private String adjustmentWeb;
	@Column(name="CVE_EMISOR")
	private String emisor;
	
	
	
	public int getpCode() {
		return pCode;
	}
	public void setpCode(int pCode) {
		this.pCode = pCode;
	}
	public String getDescPCode() {
		return descPCode;
	}
	public void setDescPCode(String descPCode) {
		this.descPCode = descPCode;
	}
	public int getConcept() {
		return concept;
	}
	public void setConcept(int concept) {
		this.concept = concept;
	}
	public String getDescConcept() {
		return descConcept;
	}
	public void setDescConcept(String descConcept) {
		this.descConcept = descConcept;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public long getApplyIVA() {
		return applyIVA;
	}
	public void setApplyIVA(long applyIVA) {
		this.applyIVA = applyIVA;
	}
	public long getApplyEdoCta() {
		return applyEdoCta;
	}
	public void setApplyEdoCta(long applyEdoCta) {
		this.applyEdoCta = applyEdoCta;
	}
	public String getDescEdoCta() {
		return descEdoCta;
	}
	public void setDescEdoCta(String descEdoCta) {
		this.descEdoCta = descEdoCta;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getAdjustmentWeb() {
		return adjustmentWeb;
	}
	public void setAdjustmentWeb(String adjustmentWeb) {
		this.adjustmentWeb = adjustmentWeb;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PCodeDto [pCode=");
		builder.append(pCode);
		builder.append(", descPCode=");
		builder.append(descPCode);
		builder.append(", concept=");
		builder.append(concept);
		builder.append(", descConcept=");
		builder.append(descConcept);
		builder.append(", origin=");
		builder.append(origin);
		builder.append(", applyIVA=");
		builder.append(applyIVA);
		builder.append(", applyEdoCta=");
		builder.append(applyEdoCta);
		builder.append(", descEdoCta=");
		builder.append(descEdoCta);
		builder.append(", product=");
		builder.append(product);
		builder.append(", createDate=");
		builder.append(createDate);
		builder.append(", adjustmentWeb=");
		builder.append(adjustmentWeb);
		builder.append(", emisor=");
		builder.append(emisor);
		builder.append("]");
		return builder.toString();
	}
	

}
