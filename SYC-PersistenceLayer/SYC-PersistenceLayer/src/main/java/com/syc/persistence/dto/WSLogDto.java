package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.syc.persistence.dao.impl.GeneralUtil;


@Entity
@Table(name = "WS_LOG")
@IdClass(WSLogPK.class)
public class WSLogDto {
	
	@Id
	@AttributeOverrides({
		@AttributeOverride(name = "numberCard", column =@Column(name = "NUMBER_CARD") ),
		@AttributeOverride(name = "currentTime", column =@Column(name = "TRANTIME") ),
		@AttributeOverride(name = "command",	column = @Column(name="COMMAND"))
	})
	private String   command;
	@Column(name = "TRANTIME", nullable = false)
	private String   currentTime;
	
	
	
	@Column(name = "NUMBER_CARD", length = 120, nullable = false)
	private String   numberCard;
	
	@Column(name = "USER_NAME", length = 15)
	private String   userName;
	
	@Column(name="BIN")
	private String   bin;
	
	@Column(name = "REQUEST", length = 400, nullable = false)
	private String   request;
	
	@Column(name = "RESPONSE", length = 400, nullable = false)
	private String   response;
	
	@Column(name = "CODE_RESPONSE")
	private int      codeResponse;
	
	@Column(name = "TRANDATE", nullable = false)
	private Date     currentDate;
	
	
	
	
	public WSLogDto(WSLogPK logPK){
		bin        = null;
		command    = logPK.getCommand();	
		numberCard   = logPK.getNumberCard();
		userName     = null;     
		request      = null;      
		response     = null;     
		codeResponse = 0; 
		currentDate  = GeneralUtil.getCurrentSqlDate();
		currentTime  = logPK.getCurrentTime();
	}
	
	public WSLogDto(){		        
		bin          = null;          
		numberCard   = null;   
		userName     = null;     
		command      = null;      
		request      = null;      
		response     = null;     
		codeResponse = 0; 
		currentDate  = GeneralUtil.getCurrentSqlDate();
		currentTime  = GeneralUtil.getCurrentTime("HHmmssSSS");
		
	}
		
	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}
		
	public String getNumberCard() {
		return numberCard;
	}
	public void setNumberCard(String numberCard) {
		this.numberCard = numberCard;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	public int getCodeResponse() {
		return codeResponse;
	}
	public void setCodeResponse(int codeResponse) {
		this.codeResponse = codeResponse;
	}
	
	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	public String getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}
	
	public String toString() {
		StringBuilder buffer = new StringBuilder();		
		buffer.append("bin[ " + bin + " ]  ");
		buffer.append("numberCard[ " + numberCard + " ]  ");
		buffer.append("userName[ " + userName + " ]  ");
		buffer.append("command[ " + command + " ]  ");
		buffer.append("request[ " + request + " ]  ");
		buffer.append("response[ " + response + " ]  ");
		buffer.append("codeResponse[ " + codeResponse + " ]  ");
		buffer.append("currentDate[ " + currentDate + " ]  ");
		buffer.append("currentTime[ " + currentTime + " ]  ");
		return buffer.toString();
	}

	
}

@Embeddable
class WSLogPK implements Serializable{

	private static final long serialVersionUID = 7326130925157166033L;
	
	public WSLogPK(String numberCard, String command, String currentTime){
		this.numberCard = numberCard;
		this.command = command;
		this.currentTime = currentTime;
	}
	
	public WSLogPK(){}
	
		
	@Column(name = "TRANTIME", nullable = false)
	private String   currentTime;
	
	
	
	@Column(name = "NUMBER_CARD", length = 120, nullable = false)
	private String   numberCard;
	
	@Column(name = "COMMAND", length = 50, nullable = false)
	private String   command;

	
	public String getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}

	public String getNumberCard() {
		return numberCard;
	}

	public void setNumberCard(String numberCard) {
		this.numberCard = numberCard;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
}
