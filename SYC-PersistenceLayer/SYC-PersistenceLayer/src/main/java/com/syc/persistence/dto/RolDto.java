package com.syc.persistence.dto;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;

@Entity
@Table(name = "ROL_WEB")
public class RolDto implements Serializable{

	private static final long serialVersionUID = 2036366095443916073L;
	
	
	private Long rolId;
		
	private String authority;
	
	private Long version = 0L;	
	
	private Set<UserDto> users;
	
	/**
	 * @return the rolId
	 */
	@Id
	@Column(name="ROL_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RolIdSeqGenerator")
    @SequenceGenerator(name="RolIdSeqGenerator", sequenceName = "SEQ_ROL_WEB", allocationSize=1)
	public Long getRolId() {
		return rolId;
	}

	/**
	 * @param rolId the rolId to set
	 */
	public void setRolId(Long rolId) {
		this.rolId = rolId;
	}

	/**
	 * @return the authority
	 */	
	@Column(name="AUTHORITY")
	public String getAuthority() {
		return authority;
	}

	/**
	 * @param authority the authority to set
	 */
	public void setAuthority(String authority) {
		this.authority = authority;
	}

	/**
	 * @return the version
	 */	
	@Column(name="version")
	@Version
	public Long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * @return the users
	 */
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
	public Set<UserDto> getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(Set<UserDto> users) {
		this.users = users;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("rolId[" + rolId + "] ");
		sb.append(" authority[" + authority + "]");		
		
		return sb.toString();
	}
	
}
