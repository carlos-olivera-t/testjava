package com.syc.persistence.dao;

import java.util.List;

import com.syc.persistence.dto.CatBinDto;

public interface CatBinDao extends PersistenceDao<CatBinDto, CatBinDto.CatBinPk>{
	
	int updateImage(String bin,String producto,byte[] image);
	
	/**
	 * Obtine la lista de bines de un propiertario
	 * @param idOwner Propietario
	 * @return
	 */
	List<String> findBinByOwner(int idOwner);
		
}
