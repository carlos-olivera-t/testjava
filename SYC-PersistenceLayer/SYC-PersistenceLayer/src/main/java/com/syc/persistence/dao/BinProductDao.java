package com.syc.persistence.dao;

import java.util.List;

import com.syc.persistence.dto.BinProductDto;
import com.syc.persistence.dto.BinProductId;

public interface BinProductDao extends PersistenceDao<BinProductDto, BinProductId> {

	List<BinProductDto> getByBin(String bin);
	
}
