package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.*;

import com.syc.persistence.dao.impl.GeneralUtil;

@Entity
@Table(name="CMS_BITACORA_MATCH")
@IdClass(BitacoraMatchPK.class)
public class BitacoraMatchDto {
	
	
	@Id
	@AttributeOverrides({
		@AttributeOverride(name = "cuenta", column = @Column(name="CUENTA_BAM")),
		@AttributeOverride(name = "authorization",	column = @Column(name="OBSERVACION")),
		@AttributeOverride(name = "emisor",	column = @Column(name="CVE_CIA"))
	})
	
	private String cuenta;
	private String emisor;

	@Column(name="OBSERVACION")
	private String authorization;
	
	@Column(name="STATUS")
	private int    status;

	@Column(name="STATUS_CUENTA")
	private String statusCuenta;
	
	@Column(name="TARJETA")
	private String tarjeta;
	
	@Column(name="NOM_LARGO", length=25)
	private String nombreLargo;
	
	@Column(name="NOM_CORTO", length=25)
	private String nombreCorto;
	
	@Column(name="DIRECCION1", length=30)
	private String direccion;
	
	@Column(name="DIRECCION2", length=30)
	private String colonia;
	
	@Column(name="ENTIDAD_FED", length=21)
	private String entidadFederativa;
	
	@Column(name="REG_FED_CONT", length=10)
	private String rfc;
	
	@Column(name="COD_POSTAL", length=5)
	private String codigoPostal;
	
	@Column(name="TELEFONO1", length=10)
	private String telefono;
	
	@Column(name="TELEFONO2", length=10)
	private String telefono2;
	
	@Column(name="SUCURSAL_SYC")
	private String sucursalSyc;
	
	@Column(name="SUCURSAL_BAM")
	private String sucursalCliente;
	
	@Column(name="FEC_ALTA")
	private Date   fechaAlta;
	
	@Column(name="ARCHIVO_BAM")
	private String archivo;
	
	@Column(name="PRODUCTO")
	private String producto;
	
	
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	
	public String getStatusCuenta() {
		return statusCuenta;
	}
	public void setStatusCuenta(String statusCuenta) {
		this.statusCuenta = statusCuenta;
	}
	
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getNombreLargo() {
		return nombreLargo;
	}
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	
	public String getEntidadFederativa() {
		return entidadFederativa;
	}
	public void setEntidadFederativa(String entidadFederativa) {
		this.entidadFederativa = entidadFederativa;
	}

	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono2() {
		return telefono2;
	}
	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getSucursalSyc() {
		return sucursalSyc;
	}
	public void setSucursalSyc(String sucursalSyc) {
		this.sucursalSyc = sucursalSyc;
	}

	public String getSucursalCliente() {
		return sucursalCliente;
	}
	public void setSucursalCliente(String sucursalCliente) {
		this.sucursalCliente = sucursalCliente;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getArchivo() {
		return archivo;
	}
	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}

	
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	
	public BitacoraMatchDto(){
	}
	
	public BitacoraMatchDto(String emisor){
		this.tarjeta         = "0000000000000000";
		this.sucursalSyc     = "000";
		this.sucursalCliente = "00000";
		this.fechaAlta       = GeneralUtil.getCurrentSqlDate();
		this.status          = 1;
		this.producto        = "0001";
		this.statusCuenta    = "A";
		this.emisor          = emisor;
	}
	
	public BitacoraMatchDto(BitacoraMatchPK bitacoraPK){
		cuenta        = bitacoraPK.getCuenta();
		authorization = bitacoraPK.getAuthorization();
		emisor        = bitacoraPK.getEmisor();
	}
	
}

@Embeddable
class BitacoraMatchPK implements Serializable{
	private static final long serialVersionUID = 1L;

	@Column(name="CUENTA_BAM")
	private String cuenta;
	
	@Column(name="OBSERVACION")
	private String authorization;
	
	@Column(name="CVE_CIA")
	private String emisor;

	public BitacoraMatchPK(String cuenta, String authorization, String emisor){
		this.cuenta         = cuenta;
		this.authorization  = authorization;
		this.emisor         = emisor;
	}
	public BitacoraMatchPK(){}
	
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	
	
}
