package com.syc.persistence.dao;


import com.syc.persistence.dto.PhoneCodeDto;

public interface PhoneCodeDao extends PersistenceDao<PhoneCodeDto, String>{

}
