package com.syc.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USER_WEB")
public class UserDto {

	@Id
	@Column(name="USER_ID")
	private double id;
	
	@Column(name="USERNAME")
	private String userName;
	
	@Column(name="USER_PASSWORD")
	private String password;
	
	@Column(name="VERSION")	
	private long version;
	
	@Column(name="ACCOUNT_EXPIRED")
	private long accountExpired;
	
	@Column(name="PASSWORD_EXPIRED")
	private long passwordExpired;
	
	@Column(name="ACCOUNT_LOCKED")
	private long accountLocked;
	
	@Column(name="ENABLED")
	private long eneabled;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="FIRSTNAME")
	private String firstName;
	
	@Column(name="LASTNAME")
	private String lastName;
	
	@Column(name="SECONDLASTNAME")
	private String secondLastName;
	
	public double getId() {
		return id;
	}

	public void setId(double id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public long getAccountExpired() {
		return accountExpired;
	}

	public void setAccountExpired(long accountExpired) {
		this.accountExpired = accountExpired;
	}

	public long getPasswordExpired() {
		return passwordExpired;
	}

	public void setPasswordExpired(long passwordExpired) {
		this.passwordExpired = passwordExpired;
	}

	public long getEneabled() {
		return eneabled;
	}

	public void setEneabled(long eneabled) {
		this.eneabled = eneabled;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public long getAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(long accountLocked) {
		this.accountLocked = accountLocked;
	}
	

}
