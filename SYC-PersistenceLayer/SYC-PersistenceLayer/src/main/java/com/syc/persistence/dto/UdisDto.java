package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.syc.persistence.dao.impl.GeneralUtil;


@Entity
@Table(name = "SYC_UDIS_HIST")
@IdClass(UdisPK.class)
@NamedQueries({
	@NamedQuery(name="udis.findlastDay", query="SELECT max(trandate) FROM UdisDto WHERE bin = CAST(:bin  as char) AND product = CAST(:product  as char)"),
	@NamedQuery(name="udis.findlastHour", query="SELECT max(trantime) FROM UdisDto WHERE bin = CAST(:bin  as char) AND product = CAST(:product  as char) and to_date(trandate) = to_date(:trandate)"),
	@NamedQuery(name="udis.findlastUdi", query=" FROM UdisDto WHERE bin = CAST(:bin  as char) AND product = CAST(:product  as char) AND to_date(trandate) = to_date(:trandate) AND trantime = :lasttime")
	
})
public class UdisDto {
	
	
	@Id
	@AttributeOverrides({
		@AttributeOverride(name = "bin", column =@Column(name = "ID_NIVEL_CAPT") ),
		@AttributeOverride(name = "product", column =@Column(name = "NIVEL_CAPT") )
		
	})
	private String bin;
	@Column(name = "NIVEL_CAPT", nullable = false)
	private String product;
	@Column(name = "VAL_UDI", nullable = false)
	private double udi;
	@Column(name = "FECHA_ACT", nullable = false)
	private Date trandate;
	@Column(name = "HORA_ACT", nullable = false)
	private String trantime;
	@Column(name = "USER_ACT", nullable = false)
	private String user;
	
	public UdisDto(UdisPK udisPK){
		bin        	= udisPK.getBin();
		product    	= udisPK.getProduct();	
		udi 		= 0;
		trandate 	= GeneralUtil.getCurrentSqlDate();
		trantime 	= GeneralUtil.getCurrentTime("HHmmss");
		user 		= "ws_user";
	}
	
	
	public UdisDto() {
		bin 		= null;
		product 	= null;
		udi 		= 0;
		trandate 	= GeneralUtil.getCurrentSqlDate();
		trantime 	= GeneralUtil.getCurrentTime("HHmmss");
		user 		= "ws_user";
	}
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public double getUdi() {
		return udi;
	}
	public void setUdi(double udi) {
		this.udi = udi;
	}
	public Date getTrandate() {
		return trandate;
	}
	public void setTrandate(Date trandate) {
		this.trandate = trandate;
	}
	public String getTrantime() {
		return trantime;
	}
	public void setTrantime(String trantime) {
		this.trantime = trantime;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}

}

@Embeddable
class UdisPK implements Serializable{

	private static final long serialVersionUID = 7326130925157166033L;
	
	public UdisPK(String bin, String product){
		this.bin = bin;
		this.product = product;
	}
	
	public UdisPK(){}
	
		
	@Column(name = "ID_NIVEL_CAPT", nullable = false)
	private String   bin;
	
	@Column(name = "NIVEL_CAPT", length = 120, nullable = false)
	private String   product;

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
