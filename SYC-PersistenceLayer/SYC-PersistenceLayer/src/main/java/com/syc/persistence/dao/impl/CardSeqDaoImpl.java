package com.syc.persistence.dao.impl;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.CardSeqDao;
import com.syc.persistence.dto.CardSeqDto;


@SuppressWarnings("unchecked")
@Repository("cardSeqDao") 
public class CardSeqDaoImpl extends AbstractPersistenceDaoImpl<CardSeqDto, String> implements CardSeqDao {

	@Autowired
	public CardSeqDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}
	
	
	public long getMaxSeq(String bin,String product) {
		StringBuilder seq = new StringBuilder().append("SEQ_").append(bin).append(product); 
		StringBuilder query = new StringBuilder().append("SELECT ").append(seq).append(".NEXTVAL as id from dual");		
						
		return (Long) getSession().createSQLQuery(query.toString()).addScalar("id", Hibernate.LONG).uniqueResult();		
	}

	public long getMaxSeqCta(String bin) {
		StringBuilder seq = new StringBuilder().append("SEQ_CTA_").append(bin); 
		StringBuilder query = new StringBuilder().append("SELECT ").append(seq).append(".NEXTVAL as id from dual");		
						
		return (Long) getSession().createSQLQuery(query.toString()).addScalar("id", Hibernate.LONG).uniqueResult();		
	}
}