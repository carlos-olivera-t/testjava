package com.syc.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.PhoneCodeDao;
import com.syc.persistence.dto.PhoneCodeDto;

@SuppressWarnings("unchecked")
@Repository("phoneCodeDao") 
public class PhoneCodeDaoImpl extends AbstractPersistenceDaoImpl<PhoneCodeDto, String> implements PhoneCodeDao{

	@Autowired
	public PhoneCodeDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}
	
}