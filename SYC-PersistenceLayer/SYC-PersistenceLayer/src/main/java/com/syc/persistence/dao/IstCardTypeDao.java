package com.syc.persistence.dao;

import java.util.List;

import com.syc.persistence.dto.IstCardTypeDto;

public interface IstCardTypeDao extends PersistenceDao<IstCardTypeDto, String>{

	List<IstCardTypeDto> findByBin(String bin);
	
	
	IstCardTypeDto findPrefixByBin(String bin, String cardType);

	
}
