package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.syc.persistence.dao.impl.GeneralUtil;

@Entity
@Table(name="PHONE_CODE")
public class PhoneCodeDto  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ACCOUNT")		
	private String account;
	@Column(name="CREATION_DATE")
	private Date   creationDate;
	@Column(name = "CREATION_TIME")
	private int    creationTime;
	@Column(name = "MODIFIED_DATE")
	private Date   modifiedDate;
	@Column(name = "MODIFIED_TIME")
	private int    modifiedTime;
	@Column(name = "NIP")
	private String nip;
	@Column(name = "OLD_NIP_1")
	private String oldNip1;
	@Column(name = "OLD_NIP_2")
	private String oldNip2;
	@Column(name = "OLD_NIP_3")
	private String oldNip3;
	@Column(name = "OLD_NIP_4")
	private String oldNip4;
	@Column(name = "CREATING_USER")
	private String creatingUser;
	@Column(name = "STATUS")
	private String status;
		
	public PhoneCodeDto(){
		
	}
	
	public PhoneCodeDto(String account, String nip, String user){
		this.account 		= account;		
		this.creationDate	= GeneralUtil.getCurrentSqlDate();
		this.creationTime   = new Integer(  GeneralUtil.getCurrentTime("HHmmss") );
		this.modifiedDate   = GeneralUtil.getCurrentSqlDate();
		this.modifiedTime   = new Integer(  GeneralUtil.getCurrentTime("HHmmss") );
		this.nip     		= nip;
		this.oldNip1        = null;
		this.oldNip2        = null;
		this.oldNip3        = null;
		this.creatingUser   = user;
		this.status         = "A";
	}
	
	/**
	 * @return the account
	 */
	
	public String getAccount() {
		return account;
	}
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}	
	/**
	 * @return the creationDate
	 */
	
	public Date getCreationDate() {
		return creationDate;
	}
	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}	
	/**
	 * @return the creationTime
	 */
	
	public int getCreationTime() {
		return creationTime;
	}
	/**
	 * @param creationTime the creationTime to set
	 */
	public void setCreationTime(int creationTime) {
		this.creationTime = creationTime;
	}
	/**
	 * @return the modifiedDate
	 */		
	
	public Date getModifiedDate() {
		return modifiedDate;
	}
	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	/**
	 * @return the modifiedTime
	 */	
	
	public int getModifiedTime() {
		return modifiedTime;
	}
	/**
	 * @param modifiedTime the modifiedTime to set
	 */
	public void setModifiedTime(int modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	/**
	 * @return the nip
	 */	 
	
	public String getNip() {
		return nip;
	}
	/**
	 * @param nip the nip to set
	 */
	public void setNip(String nip) {
		this.nip = nip;
	}
	/**
	 * @return the oldNip1
	 */			
	
	public String getOldNip1() {
		return oldNip1;
	}
	/**
	 * @param oldNip1 the oldNip1 to set
	 */
	public void setOldNip1(String oldNip1) {
		this.oldNip1 = oldNip1;
	}
	/**
	 * @return the oldNip2
	 */
	
	public String getOldNip2() {
		return oldNip2;
	}
	/**
	 * @param oldNip2 the oldNip2 to set
	 */
	public void setOldNip2(String oldNip2) {
		this.oldNip2 = oldNip2;
	}
	/**
	 * @return the oldNip3
	 */
	public String getOldNip3() {
		return oldNip3;
	}
	/**
	 * @param oldNip3 the oldNip3 to set
	 */
	public void setOldNip3(String oldNip3) {
		this.oldNip3 = oldNip3;
	}
	
	
	public String getOldNip4() {
		return oldNip4;
	}

	public void setOldNip4(String oldNip4) {
		this.oldNip4 = oldNip4;
	}

	public String getCreatingUser() {
		return creatingUser;
	}
	/**
	 * @param creatingUser the creatingUser to set
	 */
	public void setCreatingUser(String creatingUser) {
		this.creatingUser = creatingUser;
	}

	/**
	 * @return the status
	 */
	
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PhoneCodeDto [account=");
		builder.append(account);
		builder.append(", creationDate=");
		builder.append(creationDate);
		builder.append(", creationTime=");
		builder.append(creationTime);
		builder.append(", modifiedDate=");
		builder.append(modifiedDate);
		builder.append(", modifiedTime=");
		builder.append(modifiedTime);
		builder.append(", nip=");
		builder.append(nip);
		builder.append(", oldNip1=");
		builder.append(oldNip1);
		builder.append(", oldNip2=");
		builder.append(oldNip2);
		builder.append(", oldNip3=");
		builder.append(oldNip3);
		builder.append(", creatingUser=");
		builder.append(creatingUser);
		builder.append(", status=");
		builder.append(status);
		builder.append("]");
		return builder.toString();
	}
	
}