package com.syc.persistence.dao.impl;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.UdisDao;
import com.syc.persistence.dto.EmployerDto;
import com.syc.persistence.dto.UdisDto;

@SuppressWarnings("unchecked")
@Repository
public class UdisDaoImpl extends AbstractPersistenceDaoImpl<UdisDto, Long> implements UdisDao{

	@Autowired
	public UdisDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}
	@Override
	public Date findLastDayUdi(String bin, String product) {
		return (Date) getSession().getNamedQuery("udis.findlastDay")
				 .setParameter("bin", bin)
				 .setParameter("product", product)
				 .uniqueResult(); 	
	}

	@Override
	public String findLastHourUdi(String bin, String product, Date lastdate) {
		return (String) getSession().getNamedQuery("udis.findlastHour")
				 .setParameter("bin", bin)
				 .setParameter("product", product)
				 .setParameter("trandate", lastdate)
				 .uniqueResult(); 
	}

	@Override
	public UdisDto findLastUdi(String bin, String product, Date lastDate, String lastTime) {
		
		return (UdisDto) getSession().getNamedQuery("udis.findlastUdi")
				 .setParameter("bin", bin)
				 .setParameter("product", product)
				 .setParameter("trandate", lastDate)
				  .setParameter("lasttime", lastTime)
				 .uniqueResult(); 
	}

	

}
