package com.syc.persistence.dao;

import java.sql.Date;
import java.util.List;

import com.syc.persistence.dto.AclaracionDto;

public interface AclaracionesDao extends PersistenceDao<AclaracionDto, Long>{
	
	/*************************************************	
	 * Servicio que devuelve una aclaracion 
	 *         a partir de un folio
	 * @param folio
	 * @return AclaracionDto
	 *************************************************/
	public AclaracionDto findByFolio( long folio );
	
	/*************************************************	
	 * Servicio que devuelve un listado de aclaracioes 
	 *         asociados a una tarjeta
	 * @param pan
	 * @return List<AclaracionDto>
	 *************************************************/
	public List<AclaracionDto> findByPan( String pan );
	
	/*************************************************	
	 * Servicio que devuelve una aclaracion asociados 
	 *         a una llave en SHCLog
	 * @param pan
	 * @return List<AclaracionDto>
	 *************************************************/
	public AclaracionDto findBySHCKey( String pan, String autorizacion, Date fechaTrx );

}
