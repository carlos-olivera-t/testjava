package com.syc.persistence.dto;


import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_EMPLEADOS")
@NamedQueries({
	@NamedQuery(name="tblemp.findEmployee", query=" FROM TblEmployeeDto AS emp WHERE  emp.pan = CAST(:pan  as char) AND emp.issCode =:issCode AND emp.status =:status AND emp.cardType =:cardType AND emp.replacementDate IS NULL")
})
public class TblEmployeeDto  implements Serializable{

	private static final long serialVersionUID = 1L;

		@Column(name = "ISS_CODE")
		private int issCode;
		
		@Column(name = "EMPR_LOC_CODE")
		private String companyCode;
		
		@Column(name = "EMPR_LOC_CODE2")
		private String companyCode2;
		
		@Column(name = "TI_TYPE")
		private String tiType;
		
		@Column(name = "CARD_TYPE")
		private String    cardType;
		
		@Column(name = "EMPLOYEE_CODE")
		private String    codeEmployee;
		
		@Column(name = "NAME")
		private String name;
		
		@Column(name = "ACCOUNT")
		private String account;
		
		@Id
		@Column(name = "CARD")
		private String pan;
		
		@Column(name = "EXP_DATE")
		private Date expDate;
		
		@Column(name = "STATUS")
		private int status;
		
		@Column(name = "FEC_ALTA")
		private Date   assignmentDate;
		
		@Column(name = "FEC_REEMP")
		private Date   replacementDate;
		
		@Column(name = "STATUS_MAQ")
		private int    statusMaq;
		
		@Column(name = "FEC_MAQUILA")
		private Date  maqDate;
		
		@Column(name = "PERSONALIZADA")
		private String  accountType;
		
		@Column(name = "NOM_CORTO_EMPRESA")
		private String  shortNameComp;
		
		public String getCompanyCode2() {
			return companyCode2;
		}

		public void setCompanyCode2(String companyCode2) {
			this.companyCode2 = companyCode2;
		}

		public int getIssCode() {
			return issCode;
		}

		public void setIssCode(int issCode) {
			this.issCode = issCode;
		}

		public String getCompanyCode() {
			return companyCode;
		}

		public void setCompanyCode(String companyCode) {
			this.companyCode = companyCode;
		}

		public String getTiType() {
			return tiType;
		}

		public void setTiType(String tiType) {
			this.tiType = tiType;
		}

		public String getCardType() {
			return cardType;
		}

		public void setCardType(String cardType) {
			this.cardType = cardType;
		}

		public String getCodeEmployee() {
			return codeEmployee;
		}

		public void setCodeEmployee(String codeEmployee) {
			this.codeEmployee = codeEmployee;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAccount() {
			return account;
		}

		public void setAccount(String account) {
			this.account = account;
		}

		public String getPan() {
			return pan;
		}

		public void setPan(String pan) {
			this.pan = pan;
		}

		public Date getExpDate() {
			return expDate;
		}

		public void setExpDate(Date expDate) {
			this.expDate = expDate;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public Date getAssignmentDate() {
			return assignmentDate;
		}

		public void setAssignmentDate(Date assignmentDate) {
			this.assignmentDate = assignmentDate;
		}

		public Date getReplacementDate() {
			return replacementDate;
		}

		public void setReplacementDate(Date replacementDate) {
			this.replacementDate = replacementDate;
		}

		public int getStatusMaq() {
			return statusMaq;
		}

		public void setStatusMaq(int statusMaq) {
			this.statusMaq = statusMaq;
		}

		public Date getMaqDate() {
			return maqDate;
		}

		public void setMaqDate(Date maqDate) {
			this.maqDate = maqDate;
		}

		public String getAccountType() {
			return accountType;
		}

		public void setAccountType(String accountType) {
			this.accountType = accountType;
		}

	
		public String getShortNameComp() {
			return shortNameComp;
		}

		public void setShortNameComp(String shortNameComp) {
			this.shortNameComp = shortNameComp;
		}

		@Override
		public String toString() {
			return "TblEmployeeDto [issCode=" + issCode + ", companyCode=" + companyCode + ", companyCode2="
					+ companyCode2 + ", tiType=" + tiType + ", cardType=" + cardType + ", codeEmployee=" + codeEmployee
					+ ", name=" + name + ", account=" + account + ", pan=" + pan + ", expDate=" + expDate + ", status="
					+ status + ", assignmentDate=" + assignmentDate + ", replacementDate=" + replacementDate
					+ ", statusMaq=" + statusMaq + ", maqDate=" + maqDate + ", accountType=" + accountType +
					", shortNameComp=" + shortNameComp + "]";
		}

		
}
