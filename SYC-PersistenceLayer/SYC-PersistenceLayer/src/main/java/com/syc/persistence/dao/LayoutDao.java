package com.syc.persistence.dao;

import java.util.List;

import com.syc.persistence.dto.LayoutDto;

public interface LayoutDao extends PersistenceDao<LayoutDto, Long>{
	
	/**
	 * Obtiene un Layout en base al ID del Embosador
	 * @author Angel Contreras
	 * @param  idEmbosser
	 * @return List<LayoutDAO>
	 */
	public List<LayoutDto> getLayoutByIdEmbosser( int idEmbosser );
}
