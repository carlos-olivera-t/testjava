package com.syc.persistence.dao;


import com.syc.persistence.dto.TblEmployeeDto;



public interface TblEmployeeDao extends PersistenceDao<TblEmployeeDto, String>{

	/**
	 * SErvicio para buscar un empleado
	 * @param pan
	 * @param issCode
	 * @param status
	 * @param cardType
	 * @return
	 */
	public TblEmployeeDto findEmployee(String pan, int issCode, int status, String cardType);
}
