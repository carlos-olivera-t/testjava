package com.syc.persistence.dao.impl;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.EmployeeDao;
import com.syc.persistence.dto.EmployeeDto;

@SuppressWarnings("unchecked")
@Repository("employeeDao")
public class EmployeeDaoImpl extends AbstractPersistenceDaoImpl<EmployeeDto, Long> implements EmployeeDao{
	
	@Autowired
	public EmployeeDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	private static final transient Log log = LogFactory.getLog(EmployeeDaoImpl.class);
	
	public EmployeeDto findEmployeeByAccount( String account ){
		String QUERY = 
						"SELECT * "+
						  "FROM ( "+
						         "SELECT * "+
						           "FROM TBL_SVL_EMPLEADOS "+
						          "WHERE ACCOUNT = '"+account+"' "+
						            "AND STATUS = 0 "+
						            "AND CARD_TYPE = 0 "+
						            "AND FEC_REEMP IS NULL "+
						        ") "+
						  "WHERE ROWNUM <= 1";

		EmployeeDto employee = (EmployeeDto) getSession().createSQLQuery(QUERY).addEntity("EmployeeDto",EmployeeDto.class ).uniqueResult(); 
		
		log.debug("Buscando Empleado con Cuenta["+account+"] :: ["+ReflectionToStringBuilder.toString(employee)+"]");
	
		return employee;
	}
	
	public EmployeeDto findEmployeeByAccount( String account, int emisor ){

		String QUERY = 
						"SELECT * "+
						  "FROM ( "+
						         "SELECT * "+
						           "FROM TBL_EMPLEADOS "+
						          "WHERE ACCOUNT = '"+account+"' "+
						            "AND STATUS = 0 "+
						            "AND CARD_TYPE = 0 "+
						            "AND FEC_REEMP IS NULL "+
						            "AND ISS_CODE = '"+emisor+"' "+
						        ") "+
						  "WHERE ROWNUM <= 1";

		EmployeeDto employee = (EmployeeDto) getSession().createSQLQuery(QUERY).addEntity("EmployeeDto",EmployeeDto.class ).uniqueResult(); 
		
		log.debug("Buscando Empleado con Cuenta["+account+"] :: ["+ReflectionToStringBuilder.toString(employee)+"]");
	
		return employee;

}
}
