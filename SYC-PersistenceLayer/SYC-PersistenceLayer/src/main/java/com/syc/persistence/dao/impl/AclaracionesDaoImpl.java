package com.syc.persistence.dao.impl;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.AclaracionesDao;
import com.syc.persistence.dto.AclaracionDto;

@SuppressWarnings("unchecked")
@Repository("aclaracionesDao")
public class AclaracionesDaoImpl extends AbstractPersistenceDaoImpl<AclaracionDto, Long> implements AclaracionesDao{
	
	@Autowired
	public AclaracionesDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	@Override
	public AclaracionDto findByFolio(long folio) {
		return (AclaracionDto)getSession().getNamedQuery("aclaracion.findByFolio")
			.setParameter("folio", folio)
			.uniqueResult();
	}

	@Override
	public List<AclaracionDto> findByPan(String pan) {
		return (List<AclaracionDto>) getSession().getNamedQuery("aclaracion.findByPan")
				.setParameter("pan", pan)
				.list();
	}

	@Override
	public AclaracionDto findBySHCKey(String pan, String autorizacion, Date fechaTrx) {
		return (AclaracionDto)getSession().getNamedQuery("aclaracion.findBySHCKey")
				.setParameter("pan", pan)
				.setParameter("autorizacion", autorizacion)
				.setParameter("fecha", fechaTrx)
				.uniqueResult();
	}
	
	

}
