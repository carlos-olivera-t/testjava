package com.syc.persistence.dao;



import java.util.List;

import com.syc.persistence.dto.UserBinDto;


public interface UserBinDao extends PersistenceDao<UserBinDto, Long>{
	
	/**
	 * Extrae la lista de bines asociados a un usuario
	 * @param userName
	 * @return
	 */
	List<String> getBinesByUser(String userName);

}
