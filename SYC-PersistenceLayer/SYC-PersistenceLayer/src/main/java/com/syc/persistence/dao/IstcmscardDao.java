package com.syc.persistence.dao;

import java.util.List;

import com.syc.persistence.dto.IstcmscardDto;

public interface IstcmscardDao extends PersistenceDao<IstcmscardDto, Long>{
	
	
	public int sqlUpdate( IstcmscardDto istcmscard );
	/**
	 * Realiza una busqueda por el nombre del tarjeta-habiente
	 * @param name
	 * @param bin
	 * @return
	 */
	public List<IstcmscardDto> getInformationByName(String name, String bin);
	/**
	 * Servicio que realiza una busqueda por tarjeta
	 * @param pan
	 
	 * @return
	 */
	public IstcmscardDto getIstcmscardInformation(String pan);
}
