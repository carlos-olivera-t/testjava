package com.syc.persistence.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.UserBinDao;
import com.syc.persistence.dto.UserBinDto;

@Repository("userBinDao")
@SuppressWarnings("unchecked")
public class UserBinDaoImpl  extends AbstractPersistenceDaoImpl<UserBinDto, Long> implements UserBinDao {
		
	private static final String qryBinesByUser = "SELECT b.bin FROM UserDto u, UserBinDto b WHERE u.id = b.userid and u.userName = ?";
	
	@Autowired
	public UserBinDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);				
	}

	
	public List<String> getBinesByUser(String userName) {
		return getSession().getNamedQuery("userBin.findByUsername").setString("user", userName).list();				
	}
	
	
}
