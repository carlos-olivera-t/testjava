package com.syc.persistence.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.TransactionDao;
import com.syc.persistence.dto.mapper.TransactionDto;
import com.syc.persistence.dto.mapper.TransactionModDto;

@Repository("transactionDao")
public class TransactionDaoImpl implements TransactionDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate; 
	
	
	/*******************************************************************
	 *  Obtiene un listado de las ultimas Transacciones
	 *******************************************************************/
	
	/**
	 * PCODES
	 * 210000, 201000      ==> TRANSFERENCIAS
	 * 1000, 2000, 3000    ==> COMPRAS
	 * 11000, 12000, 13000 ==> DISPOSICIONES
	 */
	
	public List<TransactionDto> getTransactionsList( String pan, int numMaxOfRows, int operationType, boolean isapproved, int codigoEmisor ){
		String operations;
		switch( operationType ){
		case( 1 ): 
			/** Cargos **/
			operations = "1";
		break;
		case( 2 ): 
			/** Abonos **/
			operations = "2,3";
		break;
		
		default:
			/** Ambas **/
			operations = "1, 2,3";
		}
		
		String QUERY = null;
				
		if(isapproved){

			 QUERY =
					"SELECT * "+  
					  "FROM ( "+  
					         "SELECT PAN, "+                                 
					                "CAT_PCODES.DESC_EDO_CTA, "+
					                "RESPCODE, "+
					                "CAT_CODIGOS_RECHAZO.DESCRIPTION AS RESPONSE_CODE_DESC, "+
					                "AMOUNT, "+ 
					                "NVL(AUTHNUM,'N/D') AS AUTHNUM, "+                   
					                "SHCLOG.PCODE, "+
					                "SHCLOG.REFNUM, "+
					                "SHCLOG.AVAL_BALANCE, "+
					                "NVL(DESC_PCODE,'N/D') AS PCODE_DESC, "+
					                "NVL(ACCEPTORNAME,'N/D') AS ACCEPTORNAME, "+            
					                "TO_CHAR(TRANDATE, 'DD/MM/YYYY') || ' '|| "+    
					                "SUBSTR(  LPAD(TRANTIME, 6, '0'),1,2) ||':'|| "+        
					                "SUBSTR(  LPAD(TRANTIME, 6, '0'),3,2) ||':'|| "+   
					                "SUBSTR(  LPAD(TRANTIME, 6, '0'),5,2) AS TRANSACTIONDATE "+             
					           "FROM SHCLOG, "+
					                "CAT_PCODES, "+
					                "CAT_CODIGOS_RECHAZO "+
					          "WHERE PAN = '"+pan+"' "+
					            "AND CVE_EMISOR ="+ codigoEmisor +    
//					            "AND AUTHNUM IS NOT NULL "+           
					            "AND MSGTYPE NOT IN('240', '6210','6410','230') "+
					            "AND SHCLOG.SHCERROR = 0 "+
					            "AND SHCLOG.PCODE = CAT_PCODES.PCODE (+) "+
					            "AND SHCLOG.RESPCODE = CAT_CODIGOS_RECHAZO.CODE (+) "+
					            "AND CAT_PCODES.CONCEPTO IN ( "+operations+" ) "+
					            "AND SHCLOG.RESPCODE = 0"+
					          "ORDER BY TRANDATE DESC, TRANTIME DESC "+  
					      ") "+  
					 "WHERE ROWNUM <= " + numMaxOfRows;
			
		}
		else{
			QUERY =
					"SELECT * "+  
					  "FROM ( "+  
					         "SELECT PAN, "+                                 
					                "CAT_PCODES.DESC_EDO_CTA, "+
					                "RESPCODE, "+
					                "CAT_CODIGOS_RECHAZO.DESCRIPTION AS RESPONSE_CODE_DESC, "+
					                "AMOUNT, "+ 
					                "NVL(AUTHNUM,'N/D') AS AUTHNUM, "+                   
					                "SHCLOG.PCODE, "+
					                "SHCLOG.REFNUM, "+
					                "SHCLOG.AVAL_BALANCE, "+
					                "NVL(DESC_PCODE,'N/D') AS PCODE_DESC, "+
					                "NVL(ACCEPTORNAME,'N/D') AS ACCEPTORNAME, "+            
					                "TO_CHAR(TRANDATE, 'DD/MM/YYYY') || ' '|| "+    
					                "SUBSTR(  LPAD(TRANTIME, 6, '0'),1,2) ||':'|| "+        
					                "SUBSTR(  LPAD(TRANTIME, 6, '0'),3,2) ||':'|| "+   
					                "SUBSTR(  LPAD(TRANTIME, 6, '0'),5,2) AS TRANSACTIONDATE "+             
					           "FROM SHCLOG, "+
					                "CAT_PCODES, "+
					                "CAT_CODIGOS_RECHAZO "+
					          "WHERE PAN = '"+pan+"' "+
					          	"AND CVE_EMISOR ="+ codigoEmisor +     
//					            "AND AUTHNUM IS NOT NULL "+           
					            "AND MSGTYPE NOT IN('240', '6210','6410','230') "+
					            "AND SHCLOG.SHCERROR = 0 "+
					            "AND SHCLOG.PCODE = CAT_PCODES.PCODE (+) "+
					            "AND SHCLOG.RESPCODE = CAT_CODIGOS_RECHAZO.CODE (+) "+
					            "AND CAT_PCODES.CONCEPTO IN ( "+operations+" ) "+
					          "ORDER BY TRANDATE DESC, TRANTIME DESC "+  
					      ") "+  
					 "WHERE ROWNUM <= " + numMaxOfRows;
			
		}
		return jdbcTemplate.query(QUERY, new TransactionMapper()); 
		
	}
	
	public List<TransactionDto> getTransactionsPeriodList( String pan, int period, int operationType, int codigoEmisor ){
		String operations;
		switch( operationType ){
		case( 1 ): 
			/** Cargos **/
			operations = "1";
		break;
		case( 2 ): 
			/** Abonos **/
			operations = "2,3";
		break;
		
		default:
			/** Ambas **/
			operations = "1, 2,3";
		}

			TreeMap<String,Date> map = GeneralUtil.getPeriodTime(period, "dd/MM/yyyy");
			
			Date fechaInicial = map.get("FechaInicial");
			Date fechaFinal = map.get("FechaFinal");
			
			String QUERY =
  
				         "SELECT PAN, "+                                 
				                "CAT_PCODES.DESC_EDO_CTA, "+
				                "RESPCODE, "+
				                "CAT_CODIGOS_RECHAZO.DESCRIPTION AS RESPONSE_CODE_DESC, "+
				                "AMOUNT, "+ 
				                "NVL(AUTHNUM,'N/D') AS AUTHNUM, "+                   
				                "SHCLOG.PCODE, "+
				                "SHCLOG.REFNUM, "+
				                "SHCLOG.AVAL_BALANCE, "+
				                "NVL(DESC_PCODE,'N/D') AS PCODE_DESC, "+
				                "NVL(ACCEPTORNAME,'N/D') AS ACCEPTORNAME, "+            
				                "TO_CHAR(TRANDATE, 'DD/MM/YYYY') || ' '|| "+    
				                "SUBSTR(  LPAD(TRANTIME, 6, '0'),1,2) ||':'|| "+        
				                "SUBSTR(  LPAD(TRANTIME, 6, '0'),3,2) ||':'|| "+   
				                "SUBSTR(  LPAD(TRANTIME, 6, '0'),5,2) AS TRANSACTIONDATE "+             
				           "FROM SHCLOG, "+
				                "CAT_PCODES, "+
				                "CAT_CODIGOS_RECHAZO "+
				          "WHERE PAN = '"+pan+"' "+
				          	"AND CVE_EMISOR ="+ codigoEmisor +     
//					        "AND AUTHNUM IS NOT NULL "+           
				            "AND MSGTYPE NOT IN('240', '6210','6410','230') "+
				            "AND SHCLOG.SHCERROR = 0 "+
				            "AND SHCLOG.PCODE = CAT_PCODES.PCODE (+) "+
				            "AND SHCLOG.RESPCODE = CAT_CODIGOS_RECHAZO.CODE (+) "+
				            "AND SHCLOG.TRANDATE BETWEEN TO_DATE('"+fechaInicial+"' , 'YYYY-MM-DD') AND TO_DATE('"+fechaFinal+"' , 'YYYY-MM-DD')"+
				            "AND CAT_PCODES.CONCEPTO IN ( "+operations+" ) "+
				          "ORDER BY TRANDATE DESC, TRANTIME DESC";
			
			//Object[] params = new Object[]{ pan };
			
		return jdbcTemplate.query(QUERY, new TransactionMapper()); 
		
	}

	@Override
	public List<TransactionModDto> getTransactionsDateList(String pan, Date initialDate, Date finalDate , int operationType, int codigoEmisor) {
		String operations;
		switch( operationType ){
		case( 1 ): 
			/** Cargos **/
			operations = "1";
		break;
		case( 2 ): 
			/** Abonos **/
			operations = "2,3";
		break;
		
		default:
			/** Ambas **/
			operations = "1, 2,3";
		}
			String QUERY =
  
				         "SELECT PAN, "+                                 
				                "CAT_PCODES.DESC_EDO_CTA, "+
				                "RESPCODE, "+
				                "CAT_CODIGOS_RECHAZO.DESCRIPTION AS RESPONSE_CODE_DESC, "+
				                "AMOUNT, "+ 
				                "NVL(AUTHNUM,'N/D') AS AUTHNUM, "+                   
				                "SHCLOG.PCODE, "+
				                "SHCLOG.CASH_BACK, "+
				                "SHCLOG.FEE, "+
				                "SHCLOG.MERCHANT_TYPE, "+
				                "SHCLOG.POS_ENTRY_CODE, "+
				                "SHCLOG.SHC_DATA_BUFFER, "+
				                "SHCLOG.REFNUM, "+
				                "SHCLOG.AVAL_BALANCE, "+
				                "NVL(DESC_PCODE,'N/D') AS PCODE_DESC, "+
				                "NVL(ACCEPTORNAME,'N/D') AS ACCEPTORNAME, "+            
				                "TO_CHAR(TRANDATE, 'DD/MM/YYYY') || ' '|| "+    
				                "SUBSTR(  LPAD(TRANTIME, 6, '0'),1,2) ||':'|| "+        
				                "SUBSTR(  LPAD(TRANTIME, 6, '0'),3,2) ||':'|| "+   
				                "SUBSTR(  LPAD(TRANTIME, 6, '0'),5,2) AS TRANSACTIONDATE "+             
				           "FROM SHCLOG, "+
				                "CAT_PCODES, "+
				                "CAT_CODIGOS_RECHAZO "+
				          "WHERE PAN = '"+pan+"' "+
				          	"AND CVE_EMISOR ="+ codigoEmisor +     
//					        "AND AUTHNUM IS NOT NULL "+           
				            "AND MSGTYPE NOT IN('240', '6210','6410','230') "+
				            "AND SHCLOG.SHCERROR = 0 "+
				            "AND SHCLOG.PCODE = CAT_PCODES.PCODE (+) "+
				            "AND SHCLOG.RESPCODE = CAT_CODIGOS_RECHAZO.CODE (+) "+
				            "AND SHCLOG.TRANDATE BETWEEN TO_DATE('"+initialDate+"' , 'YYYY-MM-DD') AND TO_DATE('"+finalDate+"' , 'YYYY-MM-DD')"+
				            "AND CAT_PCODES.CONCEPTO IN ( "+operations+" ) "+
				          "ORDER BY TRANDATE DESC, TRANTIME DESC";
			
		return jdbcTemplate.query(QUERY, new TransactionModMapper()); 
	}
	
	@Override
	public List<TransactionDto> getTransactionsDateList(String pan, Date initialDate, Date finalDate , int operationType, int numMaxOfRows, int codigoEmisor) {
		String operations;
		switch( operationType ){
		case( 1 ): 
			/** Cargos **/
			operations = "1";
		break;
		case( 2 ): 
			/** Abonos **/
			operations = "2,3";
		break;
		
		default:
			/** Ambas **/
			operations = "1, 2,3";
		}
			String QUERY =
  
					"SELECT * "+  
							  "FROM ( "+  
							         "SELECT PAN, "+                                 
							                "CAT_PCODES.DESC_EDO_CTA, "+
							                "RESPCODE, "+
							                "CAT_CODIGOS_RECHAZO.DESCRIPTION AS RESPONSE_CODE_DESC, "+
							                "AMOUNT, "+ 
							                "NVL(AUTHNUM,'N/D') AS AUTHNUM, "+                   
							                "SHCLOG.PCODE, "+
							                "SHCLOG.REFNUM, "+
							                "SHCLOG.AVAL_BALANCE, "+
							                "NVL(DESC_PCODE,'N/D') AS PCODE_DESC, "+
							                "NVL(ACCEPTORNAME,'N/D') AS ACCEPTORNAME, "+            
							                "TO_CHAR(TRANDATE, 'DD/MM/YYYY') || ' '|| "+    
							                "SUBSTR(  LPAD(TRANTIME, 6, '0'),1,2) ||':'|| "+        
							                "SUBSTR(  LPAD(TRANTIME, 6, '0'),3,2) ||':'|| "+   
							                "SUBSTR(  LPAD(TRANTIME, 6, '0'),5,2) AS TRANSACTIONDATE "+             
							           "FROM SHCLOG, "+
							                "CAT_PCODES, "+
							                "CAT_CODIGOS_RECHAZO "+
							          "WHERE PAN = '"+pan+"' "+
							          	"AND CVE_EMISOR ="+ codigoEmisor +     
//							            "AND AUTHNUM IS NOT NULL "+           
							            "AND MSGTYPE NOT IN('240', '6210','6410','230') "+
							            "AND SHCLOG.SHCERROR = 0 "+
							            "AND SHCLOG.PCODE = CAT_PCODES.PCODE (+) "+
							            "AND SHCLOG.RESPCODE = CAT_CODIGOS_RECHAZO.CODE (+) "+
							            "AND SHCLOG.TRANDATE BETWEEN TO_DATE('"+initialDate+"' , 'YYYY-MM-DD') AND TO_DATE('"+finalDate+"' , 'YYYY-MM-DD')"+
							            "AND CAT_PCODES.CONCEPTO IN ( "+operations+" ) "+
							            "AND SHCLOG.RESPCODE = 0"+
							          "ORDER BY TRANDATE DESC, TRANTIME DESC "+  
							      ") "+  
							 "WHERE ROWNUM <= " + numMaxOfRows;

		return jdbcTemplate.query(QUERY, new TransactionMapper()); 
	}

}

final class TransactionMapper implements RowMapper<TransactionDto>{
	
	public TransactionDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		TransactionDto transaction = new TransactionDto();
		transaction.setPan                ( rs.getString("PAN")               );
		transaction.setConceptDescription ( rs.getString("DESC_EDO_CTA")     );
		transaction.setRespCode           ( rs.getInt   ("RESPCODE")          );
		transaction.setRespCodeDescription( rs.getString("RESPONSE_CODE_DESC"));
		transaction.setAmount             ( rs.getDouble("AMOUNT")            );
		transaction.setAuthorization      ( rs.getString("AUTHNUM")           );
		transaction.setTrxCode            ( rs.getString("PCODE")             );
		transaction.setTrxDescription     ( rs.getString("PCODE_DESC")        );
		transaction.setAcceptorName       ( rs.getString("ACCEPTORNAME")      );
		transaction.setTransactionDate    ( rs.getString("TRANSACTIONDATE")   );
		transaction.setReference          ( rs.getString("REFNUM")            );
		transaction.setAvailableBalance   ( rs.getDouble("AVAL_BALANCE")      );
		return transaction;
	}
}

final class TransactionModMapper implements RowMapper<TransactionModDto>{
	
	public TransactionModDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		TransactionModDto transaction = new TransactionModDto();
		transaction.setPan                ( rs.getString("PAN")               );
		transaction.setConceptDescription ( rs.getString("DESC_EDO_CTA")     );
		transaction.setRespCode           ( rs.getInt   ("RESPCODE")          );
		transaction.setRespCodeDescription( rs.getString("RESPONSE_CODE_DESC"));
		transaction.setAmount             ( rs.getDouble("AMOUNT")            );
		transaction.setAuthorization      ( rs.getString("AUTHNUM")           );
		transaction.setTrxCode            ( rs.getString("PCODE")             );
		transaction.setTrxDescription     ( rs.getString("PCODE_DESC")        );
		transaction.setAcceptorName       ( rs.getString("ACCEPTORNAME")      );
		transaction.setTransactionDate    ( rs.getString("TRANSACTIONDATE")   );
		transaction.setReference          ( rs.getString("REFNUM")            );
		transaction.setAvailableBalance   ( rs.getDouble("AVAL_BALANCE")      );
		transaction.setCash_back		  ( rs.getDouble("CASH_BACK")     	  );
		transaction.setFee				  ( rs.getDouble("FEE")		     	  );
		transaction.setMerchantType		  ( rs.getInt("MERCHANT_TYPE")		  );
		transaction.setPosEntryCode		  ( rs.getInt("POS_ENTRY_CODE")		  );
		transaction.setShcDataBuffer	  ( rs.getString("SHC_DATA_BUFFER")	  );
		
		return transaction;
	}
}




