package com.syc.persistence.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ISTCMSCARD")
public class EmbosserPropertiesDto {

	@Id
	@Column(name = "PAN")
	private String cardNumber;

	@Column(name = "EXPIRYDATE")
	private Date expiryDate;

	@Column(name = "NEWEXPIRYDATE")
	private Date newExpiryDate;

	@Column(name = "EMBOSSNAME_1", length = 25)
	private String shortName;

	@Column(name = "EMBOSSNAME_2", length = 25)
	private String name2;

	@Column(name = "MAGSTRIPNAME", length = 25)
	private String magstripname;

	@Column(name = "CARDINDICATOR", length = 1)
	private String cardIndicator;

	@Column(name = "ISSUEDATE")
	private Date issueDate;

	@Column(name = "CLERK")
	private int clerk;

	@Column(name = "SUPERVISOR")
	private int supervisor;

	@Column(name = "EMERGENCY_REISSUE", length = 1)
	private String emergencyReissue;

	@Column(name = "ISSUANCEFEE", length = 1)
	private String issuancefee;

	@Column(name = "PRODUCTION_IND", length = 1)
	private String productionInd;

	public EmbosserPropertiesDto(){
		
	}
	
	/**
	 * Constructor para crear un registro nuevo
	 */
	public EmbosserPropertiesDto(boolean insertRow) {
		this.name2 = " ";
		this.magstripname = " ";
		this.clerk = 232;
		this.supervisor = 0;
		this.emergencyReissue = "0";
		this.issuancefee = "0";
		this.productionInd = "1";

	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getNewExpiryDate() {
		return newExpiryDate;
	}

	public void setNewExpiryDate(Date newExpiryDate) {
		this.newExpiryDate = newExpiryDate;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getMagstripname() {
		return magstripname;
	}

	public void setMagstripname(String magstripname) {
		this.magstripname = magstripname;
	}

	public String getCardIndicator() {
		return cardIndicator;
	}

	public void setCardIndicator(String cardIndicator) {
		this.cardIndicator = cardIndicator;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public int getClerk() {
		return clerk;
	}

	public void setClerk(int clerk) {
		this.clerk = clerk;
	}

	public int getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(int supervisor) {
		this.supervisor = supervisor;
	}

	public String getEmergencyReissue() {
		return emergencyReissue;
	}

	public void setEmergencyReissue(String emergencyReissue) {
		this.emergencyReissue = emergencyReissue;
	}

	public String getIssuancefee() {
		return issuancefee;
	}

	public void setIssuancefee(String issuancefee) {
		this.issuancefee = issuancefee;
	}

	public String getProductionInd() {
		return productionInd;
	}

	public void setProductionInd(String productionInd) {
		this.productionInd = productionInd;
	}

	public String toString() {
		StringBuilder buffer = new StringBuilder();
		buffer.append("cardNumber[ " + cardNumber + " ]  ");
		buffer.append("shortName[ " + shortName + "]  ");
		buffer.append("name2[ " + name2 + " ]  ");
		buffer.append("newExpiryDate[ " + newExpiryDate + " ]  ");
		return buffer.toString();
	}

}
