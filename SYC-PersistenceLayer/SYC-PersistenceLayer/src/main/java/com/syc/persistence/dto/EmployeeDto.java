package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_SVL_EMPLEADOS")
public class EmployeeDto implements Serializable {
	
	private static final long serialVersionUID = -8026372632740627386L;

	@Column(name = "ACCOUNT")
	private String account;
	
	@Id
	@Column(name = "CARD")
	private String pan;
	
	@Column(name = "EMPR_LOC_CODE")
	private String companyCode;
	
	@Column(name = "CARD_TYPE")
	private int    cardType;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "FEC_REEMP")
	private Date   replacementDate;
	
	@Column(name = "STATUS_MAQ")
	private int    status;
	
	@Column(name = "PERSONALIZADA")
	private String  accountType;
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}

	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getReplacementDate() {
		return replacementDate;
	}

	public void setReplacementDate(Date replacementDate) {
		this.replacementDate = replacementDate;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getAccountType() {
		return accountType;
	}
	
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeDto [account=").append(account)
				.append(", pan=").append(pan).append(", companyCode=")
				.append(companyCode).append(", cardType=").append(cardType)
				.append(", name=").append(name).append(", replacementDate=")
				.append(replacementDate).append(", status=").append(status)
				.append(", accountType=").append(accountType).append("]");
		return builder.toString();
	}
	
	
}

