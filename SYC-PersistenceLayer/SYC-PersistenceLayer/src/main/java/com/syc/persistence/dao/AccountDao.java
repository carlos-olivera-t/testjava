package com.syc.persistence.dao;

import java.sql.Date;
import java.util.List;

import org.hibernate.StaleObjectStateException;

import com.syc.persistence.dto.AccountDto;
import com.syc.persistence.dto.AccountFullDto;
import com.syc.persistence.dto.mapper.PanDto;


public interface AccountDao extends PersistenceDao<AccountDto, Long>{
	
	
	public AccountDto findInfoAccount(String bin, String account);
	public int sqlUpdate( AccountDto account ) throws StaleObjectStateException;
	
	public int accountUpdate( AccountDto account, String newAccount );
	
	public int accountUpdateFromDummy(AccountDto account, String newAccount);
	
	void saveNewAccount(AccountFullDto account);
	

	public int acumulateBalanceUpdate(double amount, Date trandate, String bin, String account);

}
