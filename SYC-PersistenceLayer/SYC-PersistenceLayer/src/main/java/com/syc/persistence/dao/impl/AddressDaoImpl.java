package com.syc.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.AddressDao;
import com.syc.persistence.dto.AddressDto;

@Repository
@SuppressWarnings("unchecked")
public class AddressDaoImpl extends AbstractPersistenceDaoImpl<AddressDto, Long> implements AddressDao {
	
	@Autowired
	public AddressDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}
	
	public AddressDto findCardAddress( String pan ){
		final String QUERY = "FROM AddressDto " +
							"WHERE tarjeta = '" + pan + "' ";
		return (AddressDto) DataAccessUtils.uniqueResult(getHibernateTemplate().find(QUERY));
	}
	
	
	public int sqlUpdate( AddressDto addressDto ){
		String hqlUpdate = "UPDATE  AddressDto " +
        					  "SET  nombre     = :nombre, "+
        					       "direccion  = :direccion, "+
        					       "direccion2 = :direccion2, "+
        					       "colonia    = :colonia, "+
        					       "localidad  = :localidad, "+
        					       "estado     = :estado, "+
        					       "cp         = :cp, "+
        					       "telefono   = :telefono, "+
        					       "telefono2  = :telefono2, "+
        					       "telefono3  = :telefono3, "+
        					       "rfc        = :rfc "+
        				    "WHERE tarjeta = '"+addressDto.getTarjeta()+"'";
		
		int updateEntity = getSession().createQuery( hqlUpdate )
        							   .setString("nombre"    , addressDto.getNombre())
        							   .setString("direccion" , addressDto.getDireccion())
        							   .setString("direccion2", addressDto.getDireccion2())
        							   .setString("colonia"   , addressDto.getColonia())
        							   .setString("localidad" , addressDto.getLocalidad())
        							   .setString("estado"    , addressDto.getEstado())
        							   .setString("cp"        , addressDto.getCp())
        							   .setString("telefono"  , addressDto.getTelefono())
        							   .setString("telefono2" , addressDto.getTelefono2())
        							   .setString("telefono3" , addressDto.getTelefono3())
        							   .setString("rfc"       , addressDto.getRfc())
        							   .executeUpdate();

		return updateEntity;

	}
}
