package com.syc.persistence.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.BinProductDao;
import com.syc.persistence.dto.BinProductDto;
import com.syc.persistence.dto.BinProductId;

@SuppressWarnings("unchecked")
@Repository("binProductDao")
public class BinProductDaoImpl extends AbstractPersistenceDaoImpl<BinProductDto, BinProductId> implements BinProductDao {

	@Autowired
	public BinProductDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}

	/* (non-Javadoc)
	 * @see com.syc.persistence.dao.BinProductDao#getByBin()
	 */
	public List<BinProductDto> getByBin(String bin) {
		return getSession().getNamedQuery("binProduct.findByBin").setString("bin", bin).list();
	}
	
	
}
