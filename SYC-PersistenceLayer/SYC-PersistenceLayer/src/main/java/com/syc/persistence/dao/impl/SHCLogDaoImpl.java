package com.syc.persistence.dao.impl;

import java.sql.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.SHCLogDao;

import com.syc.persistence.dto.SHCLogDto;

@SuppressWarnings("unchecked")
@Repository("shclogDao")
public class SHCLogDaoImpl extends AbstractPersistenceDaoImpl<SHCLogDto, Long> implements SHCLogDao{
	
	@Autowired
	public SHCLogDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	public List<SHCLogDto> getLogByCardNumber( String numberCard ){
		final String QUERY = "from SHCLogDto log where log.numberCard = ?";
		return (List<SHCLogDto>) getHibernateTemplate().find(QUERY, numberCard);
	}
		
	
	public SHCLogDto getLogByCardNumberDetails(String numberCard, Date   tranDate, int msgType, String  authNum ){
		final String QUERY = "from SHCLogDto log "+ 
							"where log.numberCard = '" +numberCard+ "' "+
							"AND log.tranDate = :fecha "+
							"AND log.msgType = :msgType "+   
							"AND log.authNum = :authNum ";
							
		Query query = getSession().createQuery(QUERY);
		query.setDate("fecha", tranDate);
		query.setInteger("msgType", msgType);
		query.setString("authNum", authNum);
		

		return (SHCLogDto) query.uniqueResult();
	}
	
	public Object getLimitsByCardNumber( String pan, Date tranDate ){
		
		Object limiteDiario = (Object) getSession().getNamedQuery("shclog.findLimitsByPan")
				.setString("pan", pan)
				.setDate("tranDate", tranDate)
				.uniqueResult();
		
		 return limiteDiario;
	
		
		
	}
		
	
	
}
