package com.syc.persistence.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.IstcmscardDao;
import com.syc.persistence.dto.IstcmscardDto;


@Repository("istcmscardDao")
@SuppressWarnings("unchecked")
public class IstcmscardDaoImpl extends AbstractPersistenceDaoImpl<IstcmscardDto, Long> implements IstcmscardDao{
	
	@Autowired
	public IstcmscardDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}
	
	
	public int sqlUpdate( IstcmscardDto istcmscard ) {	
		
		String hqlUpdate = "UPDATE IstcmscardDto " +
				              "SET name  = :name ,"+
				                  "name2 = :name2 "+
				            "WHERE cardNumber = '"+istcmscard.getCardNumber()+"'";
		
		int updateEntity = getSession().createQuery( hqlUpdate )
						               .setString("name"       , istcmscard.getName())
						               .setString("name2"      , istcmscard.getName2())
						               .executeUpdate();
		
		return updateEntity;
	}
	
	public List<IstcmscardDto> getInformationByName(String name, String bin){
			return (List<IstcmscardDto>) getSession().getNamedQuery("cmscard.findByName")
					 .setParameter("name", '%'+name.toUpperCase()+'%')
					 .setParameter("bin", bin+'%')
					 .list();			
	
	}
	
	public IstcmscardDto getIstcmscardInformation(String pan){
		
		return (IstcmscardDto) getSession().getNamedQuery("cmscard.findByPan")
				 .setParameter("pan", pan)
				 .uniqueResult();				
	}



	
	
	
}
