package com.syc.persistence.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.UserDao;
import com.syc.persistence.dto.UserDto;

@Repository("userDao")
@SuppressWarnings("unchecked")
public class UserDaoImpl extends AbstractPersistenceDaoImpl<UserDto, Long> implements UserDao{
	
	private static int pageSize = 10;
	
	@Autowired
	public UserDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);				
	}

	public UserDto findUserByName(String name) {			
		return (UserDto) getSession().getNamedQuery("user.findByName")
									 .setParameter("userName", name)
									 .uniqueResult(); 	
	}
	
	
	public List<UserDto> findUserByName(String name, int status) {			
		return (List<UserDto>) getSession().getNamedQuery("user.findByNameEneabled")
									 .setParameter("userName", name)
									 .setParameter("eneabled", status)
									 .list();	
	}
	
				
	public List<UserDto> findAllUsers(int pageNumber, int idOwner) {
		return  getSession().getNamedQuery("user.findAll")
						   .setParameter("idOwner", idOwner)
						   .setFirstResult(pageSize * (pageNumber - 1))
						   .setMaxResults(pageSize)
						   .list();			   	    
	}

	public long countAllUsers(int idOwner) {
		return (Long) getSession().getNamedQuery("user.findAll.count")
								  .setParameter("idOwner", idOwner)
								  .uniqueResult();				
	}


	public int borraUsuario(long Id) {
		return getSession().getNamedQuery("user.delete")
							.setParameter("userId", Id)
							.executeUpdate();
	}
	
	public UserDto findUserByProfile(String name, Long idOwner){
		
		return (UserDto) getSession().getNamedQuery("user.findByProfile")
				 .setParameter("userName", name)
				 .setParameter("userName", idOwner)
				 .uniqueResult(); 	
		
	}
	
	
}
