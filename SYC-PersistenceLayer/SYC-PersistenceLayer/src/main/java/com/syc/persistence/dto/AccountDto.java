
package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity
@Table(name="ISTDBACCT")
@IdClass(AccountPk.class)
public class AccountDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@AttributeOverrides({
		@AttributeOverride(name = "bin", column =@Column(name = "BIN") ),
		@AttributeOverride(name = "account", column =@Column(name = "ACCOUNT") )
		
	})
	private String account;
	private String bin;
	
	@Column(name="PRODUCTCODE", length=2)
	private String productCode;
	@Column(name="STATUS", length=2)
	private String status;
	@Column(name="LEDGER_BALANCE")
	private double ledgerBalance;
	@Column(name="AVAILABLE_BALANCE")
	private double availableBalance;
	@Column(name="LAST_ACTIVITY_DATE", length=32)
	private Date   lastActivityDate;
	@Column(name="SHORTNAME")		
	private String shortName;
	@Column(name="CUSTOMER_TYPE")	
	private String customerType;
	@Column(name="CASH_DEP_USED")	
	private double cash_dep_used;
	@Column(name="CASH_DEP_DATE")	
	private Date cash_dep_date;
	@Column(name="MISID")	
	private String assignmentAccount;
	
	@Version
	@Column(name = "TSF_BASE_CUR_TIME")
	private long version;
	

	public AccountDto() {
		this.account = null;
		this.bin = null;
		this.productCode = null;
		this.status = null;
		this.ledgerBalance = 0.0;
		this.availableBalance = 0.0;
		this.lastActivityDate = null;
		this.shortName = null;
		//this.customerType = null;
		this.cash_dep_used = 0.0;
		this.cash_dep_date = null;
		this.assignmentAccount = "1";
	}
	
	

	public long getVersion() {
		return version;
	}



	public void setVersion(long version) {
		this.version = version;
	}



	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getLedgerBalance() {
		return ledgerBalance;
	}
	public void setLedgerBalance(double ledgerBalance) {
		this.ledgerBalance = ledgerBalance;
	}
	public double getAvailableBalance() {
		return availableBalance;
	}
	public void setAvailableBalance(double availableBalance) {
		this.availableBalance = availableBalance;
	}
	public Date getLastActivityDate() {
		return lastActivityDate;
	}
	public void setLastActivityDate(Date lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}
	
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public double getCash_dep_used() {
		return cash_dep_used;
	}
	public void setCash_dep_used(double cash_dep_used) {
		this.cash_dep_used = cash_dep_used;
	}
	public Date getCash_dep_date() {
		return cash_dep_date;
	}
	public void setCash_dep_date(Date cash_dep_date) {
		this.cash_dep_date = cash_dep_date;
	}
	
	public String getAssignmentAccount() {
		return assignmentAccount;
	}
	public void setAssignmentAccount(String assignmentAccount) {
		this.assignmentAccount = assignmentAccount;
	}
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AccountDto [account=");
		builder.append(account);
		builder.append(", bin=");
		builder.append(bin);
		builder.append(", productCode=");
		builder.append(productCode);
		builder.append(", status=");
		builder.append(status);
		builder.append(", ledgerBalance=");
		builder.append(ledgerBalance);
		builder.append(", availableBalance=");
		builder.append(availableBalance);
		builder.append(", lastActivityDate=");
		builder.append(lastActivityDate);
		builder.append(", shortName=");
		builder.append(shortName);
		builder.append(", customerType=");
		builder.append(customerType);
		builder.append(", cash_dep_used=");
		builder.append(cash_dep_used);
		builder.append(", cash_dep_date=");
		builder.append(cash_dep_date);
		builder.append(", assignmentAccount=");
		builder.append(assignmentAccount);
		builder.append("]");
		return builder.toString();
	}
	
	
}

@Embeddable
class AccountPk implements Serializable{

	private static final long serialVersionUID = 7326130925157166033L;
	
	public AccountPk(String account, String bin){
		this.account = account;
		this.bin = bin;
	}
	
	public AccountPk(){}
	
	@Column(name = "ACCOUNT", nullable = false)
	private String   account;
	@Column(name = "BIN", length = 120, nullable = false)
	private String   bin;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	@Override
	public String toString() {
		return "AccountPk [account=" + account + ", bin=" + bin + "]";
	}
	
	

}
