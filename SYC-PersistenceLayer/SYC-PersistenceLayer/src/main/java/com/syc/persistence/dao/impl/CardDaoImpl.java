package com.syc.persistence.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.CardFullDto;

@SuppressWarnings("unchecked")
@Repository("cardDao")
public class CardDaoImpl extends AbstractPersistenceDaoImpl<CardDto, Long> implements CardDao {
	
	@Autowired
	public CardDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	
	public CardDto findByNumberCard( String numberCard ){
		final String QUERY = "FROM CardDto card " +
				            "WHERE card.numberCard = '"+numberCard+"'";
		return (CardDto) DataAccessUtils.uniqueResult(getHibernateTemplate().find(QUERY));
	}
	
	public List<CardDto> findByAccount( String account ) {
		final String QUERY =  "FROM CardDto card " +
				             "WHERE card.account = '"+account+"'";
		return (List<CardDto>) getHibernateTemplate().find(QUERY);
	}
	
	public List<CardDto> findByAccountMaxOneRow( String account ) {
		return (List<CardDto>) getSession().getNamedQuery("card.findByAccountMaxOneRow")
				.setParameter("account", account)
				.setMaxResults(1)
				.list();
	}
	
	public CardDto findByAccountMonex( String bin, String account ){
		String QUERY = 
						"SELECT * "+   
						  "FROM ( "+
						        "SELECT ISTCARD.* "+
						          "FROM ISTCARD, "+
						               "ISTREL, "+       
						               "ISTCMSCARD "+
						         "WHERE ISTCARD.BASE = ISTREL.BASE "+
						           "AND ISTCARD.PAN  = ISTREL.ACCOUNT "+
						           "AND ISTCMSCARD.PAN = ISTCARD.PAN "+
						           "AND ISTCARD.PRIMARYACCT = '"+account+"' "+   
						           "AND ISTREL.BIN = '"+bin+"' "+
						           "ORDER BY ISTCMSCARD.CARDINDICATOR ASC "+
						        ") "+
						 "WHERE ROWNUM <= 1 ";
		
		return (CardDto) getSession().createSQLQuery(QUERY).addEntity("CardDto",CardDto.class ).uniqueResult();
	}
	
	public List<CardDto> findAll( String numberCard ) {
		final String QUERY = "from CardDto card where card.numberCard = ?";
		return (List<CardDto>) getHibernateTemplate().find(QUERY, numberCard);
	}

	public CardDto getTitCardByAccount( String account ){
		String QUERY = 
  				   "SELECT ISTCARD.* "+       
					  "FROM ISTCARD, "+
					       "ISTCMSCARD "+
					 "WHERE ISTCARD.PRIMARYACCT = '"+account+"' "+
					   "AND ISTCARD.PAN = ISTCMSCARD.PAN "+
					   "AND ISTCMSCARD.CARDINDICATOR = 0"+
					   "AND ISTCARD.STATUS IN (50,51,59)";
					
		return (CardDto) getSession().createSQLQuery(QUERY).addEntity("CardDto",CardDto.class ).uniqueResult();
		
	}
	
	public CardDto findByAccountTblEmployeers(String account){
		String QUERY = 
						"SELECT * "+
						  "FROM ( "+
						         "SELECT ISTCARD.* "+
						           "FROM TBL_SVL_EMPLEADOS EMPLEADOS, "+
						                "ISTCARD "+
						          "WHERE EMPLEADOS.ACCOUNT = '"+account+"' "+
						            "AND EMPLEADOS.STATUS = 0 "+
						            "AND EMPLEADOS.CARD_TYPE = 0 "+
						            "AND EMPLEADOS.FEC_REEMP IS NULL "+
						            "AND ISTCARD.PAN = EMPLEADOS.CARD "+
						        ") "+
						  "WHERE ROWNUM <= 1";
		
		return (CardDto) getSession().createSQLQuery(QUERY).addEntity("CardDto",CardDto.class ).uniqueResult();
	}
	
	public int sqlUpdate( CardDto card ) {	
		
		String hqlUpdate = "UPDATE CardDto " +
				              "SET status     = :newStatus ,"+
				                  "cardType   = :cardType, "+
				                  "account    = :account, "+
				                  "clientId    = :base "+
				            "WHERE numberCard = :cardNumber ";
		
		int updateEntity = getSession().createQuery( hqlUpdate )
						               .setInteger("newStatus"  , card.getStatus())
						               .setString ("cardType"   , card.getCardType())
						               .setString ("account"    , card.getAccount())
						               .setString ("base"    , card.getClientId())
						               .setString ("cardNumber" , card.getNumberCard())
						               .executeUpdate();
		return updateEntity;
	}
	
	public void runException(){
		throw new NullPointerException("Lance una excepcion a proposito");
	}
	
	public void saveNewCard(CardFullDto card){				
		getHibernateTemplate().save(card);				
	}


	@Override
	public List<Object[]> findCardsByAccount(String bin, String account) {
		
		return  (List<Object[]>) getSession().getNamedQuery("card.findCardsByAccount")
//				 .setParameter("bin", bin)
				 .setParameter("account", account)
				 .list(); 
	}
	
	public CardDto findPanByAccount( String bin, String account ){
		String QUERY = 
				
				"SELECT *  "+
				 " FROM ( "+
				  	"SELECT ISTCARD.* "+
				  		"FROM ISTCARD,"+
				  		" ISTREL      "+
				  		"WHERE ISTCARD.BASE = ISTREL.BASE "+
				  		"AND ISTCARD.PAN  = ISTREL.ACCOUNT "+
				  		"AND ISTCARD.PRIMARYACCT = '"+account+"' "+   
				        "AND ISTREL.BIN = '"+bin+"' "+
				        " ) "+
				"WHERE ROWNUM <= 1"; 
		
		return (CardDto) getSession().createSQLQuery(QUERY).addEntity("CardDto",CardDto.class ).uniqueResult();
	}


	public List<CardDto> findIdClient(String bin, String base){
		
		return (List<CardDto>) getSession().getNamedQuery("card.findIdClient")
				.setParameter("base", base)
				.setParameter("bin", bin)
				.list();
	}
	
	public List<CardDto> findIdClientMaxOneRow(String bin, String base){
		
		return (List<CardDto>) getSession().getNamedQuery("card.findIdClient")
				.setParameter("base", base)
				.setParameter("bin", bin)
				.setMaxResults(1)
				.list();
	}
	
	public List<CardDto> findIdClientBus(String bin, String base){
		
		return (List<CardDto>) getSession().getNamedQuery("card.findIdClientBus")
				.setParameter("base", base)
				.setParameter("bin", bin)
				.list();
	}
}


