package com.syc.persistence.dao;


import com.syc.persistence.dto.CardLimitDto;


public interface CardLimitDao  extends PersistenceDao<CardLimitDto, CardLimitDto.CardLimitPk>{
		
	CardLimitDto findBypan(String bin, String pan);
	
	
	public CardLimitDto getLimitsDetail(String pan, String bin);
		
}
