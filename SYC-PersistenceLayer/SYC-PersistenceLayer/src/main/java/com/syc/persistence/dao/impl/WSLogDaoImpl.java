package com.syc.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.WSLogDto;

@SuppressWarnings("unchecked")
@Repository("wsLogDao")
public class WSLogDaoImpl extends AbstractPersistenceDaoImpl<WSLogDto, Long> implements WSLogDao {
	
	@Autowired
	public WSLogDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}

}
