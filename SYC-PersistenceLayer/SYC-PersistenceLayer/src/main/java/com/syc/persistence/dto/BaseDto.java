package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ISTBASE")
public class BaseDto implements Serializable {

	private static final long serialVersionUID = 5129223015939601643L;
		
	@Column(name = "BIN", length = 10)
	private String bin;

	@Id
	@Column(name = "BASE", length = 10)
	private String base;

	@Column(name = "PARENT", length = 10)
	private String parent;

	@Column(name = "REGION", length = 1)
	private String region;

	@Column(name = "BRANCH", length = 3)
	private String branch;

	@Column(name = "MISID", length = 8)
	private String misid;

	@Column(name = "GROUPCODE", length = 1)
	private String groupCode;

	@Column(name = "BASESTATUS", length = 2)
	private String baseStatus;

	@Column(name = "CUSTOMER_TYPE", length = 1)
	private String customerType;

	@Column(name = "CUSTOMER_CATAGORY", length = 1)
	private String customerCatagory;

	@Column(name = "OPEN_DATE")
	private Date openDate;

	@Column(name = "TXNCODE", length = 4)
	private String txcode;

	@Column(name = "BLOCKCODE", length = 1)
	private String blockCode;

	@Column(name = "CASH_WD_AMT_USED")
	private double cashWdAmtUsed;

	@Column(name = "CASH_WD_COUNT")
	private int cashWdCount;

	@Column(name = "CASH_WD_DATE")
	private Date cashWdDate;

	@Column(name = "CASH_WD_TIME")
	private int cashWdTime;

	@Column(name = "TC_WD_AMT_USED")
	private double tcWdAmtUsed;

	@Column(name = "TC_WD_COUNT")
	private int tcWdCount;

	@Column(name = "TC_WD_DATE")
	private Date tcWdDate;

	@Column(name = "TC_WD_TIME")
	private int tcWdTime;

	@Column(name = "CASH_TC_AMT_USED")
	private double cashTcAmtUsed;

	@Column(name = "CASH_TC_DATE")
	private Date cashTcDate;

	@Column(name = "CASH_TC_TIME")
	private int cashTcTime;

	@Column(name = "PURCH_AMT_USED")
	private double purchAmtUsed;

	@Column(name = "PURCH_COUNT")
	private int purchCount;

	@Column(name = "PURCH_DATE")
	private Date purchDate;

	@Column(name = "PURCH_TIME")
	private int purchTime;

	@Column(name = "TSF_BASE_CUR_USED")
	private double tsfBaseCurUsed;

	@Column(name = "TSF_BASE_CUR_DATE")
	private Date tsfBaseCurDate;

	@Column(name = "TSF_BASE_CUR_TIME")
	private int tsfBaseCurTime;

	@Column(name = "TSF_MULTI_CUR_USED")
	private double tsfMultiCurUsed;

	@Column(name = "TSF_MULTI_CUR_DATE")
	private Date tsfMultiCurDate;

	@Column(name = "TSF_MULTI_CUR_TIME")
	private int tsfMultiCurTime;

	@Column(name = "AGGREGATE_INDICATOR", length = 1)
	private String aggregateIndicator;

	@Column(name = "T_PVKI", length = 1)
	private String tPvki;

	@Column(name = "T_PVV", length = 4)
	private String tPvv;

	@Column(name = "CARD_SEQNO")
	private int cardSeqNo;

	@Column(name = "ID_NUMBER", length = 20)
	private String idNumber;

	@Column(name = "ID_TYPE", length = 2)
	private String idType;

	@Column(name = "TEMP_PKVI", length = 1)
	private String tempPkvi;

	@Column(name = "TEMP_PVV", length = 4)
	private String tempPvv;

	@Column(name = "ACCESSTYPE", length = 1)
	private String accessType;

	public BaseDto(){
		
	}
	
	public BaseDto(boolean newRow){
		Date fecha = null;
		
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		
		try {			
			fecha = new Date(formatoFecha.parse("01/01/1980").getTime());						
		} catch (ParseException e) {
			//Si no genera las fechas la manda como nula
		}
		
		this.region = "1";
		this.groupCode = "R";
		this.baseStatus = "00";
		this.customerType = "0";
		this.customerCatagory = "0";
		this.openDate = new Date(new java.util.Date().getTime());
		this.txcode = "0001";
		this.cashWdDate = fecha;
		this.tcWdDate = fecha;
		this.cashTcDate = fecha;
		this.purchDate = fecha;
		this.tsfBaseCurDate = fecha;
		this.tsfMultiCurDate = fecha;
		this.accessType = "P";
		
	}
	
	
	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getMisid() {
		return misid;
	}

	public void setMisid(String misid) {
		this.misid = misid;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getBaseStatus() {
		return baseStatus;
	}

	public void setBaseStatus(String baseStatus) {
		this.baseStatus = baseStatus;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCustomerCatagory() {
		return customerCatagory;
	}

	public void setCustomerCatagory(String customerCatagory) {
		this.customerCatagory = customerCatagory;
	}

	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public String getTxcode() {
		return txcode;
	}

	public void setTxcode(String txcode) {
		this.txcode = txcode;
	}

	public String getBlockCode() {
		return blockCode;
	}

	public void setBlockCode(String blockCode) {
		this.blockCode = blockCode;
	}

	public double getCashWdAmtUsed() {
		return cashWdAmtUsed;
	}

	public void setCashWdAmtUsed(double cashWdAmtUsed) {
		this.cashWdAmtUsed = cashWdAmtUsed;
	}

	public int getCashWdCount() {
		return cashWdCount;
	}

	public void setCashWdCount(int cashWdCount) {
		this.cashWdCount = cashWdCount;
	}

	public Date getCashWdDate() {
		return cashWdDate;
	}

	public void setCashWdDate(Date cashWdDate) {
		this.cashWdDate = cashWdDate;
	}

	public int getCashWdTime() {
		return cashWdTime;
	}

	public void setCashWdTime(int cashWdTime) {
		this.cashWdTime = cashWdTime;
	}

	public double getTcWdAmtUsed() {
		return tcWdAmtUsed;
	}

	public void setTcWdAmtUsed(double tcWdAmtUsed) {
		this.tcWdAmtUsed = tcWdAmtUsed;
	}

	public int getTcWdCount() {
		return tcWdCount;
	}

	public void setTcWdCount(int tcWdCount) {
		this.tcWdCount = tcWdCount;
	}

	public Date getTcWdDate() {
		return tcWdDate;
	}

	public void setTcWdDate(Date tcWdDate) {
		this.tcWdDate = tcWdDate;
	}

	public int getTcWdTime() {
		return tcWdTime;
	}

	public void setTcWdTime(int tcWdTime) {
		this.tcWdTime = tcWdTime;
	}

	public double getCashTcAmtUsed() {
		return cashTcAmtUsed;
	}

	public void setCashTcAmtUsed(double cashTcAmtUsed) {
		this.cashTcAmtUsed = cashTcAmtUsed;
	}

	public Date getCashTcDate() {
		return cashTcDate;
	}

	public void setCashTcDate(Date cashTcDate) {
		this.cashTcDate = cashTcDate;
	}

	public int getCashTcTime() {
		return cashTcTime;
	}

	public void setCashTcTime(int cashTcTime) {
		this.cashTcTime = cashTcTime;
	}

	public double getPurchAmtUsed() {
		return purchAmtUsed;
	}

	public void setPurchAmtUsed(double purchAmtUsed) {
		this.purchAmtUsed = purchAmtUsed;
	}

	public int getPurchCount() {
		return purchCount;
	}

	public void setPurchCount(int purchCount) {
		this.purchCount = purchCount;
	}

	public Date getPurchDate() {
		return purchDate;
	}

	public void setPurchDate(Date purchDate) {
		this.purchDate = purchDate;
	}

	public int getPurchTime() {
		return purchTime;
	}

	public void setPurchTime(int purchTime) {
		this.purchTime = purchTime;
	}

	public double getTsfBaseCurUsed() {
		return tsfBaseCurUsed;
	}

	public void setTsfBaseCurUsed(double tsfBaseCurUsed) {
		this.tsfBaseCurUsed = tsfBaseCurUsed;
	}

	public Date getTsfBaseCurDate() {
		return tsfBaseCurDate;
	}

	public void setTsfBaseCurDate(Date tsfBaseCurDate) {
		this.tsfBaseCurDate = tsfBaseCurDate;
	}

	public int getTsfBaseCurTime() {
		return tsfBaseCurTime;
	}

	public void setTsfBaseCurTime(int tsfBaseCurTime) {
		this.tsfBaseCurTime = tsfBaseCurTime;
	}

	public double getTsfMultiCurUsed() {
		return tsfMultiCurUsed;
	}

	public void setTsfMultiCurUsed(double tsfMultiCurUsed) {
		this.tsfMultiCurUsed = tsfMultiCurUsed;
	}

	public Date getTsfMultiCurDate() {
		return tsfMultiCurDate;
	}

	public void setTsfMultiCurDate(Date tsfMultiCurDate) {
		this.tsfMultiCurDate = tsfMultiCurDate;
	}

	public int getTsfMultiCurTime() {
		return tsfMultiCurTime;
	}

	public void setTsfMultiCurTime(int tsfMultiCurTime) {
		this.tsfMultiCurTime = tsfMultiCurTime;
	}

	public String getAggregateIndicator() {
		return aggregateIndicator;
	}

	public void setAggregateIndicator(String aggregateIndicator) {
		this.aggregateIndicator = aggregateIndicator;
	}

	public String gettPvki() {
		return tPvki;
	}

	public void settPvki(String tPvki) {
		this.tPvki = tPvki;
	}

	public String gettPvv() {
		return tPvv;
	}

	public void settPvv(String tPvv) {
		this.tPvv = tPvv;
	}

	public int getCardSeqNo() {
		return cardSeqNo;
	}

	public void setCardSeqNo(int cardSeqNo) {
		this.cardSeqNo = cardSeqNo;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getTempPkvi() {
		return tempPkvi;
	}

	public void setTempPkvi(String tempPkvi) {
		this.tempPkvi = tempPkvi;
	}

	public String getTempPvv() {
		return tempPvv;
	}

	public void setTempPvv(String tempPvv) {
		this.tempPvv = tempPvv;
	}

	public String getAccessType() {
		return accessType;
	}

	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}

	public String toString() {
		StringBuilder buffer = new StringBuilder();
		buffer.append(" bin[ " + bin + " ]");
		buffer.append(" base[ " + base + " ]");
		return buffer.toString();
	}

}
