package com.syc.persistence.dao;

import com.syc.persistence.dto.BitacoraMatchDto;

public interface BitacoraMatchDao extends PersistenceDao<BitacoraMatchDto, Long> {
	
	public BitacoraMatchDto findByAccount(String account, String emisor);

}
