package com.syc.persistence.dto.mapper;

public class PanDto {
	
	private String numberCard;
	private String cardIndicator;
	private int status;
	
	

	public String getNumberCard() {
		return numberCard;
	}
	public void setNumberCard(String numberCard) {
		this.numberCard = numberCard;
	}
	public String getCardIndicator() {
		return cardIndicator;
	}
	public void setCardIndicator(String cardIndicator) {
		this.cardIndicator = cardIndicator;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PanDto [numberCard=");
		builder.append(numberCard);
		builder.append(", cardIndicator=");
		builder.append(cardIndicator);
		builder.append(", status=");
		builder.append(status);
		builder.append("]");
		return builder.toString();
	}
	
	

}
