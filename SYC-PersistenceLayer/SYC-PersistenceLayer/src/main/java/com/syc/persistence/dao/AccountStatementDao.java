package com.syc.persistence.dao;

import java.util.List;
import java.util.TreeMap;

import com.syc.persistence.dto.mapper.AccountStatementDto;

public interface AccountStatementDao {
	
	public List <AccountStatementDto> findSqlServerAccountStatement(String employer, String product, TreeMap<Integer,String> mapOfDates);
	
//	public List <AccountStatementDto> findPostgresAccountStatement(String employer, String product, TreeMap<Integer,String> mapOfDates);

}
