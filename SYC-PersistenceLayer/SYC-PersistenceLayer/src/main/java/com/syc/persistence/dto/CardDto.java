package com.syc.persistence.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "ISTCARD")

@NamedQueries({
	@NamedQuery(name="card.findCardsByAccount", query="SELECT c.numberCard,c.status,i.cardIndicator FROM CardDto c, IstcmscardDto i WHERE c.numberCard = i.cardNumber  AND c.account = CAST(:account  as char)"),
	@NamedQuery(name="card.findIdClient", query="SELECT card FROM CardDto AS card, IstrelDto as rel WHERE card.clientId = rel.base and card.numberCard = rel.account and card.clientId = :base and rel.bin = :bin "),
	@NamedQuery(name="card.findIdClientBus", query="SELECT card FROM CardDto AS card, IstrelDto as rel WHERE card.clientId = rel.base and card.numberCard = rel.account and card.clientId = :base and rel.bin = :bin "),
	@NamedQuery(name="card.findByAccountMaxOneRow", query="FROM CardDto c WHERE c.account = CAST(:account  as char)"),
})
public class CardDto implements Serializable {

	private static final long serialVersionUID = -2321401983939228026L;

	@Id
	@Column(name = "PAN", length = 19, nullable = false)
	private String numberCard;
	@Column(name = "PRIMARYACCT", length = 32, nullable = false)
	private String account;
	@Column(name = "BASE", length = 10, nullable = false)
	private String clientId;
	@Column(name = "STATUS", nullable = false)
	private int status;
	@Column(name = "EXPIRYDATE", nullable = false)
	private Date expirationDate;
	@Column(name = "PRIMARYACCTTYPE", length = 2, nullable = false)
	private String typeAccount;
	@Column(name = "CARDTYPE")
	private String cardType;
	@Column(name = "LIMITINDICATOR")
	private String limitIndicator;


	//CAMPOS PARA ALTERAR EN DESBLOQUEO DE NIP
		@Column(name = "PINRETRY")
		private int pinRetry;
		@Column(name = "ALTBLOCKCODE")
		private char altBlockCode;
		@Column(name = "DECLINE_COUNT")
		private int declineCount;
		@Column(name = "TPINRETRY")
		private int tPinRetry;

		public int getPinRetry() {
			return pinRetry;
		}

		public void setPinRetry(int pinRetry) {
			this.pinRetry = pinRetry;
		}

		public int gettPinRetry() {
			return tPinRetry;
		}

		public void settPinRetry(int tPinRetry) {
			this.tPinRetry = tPinRetry;
		}

	
	public String getNumberCard() {
		return numberCard;
	}

	public void setNumberCard(String numberCard) {
		this.numberCard = numberCard;
	}
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getTypeAccount() {
		return typeAccount;
	}

	public void setTypeAccount(String typeAccount) {
		this.typeAccount = typeAccount;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getLimitIndicator() {
		return limitIndicator;
	}

	public void setLimitIndicator(String limitIndicator) {
		this.limitIndicator = limitIndicator;
	}

	public String toString() {
		String buffer;
		buffer = "numberCard[ " + numberCard + " ]  ";
		buffer += "account[ " + account + " ]  ";
		buffer += "clientId[ " + clientId + " ]  ";
		buffer += "status[ " + status + " ]  ";
		buffer += "expirationDate[ " + expirationDate + " ]  ";
		buffer += "typeAccount[ " + typeAccount + " ]  ";
		buffer += "cardType[ " + cardType + " ]  ";
		return buffer;
	}

}