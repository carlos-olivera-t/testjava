package com.syc.persistence.dao.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.LayoutDao;
import com.syc.persistence.dto.LayoutDto;

@SuppressWarnings("unchecked")
@Repository("layoutDao")
public class LayoutDaoImpl extends AbstractPersistenceDaoImpl<LayoutDto, Long> implements LayoutDao{
	
	private static final transient Log log = LogFactory.getLog(LayoutDaoImpl.class);
	
	@Autowired
	public LayoutDaoImpl( HibernateTemplate hibernateTemplate ) {
		super( hibernateTemplate );
	}
	
	
	public List<LayoutDto> getLayoutByIdEmbosser( int idEmbosser ){
		log.debug("Buscando Layout por Embosador..");
		
		final String QUERY = "FROM LayoutDto " +
							"WHERE idEmbosser = ?";
		
		return (List<LayoutDto>) getHibernateTemplate().find(QUERY, idEmbosser);
	}

}
