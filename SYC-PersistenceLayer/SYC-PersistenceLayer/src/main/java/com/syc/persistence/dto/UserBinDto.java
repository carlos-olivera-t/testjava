package com.syc.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name = "USER_BIN")
@NamedQueries({
	@NamedQuery(name = "userBin.findByUsername", query = "SELECT b.bin FROM UserDto u, UserBinDto b WHERE u.id = b.userid and u.userName = :user")
})
public class UserBinDto {

	@SequenceGenerator(name="SEQ_USER_BIN", sequenceName = "SEQ_USER_BIN")
	
	@Id
	@Column(name = "ID")	
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "SEQ_USER_BIN")    
	private long id;

	@Column(name = "USER_ID")
	private long userid;

	@Column(name = "BIN", length = 8)
	private String bin;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserid() {
		return userid;
	}

	public void setUserid(long userid) {
		this.userid = userid;
	}

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public String toString() {
		String buffer = null;
		buffer = "bin[ " + bin + " ]  ";
		buffer += "userid[ " + userid + " ]  ";
		return buffer;
	}

}
