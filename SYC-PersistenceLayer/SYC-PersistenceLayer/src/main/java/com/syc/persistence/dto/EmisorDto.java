package com.syc.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_EMISORES")
public class EmisorDto {
	
	@Id
	@Column(name="CVE_EMISOR")
	private long cveEmisor;
	
	@Column(name="NOM_EMISOR")	
	private String emisor;
	
	@Column(name="BIN")
	private String bin;
	
	@Column(name="CUENTA")
	private String cuenta;
	
	@Column(name="PREFIJO")
	private String prefijo;

	public long getCveEmisor() {
		return cveEmisor;
	}

	public void setCveEmisor(long cveEmisor) {
		this.cveEmisor = cveEmisor;
	}

	public String getEmisor() {
		return emisor;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}
	
	public String toString(){
		StringBuilder buffer = new StringBuilder();
		
		buffer.append("cveEmisor["+cveEmisor+"],");
		buffer.append("emisor["+emisor+"],");
		buffer.append("bin["+bin+"],");
		buffer.append("cuenta["+cuenta+"],");
		buffer.append("prefijo["+prefijo+"],");
		
		return buffer.toString();
	}	
}
