package com.syc.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.IstrelDao;
import com.syc.persistence.dto.IstrelDto;

@SuppressWarnings("unchecked")
@Repository("istrelDao")
public class IstrelDaoImpl extends AbstractPersistenceDaoImpl<IstrelDto, Long> implements IstrelDao {
	
	@Autowired
	public IstrelDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	public IstrelDto findAccount( String numberCard, String base ){
		final String QUERY =  "FROM IstrelDto istrel " +
        					 "WHERE istrel.base = '"+base+"' " +
                               "AND istrel.account = '"+numberCard+"'";
		return (IstrelDto) DataAccessUtils.uniqueResult(getHibernateTemplate().find(QUERY));
	}
	
	public int sqlUpdate( IstrelDto istrelDto ) {	
		
		String hqlUpdate = "UPDATE IstrelDto " +
				              "SET status  = :status "+
				            "WHERE bin     = :bin "+
				              "AND account = :account";
		
		int updateEntity = getSession().createQuery( hqlUpdate )
						               .setString("status"  , istrelDto.getStatus())
						               .setString("bin"     , istrelDto.getBin())
						               .setString("account" , istrelDto.getAccount())
						               .executeUpdate();
		
		return updateEntity;
	}
	
	public int accountUpdate( IstrelDto istrelDto, String newAccount ) {	
		
		String hqlUpdate = "UPDATE IstrelDto " +
				              "SET account = :newAccount ,"+
				              "base = :base "+
				            "WHERE bin     = :bin "+
				              "AND account = :account";
		
		int updateEntity = getSession().createQuery( hqlUpdate )
						               .setString("newAccount" , newAccount)
						               .setString("base"        , istrelDto.getBase())
						               .setString("bin"        , istrelDto.getBin())
						               .setString("account"    , istrelDto.getAccount())
						               .executeUpdate();
		
		return updateEntity;
	}
}
