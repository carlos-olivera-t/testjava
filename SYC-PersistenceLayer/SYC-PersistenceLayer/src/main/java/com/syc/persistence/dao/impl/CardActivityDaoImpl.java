package com.syc.persistence.dao.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.CardActivityDao;

import com.syc.persistence.dto.CardActivityDto;

@SuppressWarnings("unchecked")
@Repository("cardActivityDao")
public class CardActivityDaoImpl extends AbstractPersistenceDaoImpl<CardActivityDto, Long> implements CardActivityDao{
	
	@Autowired
	public CardActivityDaoImpl(HibernateTemplate hibernateTemplate) {
		super(hibernateTemplate);
	}
	
	@Override
	public Object[] findByPan(String pan) {
		return (Object[])getSession().getNamedQuery("activity.findByPan")
			.setParameter("pan", pan)
			.setMaxResults(1)
			.uniqueResult();
	}
	
}
