package com.syc.persistence.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.EmployerDetailDao;
import com.syc.persistence.dto.EmployerDetailDto;

@SuppressWarnings("unchecked")
@Repository("employerDetailDao")
public class EmployerDetailDaoImpl extends AbstractPersistenceDaoImpl<EmployerDetailDto, Long> implements EmployerDetailDao{

	@Autowired
	public EmployerDetailDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}

	@Override
	public EmployerDetailDto findEmployer(String employerLoc, int issCode, String tiType) {
		return (EmployerDetailDto) getSession().getNamedQuery("employerDetail.findByEmprLocCode")
				 .setParameter("emprLocCode", employerLoc)
				 .setParameter("issCode", issCode)
				 .setParameter("tiType", tiType)
				 .uniqueResult(); 	
	}
}
