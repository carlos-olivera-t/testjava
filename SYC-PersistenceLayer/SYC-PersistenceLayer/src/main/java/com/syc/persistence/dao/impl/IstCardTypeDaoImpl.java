package com.syc.persistence.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.IstCardTypeDao;
import com.syc.persistence.dto.IstCardTypeDto;

@SuppressWarnings("unchecked")
@Repository("istCardTypeDao")
public class IstCardTypeDaoImpl extends AbstractPersistenceDaoImpl<IstCardTypeDto, String> implements IstCardTypeDao{

	
	@Autowired
	public IstCardTypeDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}
	
	
	public List<IstCardTypeDto> findByBin(String bin) {
		return  (List<IstCardTypeDto>) getSession().getNamedQuery("cardType.findTypeByBin")
							.setString("bin", bin)
							.list();				
	}


	@Override
	public IstCardTypeDto findPrefixByBin(String bin, String cardType) {
		return  (IstCardTypeDto) getSession().getNamedQuery("cardType.findPrefix")
				.setString("bin", bin)
				.setString("cardType", cardType)
				.uniqueResult();	
	}


	
}
