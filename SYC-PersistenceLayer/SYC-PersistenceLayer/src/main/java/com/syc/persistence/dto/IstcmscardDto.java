package com.syc.persistence.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="ISTCMSCARD")

@NamedQueries({
	@NamedQuery(name="cmscard.findByName", query="FROM IstcmscardDto AS card WHERE upper(card.name) like :name  AND card.cardNumber like :bin"),
	@NamedQuery(name="cmscard.findByPan", query="FROM IstcmscardDto AS card WHERE card.cardNumber = CAST(:pan  as char)")

})
public class IstcmscardDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4725448381538158563L;

	@Id
	@Column(name="PAN")
	private String cardNumber;
	
	@Column(name="EMBOSSNAME_1", length=25)
	private String name;
	
	@Column(name="EMBOSSNAME_2", length=25)
	private String name1;
	
	@Column(name="MAGSTRIPNAME", length=25)
	private String name2;
	
	@Column(name="CARDINDICATOR", length=2)
	private String cardIndicator;
	
	
	public String getName1() {
		return name1;
	}
	public void setName1(String name1) {
		this.name1 = name1;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName2() {
		return name2;
	}
	public void setName2(String name2) {
		this.name2 = name2;
	}
	
	public String getCardIndicator() {
		return cardIndicator;
	}
	public void setCardIndicator(String cardIndicator) {
		this.cardIndicator = cardIndicator;
	}
	public String toString(){
		String buffer = null;
		buffer += "cardNumber[ "+cardNumber+" ]  ";
		buffer += "name[ "+name+"]  ";
		buffer += "name2[ "+name2+" ]  ";
		return buffer;
	}

	
}
