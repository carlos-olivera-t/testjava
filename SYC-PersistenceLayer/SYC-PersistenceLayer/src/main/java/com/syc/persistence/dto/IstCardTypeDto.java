package com.syc.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="ISTCARDTYPE")
@NamedQueries({
		@NamedQuery(name = "cardType.findTypeByBin", query = "FROM IstCardTypeDto WHERE bin = :bin"),
		@NamedQuery(name = "cardType.findPrefix", query = "FROM IstCardTypeDto WHERE bin = :bin AND cardType = :cardType")})
public class IstCardTypeDto {
	
	@Id
	@Column(name="BIN")
	private String bin;
	@Column(name="CARDTYPE")
	private String cardType;
	@Column(name="A_DESC")
	private String description;
	@Column(name="E_DESC")
	private String description2;
	@Column(name="TXNCODE")
	private String txnCode;
	@Column(name="CASH_WD_LIMIT")
	private String cashWdLimit;
	@Column(name="CASH_WD_COUNT")
	private String cashWdCount;
	@Column(name="ICC_INDICATOR")
	private String iccIndicator;
	@Column(name="SERVICE_CODE")
	private String serviceCode;
	@Column(name="ICC_CARD_PREFIX")
	private String cardPrefix;
	@Column(name = "CARD_LIFE")
	private String cardLife;
	@Column(name = "TSF_MULTI_LIMIT")
	private double maxAvailableBalance;
	@Column(name = "CASH_DEP_LIMIT")
	private double maxBalanceByMonth;
	
	
	public double getMaxAvailableBalance() {
		return maxAvailableBalance;
	}
	public void setMaxAvailableBalance(double maxAvailableBalance) {
		this.maxAvailableBalance = maxAvailableBalance;
	}
	public double getMaxBalanceByMonth() {
		return maxBalanceByMonth;
	}
	public void setMaxBalanceByMonth(double maxBalanceByMonth) {
		this.maxBalanceByMonth = maxBalanceByMonth;
	}
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescription2() {
		return description2;
	}
	public void setDescription2(String description2) {
		this.description2 = description2;
	}
	public String getTxnCode() {
		return txnCode;
	}
	public void setTxnCode(String txnCode) {
		this.txnCode = txnCode;
	}
	public String getCashWdLimit() {
		return cashWdLimit;
	}
	public void setCashWdLimit(String cashWdLimit) {
		this.cashWdLimit = cashWdLimit;
	}
	public String getCashWdCount() {
		return cashWdCount;
	}
	public void setCashWdCount(String cashWdCount) {
		this.cashWdCount = cashWdCount;
	}
	public String getIccIndicator() {
		return iccIndicator;
	}
	public void setIccIndicator(String iccIndicator) {
		this.iccIndicator = iccIndicator;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getCardPrefix() {
		return cardPrefix;
	}
	public void setCardPrefix(String cardPrefix) {
		this.cardPrefix = cardPrefix;
	}
	public String getCardLife() {
		return cardLife;
	}
	public void setCardLife(String cardLife) {
		this.cardLife = cardLife;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IstCardTypeDto [bin=");
		builder.append(bin);
		builder.append(", cardType=");
		builder.append(cardType);
		builder.append(", description=");
		builder.append(description);
		builder.append(", description2=");
		builder.append(description2);
		builder.append(", txnCode=");
		builder.append(txnCode);
		builder.append(", cashWdLimit=");
		builder.append(cashWdLimit);
		builder.append(", cashWdCount=");
		builder.append(cashWdCount);
		builder.append(", iccIndicator=");
		builder.append(iccIndicator);
		builder.append(", serviceCode=");
		builder.append(serviceCode);
		builder.append(", cardPrefix=");
		builder.append(cardPrefix);
		builder.append(", cardLife=");
		builder.append(cardLife);
		builder.append(", maxAvailableBalance=");
		builder.append(maxAvailableBalance);
		builder.append(", maxBalanceByMonth=");
		builder.append(maxBalanceByMonth);
		builder.append("]");
		return builder.toString();
	}
	

}
