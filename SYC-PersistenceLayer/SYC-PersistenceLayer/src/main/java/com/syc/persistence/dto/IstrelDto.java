package com.syc.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ISTREL")
public class IstrelDto {

	private String bin;
	private String base;
	private String account;
	private String productCode;
	private String parentBase;
	private String status;
	private String type;
	private String primaryIndicator;
	private String addressLink;

	
	public IstrelDto(){
		
	}
	
	public IstrelDto(boolean newRow){
		this.productCode = "01";
		this.parentBase = "0000000000";
		this.status = "00";
		this.type = "0";
		this.primaryIndicator = "0";
		this.addressLink = "0000000000";
	}
	
	@Column(name = "BIN", length = 10, nullable = false)
	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	@Column(name = "BASE", length = 10, nullable = false)
	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	@Id
	@Column(name = "ACCOUNT", length = 32, nullable = false)
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	@Column(name = "PRODUCTCODE", length = 2, nullable = false)
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	
	@Column(name = "PARENTBASE", length = 10)
	public String getParentBase() {
		return parentBase;
	}

	public void setParentBase(String parentBase) {
		this.parentBase = parentBase;
	}

	public String getStatus() {
		return status;
	}

	@Column(name = "STATUS")
	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "TYPE", length = 1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "PRIMARY_INDICATOR", length = 1)
	public String getPrimaryIndicator() {
		return primaryIndicator;
	}

	public void setPrimaryIndicator(String primaryIndicator) {
		this.primaryIndicator = primaryIndicator;
	}

	@Column(name = "ADDRESS_LINK", length = 10)
	public String getAddressLink() {
		return addressLink;
	}

	public void setAddressLink(String addressLink) {
		this.addressLink = addressLink;
	}

	public String toString() {
		String buffer = null;
		buffer = "bin[ " + bin + " ]  ";
		buffer += "base[ " + base + " ]  ";
		buffer += "account[ " + account + " ]  ";
		buffer += "productCode[ " + productCode + "]  ";
		buffer += "status[ " + status + " ]  ";
		return buffer;
	}

}
