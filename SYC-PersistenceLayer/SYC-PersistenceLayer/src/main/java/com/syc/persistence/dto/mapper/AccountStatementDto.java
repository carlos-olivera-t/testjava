package com.syc.persistence.dto.mapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountStatementDto", propOrder = {
	"employer",
	"cutDate",
    "startShowDate",
    "finalShowDate",
    "pdfPath",
    "excelPath",
    "xmlPath",
    "idProduct"
})
public class AccountStatementDto {
	
	private String employer;
	private String cutDate;
	private String startShowDate;
	private String finalShowDate;
	private String pdfPath;
	private String excelPath;
	private String xmlPath;
	private String idProduct;
	
	public String getEmployer() {
		return employer;
	}
	public void setEmployer(String employer) {
		this.employer = employer;
	}
	
	public String getCutDate() {
		return cutDate;
	}
	public void setCutDate(String cutDate) {
		this.cutDate = cutDate;
	}
	
	public String getStartShowDate() {
		return startShowDate;
	}
	public void setStartShowDate(String startShowDate) {
		this.startShowDate = startShowDate;
	}
	
	public String getFinalShowDate() {
		return finalShowDate;
	}
	public void setFinalShowDate(String finalShowDate) {
		this.finalShowDate = finalShowDate;
	}
	
	public String getPdfPath() {
		return pdfPath;
	}
	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}
	
	public String getExcelPath() {
		return excelPath;
	}
	public void setExcelPath(String excelPath) {
		this.excelPath = excelPath;
	}
	
	public String getXmlPath() {
		return xmlPath;
	}
	public void setXmlPath(String xmlPath) {
		this.xmlPath = xmlPath;
	}
	
	public String getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}
	
	public String toString() {
		String buffer;
		buffer = "employer[ " + employer + " ]  ";
		buffer += "cutDate[ " + cutDate + " ]  ";
		buffer += "startShowDate[ " + startShowDate + " ]  ";
		buffer += "finalShowDate[ " + finalShowDate + " ]  ";
		buffer += "pdfPath[ " + pdfPath + " ]  ";
		buffer += "excelPath[ " + excelPath + " ]  ";
		buffer += "xmlPath[ " + xmlPath + " ]  ";
		buffer += "idProduct[ " + idProduct + " ]  ";
		return buffer;
	}
}
