package com.syc.persistence.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="IST_BIN")
public class IstBinDto {
	
	@Id
	@Column(name="IST_BIN")
	private String bin;
	
	@Column(name="IST_BAN_SIGLA")
	private String siglas;
	
	@Column(name="IST_BAN_NOMB")
	private String bankName;
	
	@Column(name="IST_BIN_PROD")
	private String product;


	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}

	public String getSiglas() {
		return siglas;
	}
	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}

	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	
	public String toString(){
		String buffer = null;
		buffer += "bin[ "+bin+" ]  ";
		buffer += "siglas[ "+siglas+"]  ";
		buffer += "bankName[ "+bankName+" ]  ";
		buffer += "product[ "+product+" ]  ";
		return buffer;
	}

}
