package com.syc.persistence.dao;

import com.syc.persistence.dto.AddressDto;

public interface AddressDao extends PersistenceDao<AddressDto,Long>{
	
	/**
	 * @param       => Pan
	 * @return      => AddressDto
	 * @description => Metodo que busca la direccion asociada a un tarjetahabiente
	 */
	public AddressDto findCardAddress( String pan );
	
	/**
	 * @param       => AddressDto
	 * @return      => int
	 * @description => Metodo que actualiza la direccion asociada a un tarjetahabiente
	 */
	public int sqlUpdate( AddressDto addressDto );
	
}
