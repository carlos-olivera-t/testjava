package com.syc.persistence.dao;

import com.syc.persistence.dto.EmployerDetailDto;

public interface EmployerDetailDao extends PersistenceDao<EmployerDetailDto, Long>{
	
	EmployerDetailDto findEmployer(String employerLoc, int issCode, String tiType);

}
