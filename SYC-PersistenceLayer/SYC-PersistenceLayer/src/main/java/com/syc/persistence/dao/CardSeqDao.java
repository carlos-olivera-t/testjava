package com.syc.persistence.dao;

import com.syc.persistence.dto.CardSeqDto;
 

public interface CardSeqDao extends PersistenceDao<CardSeqDto, String>{

	public long getMaxSeq(String bin, String product);
	
	public long getMaxSeqCta(String bin);
	
}