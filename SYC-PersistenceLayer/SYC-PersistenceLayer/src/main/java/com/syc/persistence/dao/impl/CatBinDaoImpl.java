package com.syc.persistence.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.syc.persistence.dao.CatBinDao;
import com.syc.persistence.dto.CatBinDto;


/**
 * <p>
 * DAO clase CardBinDto 
 * <p>
 * @version 1.2-SNAPSHOT 30/04/2012.
 */
@SuppressWarnings("unchecked")
@Repository("catBinDao")
public class CatBinDaoImpl extends AbstractPersistenceDaoImpl<CatBinDto, CatBinDto.CatBinPk> implements CatBinDao {

	
	@Autowired
	public CatBinDaoImpl( HibernateTemplate hibernateTemplate ){
		super(hibernateTemplate);
	}

	
	public int updateImage(String bin, String producto,byte[] image) {				
		return getSession().getNamedQuery("CatBinDto.updateImage")				
							  .setBinary("imagen", image)
							  .setString("bin", bin)
							  .setString("producto", producto)
							  .executeUpdate();				
	}
	

	/** 
	 * {@inheritDoc}
	 * @see com.syc.persistence.dao.CatBinDao#findBinByOwner(int)
	 */	
	public List<String> findBinByOwner(int idOwner) {
		return  getSession().getNamedQuery("catBinDto.findBinByOwner")
							.setInteger("idOwner", idOwner)
							.list();		
	}


	
	
	

}
