package com.syc.persistence.dao;

import java.sql.Date;

import com.syc.persistence.dto.UdisDto;

public interface UdisDao {
	
	/**
	 * Busca el ultimo dia en el que se actualizo el valor de la udi
	 * @param bin
	 * @param product
	 * @return
	 */
	Date findLastDayUdi(String bin, String product);
	
	
	/**
	 * Busca la ultima actualizacion de la udi
	 * @param bin
	 * @param product
	 * @param lastdate
	 * @return
	 */
	String findLastHourUdi(String bin, String product, Date lastDate);
	
	
	/**
	 * Busca el valor de la udi
	 * @param bin
	 * @param product
	 * @param lastDate
	 * @param lastTime
	 * @return
	 */
	UdisDto findLastUdi(String bin, String product, Date lastDate, String lastTime);

}
