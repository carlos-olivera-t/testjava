package com.syc.routes;

import org.apache.camel.builder.RouteBuilder;

public class NotificationsRoute extends RouteBuilder {
	
	public void configure(){
		//172.16.28.85 desarrollo
		//200.15.1.143 produccion
		
		from("seda:notifications")
		    .to("mina:tcp://200.15.1.143:7010?textline=true&sync=false&disconnect=false")
		    .end();		

	}
}
