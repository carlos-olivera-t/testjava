package com.syc.routes.codecs;

import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class NotificationsCodec implements ProtocolCodecFactory{

	@Override
	public ProtocolDecoder getDecoder() throws Exception {
		return new NotificationsDecoder();
	}

	@Override
	public ProtocolEncoder getEncoder() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
