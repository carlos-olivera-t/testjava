package com.syc.routes;

import org.apache.camel.builder.RouteBuilder;

public class OasisTcpRoute extends RouteBuilder{
	
	public void configure(){
		//172.16.1.34 produccion
		//172.16.28.131 desarrollo
		//172.16.10.15  DRP
		from("seda:oasis")
	    .to("mina:tcp://172.16.1.35:9009?textline=true&sync=true&disconnect=false")
	    .end();			
	}

}
 