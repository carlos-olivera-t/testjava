package com.syc.routes.codecs;

import org.apache.mina.common.ByteBuffer;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

public class NotificationsDecoder extends CumulativeProtocolDecoder{
	
	  protected boolean doDecode(IoSession session, ByteBuffer in, ProtocolDecoderOutput out) throws Exception {
//        if (in.remaining() >= PAYLOAD_SIZE) {
            byte[] buf = new byte[in.remaining()];
            in.get(buf);
            
            StringBuilder sb = new StringBuilder();
            sb.append(new String(buf, 0, buf.length));
            out.write(sb.toString());
            return true;
//        } else {
//            return false;
//        }
    }

}
