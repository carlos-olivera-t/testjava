package com.syc.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TreeMap;

import org.apache.commons.lang.RandomStringUtils;

public class GeneralUtil {
	
	public static void main(String ...args){
		String valor = generateAuthorizationNumberAlphaNumeric(16);
		System.out.println("Valor:" + valor);
		dateAndTime();
		
		System.out.println(convertStringToAmount("25030"));
		
		System.out.println(convertAmountToString(550.2));
	
	}
	
	public static String generateAuthorizationNumberAlphaNumeric(int randomLenght) {		
		return RandomStringUtils.random(randomLenght, true, true);
	}
	
	public static String generateAuthorizationNumber(int randomLenght) {
		return RandomStringUtils.random(randomLenght, false, true);	
	}

	public static double roundToDecimals(double d, int places) {
		return Math.round(d * Math.pow(10, (double) places))
				/ Math.pow(10, (double) places);
	}
	
	/*************************************************************
	 * Regresa un objeto sqlDate con la fecha actual
	 **************************************************************/
	public static java.sql.Date getCurrentSqlDate() {
		return new java.sql.Date(System.currentTimeMillis());
	}
	
	/*************************************************************
	 * Devuelve la hora actual en un formato especificado
	 **************************************************************/
	public static String getCurrentTime( String format ){
		 return formatTimeStamp(format, new Timestamp(System.currentTimeMillis()));
	 }

	/*************************************************************
	 * Regresa una cadena con el formato especificado *
	 **************************************************************/
	public static String formatDate(String format, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return date != null ? sdf.format(date) : "";
	}
	
	/*************************************************************
	 * Regresa una cadena con la fecha y hora del sistema *
	 **************************************************************/
	
	public static String dateAndTime(){
		 //format = ("dd/mm/yyyy hh-MM-ss");
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy h:mm:ss a");
		
		 Calendar calendarioAhora = Calendar.getInstance();

	        Date dateAhora = calendarioAhora.getTime();
	        String resultado = formato.format(dateAhora);
	        
	        //H:mm:ss
	        System.out.println(resultado);
		return resultado;
	}
	
	/**regresa mes y año actual**/
	public static String monthAndYear(){
		 //format = ("dd/mm/yyyy hh-MM-ss");
		SimpleDateFormat formato = new SimpleDateFormat("MM/yyyy");
		
		 Calendar calendarioAhora = Calendar.getInstance();

	        Date dateAhora = calendarioAhora.getTime();
	        String resultado = formato.format(dateAhora);
	        
	        //H:mm:ss
	        System.out.println(resultado);
		return resultado;
	}

	/******************************************************
	 * Genera un objeto fecha a partir de una cadena. *
	 ******************************************************/
	public static Date getDateFormat(String formato, String fecha) {
		try {
			SimpleDateFormat formatoFecha = new SimpleDateFormat(formato);
			return formatoFecha.parse(fecha);
		} catch (Exception ex) {
			// Logger.getLogger(UtilDate.class.getName()).log(Level.SEVERE,
			// null, ex);
		}
		return null;
	}

	/*************************************************************
	 * Regresa una cadena con el formato especificado *
	 **************************************************************/
	public static String formatTimeStamp(String format, Timestamp date) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	/***********************************
	 * Valida si una Cadena esta Vacia
	 ***********************************/
	public static boolean isEmtyString(String field) {
		if (field != null && field.trim().length() > 0) {
			return false;
		}
		return true;
	}
	
	
	/********************
	 * Compara 2 cadenas
	 ********************/
	public static boolean isEqualsString(String value, String strCompare) {
		if (value != null && strCompare != null) {
			value = value.trim().toUpperCase();
			strCompare = strCompare.trim().toUpperCase();
			if (!value.equals(strCompare))
				return false;
		} else
			return false;
		return true;
	}

	public static boolean containsBin(String bin, String card) {
		boolean flag = false;
		if (card.contains(bin)) {
			flag = true;
		}
		return flag;
	}
	
	/*******************************************
	 *   Formatea el Número de una Tarjeta 
	 *******************************************/
	public static String formatStringCardNumber( String cardNumber, String format ){
		String value = "";
		int j = 0;
			try{
				char[] charsPan    = cardNumber.toCharArray();
				char[] charsFormat = format.toCharArray();
				for( int i=0; i<charsFormat.length; i++ ){
					if( charsFormat[i] == ' ' ){
						value += " ";
					}else{
						value += charsPan[j];
						j++;
					}
				}
			}catch(Exception e){
				return value;
			}
			return value;
	}
	
	/***************************************************************
	 * Rellena de Caracteres una cadena a una Longitud determinada
	 ***************************************************************/
	public static String completaCadena( String cadena, char caracter, int longitud, String posicion ){
		String cadenaFinal = null;
		String aux         = "";
		if( cadena!=null ){
			if( cadena.length() > longitud ){
				cadenaFinal = cadena.substring(0,longitud);
			}else{
				for( int i=0; i<(longitud-cadena.length()); i++ ){
					aux += caracter;				  
				}
				cadenaFinal = posicion.equals("R") ? cadena+aux : aux+cadena;
			}
		}
		return cadenaFinal;
	}
	
	/***************************************************************
	 * Rellena de Caracteres una cadena a una Longitud determinada
	 ***************************************************************/
	
	public static String buildPrimaryAccount( String paramAccout, String accountPrefix ){
		int delta  = 0; 
		if( accountPrefix!=null ){
			accountPrefix = accountPrefix.trim();
			delta = SystemConstants.ACCOUNT_LENGTH - accountPrefix.length();
		}
		return accountPrefix += completaCadena(paramAccout, '0', delta, "L");
	}


	/***************************************************************
	 * Genera un numero de tarjeta
	 ***************************************************************/
	public static String buildNumberCard( String bin, String product, long cardSeq ){				
		int iValor = 0;
		int iNones = 0;
		int iPares = 0;
				
		StringBuilder sb = new StringBuilder();
		sb.append(bin).append(product).append(String.format("%07d", cardSeq));
		
		String valor = sb.toString().trim();
		
		for(int i=1; i<=15; i++){			
		    iValor= Integer.valueOf(valor.substring(i-1,i)).intValue();
			
		    if(i % 2 == 1){
		    	iValor = iValor * 2;
		    	
		    	if(iValor >= 10){
		    		 iValor =  iValor-9;
		    	}		    			      		    		    
		      iNones = iNones + iValor;
		    }
		    else{
		    	 iPares = iPares+ iValor;
		    }		    
		}
				
		iValor = 10 - (iNones + iPares) % 10 ;		
		
		if(iValor == 10){
			iValor = 0;
		}		 
		String card = valor + iValor;
		
		return card;
	}
	
	/*************************************
	 *  Convierte un String en un Entero
	 *************************************/
	public static int convertStringToInt( String value ){
		int integerValue = -1;
		if( value != null && value.matches("[0-9]*") ){
			integerValue = Integer.parseInt(value);
		}
		return integerValue;
	}
	
	/***************************************************
	 *  Valida que dos tarjeta sean del mismo Producto
	 ***************************************************/
	public static boolean areTheSameProducts( String firstPan, String secondPan ){
		final int POSITIONS_NUMBER = 8;
		
		boolean flag = false;
		if( firstPan!=null && secondPan!=null ){
			flag = isEqualsString(firstPan.substring(0, POSITIONS_NUMBER), secondPan.substring(0, POSITIONS_NUMBER));
		}
		return flag;
	}
	
	/*************************************************************
	 * Genera Fechas Inicial (Fecha de corte mes Anterior) y 
	 * Final (Fecha de corte Inicial - Numero de Meses)
	 * con  formato DD/MM/YYYY  
	 **************************************************************/
    @SuppressWarnings("static-access")
	public static TreeMap<Integer,String> getAccountStatementCutDates( int numberOfPeriods ) {  
    	 TreeMap<Integer,String> map = new TreeMap<Integer,String>();
    	 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    	 String currentDate = null;
    	 String startDate   = null;
    	 String endDate     = null;
    	 
         Calendar calendar = new GregorianCalendar();
         
         currentDate = sdf.format(calendar.getTime());
         
         /** Obteniendo la Fecha de Corte Inicial **/
         calendar.add(Calendar.MONTH,-1);
         calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(calendar.DAY_OF_MONTH));
         startDate= sdf.format(calendar.getTime());

         /** Obteniendo la Fecha de Corte Final **/
         calendar.add(Calendar.MONTH, (numberOfPeriods-1)*-1);
         calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(calendar.DAY_OF_MONTH));
         endDate = sdf.format(calendar.getTime());
         
         map.put(1, currentDate);
         map.put(2, startDate);
         map.put(3, endDate);
         
        return map;
         
     }

    public static Date addMonths(Date fecha, int months){
    	 Calendar calendar = new GregorianCalendar();
    	 calendar.setTime(fecha);
    	
    	 calendar.add(Calendar.MONTH, months);    	 
    	 
    	 return new Date(calendar.getTimeInMillis());
    }
    
    public static boolean isNumeric(String cadena){
    	try {
    		Long.parseLong(cadena);    		
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}
    }
    

    /***********************
     * Esta es una forma simple de poder convertir fecha
     * de formato String a formato java.sql.Date
     * @return 
     *
     *****************************/
    public static java.sql.Date fechaToString(String fecha, String formato){  
	 
	 
	 java.sql.Date fecFormatoDate = null;
	 try {
		 SimpleDateFormat sdf = new java.text.SimpleDateFormat(formato, new Locale("es", "ES"));
		 fecFormatoDate = new java.sql.Date(sdf.parse(fecha).getTime());
		
	 } catch (Exception ex) {
		 System.out.println("Error al obtener el formato de la fecha: " + ex.getMessage());
		 return null;
	 }
	 return fecFormatoDate;
    
 }
 
	 /********************************************************
	  * Convertir un String a SqlDate
	  ********************************************************/
	 
	 public static java.sql.Date convertStringToSqlDate( String date, String format ){
		 java.sql.Date sqltDate = null;
		 try{
			 DateFormat formater = new SimpleDateFormat( format );  
			 java.util.Date parsedUtilDate = formater.parse(date);  
			 sqltDate= new java.sql.Date(parsedUtilDate.getTime());  
		 }catch(Exception e){e.printStackTrace();}
		return sqltDate;
	 }
	 
	 /********************************************************
	  * Obtener Periodos de Tiempo para Consulta de Movimientos
	  ********************************************************/
	 
	 public static TreeMap<String,Date> getPeriodTime( int period, String format ){
			
			TreeMap<String,Date> map = new TreeMap<String,Date>();
			
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	
			int firstDay = - (calendar.get(Calendar.DATE) - 1);
			String startDate;
			String finalDate;
			
			if( period == 1 ){
				finalDate = dateFormat.format(calendar.getTime());
				calendar.roll(Calendar.DATE, firstDay);
				startDate = dateFormat.format(calendar.getTime());
				
			}else{
				calendar.roll(Calendar.MONTH, -(period - 1));
				calendar.roll(Calendar.DATE, firstDay);
				startDate = dateFormat.format(calendar.getTime());
				int finalDay = + (calendar.getActualMaximum(Calendar.DATE) - 1);
				calendar.roll(Calendar.DATE, finalDay);
				finalDate = dateFormat.format(calendar.getTime());			
			}
			
			Date starDateSql = null;
			Date finalDateSql = null;
			starDateSql = convertStringToSqlDate(startDate, format);
			finalDateSql = convertStringToSqlDate(finalDate, format);
			
			map.put("FechaInicial", starDateSql);
	        map.put("FechaFinal", finalDateSql);
	     
	        return map;
			
	 }
	 
	 /**
	 	 * Completa una cadena a la longitud indicada
	 	 * @param s Cadena
	 	 * @param n longitud final
	 	 * @param charFill Caracter para rellenar
	 	 * @return
	 	 */
	 	public static String padLeft(String s, int n, char charFill) {
		    return String.format("%1$" + n + "s", s).replace(' ', charFill );  
		}

	 	/***************************************************************
		 * Rellena de Caracteres una cadena a una Longitud determinada
		 ***************************************************************/
		public static String rellenaCadena( String cadena, char caracter, int longitud, String posicion ){
			String cadenaFinal = null;
			String aux         = "";
			if( cadena!=null ){
				if( cadena.length() > longitud ){
					cadenaFinal = cadena.substring(0,longitud);
				}else{
					for( int i=0; i<(longitud-cadena.length()); i++ ){
						aux += caracter;				  
					}
					cadenaFinal = posicion.equals("R") ? cadena+aux : aux+cadena;
				}
			}
			return cadenaFinal;
		}		
		
		/*********************************************
		 * Función que crea un folio de error ante 
		 *     una excepción de la aplicacion
		 *********************************************/
		public static String getFolioErrorApp(){
			return getCurrentTime("ddMMyy") +"-"+generateAuthorizationNumber(6)+"-"+getCurrentTime("HHmmss");
		}
		
		
		/**
		 * metodo que convierte un String a tipo de dato double
		 * @param strAmount
		 * @return
		 */
		public static double convertStringToAmount(String strAmount) {
			double amount = 0;
			try{
				amount = Double.parseDouble(strAmount)/100;
			}catch(NumberFormatException e){
				return -1;
			}
			return amount;
		}

		/**
		 * metodo que convierte un tipo de dato double a String
		 * @param amount
		 * @return
		 */
		public static String convertAmountToString(double amount) {
			NumberFormat formatter = new DecimalFormat("#0.00");
			String strAmount = formatter.format(amount).replace(".", "");
			return strAmount;
		}
		
		
		/** Metodo que convierte una cadena en un objeto util Date **/
		public static java.util.Date convertStringToUtilDate(String format, String date){

			java.util.Date utilDate = null;

			try{
				if( format!= null && date!=null ){
					utilDate = new SimpleDateFormat(format).parse(date);
				}
			}catch(Exception ex){
				ex.printStackTrace();
				return utilDate;
			}
			return utilDate;
		}

}

