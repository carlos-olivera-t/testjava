package com.syc.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

public class SecurityUtils {

	/**
	 * Método que obtine el usuario que esta logueado
	 * @return
	 */
	public static String getCurrentUser(){
		return SecurityContextHolder.getContext().getAuthentication().getName();				 						
	}
	
	public static String getRemoteIP(){
		String remoteAddress = "N/D";
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if(authentication != null){
			WebAuthenticationDetails details = (WebAuthenticationDetails) authentication.getDetails();
			remoteAddress = details.getRemoteAddress();
		}

     return remoteAddress;
	}

}
