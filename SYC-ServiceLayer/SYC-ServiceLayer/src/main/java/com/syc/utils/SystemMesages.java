package com.syc.utils;

public class SystemMesages {
	
	/** Mensajes generales para respuestas en servicios*/
	public static final String SERVICE_OK = "catalog.general.serviceOK";
	public static final String INVALID_FIELD_SIZE = "catalog.general.invalidSize";
	public static final String IVALIDA_CARD = "catalog.general.noCard";
	
	/** Mesajes genearales */
	public static final String OK = "catalog.general.ok";
	
	/** Nombres de campos */
	public static final String CP = "catalog.field.cp";
	public static final String ADDRESS = "catalog.field.direccion";
    public static final String SHORT_NAME = "catalog.field.nombreCorto";
	public static final String LARGE_NAME = "catalog.field.nombreLargo";
	public static final String RFC = "catalog.field.rfc";
	public static final String PHONE = "catalog.field.telefono";
	public static final String ENTIDAD = "catalog.field.entidad";

}
