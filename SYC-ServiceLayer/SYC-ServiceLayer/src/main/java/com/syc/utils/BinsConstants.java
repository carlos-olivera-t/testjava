package com.syc.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BinsConstants
{
  private Map<String, List<String>> mapaBins = new HashMap();
  
  @SuppressWarnings({ "rawtypes", "unchecked" })
public BinsConstants()
  {
    ArrayList localArrayList1 = new ArrayList();
    

    localArrayList1.add("ws_nemesis");
    localArrayList1.add("nemesis");
    localArrayList1.add("master");
    this.mapaBins.put("50628101", localArrayList1);
    

    ArrayList localArrayList2 = new ArrayList();
    localArrayList2.add("ws_snicolas");
    localArrayList2.add("snicolas");
    localArrayList2.add("master");
    this.mapaBins.put("50624701", localArrayList2);
    

    ArrayList localArrayList3 = new ArrayList();
    localArrayList3.add("prodic_ws_user");
    localArrayList3.add("ws_adelanto");
    localArrayList3.add("master");
    this.mapaBins.put("53398709", localArrayList3);
    

    ArrayList localArrayList4 = new ArrayList();
    localArrayList4.add("prodic_akala");
    localArrayList4.add("master");
    localArrayList4.add("akala_ws");
    localArrayList4.add("ws_reyan");
    this.mapaBins.put("50619901", localArrayList4);
    this.mapaBins.put("50619960", localArrayList4);
    


    ArrayList localArrayList5 = new ArrayList();
    localArrayList5.add("ws_cibanco");
    localArrayList5.add("pruebaCiBanco");
    localArrayList5.add("master");
    this.mapaBins.put("46006800", localArrayList5);
    this.mapaBins.put("46006801", localArrayList5);
    

    ArrayList localArrayList6 = new ArrayList();
    localArrayList6.add("ws_cristobal_colon");
    localArrayList6.add("cristobal_colon");
    localArrayList6.add("master");
    this.mapaBins.put("50624501", localArrayList6);
    

    ArrayList localArrayList7 = new ArrayList();
    localArrayList7.add("ws_ucnl");
    localArrayList7.add("ucnlUser");
    localArrayList7.add("master");
    this.mapaBins.put("50626601", localArrayList7);
    

    ArrayList localArrayList8 = new ArrayList();
    localArrayList8.add("ws_edenred");
    localArrayList8.add("anonymousUser");
    localArrayList8.add("edenRed");
    localArrayList8.add("master");
    this.mapaBins.put("50630301", localArrayList8);
    this.mapaBins.put("50630302", localArrayList8);
    this.mapaBins.put("50630303", localArrayList8);
    this.mapaBins.put("50630304", localArrayList8);
    this.mapaBins.put("63631811", localArrayList8);
    this.mapaBins.put("63631803", localArrayList8);
    this.mapaBins.put("55830603", localArrayList8);
    this.mapaBins.put("55830601", localArrayList8);
    this.mapaBins.put("63631813", localArrayList8);
    this.mapaBins.put("63631801", localArrayList8);
    this.mapaBins.put("55830605", localArrayList8);
    this.mapaBins.put("63631804", localArrayList8);



    ArrayList localArrayList9 = new ArrayList();
    localArrayList9.add("ws_galicia");
    localArrayList9.add("nva_galicia");
    localArrayList9.add("master");
    this.mapaBins.put("50632001", localArrayList9);
    

    ArrayList localArrayList10 = new ArrayList();
    localArrayList10.add("centralUser");
    localArrayList10.add("ws_central");
    localArrayList10.add("master");
    this.mapaBins.put("50633201", localArrayList10);
    this.mapaBins.put("50633202", localArrayList10);
    

    ArrayList localArrayList11 = new ArrayList();
    localArrayList11.add("ws_auxi");
    localArrayList11.add("master");
    this.mapaBins.put("50633701", localArrayList11);
    this.mapaBins.put("50633702", localArrayList11);
    this.mapaBins.put("50633703", localArrayList11);
    this.mapaBins.put("50633704", localArrayList11);
    this.mapaBins.put("50633705", localArrayList11);
    this.mapaBins.put("50633708", localArrayList11);
    


    ArrayList localArrayList12 = new ArrayList();
    localArrayList12.add("cicashUser");
    localArrayList12.add("ws_cicash");
    localArrayList12.add("master");
    this.mapaBins.put("47464601", localArrayList12);
    this.mapaBins.put("47464600", localArrayList12);
    

    ArrayList localArrayList13 = new ArrayList();
    localArrayList13.add("ws_fam");
    localArrayList13.add("famUser");
    localArrayList13.add("master");
    this.mapaBins.put("50633501", localArrayList13);
    

    ArrayList localArrayList14 = new ArrayList();
    localArrayList14.add("ws_tecol");
    localArrayList14.add("tecolUser");
    localArrayList14.add("master");
    this.mapaBins.put("50628301", localArrayList14);
    

    ArrayList localArrayList15 = new ArrayList();
    localArrayList15.add("ws_eco");
    localArrayList15.add("ecoUser");
    localArrayList15.add("master");
    this.mapaBins.put("50631901", localArrayList15);
    

    ArrayList localArrayList16 = new ArrayList();
    localArrayList16.add("ws_hostio");
    localArrayList16.add("hostioUser");
    localArrayList16.add("master");
    this.mapaBins.put("50629501", localArrayList16);
    

    ArrayList localArrayList17 = new ArrayList();
    localArrayList17.add("ws_guachi");
    localArrayList17.add("guachiUser");
    localArrayList17.add("master");
    this.mapaBins.put("50633401", localArrayList17);
    

    ArrayList localArrayList18 = new ArrayList();
    localArrayList18.add("ws_acreime");
    localArrayList18.add("acreimUser");
    localArrayList18.add("master");
    this.mapaBins.put("50634301", localArrayList18);
    this.mapaBins.put("50634300", localArrayList18);
    

    ArrayList localArrayList19 = new ArrayList();
    localArrayList19.add("ws_pagat");
    localArrayList19.add("pagatUser");
    localArrayList19.add("ws_crm");
    localArrayList19.add("crmUser");
    localArrayList19.add("master");
    this.mapaBins.put("53898401", localArrayList19);
    this.mapaBins.put("53898402", localArrayList19);
    this.mapaBins.put("53898403", localArrayList19);
    this.mapaBins.put("53898404", localArrayList19);
    this.mapaBins.put("53898406", localArrayList19);
    


    ArrayList localArrayList20 = new ArrayList();
    localArrayList20.add("ws_cvalle");
    localArrayList20.add("valleUser");
    localArrayList20.add("master");
    this.mapaBins.put("50634200", localArrayList20);
    this.mapaBins.put("50634201", localArrayList20);
    

    ArrayList localArrayList21 = new ArrayList();
    localArrayList21.add("ws_csierra");
    localArrayList21.add("sierraUser");
    localArrayList21.add("master");
    this.mapaBins.put("50634400", localArrayList21);
    this.mapaBins.put("50634401", localArrayList21);
    


    ArrayList localArrayList22 = new ArrayList();
    localArrayList22.add("master");
    localArrayList22.add("auxiUser");
    this.mapaBins.put("50633701", localArrayList22);
    

    ArrayList localArrayList23 = new ArrayList();
    localArrayList23.add("perseverancia");
    localArrayList23.add("master");
    this.mapaBins.put("50635600", localArrayList23);
    this.mapaBins.put("50635601", localArrayList23);
    

    ArrayList localArrayList24 = new ArrayList();
    localArrayList24.add("ws_maria");
    localArrayList24.add("master");
    this.mapaBins.put("50635201", localArrayList24);
    this.mapaBins.put("50635200", localArrayList24);
    

    ArrayList localArrayList25 = new ArrayList();
    localArrayList25.add("huejiquilla");
    localArrayList25.add("master");
    this.mapaBins.put("50636401", localArrayList25);
    this.mapaBins.put("50636400", localArrayList25);
    
    ArrayList localArrayList26 = new ArrayList();
    localArrayList26.add("huerta");
    localArrayList26.add("master");
    localArrayList26.add("huertaUser");
    this.mapaBins.put("50638401", localArrayList26);
    this.mapaBins.put("50636400", localArrayList26);
    
    ArrayList localArrayList27 = new ArrayList();
    localArrayList27.add("fray_J");
    localArrayList27.add("master");
    localArrayList27.add("frayUser");
    this.mapaBins.put("50638601", localArrayList27);
    this.mapaBins.put("50638600", localArrayList27);
    
    ArrayList localArrayList28 = new ArrayList();
    localArrayList28.add("unifarm");
    localArrayList28.add("master");
    localArrayList28.add("uniUser");
    this.mapaBins.put("50638301", localArrayList28);
    this.mapaBins.put("50638300", localArrayList28);
    
    ArrayList localArrayList29 = new ArrayList();
    localArrayList29.add("ws_sodexo");
    localArrayList29.add("ws_servibonos");
    localArrayList29.add("master");
    localArrayList29.add("sodexo_ivr");
    localArrayList29.add("anonymousUser");
    this.mapaBins.put("63938810", localArrayList29);
    this.mapaBins.put("63938820", localArrayList29);
    this.mapaBins.put("50633601", localArrayList29);
    this.mapaBins.put("50633606", localArrayList29);
    this.mapaBins.put("50627330", localArrayList29);
    this.mapaBins.put("50627320", localArrayList29);
    this.mapaBins.put("50627310", localArrayList29);
    
    
    ArrayList localArrayList30 = new ArrayList();
    localArrayList30.add("cj_guadalupana");
    localArrayList30.add("master");
    //localArrayList30.add("uniUser");
    this.mapaBins.put("50640201", localArrayList30);
    this.mapaBins.put("50640200", localArrayList30);
    
  }
  
  public List<String> getUsuarios(String paramString)
  {
	  List<String> localObject = new ArrayList();
    if (getMapaBins().containsKey(paramString)) {
      localObject = (List)getMapaBins().get(paramString);
    }
    return localObject;
  }
  
  public Map<String, List<String>> getMapaBins()
  {
    return this.mapaBins;
  }
  
  public void setMapaBins(Map<String, List<String>> paramMap)
  {
    this.mapaBins = paramMap;
  }
}
