package com.syc.utils;

import java.util.TreeMap;

public class CatalogUtil {

	public static TreeMap<Integer, String> getCatalogDBSatusDescription(){
		TreeMap<Integer, String> cat = new TreeMap<Integer, String>();
		cat.put(new Integer(1), "CANCELADA");
		cat.put(new Integer(2), "INACTIVA");
		cat.put(new Integer(3), "ROBO");
		cat.put(new Integer(4), "EXTRAVIO");
		cat.put(new Integer(5), "MALTRATADA");
		cat.put(new Integer(6), "NORMAL");
		cat.put(new Integer(7), "BLOQUEADA POR INTENTOS DE NIP");
		return cat;
	}
	
	
	public static TreeMap<Integer, String> getCatalogDBSatus(){
		TreeMap<Integer, String> cat = new TreeMap<Integer, String>();
		cat.put(new Integer(60), "CANCELADA");
		cat.put(new Integer(51), "INACTIVA");
		cat.put(new Integer(54), "ROBO");
		cat.put(new Integer(52), "EXTRAVIO");
		cat.put(new Integer(56), "MALTRATADA");
		cat.put(new Integer(50), "ACTIVADA");
		cat.put(new Integer(75), "BLOQUEADA POR INTENTOS DE NIP");
		cat.put(new Integer(59), "BLOQUEADA TEMPORALMENTE");
		
		return cat;
	}
	
	/**
	 * Devuelve el catalogo del servicio de Activacion de Tarjeta
	 */
	public static TreeMap<Integer,String> getCatalogActivationCard(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Activacion exitosa.");
		cat.put(new Integer(2), "Tarjeta no Registrada.");
		cat.put(new Integer(3), "La Tarjeta ya fue activada con anterioridad.");
		return cat;
	}
	
	/**
	 * Devuelve el catalogo del servicio de Activacion de Tarjeta
	 */
	public static TreeMap<Integer,String> getCatalogBalanceQueryByAccount(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Cuenta Registrada.");
		cat.put(new Integer(2), "Cuenta no Registrada.");
		cat.put(new Integer(3), "Cuenta no asociada.");
		cat.put(new Integer(4), "Prefijo no encontrado.");
		cat.put(new Integer(5), "Cuenta no existe.");
		return cat;
	}
	
	/**
	 * Devuelve el catalogo del servicio de Bloqueo de Tarjeta
	 */
	public static TreeMap<Integer,String> getCatalogCardLock(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Operacion realizada con exito.");
		cat.put(new Integer(2), "Tarjeta no Registrada.");
		cat.put(new Integer(3), "La Tarjeta no se encuentra Activa.");
		cat.put(new Integer(4), "Tipo de Operacion no Valida.");
		cat.put(new Integer(5), "Estatus no válido para la operación.");
		cat.put(new Integer(6), "Cuenta no registrada.");
		return cat;
	}
	
	public static TreeMap<Integer,String> getCatalogAccountTemLock(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Operacion realizada con exito.");
		cat.put(new Integer(2), "Cuenta no existe.");
		cat.put(new Integer(3), "La cuenta no se encuentra Activa.");
		cat.put(new Integer(9), "Bin no encontrado.");
		cat.put(new Integer(4), "operación no permitida.");
		return cat;
	}
	/**
	 * Devuelve el catalogo del servicio de Recarga de Saldo
	 */
	
	public static TreeMap<Integer,String> getCatalogDepositAccount(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Recarga realizada con exito.");
		cat.put(new Integer(2), "Tarjeta Inactiva.");
		cat.put(new Integer(3), "La cuenta no existe.");
		cat.put(new Integer(4), "Tarjeta Expirada.");
		cat.put(new Integer(6), "El monto a depositar es incorrecto.");
		cat.put(new Integer(8), "Tarjeta Inexistente.");
		cat.put(new Integer(15), "Campo description demasiado largo.");
		cat.put(new Integer(16), "Campo reference demasiado largo.");
		cat.put(new Integer(17), "No fue posible realizar deposito, sobrepasa limite general.");
		cat.put(new Integer(18), "No fue posible realizar deposito, sobrepasa limite diario.");
		cat.put(new Integer(19), "Cuenta inactiva.");
		cat.put(new Integer(101), "El campo PAN es obligatorio.");
		cat.put(new Integer(102), "El importe a recargar es Incorrecto.");
		cat.put(new Integer(50), "No se pudo realizar transaccion, intentelo de nuevo");
		
		return cat;
	}
	
	/************************************************************
	 * Devuelve el catalogo del servicio de Notificaciones para CIBanco
	 ************************************************************/
	
	public static TreeMap<Integer,String> getCatalogNotificationsCiBanco(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Operacion exitosa");
		cat.put(new Integer(2), "la tarjeta no es valida");
		cat.put(new Integer(3), "El campo correspondiente al bin no es valido para CIBanco");
		cat.put(new Integer(4), "El campo correspondiente a la tarjeta deben ser numeros");
		cat.put(new Integer(5), "La cadena no empieza correctamente (SYC)");
		cat.put(new Integer(6), "La cadena no tiene la longitud establecida");

		cat.put(new Integer(7), "El tipo de operacion es invalido");
		cat.put(new Integer(8), "El campo despues del numero de tarjeta debe estar vacio");
		cat.put(new Integer(9), "El registro no contiene mail");	
		cat.put(new Integer(10),"No se pudo establecer la conexion");
		cat.put(new Integer(11),"No se pudo actualizar el estatus");
		cat.put(new Integer(12),"numero de tarjeta inexistente");
		cat.put(new Integer(13),"No se pudo enviar el correo");
		cat.put(new Integer(14),"Mail no tiene la estructura apropiada");
		cat.put(new Integer(15),"No se encontraron datos demograficos");
		
		
		return cat;
	}

	/************************************************************
	 * Devuelve el catalogo del servicio de Retiros
	 ************************************************************/
	public static TreeMap<Integer,String> getCatalogWithdrawalAccount(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Retiro realizado con exito.");
		cat.put(new Integer(2), "Tarjeta Inactiva.");
		cat.put(new Integer(3), "La cuenta no existe.");
		cat.put(new Integer(4), "Tarjeta Expirada.");
		cat.put(new Integer(5), "Fondos Insuficientes.");
		cat.put(new Integer(6), "El monto a retirar es incorrecto.");
		cat.put(new Integer(8), "Tarjeta Inexistente.");
		cat.put(new Integer(9), "Cuenta inactiva.");
		cat.put(new Integer(15), "Campo description demasiado largo.");
		cat.put(new Integer(16), "Campo reference demasiado largo.");
		cat.put(new Integer(20), "Codigo de monto incorrecto.");
		cat.put(new Integer(21), "Codigo de comision incorrecto.");
		cat.put(new Integer(22), "Prefijo no encontrado.");
		cat.put(new Integer(50), "No se pudo realizar transaccion, intentelo de nuevo");
	
		return cat;
	}
	
	public static TreeMap<Integer,String> getCatalogTransactions(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(0), "No se encontraron Movimientos con esta Tarjeta");
		cat.put(new Integer(1), "Movimientos Registrados.");
		cat.put(new Integer(2), "Tarjeta Inactiva");
		cat.put(new Integer(3), "Tarjeta no registrada");
		cat.put(new Integer(4), "parametros inválidos");
		cat.put(new Integer(5), "Prefijo no encontrado.");
		cat.put(new Integer(6), "Se encontraron mas de 2 tarjetas asociadas a la cuenta, intentar por numero de tarjeta.");
		return cat;
	}
	
	/************************************************************
	 * Devuelve el catalogo del servicio de transferencias de tarjetas
	 ************************************************************/
	public static TreeMap<Integer,String> getCatalogTransferCards(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Transferencia realizada con exito.");
		cat.put(new Integer(2), "se encuentra inactiva.");
		cat.put(new Integer(3), "La cuenta no existe.");
		cat.put(new Integer(4), "se encuentra expirada.");
		cat.put(new Integer(5), "no cuenta con saldo suficiente.");
		cat.put(new Integer(7), "No fue posible realizar la transferencia.");
		cat.put(new Integer(8), " Inexistente.");
		cat.put(new Integer(9), "El monto a transferir debe ser mayor a 0.00.");
		
		return cat;
	}
	
	/************************************************************
	 * Devuelve el catalogo del servicio de Consulta de Estatus
	 ************************************************************/
	public static TreeMap<Integer,String> getCatalogCardStatus(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(0) , "Tarjeta no Registrada.");
		cat.put(new Integer(60), "Tarjeta Cancelada.");
		cat.put(new Integer(51), "Tarjeta Inactiva.");
		cat.put(new Integer(54), "Tarjeta Robada.");
		cat.put(new Integer(52), "Tarjeta Extraviada.");
		cat.put(new Integer(56), "Tarjeta Maltratada.");
		cat.put(new Integer(50), "Tarjeta Activa.");
		cat.put(new Integer(57), "Tarjeta Reemplazada.");
		cat.put(new Integer(59), "Tarjeta Bloqueada Temporalmente.");
		cat.put(new Integer(75), "Tarjeta Bloqueada por Intentos de NIP.");
		return cat;
	}
	
	/************************************************************
	 * Devuelve el catalogo del servicio de Busqueda de clientes
	 ************************************************************/
	public static TreeMap<Integer,String> getCatalogSearchClient(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1) , "Operación realizada con éxito.");
		cat.put(new Integer(2), "No se encontraron coincidencias.");
		cat.put(new Integer(4), "Parametros no válidos.");
		return cat;
	}
	
	/************************************************************
	 * Devuelve el catalogo del servicio de Monex-Traspasos
	 ************************************************************/
	public static TreeMap<Integer,String> getCatalogTransfer(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(0) , "Aceptado.");
		cat.put(new Integer(1) , "Cuenta origen no concentradora.");
		cat.put(new Integer(2) , "Cuenta origen cancelada.");
		cat.put(new Integer(3) , "Cuenta origen saldo insuficiente.");
		cat.put(new Integer(4) , "Cuenta destino no concentradora.");
		cat.put(new Integer(5) , "Cuenta destino cancelada.");
		cat.put(new Integer(6) , "Cuentas de diferente producto.");
		cat.put(new Integer(7) , "Saldo Cero.");
		cat.put(new Integer(9) , "Cuenta origen inexistente.");
		cat.put(new Integer(11), "Cuenta destino inexistente.");
		cat.put(new Integer(12), "Cuenta origen y cuenta destino no puede ser la misma.");
		cat.put(new Integer(13),"Tipo de movimiento invalido.");		
		cat.put(new Integer(14), "Tarjeta origen invalida para proceso.");		
		cat.put(new Integer(15), "Tarjeta Destino invalida para proceso.");		
		cat.put(new Integer(16), "Error no manejado.");
				
		return cat;
	}
	
	/************************************************************
	 * Devuelve el catalogo del servicio de Monex-Traspasos
	 ************************************************************/
	public static TreeMap<Integer,String> getCatalogTransfersSpei(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1) , "Transferencia realizada con exito");
		cat.put(new Integer(2) , "No se encontro registro de cuenta origen.");
		cat.put(new Integer(3) , "No se encontro registro de cuenta destino.");
		cat.put(new Integer(4) , "Saldo cuenta destino sobrepasa limite general.");
		cat.put(new Integer(5) , "Saldo cuenta destino sobrepasa limite mensual.");
		cat.put(new Integer(6) , "Cuentas no asociadas.");
		cat.put(new Integer(7) , "Cuenta origen saldo insuficiente.");
		cat.put(new Integer(8) , "Cuenta origen y destino iguales.");
		cat.put(new Integer(9) , "Prefijo no encontrado.");
		cat.put(new Integer(10) , "Cuenta destino no tiene nivel de captación.");
		cat.put(new Integer(13), "monto debe ser mayor a 0.");
		cat.put(new Integer(14), "cuenta bloqueada o cancelada");
		
		return cat;
	}
	
	/************************************************************
	 * Devuelve el catalogo del servicio de Asignacion de Cuentas
	 ************************************************************/
	public static TreeMap<Integer,String> getCatalogAssignmentAccount(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1) , "Alta realizada con exito.");
		cat.put(new Integer(2) , "Tarjeta no registrada.");
		cat.put(new Integer(3) , "La tarjeta a procesar ya fue asignada previamente.");
		cat.put(new Integer(4) , "La cuenta ya se encuentra asignada.");
		cat.put(new Integer(5) , "No existe una tarjeta titular");
		cat.put(new Integer(6) , "La tarjeta a reemplzar esta activa");
		cat.put(new Integer(7) , "No existe la tarjeta a remplazar");
		cat.put(new Integer(8) , "La cuenta no esta activa");
		cat.put(new Integer(9) , "No se encuentra cuenta de tarjeta a remplazar");
		cat.put(new Integer(11) , "Id de cliente ya existe");
		cat.put(new Integer(12) , "Id de cliente no encontrado");
		cat.put(new Integer(14) , "Producto no válido");
		cat.put(new Integer(15) , "no se logro obtener número de cuenta");
		cat.put(new Integer(16) , "producto invalido para personas físicas");
		cat.put(new Integer(17) , "campo phoneCode inválido");
		cat.put(new Integer(18) , "productos diferentes");
		cat.put(new Integer(19) , "Empleado no encontrado");
		cat.put(new Integer(20) , "Empleado ya existe");
		cat.put(new Integer(21) , "Empleadora no encontrada");
		cat.put(new Integer(22) , "Tarjeta Reemplazar(Nueva) inexistente.");
		cat.put(new Integer(23) , "Codigo de domicilio no encontrado.");


		
		
		return cat;
	}
	
	/************************************************************
	 * Devuelve el customer_id dependiendo el cardtype
	 ************************************************************/
	public static TreeMap<String,String> getCatalogCustomerId(){
		TreeMap<String,String> cat = new TreeMap<String,String>();
		cat.put(new String("0001") , "1");
		cat.put(new String("0002") , "4");
		cat.put(new String("0003") , "4");
		cat.put(new String("0004") , "4");
		cat.put(new String("0006") , "2");
		
		return cat;
	}
	
	/***************************************************************
	 * Devuelve el catalogo del servicio de CardholderRegistration
	 ***************************************************************/
	public static TreeMap<Integer,String> getCardholderRegistration(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(0), "Tarjeta no Registrada.");
		cat.put(new Integer(1), "Actualizacion exitosa.");
		cat.put(new Integer(2), "El nombre excede la longitud maxima permitida.");
		cat.put(new Integer(3), "La tarjeta ya se encuentra Activa.");
		return cat;
	}
	
	/*********************************************************************
	 * Devuelve el catalogo del servicio para consulta de Saldo de Akala
	 *********************************************************************/
	public static TreeMap<Integer,String> getAkalaSaldo(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(0), "Error al procesar la peticion con Akala.");
		cat.put(new Integer(1), "Finalizo Correctamente.");
		cat.put(new Integer(2), "Cuenta no encontrada.");
		cat.put(new Integer(3), "Cuenta en estado Revisada.");
		cat.put(new Integer(4), "Cuenta en estado Cancelada.");
		return cat;
	}
	
	public static TreeMap<Integer,String> getAkalaDepositAccount(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(0), "Error al procesar la peticion con Akala.");
		cat.put(new Integer(1), "Finalizo Correctamente.");
		cat.put(new Integer(2), "El Monto debe ser mayor a Cero");
		cat.put(new Integer(3), "Cuenta No Encontrada");
		cat.put(new Integer(4), "Cuenta en estado Revisada");
		cat.put(new Integer(5), "Cuenta en estado Cancelada");
		cat.put(new Integer(6), "Fondos Insuficientes");
		cat.put(new Integer(7), "No se pudo afectar el Saldo");
		cat.put(new Integer(8), "No se pudo insertar el Movimiento");
		
		return cat;
	}
	
	public static TreeMap<String, String> getAccountStatementsProducts(){
		TreeMap<String, String> productos = new TreeMap<String, String>();
		productos.put("01", "GASOLINA");
		productos.put("02", "VIAJES");
		productos.put("03", "DOLARES");
		productos.put("04", "RESTAURANTE");
		productos.put("05", "MIS_COMPRAS");
		productos.put("06", "UNIFORMES");
		productos.put("07", "GASOLINA_FLEET");
		productos.put("08", "DIESEL_FLEET");
		productos.put("09", "VIAJES_FLEET");
		productos.put("10", "VIAJES_MONEX");
		productos.put("12", "VALE_ELECTRON");
		productos.put("13", "C_VIAJES_SIVALE");
		productos.put("14", "C_RESTAURANTE");
		productos.put("15", "C_UNIFORME");
		productos.put("16", "CONSUMO_COMBUSTIBLE");
		productos.put("17", "CFE_VALE_ELECTRONICO");
		productos.put("18", "RESTAURANTE_MARCA_PROPIA");
		productos.put("19", "GASOLINA_FLEET_PREMIUM");
		productos.put("20", "TURBOSINA_FLEET");
		productos.put("21", "GAS_LP_FLEET");
		
		
		return productos;
	}
	
	/**
	 * Devuelve el catalogo del servicio de Busqueda de Tarjetahabiete IVR Paga Todo
	 */
	public static TreeMap<Integer,String> getCatalogIVRCardInfo(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Tarjetahabiente encontrado");
		cat.put(new Integer(2), "Tarjetahabiente no encontrado.");
		cat.put(new Integer(3), "Este prodcto opera solo con cuenta.");
		cat.put(new Integer(4), "Este producto solo opera con tarjeta.");
		return cat;
	}
	/**
	 * Catalogo de descripciones para el servicio de bloqueo de cuenta
	 * @return
	 */
	public static TreeMap<Integer, String> getCatalogAccountLock(){
        TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
        cat.put(new Integer(0), "Operacion realizada con exito.");
        cat.put(new Integer(1), "Cuenta no válida");
        cat.put(new Integer(2), "No se encontraron datos para la cuenta");
        cat.put(new Integer(3), "La cuenta no esta activa");
        cat.put(new Integer(4), "Saldo disponible mayor a 0.0");
        
        return cat;
    }

	/**
	 * Catalogo de descripciones para la busqueda de tarjetas
	 * para cicash
	 * @return
	 */
	public static TreeMap<Integer,String> getCatalogFindDetailPans(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(1), "Tarjetas encontradas.");
		cat.put(new Integer(5), "cvv inválido.");
		cat.put(new Integer(6), "bin no encontrado.");
		cat.put(new Integer(7), "tarjeta no encontrada.");
		cat.put(new Integer(8), "La tarjeta debe estar activa");
		cat.put(new Integer(9), "No se encontro tipo de tarjeta");
		return cat;
	}
	
}

