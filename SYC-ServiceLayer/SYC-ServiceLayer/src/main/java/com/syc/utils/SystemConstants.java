
package com.syc.utils;

public class SystemConstants {
	
	public final static int    RANDOM_LENGHT   = 6;
	public final static String WS_USER_NAME    = "ws_user";
	public final static double IVA             = 16;
	public final static int    ACCOUNT_LENGTH  = 19;
	
	/********************************
	 * Catalogo de bines para niveles de captacion
	 * 
	 ***********************************/
	
	public final static String CAPTATION_LEVEL_BIN = "0099999901";
	
	/**clave emisor generica para transacciones**/
	public final static int CODIGO_GENERICO = 99;
	
	/*****************************
	 *     Catalogo de tipos en INVEX_MESSAGETYPE
	 *****************************/
	public final static int TYPE_MESSAGE_TEXT        = 1;
	public final static int TYPE_MESSAGE_QUESTION    = 2;
	public final static int TYPE_MESSAGE_LOCK        = 3;
	public final static int TYPE_MESSAGE_REPLACEMENT = 4;
	public final static int TYPE_MESSAGE_UNLOCK      = 5;
	public final static int TYPE_MESSAGE_REQ_INCRESE_LIMIT = 7;
	
	
	public final static int TYPE_MESSAGE_STATUS_EMPTY = 0;
	
	/*****************************
	 *     CardStatus
	 *****************************/
	public final static int STATUS_ACTIVE_CARD    		= 50;
	public final static int STATUS_INACTIVE_CARD  		= 51;
	public final static int STATUS_STOLEN_CARD    		= 54;
	public final static int STATUS_LOST_CARD      		= 52;
	public final static int STATUS_DAMAGED_CARD   		= 56; //DaÃ±ada
	public final static int STATUS_REPLACED_CARD 		= 57;
	public final static int STATUS_TEMPORARY_LOCK 		= 59;
	public final static int STATUS_CANCEL_CARD    		= 60;
	public final static int STATUS_LOCK_BY_NIP_CARD    	= 75;
	
	
	/*****************************
	 *     AccountStatus
	 *****************************/
	public final static String STATUS_ACTIVE_ACCOUNT    		= "00";
	public final static String STATUS_CANCEL_ACCOUNT    		= "60"; //TODO: Validar este codigo
	public final static String STATUS_TEMPORARY_LOCK_ACCOUNT    = "05";
	
	
	/*****************************
	 *     Formato Fecha
	 *****************************/
    public static final String FORMATO_DDMMAAAA  = "dd/MM/yyyy";
    public static final String FORMATO_FECHA_DEF = "ddMMyyyy";
	
    /*****************************
	 *  CÃ³digo Tipo de tarjeta
	 *****************************/
    public static final String CARDTYPE_TITULAR   = "0";
    public static final String CARDTYPE_ADICIONAL = "1";
    
	/*****************************
	 *     Status Base de Datos
	 *****************************/
	public final static int STATUS_ACTIVE_FIELD = 1;
	public final static int STATUS_INACTIVE_FIELD = 0;
	
	/*****************************
	 *     Validators
	 *****************************/
	public final static int VALIDATE_ACTIVE_CARD     = 2;
	public final static int VALIDATE_EXISTS_ACCOUNT  = 3;
	public final static int VALIDATE_EXPIRATION_DATE = 4;
	public final static int VALIDATE_CVV             = 5;
	public final static int VALIDATE_NAME            = 6;
	public final static int VALIDATE_EXISTS_WEBUSER_ACCOUNT = 7;
	public final static int VALIDATE_CARD_NOT_FOUND  = 8;
	public final static int VALIDATE_ACCOUNT_ACTIVE  = 14;
	
	/*****************************
	 *     PCodes Definition
	 *****************************/  
	 public final static int PCODE_WS_RECARGA_SALDO          =  210000;
	 public final static int PCODE_WS_RECARGA_SALDO_ACCOUNT  =  210002;
	 public final static int PCODE_HSBC_RECARGA_SALDO        =  210001;
	 public final static int PCODE_WS_CASH_WHITHDRAWAL       =  12000;
	 public final static int PCODE_WS_CONSULTA_SALDO         =  302000;
	 public final static int PCODE_WS_CARGO_A_TARJETA        =  2000;
	 public final static int PCODE_INVEX_WS_REPLACEMENT_CARD =  500002;
	 public final static int PCODE_INVEX_WS_IVA              =  900000;
	 public final static int PCODE_INVEX_COMISION_RECARGA    =  500001;
	 public final static int PCODE_CONSULTA_IVR              =  500006;  
	 public final static int PCODE_SPEI_TRANSFER             =  210001;
	 public final static int PCODE_WS_RECARGA_AUXI           =  210002;
	 
	 
	 public final static int PCODE_WINDOW_WITHDRAWAL         =  13000;
	 public final static int PCODE_SMS_TRANSFER              =  13001;
	 public final static int PCODE_COMMISSION_SMS_TRANSFER   =  500003;
	 public final static int PCODE_LOAD_BALANCE_SMS_TRANSFER =  210002;

	 public final static int PCODE_MONEX_TRANSFER            =  13002;
	 public final static int PCODE_LOAD_BALANCE_MONEX_TRANSFER  =  210003;
	 public final static int PCODE_LOAD_BALANCE_MONEX_MI_ADELANTO  =  210004;
	 public final static int PCODE_MONEX_BALANCE_QUERY_BATCH_COMMISION =  330000;
	 
	 /** PCODES TRASPASOS EN LÃ�NEA  **/
	 /** Cuenta Concentradora a Tarjeta **/
	 public final static int PCODE_MONEX_TRANSFER_CC_TO_PAN_CARGO = 142010;
	 public final static int PCODE_MONEX_TRANSFER_CC_TO_PAN_ABONO = 142011;
	 
	 /** Transferencia Tarjeta a Tarjeta **/
	 public final static int PCODE_MONEX_TRANSFER_PAN_TO_PAN_CARGO = 142012;
	 public final static int PCODE_MONEX_TRANSFER_PAN_TO_PAN_ABONO = 142013;
	 
	 /** Transferencia Tarjeta a Cuenta Concentradora **/
	 public final static int PCODE_MONEX_TRANSFER_PAN_TO_CC_CARGO  = 142014;
	 public final static int PCODE_MONEX_TRANSFER_PAN_TO_CC_ABONO  = 142015;
	 
	 /*************************************
	  *   Importe de Comisiones INVEX
	  *************************************/  
	 public final static double INVEX_COMISION_POR_REEMPLAZO  = 100;              
	 
	 
	 /*************************************
	  *   ALGUNOS BINES UTILIZADOS
	  *************************************/
	 public final static String BIN_PU_CHEDRAUI       = "533987";
	 public final static String BIN_PU_PREPAGO_ENVIOS = "53398701";
	 
	 /*************************************
	  *    PCODES SERVICIOS
	  *************************************/
	 public final static String PCODE_LOG_ACTIVACION       		=  "5000";
	 public final static String PCODE_LOG_LOCK       	   		=  "5001";
	 public final static String PCODE_LOG_TEMPORARYLOCK    		=  "5002";
	 public final static String PCODE_LOG_NIPLOCK    		    =  "5003";
	 public final static String PCODE_LOG_DEPOSIT_CHEQUE  		=  "6000";
	 public final static String PCODE_LOG_DEPOSIT_ACCOUNT	 	=  "2100";
	 public final static String PCODE_LOG_WITHDRAWAL_ACCOUNT  	=  "4000";
	 public final static String PCODE_LOG_WITHDRAWAL_TRANSFERS  =  "4001";
	 public final static String PCODE_LOG_WITHDRAWAL_ACCOUNT_A  =  "4002";
	 public final static String PCODE_LOG_PIN_VALIDATOR    		=  "7000";
	 public final static String PCODE_LOG_PIN_ASSIGNMENT   		=  "7010";
	 public final static String PCODE_LOG_PIN_CHANGE      		=  "7020";
	 public final static String PCODE_BALANCE_QUERY        		=  "8000";
	 public final static String PCODE_BALANCE_QUERY_BY_ACCOUNT  =  "8001";
	 public final static String PCODE_MONEX_IVR_BALANCE_QUERY   =  "8030";
	 public final static String PCODE_STATUS_CARD          		=  "8010";
	 public final static String PCODE_LOG_SMS_TRANSFER     		=  "9000";
	 public final static String PCODE_LOG_MONEX_TRANSFERS  		=  "9001";
	 public final static String PCODE_LOG_MONEX_ACC_STATMENT  	=  "8020";
	 public final static String PCODE_LOG_TRANSACTIONS_LIST  	=  "1010";
	 public final static String PCODE_LOG_TRANSACTIONS_PERIOD  	=  "1011";
	 public final static String PCODE_LOG_CHECK_LEVEL_CAP  		=  "1013";
	 public final static String PCODE_LOG_FIND_BY_USER_PAN  	=  "1012";
	 
	 public final static String PCODE_LOG_ASIGNMENT_ACCOUNT_STOCK  					=  "3000";
	 public final static String PCODE_LOG_ASIGNMENT_ACCOUNT_EMBOSSER  				=  "3003";
	 public final static String PCODE_LOG_CARDHOLDER_REG     						=  "3001";
	 public final static String PCODE_UPDATE_DEMOGRAPHIC_DATA						=  "3002";
	 public final static String PCODE_LOG_ASIGNMENT_ACCOUNT_CONCENTRATOR			=  "3004";
	
	 public final static String PCODE_LOG_DETAIL_PANS                                =  "3010";
	 
	 public final static String PCODE_LOG_ADJUSTMENT                                =  "3011";
	 
	 public final static String PCODE_LOG_TEMPORARY_LOCK_ACCOUNT                      =  "3012";
	 public final static String PCODE_LOG_LOCK_ACCOUNT                                =  "3005"; //TODO: Validar este codigo
	 public final static String PCODE_ASIGNMENT_PHONE_CODE                            =  "3006"; //TODO: Validar este codigo
     public final static String PCODE_VALIDATE_PHONE_CODE                             =  "3007"; //TODO: Validar este codigo
     public final static String PCODE_CHANGE_PHONE_CODE                               =  "3008"; //TODO: Validar este codigo
     public final static String PCODE_CHANGE_STATUS_PHONE_CODE                        =  "3009"; //TODO: Validar este codigo


	 public final static String PCODE_WS_REQ_AKALA           =  "2000";
	 public final static String PCODE_BALANCE_QUERY_CASH_AME =  "2001";
	 public final static String PCODE_DEPOSIT_AKALA			 =  "2002";
	 public final static String PCODE_DEPOSIT_CASH_AME	     =  "2003";
	 public final static String PCODE_DEPOSIT_CASH_AME_EXC   =  "2004";
	 public final static String PCODE_NOTIFICATIONS_CIBANCO  =  "2005";
	 public final static String PCODE_WS_NOTIFICATIONS_CIBANCO  =  "2006";
	 
	 
	 public final static String PCODE_IVR_INFO_CLIENT    		 =  "8031";
	 public final static String PCODE_SEARCH_ACCOUNT_BY_PAN      =  "8032";
	 public final static String PCODE_NEW_CLARIFICATION  		 =  "2010";
	 public final static String PCODE_END_CLARIFICATION   		=  "2020";
	 
	 public final static String PCODE_LOG_LIMITS_QUERY   			=  "6010";
	 public final static String PCODE_LOG_LIMITS_CREATE_UPDATE   	=  "6020";
	 public final static String PCODE_LOG_LIMITS_DELETE   			=  "6030";
	 
	 
	 /**PCODES de retiros por cuenta**/
	 public final static String PCODE_MONEX_COMMISION =  "502001";
	 /*****************************
	  *  OASIS Sockets Properties
	  *****************************/  
	 public final static String SOCKET_OASIS_HOST = "200.15.1.131";
	 public final static int    SOCKET_OASIS_PORT = 9009;
	 public final static int    SOCKET_OASIS_DEFAULT_TIMEOUT = 7;
	 
	 public final static double UDIS_MONTH_LIMIT_BASE = 1500;
	 public final static double UDIS_MONTH_LIMIT_2    = 5000;
	 
	 public final static int    VALIDACIONES_SUPERADAS = 16;
	 
	 public final static int    PROCESO_FINALIZADO_EXITOSAMENTE = 1;
	 
	 /** ASIGNACION DE CUENTAS **/
	 public final static String CUENTA_DISPONIBLE_PARA_ASIGNACION = "0";
	 public final static String CUENTA_ASIGNADA                   = "1";	 
	 	 	 	 

}
