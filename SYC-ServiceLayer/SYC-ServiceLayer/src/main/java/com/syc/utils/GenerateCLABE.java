package com.syc.utils;

public class GenerateCLABE {

	/**
	 * Servicio que sirve para generar la CLABE interbancaria
	 * @param IdBanco
	 * @param IdPlaza
	 * @param account
	 * @return
	 */
	public static String generateCLABE(String IdBanco, String IdPlaza, String account){
		int i;
		int j;
		int digitoVer = 0;
		int suma;
		int sum = 0;
		String strFactor = "371";
		StringBuffer strAccount = new StringBuffer();
		strAccount.append(IdBanco).append(IdPlaza).append(account).trimToSize();
		suma = i = 0;
		System.out.println("Entra y esta es la cuenta<" + strAccount + ">");
		
		System.out.println(strAccount.length());

		if(strAccount.length() == 17){
				
				for(i= 0; i <= strAccount.length()-1; i++ ){
					if(sum == 3)
						sum = 0;
					etiqueta:
					for(j=sum; j <= strFactor.length(); j++){
						int m1 = 0;
						int m2 = 0;
						if(i < 17){
						 m1 = Integer.parseInt(strAccount.substring(i, i+1));
						 m2 = Integer.parseInt(strFactor.substring(j, j+1));
						}else if(i == 17){
							 m1 = Integer.parseInt(strAccount.substring(i-1));
							 m2 = Integer.parseInt(strFactor.substring(j, j+1));
						}
						int sumita = m1*m2;
						int sumar = (String.valueOf(sumita).length() == 2) ? Integer.parseInt(String.valueOf(sumita).substring(1)) : sumita;
						suma += sumar;
						sum = j+1;
						break etiqueta;
					}
				}
		}
		suma %= 10;

		digitoVer = 10 - suma;

		if(digitoVer == 10){
			digitoVer = 0;
		}
		strAccount.append(digitoVer);

		return strAccount.toString();
	}
	
	public static void main(String args[]){
		System.out.println(generateCLABE("148", "180", "10000000751"));
	}

}
