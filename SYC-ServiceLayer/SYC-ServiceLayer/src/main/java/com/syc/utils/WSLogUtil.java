package com.syc.utils;

import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.WSLogDto;

public class WSLogUtil {
	
	WSLogDao wsLogDao;
	
	public WSLogUtil( WSLogDao _wsLogDao ){
		wsLogDao = _wsLogDao;
	}
	
	public void saveLog( String bin, String numberCard, String username, String request, int code, Object object){
		request = (request!=null && request.length()>400) ? request.substring(0,399) : request;
		
		WSLogDto wsLog = new WSLogDto();
		wsLog.setBin( bin );
		wsLog.setNumberCard(numberCard!=null?numberCard.trim():"0");
		wsLog.setUserName(username);
		wsLog.setCommand( object.getClass().getSimpleName().replace("ResponseDto", "Command") );
		wsLog.setRequest(request);
		wsLog.setCodeResponse(code);
		wsLog.setResponse( (object.toString()!=null && object.toString().length()>400) ? object.toString().substring(0,399) : object.toString());
		
		
		wsLogDao.create( wsLog );
		
	}
	
	
	public void saveLog( String bin, String numberCard, String username, String request, int code, Object object, String command  ){
		request  = (request!=null && request.length()>400) ? request.substring(0,399) : request;
		String response = (object!=null && object.toString().length()>400) ? object.toString().substring(0,399) : object.toString();
		
		WSLogDto wsLog = new WSLogDto();
		wsLog.setBin( bin );
		wsLog.setNumberCard(numberCard!=null?numberCard.trim():"0");
		wsLog.setUserName(username);
		wsLog.setCommand( command );
		wsLog.setRequest(request);
		wsLog.setCodeResponse(code);
		wsLog.setResponse(response);
		
		wsLogDao.create( wsLog );
	
		//session.getTransaction().commit();
		
	}
	
	
}
