package com.syc.utils;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TreeMap;

import com.syc.persistence.dto.AccountDto;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.CardholderDto;


public class ValidatorUtil {
	
	public int validateWebMethod( TreeMap<Integer, Integer> validations, CardholderDto cardholderDto ){
		
		/***************************************
		 * Valida que la Tarjeta exista
		 ***************************************/
		if( cardholderDto == null || cardholderDto.getCard()==null ){
			return SystemConstants.VALIDATE_CARD_NOT_FOUND;
		}
		
		/**
		 * Valida que la cuenta este activa
		 */
		if( validations.containsKey( SystemConstants.VALIDATE_ACCOUNT_ACTIVE ) ){
			if( !isActiveAccount( cardholderDto.getAccount()  )  ){
				return SystemConstants.VALIDATE_ACCOUNT_ACTIVE;
			}
		}
		
		/***************************************
		 * Validando que la tarjeta este activa
		 ***************************************/
		if( validations.containsKey( SystemConstants.VALIDATE_ACTIVE_CARD ) ){
			if( !isActiveCard( cardholderDto.getCard()  )  ){
				return SystemConstants.VALIDATE_ACTIVE_CARD;
			}
		}
		
		/******************************************************
		 * Validando que la tarjeta tenga asociada una Cuenta
		 *******************************************************/
		if( validations.containsKey( SystemConstants.VALIDATE_EXISTS_ACCOUNT ) ){
			if( !existAccount( cardholderDto.getAccount()  )  ){
				return SystemConstants.VALIDATE_EXISTS_ACCOUNT;
			}
		}
		
		/******************************************************
		 * Validando la fecha de Vencimiento de la Tarjeta
		 *******************************************************/
		if( validations.containsKey( SystemConstants.VALIDATE_EXPIRATION_DATE ) ){
			if( isExpiredCard( cardholderDto.getCard().getExpirationDate()  )  ){
				return SystemConstants.VALIDATE_EXPIRATION_DATE;
			}
		}
		
		/******************************************************
		 *                 Validando el CVV
		 ******************************************************
		if( validations.containsKey( SystemConstants.VALIDATE_CVV ) ){
			int resultOfValidation = validateCVV( cardholderDto );
			System.out.println("Result of Validation: " + resultOfValidation);
			if( resultOfValidation > 0 ){
				return resultOfValidation;
			}
		}
		
		/******************************************************
		 *                 Validando el Nombre
		 ******************************************************
		if( validations.containsKey( SystemConstants.VALIDATE_NAME ) ){
			if( cardholderDto.getObject() instanceof String ){
				if( !isValidName(cardholderDto.getIstcmscard(), (String)cardholderDto.getObject()) ){
					return SystemConstants.VALIDATE_NAME;
				}
			}
		}	
		
		*/
		
		return 0;
	}
	
	
	/***********************************
	 * Valida si una tarjeta esta Activa
	 ***********************************/
	public boolean isActiveCard( CardDto card ){
		if( card!=null && card.getStatus() == SystemConstants.STATUS_ACTIVE_CARD ){
			return true;
		}
		return false;
	}
	
	public boolean isActiveAccount( AccountDto account  ){
		if( account!=null && account.getStatus().equals(SystemConstants.STATUS_ACTIVE_ACCOUNT )){
			return true;
		}
		return false;
	}
	public boolean existAccount( AccountDto account ){
		if( account != null ){
			return true;
		}
		return false;
	}
	
	public boolean isExpiredCard( Date expirationDate ){

		final DateFormat df = new SimpleDateFormat("yyyyMM");
		final Calendar calendar = Calendar.getInstance();

		try{
			calendar.setTime(expirationDate);
			int intExpiryDate = GeneralUtil.convertStringToInt(  df.format(calendar.getTime()) );

			calendar.setTime( new Date(System.currentTimeMillis()) );
			int intCurrentDate = GeneralUtil.convertStringToInt(  df.format(calendar.getTime()) );

			if( intExpiryDate >= intCurrentDate ){
				return false;
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		return true;
	}
}
