package com.syc.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LockClientRespDto", propOrder = {
	"code",
	"description",
	"accounts"
	
	
})
@XmlRootElement(name = "LockClientRespDto")
public class LockClientRespDto {
	
	
	private int    code;
	private String description;
	private List<BasicAcctResponseDto> accounts;
	
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<BasicAcctResponseDto> getAccounts() {
		return accounts;
	}
	public void setAccounts(List<BasicAcctResponseDto> accounts) {
		this.accounts = accounts;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LockClientRespDto [code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", accounts=");
		builder.append(accounts);
		builder.append("]");
		return builder.toString();
	}

}
