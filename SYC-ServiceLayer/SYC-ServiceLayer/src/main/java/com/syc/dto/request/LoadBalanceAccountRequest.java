package com.syc.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loadBalanceRequest", propOrder = {
    "account",
    "amount"
})
public class LoadBalanceAccountRequest {

	
	String account;
	double amount;
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LoadBalanceAccountRequest [account=").append(account)
				.append(", amount=").append(amount).append("]");
		return builder.toString();
	}
	
	
}
