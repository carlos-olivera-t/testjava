package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardLimitsResponseDto", propOrder = {
	"code",
    "description",
    "cardLimits"
    
})
@XmlRootElement(name = "CardLimitsResponseDto")
public class CardLimitsResponseDto {
	
	
	private int code;
	private String description;
	private IndividualLimitsDto cardLimits;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public IndividualLimitsDto getCardLimits() {
		return cardLimits;
	}
	public void setCardLimits(IndividualLimitsDto cardLimits) {
		this.cardLimits = cardLimits;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CardLimitsResponseDto [code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", cardLimits=");
		builder.append(cardLimits);
		builder.append("]");
		return builder.toString();
	}
	
	

}
