/**
 * 
 */
package com.syc.dto.response;

/**
 * @author Yessica Gomez -PMP06011
 *
 */
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "code",
    "status",
    "description",
    "availableAmount"
})
@XmlRootElement(name = "BalanceResponseDto")
public class BalanceQueryStatusResponseDto {
	
	
		
		private int code;
		private String description;
		private int status;
		private double availableAmount;

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}
		
		
		

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public double getAvailableAmount() {
			return availableAmount;
		}

		public void setAvailableAmount(double availableAmount) {
			this.availableAmount = availableAmount;
		}
		
		public String toString(){
			String response = null;
			response  = "code["+code+"],";
			response += "status["+status+"]";
			response += "description["+description+"]";
			response += "availableAmount["+availableAmount+"]";
			return response;
		}

	}


