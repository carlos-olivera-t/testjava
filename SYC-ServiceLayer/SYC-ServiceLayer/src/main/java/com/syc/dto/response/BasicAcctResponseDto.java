package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasicAcctResponseDto", propOrder = {
	"account",
	"code",
	"description",
	"authorization"
	
})
@XmlRootElement(name = "BasicAcctResponseDto")
public class BasicAcctResponseDto {
	
	
	private String account;
	private int    code;
	private String description;
	private String authorization;
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BasicAcctResponseDto [account=");
		builder.append(account);
		builder.append(", code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", authorization=");
		builder.append(authorization);
		builder.append("]");
		return builder.toString();
	}
	
	
	

}
