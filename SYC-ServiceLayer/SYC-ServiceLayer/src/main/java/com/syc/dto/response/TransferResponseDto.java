package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"code",                       
	"description",                
	"sourceAvailableBalance",
	"sourcePreviousBalance",
	"destinationAvailableBalance",
	"destinationPreviousBalance",
	"sourceAuthorization",        
	"destinationAuthorization"
})
@XmlRootElement(name = "TransferResponseDto")
public class TransferResponseDto {
	
	private String code;
	private String description;
	private double sourceAvailableBalance;
	private double sourcePreviousBalance;
	private double destinationAvailableBalance;
	private double destinationPreviousBalance;
	private String sourceAuthorization;
	private String destinationAuthorization;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getSourceAvailableBalance() {
		return sourceAvailableBalance;
	}
	public void setSourceAvailableBalance(double sourceAvailableBalance) {
		this.sourceAvailableBalance = sourceAvailableBalance;
	}
	
	public double getDestinationAvailableBalance() {
		return destinationAvailableBalance;
	}
	public void setDestinationAvailableBalance(double destinationAvailableBalance) {
		this.destinationAvailableBalance = destinationAvailableBalance;
	}
	
	public String getSourceAuthorization() {
		return sourceAuthorization;
	}
	public void setSourceAuthorization(String sourceAuthorization) {
		this.sourceAuthorization = sourceAuthorization;
	}
	
	public String getDestinationAuthorization() {
		return destinationAuthorization;
	}
	public void setDestinationAuthorization(String destinationAuthorization) {
		this.destinationAuthorization = destinationAuthorization;
	}
	
	public double getSourcePreviousBalance() {
		return sourcePreviousBalance;
	}
	public void setSourcePreviousBalance(double sourcePreviousBalance) {
		this.sourcePreviousBalance = sourcePreviousBalance;
	}
	public double getDestinationPreviousBalance() {
		return destinationPreviousBalance;
	}
	public void setDestinationPreviousBalance(double destinationPreviousBalance) {
		this.destinationPreviousBalance = destinationPreviousBalance;
	}
	public TransferResponseDto(){
		sourceAuthorization      = "0"; 
		destinationAuthorization = "0";
	}

	public String toString() {
		String buffer;
		buffer = "code[" + code + "]";
		buffer += "description[" + description + "]";
		buffer += "sourceAvBalance[" + sourceAvailableBalance + "]";
		buffer += "sourcePrevBalance [" + sourcePreviousBalance  + "]";
		buffer += "destAvBalance[" + destinationAvailableBalance + "]";
		buffer += "destPrevBalance [" + destinationPreviousBalance  + "]";
		buffer += "sAuthorization[" + sourceAuthorization + "]";
		buffer += "dAuthorization[" + destinationAuthorization + "]";
		return buffer;
	}

}
