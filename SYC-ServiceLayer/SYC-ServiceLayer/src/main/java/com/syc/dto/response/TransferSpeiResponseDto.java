package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"code",                       
	"description",                
	"sourceAvailableBalance",
	"sourcePreviousBalance",
	"destinationAvailableBalance",
	"destinationPreviousBalance",
	"authorization"
})
@XmlRootElement(name = "TransferSpeiResponseDto")
public class TransferSpeiResponseDto {
	
	private int code;
	private String description;
	private double sourceAvailableBalance;
	private double sourcePreviousBalance;
	private double destinationAvailableBalance;
	private double destinationPreviousBalance;
	private String authorization;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getSourceAvailableBalance() {
		return sourceAvailableBalance;
	}
	public void setSourceAvailableBalance(double sourceAvailableBalance) {
		this.sourceAvailableBalance = sourceAvailableBalance;
	}
	public double getSourcePreviousBalance() {
		return sourcePreviousBalance;
	}
	public void setSourcePreviousBalance(double sourcePreviousBalance) {
		this.sourcePreviousBalance = sourcePreviousBalance;
	}
	public double getDestinationAvailableBalance() {
		return destinationAvailableBalance;
	}
	public void setDestinationAvailableBalance(double destinationAvailableBalance) {
		this.destinationAvailableBalance = destinationAvailableBalance;
	}
	public double getDestinationPreviousBalance() {
		return destinationPreviousBalance;
	}
	public void setDestinationPreviousBalance(double destinationPreviousBalance) {
		this.destinationPreviousBalance = destinationPreviousBalance;
	}
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransferSpeiResponseDto [code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", sourceAvailableBalance=");
		builder.append(sourceAvailableBalance);
		builder.append(", sourcePreviousBalance=");
		builder.append(sourcePreviousBalance);
		builder.append(", destinationAvailableBalance=");
		builder.append(destinationAvailableBalance);
		builder.append(", destinationPreviousBalance=");
		builder.append(destinationPreviousBalance);
		builder.append(", authorization=");
		builder.append(authorization);
		builder.append("]");
		return builder.toString();
	}
	
	
}
