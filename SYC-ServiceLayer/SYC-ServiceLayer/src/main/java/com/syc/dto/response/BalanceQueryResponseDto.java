package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BalanceQueryResponseDto", propOrder = {
    "code",
    "description",
    "availableAmount"
})
@XmlRootElement(name = "BalanceQueryResponseDto")
public class BalanceQueryResponseDto {
	
	private int code;
	private String description;
	private double availableAmount;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAvailableAmount() {
		return availableAmount;
	}

	public void setAvailableAmount(double availableAmount) {
		this.availableAmount = availableAmount;
	}
	
	public String toString(){
		String response = null;
		response  = "code["+code+"],";
		response += "description["+description+"]";
		response += "availableAmount["+availableAmount+"]";
		return response;
	}

}

