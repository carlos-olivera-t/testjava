package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "code", "description", "tarjeta", "nombre",
		"direccion", "direccion2", "colonia", "localidad", "estado", "cp",
		"telefono", "telefono2", "telefono3", "rfc" })
@XmlRootElement(name = "AddressDto")
public class AddresRespDto {

	private String tarjeta;
	private String nombre;
	private String direccion;
	private String direccion2;
	private String colonia;
	private String localidad;
	private String estado;
	private String cp;
	private String telefono;
	private String telefono2;
	private String telefono3;
	private String rfc;
	private int code;
	private String description;

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDireccion2() {
		return direccion2;
	}

	public void setDireccion2(String direccion2) {
		this.direccion2 = direccion2;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getTelefono3() {
		return telefono3;
	}

	public void setTelefono3(String telefono3) {
		this.telefono3 = telefono3;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AddresRespDto [tarjeta=");
		builder.append(tarjeta);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", direccion=");
		builder.append(direccion);
		builder.append(", direccion2=");
		builder.append(direccion2);
		builder.append(", colonia=");
		builder.append(colonia);
		builder.append(", localidad=");
		builder.append(localidad);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", cp=");
		builder.append(cp);
		builder.append(", telefono=");
		builder.append(telefono);
		builder.append(", telefono2=");
		builder.append(telefono2);
		builder.append(", telefono3=");
		builder.append(telefono3);
		builder.append(", rfc=");
		builder.append(rfc);
		builder.append(", code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}

	
}
