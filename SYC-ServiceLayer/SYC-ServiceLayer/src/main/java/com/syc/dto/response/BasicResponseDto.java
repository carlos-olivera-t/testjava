package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasicResponse", propOrder = {
    "codigo",
    "descripcion"
})

@XmlRootElement(name = "BasicResponseDto")

public class BasicResponseDto {
	
	private int    codigo;
	private String descripcion; 
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String toString(){
		String response = null;
		response  = "codigo["+codigo+"],";
		response += "descripcion["+descripcion+"]";
		return response;
	}
	
	public BasicResponseDto(){
		codigo      = 0;
		descripcion = null;
	}
	
	public BasicResponseDto( int code, String description ){
		codigo      = code;
		descripcion = description;
	}
	
}
