package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"authorization",
	"code",
    "description",
    "balance",
    "currentBalance"
})
@XmlRootElement(name = "LoadBalanceDto")

public class BalanceMovementRespDto {
	
	public BalanceMovementRespDto(){
		this.authorization  = "0";
		this.code           =  0;
		this.description    = null;
		this.balance        =  0;
		this.currentBalance =  0;
	}
	
	@XmlElement(name = "authorization", required = true)
	private String authorization;
	
	@XmlElement(name = "code", required = true)
	private int    code;
	
	@XmlElement(name = "description", required = true)
	private String description;

	private double balance;
	private double currentBalance;
	
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public double getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}
	
	public String toString(){
		String response = null;
		response  = "authorization["+authorization+"],";
		response += "code["+code+"],";
		response += "description["+description+"]";
		response += "balance["+balance+"]";
		response += "currentBalance["+currentBalance+"]";
		return response;
	}
	
	
}
