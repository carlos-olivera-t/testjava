package com.syc.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.syc.persistence.dto.mapper.AccountStatementDto;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idEmployer",
    "numberOfPeriods",
    "listAccountStatement"
})
@XmlRootElement(name = "AccountStatementResponseDto")
public class AccountStatementResponseDto {
	
	private String idEmployer;
	private int    numberOfPeriods;
	private List<AccountStatementDto> listAccountStatement;
	
	public String getIdEmployer() {
		return idEmployer;
	}
	public void setIdEmployer(String idEmployer) {
		this.idEmployer = idEmployer;
	}
	public int getNumberOfPeriods() {
		return numberOfPeriods;
	}
	public void setNumberOfPeriods(int numberOfPeriods) {
		this.numberOfPeriods = numberOfPeriods;
	}
	public List<AccountStatementDto> getListAccountStatement() {
		return listAccountStatement;
	}
	public void setListAccountStatement(
			List<AccountStatementDto> listAccountStatement) {
		this.listAccountStatement = listAccountStatement;
	}
}
