package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.syc.utils.GeneralUtil;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"code",
    "description",
    "authorization",
    "noCuenta",
    "noClient",
    "clientName",
    "CLABE",
    "issueDate",
    "currentTime"
})
@XmlRootElement(name = "ClientAccountResponseDto")
public class ClientAccountResponseDto {
	
	private int    code;
	private String description;
	private String authorization;
	private String noCuenta;
	private String noClient;
	private String clientName;
	private String CLABE;
	private String issueDate;
	private String currentTime;
	
	public ClientAccountResponseDto(){
		authorization = "0";
		issueDate     = GeneralUtil.formatDate( "dd-MM-yyyy" , GeneralUtil.getCurrentSqlDate() );
		currentTime   = GeneralUtil.formatDate( "HH:mm:ss" , GeneralUtil.getCurrentSqlDate() );
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String getNoCuenta() {
		return noCuenta;
	}

	public void setNoCuenta(String noCuenta) {
		this.noCuenta = noCuenta;
	}

	public String getNoClient() {
		return noClient;
	}

	public void setNoClient(String noClient) {
		this.noClient = noClient;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCLABE() {
		return CLABE;
	}

	public void setCLABE(String CLABE) {
		this.CLABE = CLABE;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ClientAccountResponseDto [code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", authorization=");
		builder.append(authorization);
		builder.append(", noCuenta=");
		builder.append(noCuenta);
		builder.append(", noClient=");
		builder.append(noClient);
		builder.append(", clientName=");
		builder.append(clientName);
		builder.append(", CLABE=");
		builder.append(CLABE);
		builder.append(", issueDate=");
		builder.append(issueDate);
		builder.append(", currentTime=");
		builder.append(currentTime);
		builder.append("]");
		return builder.toString();
	}


}
