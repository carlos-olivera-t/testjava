package com.syc.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loadBalanceRequest", propOrder = {
    "pan",
    "amount"
})
public class LoadBalanceRequest {
	
	private String pan;
	private double amount;
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	public String toString() {
		return "LoadBalanceRequest [pan=" + pan + ", amount=" + amount + "]";
	}
	
	

}
