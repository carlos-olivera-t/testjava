package com.syc.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)

@XmlType(name = "", propOrder = {
	"pan",              
	"pCode",          
	"amount", 
	"trandate",
	"authorization",         
	"commerce" 
})
@XmlRootElement(name = "ClarificationRequestDto")
public class ClarificationRequestDto {
	
	private String pan;
	private String pCode;
	private double amount;
	private String trandate;
	private String authorization;
	private String commerce;
	
	
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getpCode() {
		return pCode;
	}
	public void setpCode(String pCode) {
		this.pCode = pCode;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getTrandate() {
		return trandate;
	}
	public void setTrandate(String trandate) {
		this.trandate = trandate;
	}
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	public String getCommerce() {
		return commerce;
	}
	public void setCommerce(String commerce) {
		this.commerce = commerce;
	}
	
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ClarificationRequestDto [pan=");
		builder.append(pan);
		builder.append(", pCode=");
		builder.append(pCode);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", trandate=");
		builder.append(trandate);
		builder.append(", authorization=");
		builder.append(authorization);
		builder.append(", commerce=");
		builder.append(commerce);
		builder.append("]");
		return builder.toString();
	}

}
