package com.syc.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.syc.persistence.dto.mapper.TransactionModDto;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "code",
    "description",
    "date",
    "lastestTransactions"
})
@XmlRootElement(name = "TransactionModListDto")
public class TransactionModListDto {
	
	private int    code;
	private String description;
	private String date;
	private List<TransactionModDto> lastestTransactions;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<TransactionModDto> getLastestTransactions() {
		return lastestTransactions;
	}
	public void setLastestTransactions(List<TransactionModDto> lastestTransactions) {
		this.lastestTransactions = lastestTransactions;
	}
	
	


}
