package com.syc.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"name",
    "shortName",
    "email",
    "address",
    "colony",
    "zipCode",
    "municipality",
    "city",
    "state",
    "rfc",
    "homePhone",
    "cellPhone",
    "workPhone"
})
public class DemographicDataRequestDto {
	
	String name;
	String shortName;
	String email;
	String address;
	String colony;
	String zipCode;
	String municipality;
	String city;
	String state;
	String rfc;
	String homePhone;
	String cellPhone;
	String workPhone;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public String getColony() {
		return colony;
	}
	public void setColony(String colony) {
		this.colony = colony;
	}

	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getMunicipality() {
		return municipality;
	}
	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String toString(){
		String response = null;
		response  = "name["+name+"],";
		response += "shortName["+shortName+"]";
		response += "email["+email+"]";
		response += "address["+address+"]";
		response += "colony["+colony+"]";
		response += "zipCode["+zipCode+"]";
		response += "municipality["+municipality+"]";
		response += "city["+city+"]";
		response += "state["+state+"]";
		response += "rfc["+rfc+"]";
		response += "homePhone["+homePhone+"]";
		response += "cellPhone["+cellPhone+"]";
		response += "workPhone["+workPhone+"]";
		return response;
	}
}
