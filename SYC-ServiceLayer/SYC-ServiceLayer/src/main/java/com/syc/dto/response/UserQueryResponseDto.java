package com.syc.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.syc.persistence.dto.IstcmscardDto;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "code",
    "description",
    "cliente"
})
@XmlRootElement(name = "UserQueryResponseDto")
public class UserQueryResponseDto {
	
	int code;
	String description;
	List<IstcmscardDto> cliente;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<IstcmscardDto> getCliente() {
		return cliente;
	}
	public void setCliente(List<IstcmscardDto> cliente) {
		this.cliente = cliente;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserQueryResponseDto [code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", cliente=");
		builder.append(cliente);
		builder.append("]");
		return builder.toString();
	}
	
	

}
