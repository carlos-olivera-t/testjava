package com.syc.dto.response;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.syc.persistence.dto.mapper.BalanceDto;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	   "codeResponse",
	   "description",
	   "issueDate",
	   "currentTime",
	   "cards"
})
@XmlRootElement(name = "CardAccountResponseDto")
public class CardsBalanceAccountResponseDto {
		
		private int codeResponse;
		private String description;
		String issueDate;
		String currentTime;
		private List<BalanceDto> cards;
		public int getCodeResponse() {
			return codeResponse;
		}
		public void setCodeResponse(int codeResponse) {
			this.codeResponse = codeResponse;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
		public String getIssueDate() {
			return issueDate;
		}
		public void setIssueDate(String issueDate) {
			this.issueDate = issueDate;
		}
		public String getCurrentTime() {
			return currentTime;
		}
		public void setCurrentTime(String currentTime) {
			this.currentTime = currentTime;
		}
		public List<BalanceDto> getCards() {
			return cards;
		}
		public void setCards(List<BalanceDto> cards) {
			this.cards = cards;
		}
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("CardAccountResponseDto [codeResponse=");
			builder.append(codeResponse);
			builder.append(", description=");
			builder.append(description);
			builder.append(", issueDate=");
			builder.append(issueDate);
			builder.append(", currentTime=");
			builder.append(currentTime);
			builder.append(", cards=");
			builder.append(cards);
			builder.append("]");
			return builder.toString();
		}
		
	}

