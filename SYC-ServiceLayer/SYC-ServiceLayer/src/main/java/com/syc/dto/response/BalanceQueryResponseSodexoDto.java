package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"pan",
    "code",
    "description",
    "availableAmount"
})
@XmlRootElement(name = "BalanceResponseSodexoDto")
public class BalanceQueryResponseSodexoDto {
	
	private String pan;
	private int code;
	private String description;
	private double availableAmount;
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getAvailableAmount() {
		return availableAmount;
	}
	public void setAvailableAmount(double availableAmount) {
		this.availableAmount = availableAmount;
	}
	
	public String toString() {
		return "BalanceQueryResponseSodexoDto [pan=" + pan + ", code=" + code
				+ ", description=" + description + ", availableAmount="
				+ availableAmount + "]";
	}
	
	
	

}
