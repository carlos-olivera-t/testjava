package com.syc.dto.request;

import java.io.Serializable;


public class ChequeDto implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2608518631102765098L;
	
	
	private double importe;
	private String banco;
	private String referencia;
	private int type; //0=Normal 1=Salvo buen cobro
	
	
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}	
	
}
