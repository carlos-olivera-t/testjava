package com.syc.dto.request;

import java.io.Serializable;

public class UpdateLimitCardDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -741349578840967472L;

	private String bin;
	private String pan;
	
	private double maximoRetiro;
	private int cantidadRetiros;
	
	private double maximoCompras;
	private int cantidadCompras;
	
	private double maximoRetiroInternacional;
	private int cantidadRetirosInternacional;
	
	
	
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public double getMaximoRetiro() {
		return maximoRetiro;
	}
	public void setMaximoRetiro(double maximoRetiro) {
		this.maximoRetiro = maximoRetiro;
	}
	public int getCantidadRetiros() {
		return cantidadRetiros;
	}
	public void setCantidadRetiros(int cantidadRetiros) {
		this.cantidadRetiros = cantidadRetiros;
	}
	public double getMaximoCompras() {
		return maximoCompras;
	}
	public void setMaximoCompras(double maximoCompras) {
		this.maximoCompras = maximoCompras;
	}
	public int getCantidadCompras() {
		return cantidadCompras;
	}
	public void setCantidadCompras(int cantidadCompras) {
		this.cantidadCompras = cantidadCompras;
	}
	public double getMaximoRetiroInternacional() {
		return maximoRetiroInternacional;
	}
	public void setMaximoRetiroInternacional(double maximoRetiroInternacional) {
		this.maximoRetiroInternacional = maximoRetiroInternacional;
	}
	public int getCantidadRetirosInternacional() {
		return cantidadRetirosInternacional;
	}
	public void setCantidadRetirosInternacional(int cantidadRetirosInternacional) {
		this.cantidadRetirosInternacional = cantidadRetirosInternacional;
	}

	
}
