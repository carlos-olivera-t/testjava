package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndividualLimitsDto", propOrder = {
	"maxWithdrawalAmount",
    "maxWithdrawalNumber",
    "maxBuysAmount",
    "maxBuysNumber",
    "maxWithdrawalAmountInt",
    "maxWithdrawalNumberInt"
})
@XmlRootElement(name = "IndividualLimitsDto")
public class IndividualLimitsDto {
	
	private double maxWithdrawalAmount;
	private int maxWithdrawalNumber;

	private double maxBuysAmount;
	private int maxBuysNumber;

	private double maxWithdrawalAmountInt;
	private int maxWithdrawalNumberInt;
	
	
	public double getMaxWithdrawalAmount() {
		return maxWithdrawalAmount;
	}
	public void setMaxWithdrawalAmount(double maxWithdrawalAmount) {
		this.maxWithdrawalAmount = maxWithdrawalAmount;
	}
	public int getMaxWithdrawalNumber() {
		return maxWithdrawalNumber;
	}
	public void setMaxWithdrawalNumber(int maxWithdrawalNumber) {
		this.maxWithdrawalNumber = maxWithdrawalNumber;
	}
	public double getMaxBuysAmount() {
		return maxBuysAmount;
	}
	public void setMaxBuysAmount(double maxBuysAmount) {
		this.maxBuysAmount = maxBuysAmount;
	}
	public int getMaxBuysNumber() {
		return maxBuysNumber;
	}
	public void setMaxBuysNumber(int maxBuysNumber) {
		this.maxBuysNumber = maxBuysNumber;
	}
	public double getMaxWithdrawalAmountInt() {
		return maxWithdrawalAmountInt;
	}
	public void setMaxWithdrawalAmountInt(double maxWithdrawalAmountInt) {
		this.maxWithdrawalAmountInt = maxWithdrawalAmountInt;
	}
	public int getMaxWithdrawalNumberInt() {
		return maxWithdrawalNumberInt;
	}
	public void setMaxWithdrawalNumberInt(int maxWithdrawalNumberInt) {
		this.maxWithdrawalNumberInt = maxWithdrawalNumberInt;
	}
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IndividualLimitsDto [maxWithdrawalAmount=");
		builder.append(maxWithdrawalAmount);
		builder.append(", maxWithdrawalNumber=");
		builder.append(maxWithdrawalNumber);
		builder.append(", maxBuysAmount=");
		builder.append(maxBuysAmount);
		builder.append(", maxBuysNumber=");
		builder.append(maxBuysNumber);
		builder.append(", maxWithdrawalAmountInt=");
		builder.append(maxWithdrawalAmountInt);
		builder.append(", maxWithdrawalNumberInt=");
		builder.append(maxWithdrawalNumberInt);
		builder.append("]");
		return builder.toString();
	}

}
