package com.syc.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)

@XmlType(name = "", propOrder = {
	"pan",
    "name",
    "address",
    "address2",
    "colony",
    "delegation",
    "federalEntity",
    "state",
    "zipCode",
    "homePhone",
    "cellPhone",
    "workPhone",
    "rfc"
})
public class CardholderInfoRequestDto {
	
	@XmlElement(name = "pan", required = true)
	private String pan;
	
	@XmlElement(name = "name", required = true)
	private String name;
	
	@XmlElement(name = "address", required = true)
	private String address;
	
	@XmlElement(name = "address2", required = false)
	private String address2;
	
	@XmlElement(name = "colony", required = true)
	private String colony;
	
	@XmlElement(name = "delegation", required = true)
	private String delegation;
	
	@XmlElement(name = "federalEntity", required = true)
	private String federalEntity;
	
	@XmlElement(name = "state", required = true)
	private String state;
	
	@XmlElement(name = "zipCode", required = false)
	private String zipCode;
	
	@XmlElement(name = "homePhone", required = false)
	private String homePhone;
	
	@XmlElement(name = "cellPhone", required = false)
	private String cellPhone;
	
	@XmlElement(name = "workPhone", required = false)
	private String workPhone;
	
	@XmlElement(name = "rfc", required = true)
	private String rfc;

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getColony() {
		return colony;
	}

	public void setColony(String colony) {
		this.colony = colony;
	}

	public String getDelegation() {
		return delegation;
	}

	public void setDelegation(String delegation) {
		this.delegation = delegation;
	}

	public String getFederalEntity() {
		return federalEntity;
	}

	public void setFederalEntity(String federalEntity) {
		this.federalEntity = federalEntity;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	
	public String toString(){
		String response = null;
		response  = "pan["+pan+"],";
		response += "name["+name+"]";
		response += "address["+address+"]";
		response += "address2["+address2+"]";
		response += "colony["+colony+"]";
		response += "delegation["+delegation+"]";
		response += "federalEntity["+federalEntity+"]";
		response += "zipCode["+zipCode+"]";
		response += "homePhone["+homePhone+"]";
		response += "cellPhone["+cellPhone+"]";
		response += "workPhone["+workPhone+"]";
		response += "rfc["+rfc+"]";
		return response;
	}

}
