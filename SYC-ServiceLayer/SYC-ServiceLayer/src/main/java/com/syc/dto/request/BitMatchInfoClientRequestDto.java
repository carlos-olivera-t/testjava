package com.syc.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cuenta",
    "nombreLargo",
    "nombreCorto",
    "direccion",
    "colonia",
    "entidadFederativa",
    "codigoPostal",
    "rfc",
    "telefono",
    "telefono2",
    "sucursal",
    "producto"
})
@XmlRootElement(name = "InfoClientRequest")
public class BitMatchInfoClientRequestDto {
	
	private String cuenta;
	private String nombreLargo;
	private String nombreCorto;
	private String direccion;
	private String colonia;
	private String entidadFederativa;
	private String codigoPostal;
	private String rfc;
	private String telefono;
	private String telefono2;
	private String sucursal;
	private String producto;
	
	
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getNombreLargo() {
		return nombreLargo;
	}
	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getEntidadFederativa() {
		return entidadFederativa;
	}
	public void setEntidadFederativa(String entidadFederativa) {
		this.entidadFederativa = entidadFederativa;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono2() {
		return telefono2;
	}
	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String toString() {
		String buffer;
		buffer = "cuenta[ " + cuenta + " ]  ";
		buffer += "nombreLargo[ " + nombreLargo + " ]  ";
		buffer += "nombreCorto[ " + nombreCorto + " ]  ";
		buffer += "direccion[ " + direccion + " ]  ";
		buffer += "colonia[ " + colonia + " ]  ";
		buffer += "entidadFederativa[ " + entidadFederativa + " ]  ";
		buffer += "codigoPostal[ " + codigoPostal + " ]  ";
		buffer += "rfc[ " + rfc + " ]  ";
		buffer += "telefono[ " + telefono + " ]  ";
		buffer += "telefono2[ " + telefono2 + " ]  ";
		buffer += "sucursal[ " + sucursal + " ]  ";
		buffer += "producto[ " + producto + " ]  ";
		return buffer;
	}
}
