package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cardNumber",
    "name",
    "cardIndicator"
})
@XmlRootElement(name = "ClientResponseDto")
public class ClientResponseDto {

	private String cardNumber;
	private String name;
	private String cardIndicator;
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCardIndicator() {
		return cardIndicator;
	}
	public void setCardIndicator(String cardIndicator) {
		this.cardIndicator = cardIndicator;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ClientResponseDto [cardNumber=").append(cardNumber)
				.append(", name=").append(name).append(", cardIndicator=")
				.append(cardIndicator).append("]");
		return builder.toString();
	}
	
	
	
}
