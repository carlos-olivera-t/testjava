package com.syc.dto.response;

import java.io.Serializable;

public class IVRInfoClientResponse implements Serializable{
	
	private static final long serialVersionUID = -9151074288650233864L;
	
	private int    code;
	private int    status;
	private String description;
	private String product;
	private String codePhoneStatus;
	
	public IVRInfoClientResponse(){
		code        = 2;
		description = "Cardholder Not Found";
		status      = 0;
		product     = null;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}

	public String getCodePhoneStatus() {
		return codePhoneStatus;
	}

	public void setCodePhoneStatus(String codePhoneStatus) {
		this.codePhoneStatus = codePhoneStatus;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IVRInfoClientResponse [code=");
		builder.append(code);
		builder.append(", status=");
		builder.append(status);
		builder.append(", description=");
		builder.append(description);
		builder.append(", product=");
		builder.append(product);
		builder.append(", codePhoneStatus=");
		builder.append(codePhoneStatus);
		builder.append("]");
		return builder.toString();
	}

}
