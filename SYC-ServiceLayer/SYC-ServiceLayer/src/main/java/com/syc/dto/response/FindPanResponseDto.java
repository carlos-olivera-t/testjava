package com.syc.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.syc.persistence.dto.mapper.PanDto;
import com.syc.utils.GeneralUtil;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"code",
    "description",
    "noCuenta",
    "clientName",
    "dateTime",
    "cards"
})
@XmlRootElement(name = "FindPanResponseDto")
public class FindPanResponseDto {
	
	
	private int code;
	private String description;
	private String noCuenta;
	private String clientName;
	private String dateTime;
	private List<PanDto> cards;
	
	
	public FindPanResponseDto(){
		
		dateTime     = GeneralUtil.formatDate( "dd-MM-yyyy  HH:mm" , GeneralUtil.getCurrentSqlDate() );
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNoCuenta() {
		return noCuenta;
	}
	public void setNoCuenta(String noCuenta) {
		this.noCuenta = noCuenta;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public List<PanDto> getCards() {
		return cards;
	}
	public void setCards(List<PanDto> cards) {
		this.cards = cards;
	}
	
	
	
	

}
