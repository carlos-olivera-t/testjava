package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LevelCaptationResponseDto", propOrder = {
    "code",
    "description",
    "monthlyAmount"
})
@XmlRootElement(name = "LevelCaptationResponseDto")
public class LevelCaptationResponseDto {

		private int code;
		private String description;
		private double monthlyAmount;

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public double getMonthlyAmount() {
			return monthlyAmount;
		}

		public void setMonthlyAmount(double monthlyAmount) {
			this.monthlyAmount = monthlyAmount;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("LevelCaptationResponseDto [code=");
			builder.append(code);
			builder.append(", description=");
			builder.append(description);
			builder.append(", monthlyAmount=");
			builder.append(monthlyAmount);
			builder.append("]");
			return builder.toString();
		}

	}

