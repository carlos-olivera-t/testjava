package com.syc.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"orderNumber",                
	"sequenceNumber",             
	"processType",                
	"movementType",               
	"sourceAccount",              
	"transactionTypeSource",      
	"sourceAmount",               
	"commissionAmountSource",     
	"accountDestination",         
	"transactionTypeDestination", 
	"destinationAmount",          
	"commissionAmountDestination"
		
})
@XmlRootElement(name = "TransferRequestDto")
public class TransferRequestDto {
	
	private int    orderNumber;
	private int    sequenceNumber;
	private String processType;
	private String movementType;
	private String sourceAccount;
	private String transactionTypeSource;
	private double sourceAmount;
	private double commissionAmountSource;
	private String accountDestination;
	private String transactionTypeDestination;
	private double destinationAmount;
	private double commissionAmountDestination;
	
	public TransferRequestDto(){};
	
	public TransferRequestDto(String sourceAccount, String accountDestination){
		this.sourceAccount     = sourceAccount;
		this.accountDestination = accountDestination;
	}
	
	public int getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public int getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	
	public String getMovementType() {
		return movementType;
	}
	public void setMovementType(String movementType) {
		this.movementType = movementType;
	}
	
	public String getSourceAccount() {
		return sourceAccount;
	}
	public void setSourceAccount(String sourceAccount) {
		this.sourceAccount = sourceAccount;
	}
	
	public String getTransactionTypeSource() {
		return transactionTypeSource;
	}
	public void setTransactionTypeSource(String transactionTypeSource) {
		this.transactionTypeSource = transactionTypeSource;
	}
	
	public double getSourceAmount() {
		return sourceAmount;
	}
	public void setSourceAmount(double sourceAmount) {
		this.sourceAmount = sourceAmount;
	}
	
	public double getCommissionAmountSource() {
		return commissionAmountSource;
	}
	public void setCommissionAmountSource(double commissionAmountSource) {
		this.commissionAmountSource = commissionAmountSource;
	}
	
	public String getAccountDestination() {
		return accountDestination;
	}
	public void setAccountDestination(String accountDestination) {
		this.accountDestination = accountDestination;
	}
	
	public String getTransactionTypeDestination() {
		return transactionTypeDestination;
	}
	public void setTransactionTypeDestination(String transactionTypeDestination) {
		this.transactionTypeDestination = transactionTypeDestination;
	}
	
	public double getDestinationAmount() {
		return destinationAmount;
	}
	public void setDestinationAmount(double destinationAmount) {
		this.destinationAmount = destinationAmount;
	}
	
	public double getCommissionAmountDestination() {
		return commissionAmountDestination;
	}
	public void setCommissionAmountDestination(double commissionAmountDestination) {
		this.commissionAmountDestination = commissionAmountDestination;
	}
	
	public String toString() {
		String buffer;
		buffer = "sourceAccount[" + sourceAccount + "]";
		buffer += "sourceAmount[" + sourceAmount + "]";
		buffer += "accountDestination[" + accountDestination + "]";
		buffer += "destinationAmount[" + destinationAmount + "]";
		buffer += "movementType[" + movementType + "]";
		buffer += "commision[" + commissionAmountDestination + "]";
		return buffer;
	}

}
