package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"account",
	"authorization",
	"code",
    "description",
    "balance",
    "currentBalance"
})
@XmlRootElement(name = "LoadBalanceAcctDto")
public class BalanceMovementAcctRespDto {
	
	String account;
	String authorization;
	int code;
	String description;
	double balance;
	double currentBalance;
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public double getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}
	
	public String toString() {
		return "BalanceMovementAcctRespDto [account=" + account
				+ ", authorization=" + authorization + ", code=" + code
				+ ", description=" + description + ", balance=" + balance
				+ ", currentBalance=" + currentBalance + "]";
	}
	
	

}
