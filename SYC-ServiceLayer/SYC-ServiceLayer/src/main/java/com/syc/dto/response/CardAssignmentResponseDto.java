package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.syc.utils.GeneralUtil;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"code",
    "description",
    "authorization",
    "pan",
    "cardholderName",
    "expiryDate",
    "status",
    "type",
    "issueDate",
    "currentTime"
})
@XmlRootElement(name = "CardAssignmentResponseDto")
public class CardAssignmentResponseDto {
	
	int    code;
	String description;
	String authorization;
	String pan;
	String cardholderName;
	String expiryDate;
	String status;
	String type;
	String issueDate;
	String currentTime;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getCardholderName() {
		return cardholderName;
	}
	public void setCardholderName(String cardholderName) {
		this.cardholderName = cardholderName;
	}

	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getCurrentTime() {
		return currentTime;
	}
	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}

	public CardAssignmentResponseDto(){
		authorization = "0";
		issueDate     = GeneralUtil.formatDate( "dd-MM-yyyy" , GeneralUtil.getCurrentSqlDate() );
		currentTime   = GeneralUtil.formatDate( "HH:mm:ss" , GeneralUtil.getCurrentSqlDate() );
	}
	
	public String toString(){
		String response = null;
		response  = "code["+code+"],";
		response += "description["+description+"]";
		response += "authorization["+authorization+"]";
		response += "pan["+pan+"]";
		response += "cardholderName["+cardholderName+"]";
		response += "expiryDate["+expiryDate+"]";
		response += "status["+status+"]";
		response += "type["+type+"]";
		response += "issueDate["+issueDate+"]";
		response += "currentTime["+currentTime+"]";
		return response;
	}

}
