package com.syc.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.syc.persistence.dto.mapper.TransactionDto;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "code",
    "description",
    "lastestTransactions"
})
@XmlRootElement(name = "TransactionsListResponseDto")

public class TransactionsListResponseDto {
	
	public int    code;
	public String description;
	public List<TransactionDto> lastestTransactions;

	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<TransactionDto> getLastestTransactions() {
		return lastestTransactions;
	}

	public void setLastestTransactions(List<TransactionDto> lastestTransactions) {
		this.lastestTransactions = lastestTransactions;
	}

	@Override
	public String toString() {
		return "TransactionsListResponseDto [code=" + code + ", description="
				+ description + "]";
	}
	
	
	
}
