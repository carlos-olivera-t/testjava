package com.syc.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)

@XmlType(name = "", propOrder = {
	"bin",              
	"account",          
	"amountWithDrawal", 
	"currency",
	"transactionCodeAmount",
	"comissionAmount",         
	"transactionCodeCommission"  
})
@XmlRootElement(name = "AccountWithDrawalRequestDto")
public class AccountWithDrawalRequestDto {
	
	private String bin;
	private String account;
	private double amountWithDrawal;
	private String currency;
	private String transactionCodeAmount;
	private double comissionAmount;
	private String    transactionCodeCommission;
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public double getAmountWithDrawal() {
		return amountWithDrawal;
	}
	public void setAmountWithDrawal(double amountWithDrawal) {
		this.amountWithDrawal = amountWithDrawal;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getTransactionCodeAmount() {
		return transactionCodeAmount;
	}
	public void setTransactionCodeAmount(String transactionCodeAmount) {
		this.transactionCodeAmount = transactionCodeAmount;
	}
	public double getComissionAmount() {
		return comissionAmount;
	}
	public void setComissionAmount(double comissionAmount) {
		this.comissionAmount = comissionAmount;
	}
	public String getTransactionCodeCommission() {
		return transactionCodeCommission;
	}
	public void setTransactionCodeCommission(String transactionCodeCommission) {
		this.transactionCodeCommission = transactionCodeCommission;
	}
	
	public String toString() {
		return "AccountWithDrawalRequestDto [bin=" + bin + ", account="
				+ account + ", amountWithDrawal=" + amountWithDrawal
				+ ", currency=" + currency + ", transactionCodeAmount="
				+ transactionCodeAmount + ", comissionAmount=" + comissionAmount
				+ ", transactionCodeCommission=" + transactionCodeCommission
				+ "]";
	}

	
}
