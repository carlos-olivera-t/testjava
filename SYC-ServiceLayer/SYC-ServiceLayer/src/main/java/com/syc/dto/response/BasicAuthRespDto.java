package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "code",
    "description",
    "authorization"
})
@XmlRootElement(name = "BasicAuthRespDto")
public class BasicAuthRespDto {
	
	private int    code;
	private String description;
	private String authorization;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	
	public BasicAuthRespDto(){
		code = 0;
		description = null;
		authorization = "0";
	}
	
	public BasicAuthRespDto( int _code, String _desc ){
		code = _code;
		description = _desc;
		authorization = "0";
	}


	public String toString(){
		String response = null;
		response  = "code["+code+"],";
		response += "description["+description+"]";
		response += "authorization["+authorization+"]";
		return response;
	}
	
}
