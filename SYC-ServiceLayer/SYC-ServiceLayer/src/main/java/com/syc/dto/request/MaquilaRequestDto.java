package com.syc.dto.request;

import java.sql.Date;

public class MaquilaRequestDto {
	
	private String bin;
	private Date   startDate;
	private Date   endDate;
	private int    idEmbosser;
	private int    idImage;
	
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public int getIdEmbosser() {
		return idEmbosser;
	}
	public void setIdEmbosser(int idEmbosser) {
		this.idEmbosser = idEmbosser;
	}
	
	public int getIdImage() {
		return idImage;
	}
	public void setIdImage(int idImage) {
		this.idImage = idImage;
	}

	public String toString() {
		String buffer;
		buffer = "bin[ " + bin + " ]  ";
		buffer += "startDate[ " + startDate + " ]  ";
		buffer += "endDate[ " + endDate + " ]  ";
		buffer += "idEmbosser[ " + idEmbosser + " ]  ";
		buffer += "idImage[ " + idImage + " ]  ";
		return buffer;
	}

}
