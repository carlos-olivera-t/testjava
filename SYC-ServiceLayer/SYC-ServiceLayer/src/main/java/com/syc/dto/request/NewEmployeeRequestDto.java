package com.syc.dto.request;

public class NewEmployeeRequestDto {
	
	
	
	private int issCode;
	private String employer;
	private String employee;
	private String product;
	private String name;
	private String AddressCode;
	private String addressShortName;
	
	
	
	
	public String getAddressCode() {
		return AddressCode;
	}
	public void setAddressCode(String addressCode) {
		AddressCode = addressCode;
	}
	public String getAddressShortName() {
		return addressShortName;
	}
	public void setAddressShortName(String addressShortName) {
		this.addressShortName = addressShortName;
	}
	public int getIssCode() {
		return issCode;
	}
	public void setIssCode(int issCode) {
		this.issCode = issCode;
	}
	public String getEmployer() {
		return employer;
	}
	public void setEmployer(String employer) {
		this.employer = employer;
	}
	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NewEmployeeRequestDto [issCode=");
		builder.append(issCode);
		builder.append(", employer=");
		builder.append(employer);
		builder.append(", employee=");
		builder.append(employee);
		builder.append(", product=");
		builder.append(product);
		builder.append("]");
		return builder.toString();
	}
	
	

}
