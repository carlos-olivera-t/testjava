package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "IndicadorEstado",
    "NumeroTarjeta",
    "Saldo"
})
@XmlRootElement(name = "Saldo")
public class AkalaBalanceQuery {
	
	private int IndicadorEstado;
	private String NumeroTarjeta;
	private double Saldo;
	
	public int getIndicadorEstado() {
		return IndicadorEstado;
	}
	public void setIndicadorEstado(int indicadorEstado) {
		IndicadorEstado = indicadorEstado;
	}
	public String getNumeroTarjeta() {
		return NumeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		NumeroTarjeta = numeroTarjeta;
	}
	public double getSaldo() {
		return Saldo;
	}
	public void setSaldo(double saldo) {
		Saldo = saldo;
	}
	
	public String toString(){
		String response = null;
		response  = "IndicadorEstado["+IndicadorEstado+"],";
		response += "NumeroTarjeta["+NumeroTarjeta+"]";
		response += "Saldo["+Saldo+"]";
		return response;
	}
	

}
