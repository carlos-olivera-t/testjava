package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"code",
	"description",
	"authorization",
	"balance",
	"currentBalance",
	"transactionCodeAmount"
})
@XmlRootElement(name = "AccountWithDrawalResponseDto")
public class AccountWithDrawalResponseDto {
	
	private String authorization;
	private int    code;
	private double balance;
	private double currentBalance;
	private int transactionCodeAmount;
	private String description;

	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public double getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}
	
	
	
	public int getTransactionCodeAmount() {
		return transactionCodeAmount;
	}
	public void setTransactionCodeAmount(int transactionCodeAmount) {
		this.transactionCodeAmount = transactionCodeAmount;
	}
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String toString(){
		String response = null;
		response  = "code["+code+"],";
		response  = "description["+description+"],";
		response += "authorization["+authorization+"],";
		response += "balance["+balance+"],";
		response += "currentBalance["+currentBalance+"],";
		response += "transactionCodeAmount["+transactionCodeAmount+"],";
		return response;
	}

}
