package com.syc.dto.request;

import java.io.Serializable;
import java.sql.Date;

public class CardIssuancePropertiesRequestDto implements Serializable{
	
	private static final long serialVersionUID = -8286891424393573875L;
		
	private String formatedBin;
	private String formatedAccount;
	private String formatedbase;
	private String pan;
	private String cardType;
	private Date   expiriDate;
	private String cardIndicator;
	private Date   issueDate;

	/**
	 * @return the formatedBin
	 */
	public String getFormatedBin() {
		return formatedBin;
	}

	/**
	 * @param formatedBin
	 *            the formatedBin to set
	 */
	public void setFormatedBin(String formatedBin) {
		this.formatedBin = formatedBin;
	}

	/**
	 * @return the formatedAccount
	 */
	public String getFormatedAccount() {
		return formatedAccount;
	}

	/**
	 * @param formatedAccount
	 *            the formatedAccount to set
	 */
	public void setFormatedAccount(String formatedAccount) {
		this.formatedAccount = formatedAccount;
	}

	/**
	 * @return the formatedbase
	 */
	public String getFormatedbase() {
		return formatedbase;
	}

	/**
	 * @param formatedbase
	 *            the formatedbase to set
	 */
	public void setFormatedbase(String formatedbase) {
		this.formatedbase = formatedbase;
	}

	/**
	 * @return the pan
	 */
	public String getPan() {
		return pan;
	}

	/**
	 * @param pan
	 *            the pan to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the expiriDate
	 */
	public Date getExpiriDate() {
		return expiriDate;
	}

	/**
	 * @param expiriDate
	 *            the expiriDate to set
	 */
	public void setExpiriDate(Date expiriDate) {
		this.expiriDate = expiriDate;
	}

	/**
	 * @return the cardIndicator
	 */
	public String getCardIndicator() {
		return cardIndicator;
	}

	/**
	 * @param cardIndicator
	 *            the cardIndicator to set
	 */
	public void setCardIndicator(String cardIndicator) {
		this.cardIndicator = cardIndicator;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
}
