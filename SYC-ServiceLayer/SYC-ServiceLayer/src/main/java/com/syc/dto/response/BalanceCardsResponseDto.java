package com.syc.dto.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.syc.persistence.dto.mapper.BalanceDto;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"code",
    "description",
    "noCuenta",
    "clientName",
    "dateTime",
    "balanceCards"
})
@XmlRootElement(name = "BalanceCardsResponseDto")
public class BalanceCardsResponseDto {
	
	private int code;
	private String description;
	private String noCuenta;
	private String clientName;
	private String dateTime;
	private List<BalanceDto> balanceCards;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getNoCuenta() {
		return noCuenta;
	}
	public void setNoCuenta(String noCuenta) {
		this.noCuenta = noCuenta;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public List<BalanceDto> getBalanceCards() {
		return balanceCards;
	}
	public void setBalanceCards(List<BalanceDto> balanceCards) {
		this.balanceCards = balanceCards;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BalanceCardsResponseDto [code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", noCuenta=");
		builder.append(noCuenta);
		builder.append(", clientName=");
		builder.append(clientName);
		builder.append(", dateTime=");
		builder.append(dateTime);
		builder.append(", balanceCards=");
		builder.append(balanceCards);
		builder.append("]");
		return builder.toString();
	}
	
	
	

}
