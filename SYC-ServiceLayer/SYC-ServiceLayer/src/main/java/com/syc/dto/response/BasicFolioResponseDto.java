package com.syc.dto.response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "code",
    "description",
    "folio"
})
@XmlRootElement(name = "BasicFolioResponseDto")
public class BasicFolioResponseDto {
	
	private int    code;
	private String description;
	private long folio;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getFolio() {
		return folio;
	}
	public void setFolio(long folio) {
		this.folio = folio;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BasicFolioResponseDto [code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", folio=");
		builder.append(folio);
		builder.append("]");
		return builder.toString();
	}
	

}
