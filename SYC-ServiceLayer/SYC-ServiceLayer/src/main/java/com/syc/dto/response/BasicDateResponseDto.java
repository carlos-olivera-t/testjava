package com.syc.dto.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasicDataResponse", propOrder = { "codigo", "descripcion", "date",
		"time" })
public class BasicDateResponseDto {

	private int codigo;
	private String descripcion;
	private String date;
	private String time;

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	

	public BasicDateResponseDto() {
		codigo = 0;
		descripcion = null;
	}

	public BasicDateResponseDto(int code, String description) {
		codigo = code;
		descripcion = description;
	}

	
	public String toString() {
		StringBuffer response = new StringBuffer();
		response.append("codigo[" + codigo + "],");
		response.append("descripcion[" + descripcion + "]");
		response.append("date[" + date + "]");
		response.append("time[" + time + "]");
		
		return response.toString();
	}
}
