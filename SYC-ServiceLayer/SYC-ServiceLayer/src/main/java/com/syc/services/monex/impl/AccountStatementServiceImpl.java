package com.syc.services.monex.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.response.AccountStatementResponseDto;
import com.syc.persistence.dao.AccountStatementDao;
import com.syc.persistence.dto.mapper.AccountStatementDto;
import com.syc.services.LogService;
import com.syc.services.monex.AccountStatementService;
import com.syc.utils.CatalogUtil;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SystemConstants;

@Service
public class AccountStatementServiceImpl implements AccountStatementService {
	
	@SuppressWarnings("unused")
	private static final transient Log log = LogFactory.getLog(AccountStatementServiceImpl.class);
	
	@Autowired
	AccountStatementDao accountStatementDao;
	
	@Autowired
	LogService logService;
	
	public final static int NUMBER_OF_PERIODS = 6;
	
	public AccountStatementResponseDto getPostgresAccountStatement( String employer, String product ){
		
		final String URL_SIVALE_ACCOUNTSTATEMENT   = "http://200.15.1.91/";
		final String PHYSICAL_PATH                 = "C:\\dpro\\sivale\\";
		
		AccountStatementResponseDto response = new AccountStatementResponseDto();
		
		TreeMap<Integer,String> mapOfDates = GeneralUtil.getAccountStatementCutDates(NUMBER_OF_PERIODS);
		List<AccountStatementDto> listFinal = new ArrayList<AccountStatementDto>();
		String aux = null;
		
		/** Obteniendo los Estados de cuenta de la Fuente de Leopoldo **/
		List<AccountStatementDto> list = accountStatementDao.findSqlServerAccountStatement(employer, product, mapOfDates);
		response.setIdEmployer( employer );
		response.setNumberOfPeriods(NUMBER_OF_PERIODS);
		
		for( AccountStatementDto row: list ){
			/*****************************************************
			 *  Generando la URL para los estados de Cta en PDF
			 ***************************************************/
			aux = " ";
			if( row.getPdfPath()!=null ){
				aux = row.getPdfPath();
				aux = aux.replace(PHYSICAL_PATH, URL_SIVALE_ACCOUNTSTATEMENT);
				aux = aux.replace("\\", "/");
			}
			row.setPdfPath( aux );
			
			/**************************************************
			 *  Generando la URL para los estados de Excel
			 **************************************************/
			if( !GeneralUtil.isEmtyString(row.getExcelPath()) ){
				aux = " ";
				if( row.getIdProduct()!=null ){
					
					if(row.getIdProduct().trim().equals("01")){
						aux = URL_SIVALE_ACCOUNTSTATEMENT+"docs/excel/GASOLINA/";
					}else if( row.getIdProduct().trim().equals("02") ){
						aux = URL_SIVALE_ACCOUNTSTATEMENT+"docs/excel/VIAJES/";
					}else if( row.getIdProduct().trim().equals("03") ){
						aux = URL_SIVALE_ACCOUNTSTATEMENT+"docs/excel/DOLARES/";
					}
					
					aux += GeneralUtil.formatDate("yyyyMM", GeneralUtil.getDateFormat( "yyyy-MM-dd", row.getCutDate()) );
					aux += "/"+row.getEmployer()+".xls";
				}
				
				row.setExcelPath(aux);
			}
			
			listFinal.add( row );
		}
		
		response.setListAccountStatement(listFinal);
		
		return response;
	}

	public AccountStatementResponseDto monexAccountStatement( String employer, String product ){
		
		final String URL_SIVALE_ACCOUNTSTATEMENT   = "http://200.15.1.91";
		final String STATUS_ENABLED = "1";
		
		AccountStatementResponseDto response = new AccountStatementResponseDto();
		
		TreeMap<Integer,String> mapOfDates = GeneralUtil.getAccountStatementCutDates(NUMBER_OF_PERIODS);
		TreeMap<String,String> products    = CatalogUtil.getAccountStatementsProducts();
		
		List<AccountStatementDto> listFinal = new ArrayList<AccountStatementDto>();
		
		String aux           = null;
		StringBuilder buffer = null;
		String dateFolder    = null;
		
		Date fechaCorte      = null;
		Date fechaPivote     = null;

		/** Obteniendo los Estados de cuenta **/
		List<AccountStatementDto> list = accountStatementDao.findSqlServerAccountStatement(employer, product, mapOfDates);
		response.setIdEmployer( employer );
		response.setNumberOfPeriods(NUMBER_OF_PERIODS);
		
		fechaPivote = GeneralUtil.convertStringToUtilDate("dd/MM/yyyy", "30/11/2014");

		for( AccountStatementDto row: list ){
			
			/** Validando la Visibilidad del registro [EL PDF ES EL MANDATORIO]**/
			aux = row.getStartShowDate();
			if( aux != null && aux.contains(STATUS_ENABLED) ){
				
				dateFolder = GeneralUtil.formatDate("yyyyMM", GeneralUtil.getDateFormat( "yyyy-MM-dd", row.getCutDate()) );
				
				fechaCorte = GeneralUtil.convertStringToUtilDate("yyyy-MM-dd", row.getCutDate());

				/*****************************************************
				 *  Generando la URL para los estados de Cta en PDF
				 ***************************************************/
				aux = " ";
				if( !GeneralUtil.isEmtyString(row.getPdfPath()) ){
					aux = URL_SIVALE_ACCOUNTSTATEMENT+row.getPdfPath();
				}
				row.setPdfPath( aux );
				row.setStartShowDate(row.getCutDate());
				row.setIdProduct( GeneralUtil.completaCadena(row.getIdProduct(), '0', 2, "L") );
				
				/*****************************************************
				 *  Generando la URL para los estados de Cta en Excel
				 *****************************************************/
				aux = row.getExcelPath();
				if( aux != null && aux.contains(STATUS_ENABLED) && row.getIdProduct()!=null ){
					buffer = new StringBuilder();
					buffer.append(URL_SIVALE_ACCOUNTSTATEMENT); 
					buffer.append("/PRESTACIONES_UNIVERSALES/EXCEL/");
					buffer.append(products.get(row.getIdProduct())+"/");
					buffer.append(dateFolder+"/");
					buffer.append(row.getEmployer());

					/** Validando si la extension es csv o xls, solo aplica para productos Gasolina Fleet(07), Disesel Fleet(08)**/

					if( fechaPivote.before(fechaCorte) && ( row.getIdProduct().contains("07") ||
															row.getIdProduct().contains("08") ||
															row.getIdProduct().contains("19") ||    // GASOLINA_FLEET_PREMIUM
															row.getIdProduct().contains("20") ||    // TURBOSINA_FLEET
															row.getIdProduct().contains("21") ) ){  // GAS_LP_FLEET
						buffer.append( ".csv");
					}else{
						buffer.append( ".xls");
					}

					row.setExcelPath(buffer.toString());		
				}else{
					row.setExcelPath("Not Found");
				}
				
				/*****************************************************
				 *  Generando la URL para los estados de Cta en XML
				 *****************************************************/
				aux = row.getXmlPath();
				if( aux != null && aux.contains(STATUS_ENABLED) && row.getIdProduct()!=null ){
					buffer = new StringBuilder();
					buffer.append(URL_SIVALE_ACCOUNTSTATEMENT);
					buffer.append("/PRESTACIONES_UNIVERSALES/XML/");
					buffer.append(products.get(row.getIdProduct())+"/");
					buffer.append(dateFolder+"/MONEXS_");
					buffer.append(row.getIdProduct()+"_01");
					buffer.append(GeneralUtil.formatDate("MMyyyy", GeneralUtil.getDateFormat( "yyyy-MM-dd", row.getCutDate()) )+"_");
					buffer.append(row.getEmployer());
					buffer.append(".xml");
							
					row.setXmlPath(buffer.toString());		
				}else{
					row.setXmlPath("Not Found");
				}
				
				/** Agregando el registro modificado **/
				listFinal.add( row );
			}
		}
		
		/** Almacenando en bitacora **/
		logService.saveLog("0", employer, "monex_user", "employer[" + employer + "]", 1, "size["+listFinal.size()+"]", SystemConstants.PCODE_LOG_MONEX_ACC_STATMENT);
		
		response.setListAccountStatement(listFinal);
		return response;
	}
}
