package com.syc.services.impl;

import java.sql.Date;
import java.util.*;

import org.apache.commons.logging.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.request.*;
import com.syc.dto.response.*;
import com.syc.persistence.dao.*;
import com.syc.persistence.dto.*;
import com.syc.services.AditionalCardRequestDto;
import com.syc.services.CardIssuanceService;
import com.syc.services.CardService;
import com.syc.services.PhoneCodeService;
import com.syc.utils.*;

@Service("cardIssuanceService")
public class CardIssuanceServiceImpl implements CardIssuanceService{
	
	private static final transient Log log = LogFactory.getLog(CardIssuanceServiceImpl.class);
	
	private static final String SODEXO_RESTOPASS = "0006";
	private static final String PREFIJO_SODEXO_RESTOPASS = "077";
		
	@Autowired
	private AddressDao       addressDao;
	@Autowired
	private PhoneCodeService       phoneCodeService;
	@Autowired
	private CardService       cardService;
	@Autowired
	private CardholderDao    cardholderDao;	
	@Autowired
	private AccountDao       accountDao;
	@Autowired
	private WSLogDao        wsLogDao;
	@Autowired
	private EmisorDao 		emisorDao;
	@Autowired
	private CardDao          cardDao;
	@Autowired
	private IstrelDao        istrelDao;
	@Autowired
	private CardActivityDao cardActivityDao;
	@Autowired
	private EmbosserPropertiesDao embosserPropertiesDao;
	@Autowired
	private CardSeqDao cardSeqDao;
	@Autowired
	private BaseDao baseDao;
	@Autowired
	private CommonFidndersDao commonFidndersDao; 
	@Autowired
	private CardLimitDao cardLimitDao;
	@Autowired
	private IstcmscardDao    istcmscardDao; 
	@Autowired
	private IstCardTypeDao          istCardTypeDao;
	@Autowired
	private EmployerDao employerDao;
	@Autowired
	private EmployerDetailDao employerDetailDao;
	@Autowired
	private TblEmployeeDao       tblEmployeeDao;
	
	WSLogUtil wsUtil;
	
	
	public CardAssignmentResponseDto embosserCardAssignment(String bin,String product, String account,DemographicDataRequestDto demographicRequestDto, String idClient) {
		int    codeResponse  = 0;
		String authorization = "0";
		String demograficoValidacion = null;
		String descripcion = null;
		Date expiryDate = null;
		//String strCardType = null;
		String cardNumberBuild = null;
		String request = "bin["+bin+"]product["+product+"]account["+account+"]"+"]demographicDto["+demographicRequestDto.toString() +"]";

		
		/** El bin en las tablas esta a 10 posiciones */
		String binSyc = GeneralUtil.completaCadena(bin,'0',10,"L");
		String cardType1 = GeneralUtil.completaCadena(product,'0',4,"L");
		/**Buscamos el registro del cardtype*/
		
		IstCardTypeDto istCardPrefix = istCardTypeDao.findPrefixByBin(binSyc, cardType1);
		
		if(istCardPrefix == null){
			codeResponse = 14;
		}else{
			/** Obtenemos al prefijo de la tabla emisores */
			EmisorDto oEmisorDto = emisorDao.findByBin(binSyc);	
			/** Generamos el numero de cuenta */
			String accountBuild      = GeneralUtil.buildPrimaryAccount( account, oEmisorDto.getCuenta().trim() );
			demograficoValidacion = isValidosDemograficos(demographicRequestDto);
			if ( demograficoValidacion.equals(SystemMesages.OK) ){
				/** Verifico que la cuenta no exista */
				if( accountDao.findInfoAccount(binSyc, accountBuild) == null ){
					/** Obteniendo un numero de Autorizacion **/
					authorization = GeneralUtil.generateAuthorizationNumber(6);
				
					
					String cardPrefix = istCardPrefix.getCardPrefix();
					if(cardPrefix != null){
					/** Generamos el numero de tarjeta */
						long noSeq = cardSeqDao.getMaxSeq(bin,cardPrefix.substring(6).trim());
						cardNumberBuild	 = GeneralUtil.buildNumberCard(cardPrefix.substring(0, 6),cardPrefix.substring(6).trim(), noSeq);
					}
					//TODO Validar la variable list = 0, list = 1; ya que cuando se quiere dar de alta una tercera cuenta devuelve un error de base no encontrada  
					int list = 0;
					String maxBaseStr = null;
					if(GeneralUtil.isEmtyString(idClient)){
						/** Obtenemos el max de ISTBASE */
						long maxBase = baseDao.getMaxBase();
						maxBaseStr = GeneralUtil.completaCadena(maxBase+"",'0',10,"L");
					}else{
						maxBaseStr = GeneralUtil.completaCadena(idClient+"",'0',10,"L");
						list = 1;
					}
					//buscar las bases
					List<CardDto> bases = cardDao.findIdClientMaxOneRow(binSyc, maxBaseStr);
					if(bases.size() == list){
						/** Obtenemos los meses de expiracion y el cardtype */
						IstCardTypeDto cardType = commonFidndersDao.findIstCardTypeInfo(binSyc);
				
						/** Fecha de expiración de la tarjeta */
						expiryDate = getExpiryDate(cardType);
			
						CardIssuancePropertiesRequestDto embosserCardDto = new CardIssuancePropertiesRequestDto();
						embosserCardDto.setCardIndicator(SystemConstants.CARDTYPE_TITULAR);
						embosserCardDto.setCardType(cardType1);
						embosserCardDto.setExpiriDate(expiryDate);
						embosserCardDto.setFormatedAccount(accountBuild);
						embosserCardDto.setFormatedBin(binSyc);
						embosserCardDto.setPan(cardNumberBuild);
						embosserCardDto.setFormatedbase(maxBaseStr);
						embosserCardDto.setIssueDate(GeneralUtil.getCurrentSqlDate());
				
						/** Guardamos tarjeta */
						saveEmbosserCard(embosserCardDto, demographicRequestDto,true, null);
						CardActivityDto cardActivity = null;
						/** Almacenando en ISTCMSACT */	
						List<String> concentrators = binsLevelCaptation();
						if(concentrators.contains(bin)){
							cardActivity = new CardActivityDto(true);
							cardActivity.setPan     (embosserCardDto.getPan());
							cardActivity.setCardType(embosserCardDto.getCardType());
							cardActivity.setActCount(89);
						}else{
							cardActivity = new CardActivityDto(true);
							cardActivity.setPan     (embosserCardDto.getPan());
							cardActivity.setCardType(embosserCardDto.getCardType());
						}
						cardActivityDao.create  (cardActivity);
				
						/** Seteando un codigo de respuesta exitoso **/
						codeResponse = 1;
					}else{
						if(list == 0)
							codeResponse = 11;//la base ya existe
						else
							codeResponse = 12;//base no encontrada
					}
				}else{
					codeResponse = 4; /** La cuenta ya fue asignada */
				}
						
			}else{
				codeResponse = 10; /** Dato no supero validacion*/
			}
		}
	
		if(codeResponse == 10)
			descripcion = demograficoValidacion;
		else 
			descripcion =  CatalogUtil.getCatalogAssignmentAccount().get(codeResponse);
		
		CardAssignmentResponseDto response = new CardAssignmentResponseDto();
		response.setCode			( codeResponse );
		response.setDescription  	( descripcion );
		
		if( codeResponse == SystemConstants.PROCESO_FINALIZADO_EXITOSAMENTE ){
			response.setPan				( cardNumberBuild );
			response.setExpiryDate      ( GeneralUtil.formatDate( "dd-MM-yyyy" ,expiryDate ) );		
			response.setType			( "Titular" );
			response.setStatus			( "Inactiva" );					
			response.setAuthorization	( authorization ); 							
			response.setCardholderName  (demographicRequestDto.getName());
		}
		
		log.debug( "::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog(binSyc, cardNumberBuild, SecurityUtils.getCurrentUser(), request, codeResponse, response, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_EMBOSSER);
				
		
		return response;
	}

	public CardAssignmentResponseDto embosserCardAssignment(String bin,String product, String account,DemographicDataRequestDto demographicRequestDto) {
		
		return embosserCardAssignment( bin, product,  account, demographicRequestDto, null);
	}
	public ClientAccountResponseDto accountAssignment( String bin,  String product, String idClient, String phoneCode, DemographicDataRequestDto demographicRequestDto , String idBanco, String plazaBanjico){
		String request = "bin["+bin+"]product["+product+"]demographicDto["+demographicRequestDto.toString() +"]";
		ClientAccountResponseDto response = new ClientAccountResponseDto();
		int codeResponse = 0;
		CardAssignmentResponseDto cardResponse = null;
		long noCta = 0;
		String authorization = "0";
		String productMoral = "04";
		String codeClient = null;
		String cuenta = null;
		String nombreCliente = null;
		String fecha = null;
		String hora = null;
		String description = null;
		String ctaClabe = null;
		
		etiqueta:
		if(productMoral.equals(product)){
			boolean isClient = GeneralUtil.isEmtyString(idClient);
			boolean isPhone = GeneralUtil.isEmtyString(phoneCode);
			if(isClient && isPhone){
				codeResponse = 17;
				nombreCliente = demographicRequestDto.getName();
				description =   CatalogUtil.getCatalogAssignmentAccount().get(codeResponse);	
				break etiqueta;
			}else{	
				try{
					//x mientras en lo que arreglamos con lorenz lo de la secuencia
					noCta = cardSeqDao.getMaxSeqCta(bin);
					if(noCta >= 0){
						cardResponse =  embosserCardAssignment(bin, product, String.valueOf(noCta), demographicRequestDto, idClient);
		
						if(cardResponse != null){
							//guardar nivel de captacion
							CardDto card = cardDao.findByNumberCard(cardResponse.getPan());
							String clabe = null;
							//validaciones para que el idbanco y idplaza sean correctos y no se genere mal una clabe
							if(card != null){
								clabe = GenerateCLABE.generateCLABE(idBanco, plazaBanjico, card.getAccount().substring(8).trim());
								bin = GeneralUtil.completaCadena(bin+"",'0',10,"L");
								if(isClient && phoneCode != null){
									phoneCodeService.createPhoneCode(card.getNumberCard().trim(), null, phoneCode);
								}
								cardService.activationCard(card.getNumberCard(), SecurityUtils.getCurrentUser());
								AccountDto accountDto = accountDao.findInfoAccount(bin, card.getAccount());
								if(accountDto != null){
									accountDto.setShortName(clabe);
									accountDto.setAssignmentAccount(SystemConstants.CUENTA_ASIGNADA);
									accountDao.update(accountDto);
								}
							
								codeClient = card.getClientId();
								cuenta =card.getAccount().substring(8).trim();
								authorization = cardResponse.getAuthorization();
								nombreCliente = cardResponse.getCardholderName();
								codeResponse = cardResponse.getCode();
								hora = cardResponse.getCurrentTime();
								description = cardResponse.getDescription();
								fecha = cardResponse.getIssueDate();
					
								ctaClabe = clabe;
								codeResponse = 1;
					
						}else{
							authorization = cardResponse.getAuthorization();
							nombreCliente = cardResponse.getCardholderName();
							codeResponse =  cardResponse.getCode();
							hora =          cardResponse.getCurrentTime();
							description =   cardResponse.getDescription();
							fecha =         cardResponse.getIssueDate();
						}
		
					}
				}else{
					codeResponse = 15;
					authorization = "0";
					nombreCliente = demographicRequestDto.getName();
					description =   CatalogUtil.getCatalogAssignmentAccount().get(codeResponse);	
				}
				}catch(Exception e){
					e.printStackTrace();
					codeResponse =14;
					authorization = "0";
					nombreCliente = demographicRequestDto.getName();
					description =   CatalogUtil.getCatalogAssignmentAccount().get(codeResponse);		
				}
			}
		}else{
			codeResponse =14;
			authorization = "0";
			nombreCliente = demographicRequestDto.getName();
			description =   CatalogUtil.getCatalogAssignmentAccount().get(codeResponse);	
		}
	
		response.setAuthorization(authorization);
		response.setCLABE(ctaClabe);
		response.setClientName(nombreCliente);
		response.setCode(codeResponse);
		response.setCurrentTime(hora);
		response.setDescription(description);
		response.setIssueDate(fecha);
		response.setNoClient(codeClient);
		response.setNoCuenta(cuenta);
		
			if( cardResponse != null ){
				log.debug( "::Almacenando Movimiento en Bitacora::");
				wsUtil = new WSLogUtil( wsLogDao );
				wsUtil.saveLog(bin, cardResponse.getPan(), SecurityUtils.getCurrentUser(), request, cardResponse.getCode(), cardResponse, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_CONCENTRATOR);

			}else{
	
			log.debug( "::Almacenando Movimiento en Bitacora::");
			wsUtil = new WSLogUtil( wsLogDao );
			wsUtil.saveLog(bin, (cardResponse != null) ? cardResponse.getPan() : "538984", SecurityUtils.getCurrentUser(), request, codeResponse, response, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_CONCENTRATOR);
			}
			
			
		return response;
	}

	public CardAssignmentResponseDto aditionalCardAssignment(AditionalCardRequestDto aditionalCardRequestDto) {
		int    codeResponse  = 0;
		String authorization = "0";
		String binSyc = null;
		String request = "aditionalCardRequestDto["+aditionalCardRequestDto+"]";
		
		String demograficoValidacion = isValidosDemograficos(aditionalCardRequestDto);
		
		Date expiryDate = null;
		String cardNumberBuild = null;
		
		if ( demograficoValidacion.equals(SystemMesages.OK) ){			
			CardholderDto titularDto = cardholderDao.getCardholderInfoByCardNumber(aditionalCardRequestDto.getCardholder());			
			EmbosserPropertiesDto embosserPropertiesDto = embosserPropertiesDao.findEmbosserProperiesByPan(aditionalCardRequestDto.getCardholder());
			
			if(titularDto != null && titularDto.getAccount() != null && 
					titularDto.getIstrel() != null && 
					titularDto.getCard() != null && 
					embosserPropertiesDto != null &&
					embosserPropertiesDto.getCardIndicator().equals(SystemConstants.CARDTYPE_TITULAR)){
					String estatus = titularDto.getAccount().getStatus().trim();															
				if(estatus.equals(SystemConstants.STATUS_ACTIVE_ACCOUNT)){
					/** Obtenemos el bin de la titular */
					binSyc = titularDto.getIstrel().getBin();
													
					/** Obtenemos la cuenta de la titular */
					String account      = titularDto.getAccount().getAccount();												
					
					/** Obteniendo un numero de Autorizacion **/
					authorization = GeneralUtil.generateAuthorizationNumber(6);								
					
					//long noSeq = cardSeqDao.getMaxSeq(binSyc.substring(4,10),"00");
					
					String cardType1 = titularDto.getCard().getCardType();
					/**Buscamos el registro del cardtype*/
					
					IstCardTypeDto istCardPrefix = istCardTypeDao.findPrefixByBin(binSyc, cardType1);
					
					if(istCardPrefix == null){
						codeResponse = 14;
					}else{
						
						String cardPrefix = istCardPrefix.getCardPrefix();
						if(cardPrefix != null){
						/** Generamos el numero de tarjeta */
							long noSeq = cardSeqDao.getMaxSeq(cardPrefix.substring(0,6),cardPrefix.substring(6).trim());
							cardNumberBuild	 = GeneralUtil.buildNumberCard(cardPrefix.substring(0, 6),cardPrefix.substring(6).trim(), noSeq);
						}
					
						/** Generamos el numero de tarjeta */
						//cardNumberBuild	 = GeneralUtil.buildNumberCard(binSyc.substring(4,10),"00", noSeq);
					
						/** Obtenemos la base de la titular */				
						String maxBaseStr = titularDto.getIstrel().getBase();
					
						/** Obtenemos los meses de expiracion y el cardtype */
						IstCardTypeDto cardType = commonFidndersDao.findIstCardTypeInfo(binSyc);
					
						/** Fecha de expiración de la tarjeta */
						expiryDate = getExpiryDate(cardType);
					
						/** Cadena con el cardType */
						//strCardType = cardType.getCardType();
						//String strType = GeneralUtil.completaCadena(strCardType,'0',4,"L");
					
						CardIssuancePropertiesRequestDto embosserCardDto = new CardIssuancePropertiesRequestDto();
						embosserCardDto.setCardIndicator(SystemConstants.CARDTYPE_ADICIONAL);
						embosserCardDto.setCardType(cardType1);
						embosserCardDto.setExpiriDate(expiryDate);
						embosserCardDto.setFormatedAccount(account);
						embosserCardDto.setFormatedBin(binSyc);
						embosserCardDto.setPan(cardNumberBuild);
						embosserCardDto.setFormatedbase(maxBaseStr);
						embosserCardDto.setIssueDate(GeneralUtil.getCurrentSqlDate());
					
						AddressDto titularAddressDto = addressDao.findCardAddress(titularDto.getCard().getNumberCard());
						DemographicDataRequestDto demographicRequestDto = new DemographicDataRequestDto();
					
						/** Construimos el objeto DemographicRequestDto*/
						if(titularAddressDto != null){
						
							demographicRequestDto.setAddress	(titularAddressDto.getDireccion());
							demographicRequestDto.setCellPhone	(titularAddressDto.getTelefono3());
							demographicRequestDto.setCity		(titularAddressDto.getCiudad());
							demographicRequestDto.setColony		(titularAddressDto.getColonia());
					
							demographicRequestDto.setHomePhone	(titularAddressDto.getTelefono());
							demographicRequestDto.setMunicipality(titularAddressDto.getLocalidad());
					
							demographicRequestDto.setState		(titularAddressDto.getEstado());
							demographicRequestDto.setWorkPhone	(titularAddressDto.getTelefono2());
							demographicRequestDto.setZipCode	(titularAddressDto.getCp());
						}
					
						demographicRequestDto.setName		(aditionalCardRequestDto.getAdditionalName());
						demographicRequestDto.setShortName	(aditionalCardRequestDto.getAdditionalShortName());
						demographicRequestDto.setEmail		(aditionalCardRequestDto.getAdditionalMail());
						demographicRequestDto.setRfc		(aditionalCardRequestDto.getAdditionalRFC());
					
//						CardLimitDto limitesIndividualesTitular = null;
//					
//						if(titularDto.getCard().getLimitIndicator().equals("1")){
//							/** Tiene limites la tarjeta */					
//							limitesIndividuales = cardLimitDao.findBypan(titularDto.getCard().getNumberCard());
//						
//						
//							limitesIndividuales.setPan(cardNumberBuild);
//						}
									
						/** Guardamos tarjeta */
						saveEmbosserCard(embosserCardDto, demographicRequestDto, false, null);
					
						/** Almacenando en ISTCMSACT */			
						CardActivityDto cardActivity = new CardActivityDto(true);
						cardActivity.setPan     (embosserCardDto.getPan());
						cardActivity.setCardType(embosserCardDto.getCardType());
						cardActivityDao.create  (cardActivity);
					
						/** Seteando un codigo de respuesta exitoso **/
						codeResponse = 1;	
					}
					}else{
					codeResponse = 8; /** La cuenta no esta activa */
				}												
			}else{
				codeResponse = 5; /** No existe tarjeta titular*/ 
			}			
		}else{
			codeResponse = 10; /** Dato no supero validacion*/
		}
		
		
		String descripcion = null;
		if(codeResponse == 10)
			descripcion = demograficoValidacion;
		else 
			descripcion =  CatalogUtil.getCatalogAssignmentAccount().get(codeResponse);
		
		CardAssignmentResponseDto response = new CardAssignmentResponseDto();
		response.setCode			( codeResponse );
		response.setDescription  	( descripcion );
		
		if( codeResponse == SystemConstants.PROCESO_FINALIZADO_EXITOSAMENTE ){
			response.setPan				( cardNumberBuild );
			response.setExpiryDate      ( GeneralUtil.formatDate( "dd-MM-yyyy" ,expiryDate ) );		
			response.setType			( "Adicional" );
			response.setStatus			( "Inactiva" );					
			response.setAuthorization	( authorization ); 							
			response.setCardholderName  (aditionalCardRequestDto.getAdditionalName());
		}	
		
		log.debug( "::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog(binSyc, cardNumberBuild, SecurityUtils.getCurrentUser(), request, codeResponse, response, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_EMBOSSER); 
				
		
		return response;

	}

	public CardAssignmentResponseDto replaceCard(String lockedCard) {
		int    codeResponse  = 0;
		String authorization = "0";
		String binSyc = null;
		String request = "lockedCard["+lockedCard+"]";
		
		Date expiryDate = null;
		String cardNumberBuild = null;
		String cardIndicator = "";
		DemographicDataRequestDto  demographicRequestDto = null;
		
		CardholderDto tarjetaRemplazarDto = cardholderDao.getCardholderInfoByCardNumber(lockedCard);			
		
		if(tarjetaRemplazarDto != null && tarjetaRemplazarDto.getAccount() != null && tarjetaRemplazarDto.getIstrel() != null && tarjetaRemplazarDto.getCard() != null){									
			if( tarjetaRemplazarDto.getCard().getStatus() == SystemConstants.STATUS_STOLEN_CARD ||
					tarjetaRemplazarDto.getCard().getStatus() == SystemConstants.STATUS_LOST_CARD || 
					tarjetaRemplazarDto.getCard().getStatus() == SystemConstants.STATUS_DAMAGED_CARD){
				
				/** Obtenemos el bin de la tarjeta a reemplzar */
				binSyc = tarjetaRemplazarDto.getIstrel().getBin();
												
				/** Obtenemos la cuenta de la tarjeta a remplazar */
				String account      = tarjetaRemplazarDto.getAccount().getAccount();												
				
				/** Obteniendo un numero de Autorizacion **/
				authorization = GeneralUtil.generateAuthorizationNumber(6);
								
				/***hay que hacer una validacion para que si no se encuentra la secuencia mande un msj y no el trace**/
				//long noSeq = cardSeqDao.getMaxSeq(binSyc.substring(4,10),"00");
				
				/** Generamos el numero de tarjeta */
				//cardNumberBuild	 = GeneralUtil.buildNumberCard(binSyc.substring(4,10),"00", noSeq);
				String cardType1 = tarjetaRemplazarDto.getCard().getCardType();
				IstCardTypeDto istCardPrefix = istCardTypeDao.findPrefixByBin(binSyc, cardType1);
				
				if(istCardPrefix == null){
					codeResponse = 14;
				}else{
					
					String cardPrefix = istCardPrefix.getCardPrefix();
					if(cardPrefix != null){
					/** Generamos el numero de tarjeta */
						long noSeq = cardSeqDao.getMaxSeq(cardPrefix.substring(0,6),cardPrefix.substring(6).trim());
						cardNumberBuild	 = GeneralUtil.buildNumberCard(cardPrefix.substring(0, 6),cardPrefix.substring(6).trim(), noSeq);
					}
					/** Obtenemos la base de la tarjeta a reemplazar */				
					String maxBaseStr = tarjetaRemplazarDto.getIstrel().getBase();
				
					/** Obtenemos los meses de expiracion y el cardtype */
					IstCardTypeDto cardType = commonFidndersDao.findIstCardTypeInfo(binSyc);
				
					/** Fecha de expiración de la tarjeta */
					expiryDate = getExpiryDate(cardType);
				
					EmbosserPropertiesDto embosserPropertiesDto = embosserPropertiesDao.findEmbosserProperiesByPan(lockedCard);				
				
					cardIndicator = embosserPropertiesDto.getCardIndicator();
				
					CardIssuancePropertiesRequestDto embosserCardDto = new CardIssuancePropertiesRequestDto();
					embosserCardDto.setCardIndicator(cardIndicator);
					embosserCardDto.setCardType(cardType1);
					embosserCardDto.setExpiriDate(expiryDate);
					embosserCardDto.setFormatedAccount(account);
					embosserCardDto.setFormatedBin(binSyc);
					embosserCardDto.setPan(cardNumberBuild);
					embosserCardDto.setFormatedbase(maxBaseStr);
					embosserCardDto.setIssueDate( embosserPropertiesDto.getIssueDate() );
				
					/** Obtengo los datos demograficos de la tarjeta a reemplzar */
					AddressDto reemplzaoAddressDto = addressDao.findCardAddress(lockedCard);
				
					/** Actualizando los datos personales en ISTCMSCARD**/								
					EmbosserPropertiesDto reemplazoIstcmscardDto = embosserPropertiesDao.findEmbosserProperiesByPan(lockedCard);
							
					/** Construimos el objeto DemographicRequestDto*/
					demographicRequestDto = new DemographicDataRequestDto();
				
					if(reemplzaoAddressDto != null){
					
						demographicRequestDto.setShortName	(reemplazoIstcmscardDto.getShortName());
					}		
				
					if(reemplzaoAddressDto != null){
						demographicRequestDto.setName		(reemplzaoAddressDto.getNombre());
				
						demographicRequestDto.setAddress	(reemplzaoAddressDto.getDireccion());
						demographicRequestDto.setCellPhone	(reemplzaoAddressDto.getTelefono3());
						demographicRequestDto.setCity		(reemplzaoAddressDto.getCiudad());
						demographicRequestDto.setColony		(reemplzaoAddressDto.getColonia());
						demographicRequestDto.setEmail		(reemplzaoAddressDto.getEmail());
						demographicRequestDto.setHomePhone	(reemplzaoAddressDto.getTelefono());
						demographicRequestDto.setMunicipality(reemplzaoAddressDto.getLocalidad());
						demographicRequestDto.setRfc		(reemplzaoAddressDto.getRfc());
						demographicRequestDto.setState		(reemplzaoAddressDto.getEstado());
						demographicRequestDto.setWorkPhone	(reemplzaoAddressDto.getTelefono2());
						demographicRequestDto.setZipCode	(reemplzaoAddressDto.getCp());
					}
				
					CardLimitDto limitesIndividuales = null;
				
					if(tarjetaRemplazarDto.getCard().getLimitIndicator().equals("1")){
						/** Tiene limites la tarjeta */					
						//CardLimitPk cardLimitPk = new CardLimitPk(binSyc,"00");
					
						CardLimitDto limitesIndividualesTarjetaBloqueada = cardLimitDao.findBypan(binSyc,tarjetaRemplazarDto.getCard().getNumberCard());
						if( limitesIndividualesTarjetaBloqueada != null ){
							limitesIndividuales = new CardLimitDto( limitesIndividualesTarjetaBloqueada );
							limitesIndividuales.setPan(cardNumberBuild);
						}
					}
								
					/** Guardamos tarjeta */
					saveEmbosserCard(embosserCardDto, demographicRequestDto, false, limitesIndividuales);
				
					/** Cambiando el estatus de la Tarjeta a Reemplazada **/
					CardDto cardDto = tarjetaRemplazarDto.getCard();
					cardDto.setStatus(SystemConstants.STATUS_REPLACED_CARD);
					cardDao.update(cardDto);
	
					 String userid = (SecurityUtils.getCurrentUser().length() <= 10) ? SecurityUtils.getCurrentUser() : SecurityUtils.getCurrentUser().substring(0, 10);
					
					CardActivityDto cardActivity1 = new CardActivityDto();
					cardActivity1.setPan       ( cardDto.getNumberCard() );
					cardActivity1.setCardType  ( tarjetaRemplazarDto.getCard().getCardType() );
					cardActivity1.setUserId    ( userid );
					cardActivity1.setSpareChar ( authorization ); 
					cardActivity1.setCmsFunc   ( "50" );
					cardActivity1.setReplReason( "BLOQUEO POR REEMPLAZO" );
					/** Almacenando en ISTCMSACT */	
					
					CardActivityDto cardActivity = new CardActivityDto(true);
					cardActivity.setPan       (embosserCardDto.getPan().trim());
					cardActivity.setPanReplace(lockedCard.trim());
					cardActivity.setUserId  (userid);
					cardActivity1.setSpareChar ( "   r"+authorization ); 
					cardActivity.setCardType  (embosserCardDto.getCardType().trim());
					cardActivity1.setCmsFunc("06");
					cardActivity1.setReplReason( "REEMPLAZO" );
					cardActivityDao.create    (cardActivity);
			
					/** Seteando un codigo de respuesta exitoso **/
					codeResponse = 1;	
				}
				}else{
					codeResponse = 6; /** La tarjeta a reemplzar aun esta activa*/ 
				}						
			}else{
				codeResponse = 7; /** No existe la tarjeta a reemplazar */ 
			}
					
			String descripcion =  CatalogUtil.getCatalogAssignmentAccount().get(codeResponse);
			String tipo = "";
		
			if(cardIndicator.equals("0")){
				tipo = "Titular";
			}else{
				tipo = "Adicional";
			}
		
			CardAssignmentResponseDto response = new CardAssignmentResponseDto();
			response.setCode			( codeResponse );
			response.setDescription  	( descripcion );
		
			if( codeResponse == SystemConstants.PROCESO_FINALIZADO_EXITOSAMENTE ){
			response.setPan				( cardNumberBuild );
			response.setExpiryDate      ( GeneralUtil.formatDate( "dd-MM-yyyy" ,expiryDate ) );		
			response.setType			( tipo );
			response.setStatus			( "Inactiva" );					
			response.setAuthorization	( authorization ); 							
			response.setCardholderName  (demographicRequestDto.getName());
		}	
		
		log.debug( "::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog(binSyc, cardNumberBuild, SecurityUtils.getCurrentUser(), request, codeResponse, response, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_EMBOSSER); 				
		
		return response;
	}


	
	public CardAssignmentResponseDto stockCardAssignment(String account,String pan, DemographicDataRequestDto demographicRequestDto, boolean accountFlag, String product, String idClient, boolean isActive, boolean isFunction, NewEmployeeRequestDto employee, boolean isSodexo) {		
		
		int    codeResponse  = 0;
		String authorization = "0";
		String bin = "0";
		/** Variable para enviar la respuesta */
		CardAssignmentResponseDto response = new CardAssignmentResponseDto();
		
		String request = "pan["+pan+"]account["+account+"]"+ "product["+product+"], idClient["+idClient+"]"+"employeeRequest["+employee+"], demographicDto["+demographicRequestDto.toString() +"]";
		String accountBuild = null;
		int listSize = 0;
					
		/** Obteniendo información de la tarjeta a Asignar**/
		CardholderDto cardholderDto = cardholderDao.getCardholderInfoByCardNumber(pan);
		
		String demograficoValidacion = isValidosDemograficos(demographicRequestDto);
		empleado:
		if(cardholderDto != null && cardholderDto.getCard() != null && cardholderDto.getAccount() != null && cardholderDto.getIstrel() != null){
			if ( demograficoValidacion.equals(SystemMesages.OK) ){
				/** Obtenemos el bin de la tarjeta a reemplzar */
				bin = cardholderDto.getIstrel().getBin();
				if( cardholderDto.getCard().getStatus() == SystemConstants.STATUS_INACTIVE_CARD ){
					
					
					if(accountFlag){
					
						/** Obtenemos al prefijo de la tabla emisores */
						EmisorDto oEmisorDto = emisorDao.findByBin(cardholderDto.getIstrel().getBin());
						
						if(isSodexo && cardholderDto.getCard().getCardType().equals(SODEXO_RESTOPASS)){
							accountBuild  = GeneralUtil.buildPrimaryAccount( account, PREFIJO_SODEXO_RESTOPASS ); 
						}else{
							accountBuild  = GeneralUtil.buildPrimaryAccount( account, oEmisorDto.getCuenta().trim() ); 
						}
					}else{	
						if(isSodexo && cardholderDto.getCard().getCardType().equals(SODEXO_RESTOPASS)){
							accountBuild  = GeneralUtil.buildPrimaryAccount( cardholderDto.getCard().getAccount().substring(3), "077" ); 
						}else{
							accountBuild = cardholderDto.getCard().getAccount();
						}
						listSize = 1;
					}
					AccountDto oAccountDto =  cardholderDto.getAccount();
					
					/** La cuenta debe existir y debe ser dummy, el assignmentAccount debe ser 0 = cuenta no asignada*/
					if(  null != oAccountDto  && oAccountDto.getAssignmentAccount()!=null && 
							oAccountDto.getAssignmentAccount().trim().equals( SystemConstants.CUENTA_DISPONIBLE_PARA_ASIGNACION )){
						/** Verifico no existan tarjetas asignadas **/
						List<CardDto> list = cardDao.findByAccountMaxOneRow(accountBuild);
						//TODO Validar por que usa el listSize en 1, ya que no permite asosciar 3 cuentas a un cliente
						if ( list.size() == listSize || (isSodexo && list.size() <= listSize)){//se modifico menor igual ya que en el producto de sodexo 0006(Restopass) el prefijo cambia
														//y no encontrará registro de la cuenta
							
							CardIssuancePropertiesRequestDto cardIssuancePropertiesRequestDto = new CardIssuancePropertiesRequestDto();
							cardIssuancePropertiesRequestDto.setFormatedAccount(accountBuild);
							cardIssuancePropertiesRequestDto.setPan(cardholderDto.getCard().getNumberCard());
							cardIssuancePropertiesRequestDto.setCardType((product != null) ? GeneralUtil.completaCadena(product,'0',4,"L") : cardholderDto.getCard().getCardType());
							
							if(GeneralUtil.isEmtyString(idClient)){
								cardIssuancePropertiesRequestDto.setFormatedbase(cardholderDto.getCard().getClientId());
							}else
								cardIssuancePropertiesRequestDto.setFormatedbase(GeneralUtil.completaCadena(idClient+"",'0',10,"L"));
							
							String maxBaseStr = cardIssuancePropertiesRequestDto.getFormatedbase();
							//buscar las bases
							List<CardDto> bases = cardDao.findIdClient(bin, maxBaseStr);
							int code = 0;
							if(bases.size() >= 1){
								/**actualizamos datos de empleado**/
								if(isFunction){
									
									employee.setName(demographicRequestDto.getShortName());
							
								
									 code = updateEmployee(pan, accountBuild, null, employee, false, isSodexo);
									 if(code != 1){
										 switch(code){
											case(3):
												codeResponse = 21;
											break empleado;
											case(2):
												codeResponse = 20;
											break empleado;
										 	case(4):
												codeResponse = 23;
											break empleado;
											}
											break empleado;
										}
								}
									
									/** Obteniendo un numero de Autorizacion **/
									authorization = GeneralUtil.generateAuthorizationNumber(6);
									/**Cambiando a asignada la cuenta**/
									AccountDto accountDto = cardholderDto.getAccount();
									accountDto.setAssignmentAccount( SystemConstants.CUENTA_ASIGNADA );

									//accountDao.update(accountDto);
									
									cardholderDto.setAccount(accountDto);
									

//									accountDao.update(accountDto);

									
									/** Actualizamos datos stock */
									updateStockCard(cardIssuancePropertiesRequestDto, demographicRequestDto, cardholderDto, SystemConstants.CARDTYPE_TITULAR, true, null);
								
									/**********************************************************************
									 * Almacenando movimiento en la Bitacora de las Tarjetas( ISTCMSACT )
									 **********************************************************************/
									CardActivityDto cardActivity = new CardActivityDto();
									cardActivity.setPan       ( cardholderDto.getCard().getNumberCard() );
									cardActivity.setCardType  ( cardholderDto.getCard().getCardType() );
									cardActivity.setUserId    ( SecurityUtils.getCurrentUser() );
									cardActivity.setSpareChar ( authorization ); 
									cardActivity.setCmsFunc   ( "03" );
									cardActivity.setReplReason( "ALTA" );
									cardActivityDao.create( cardActivity );
							
									codeResponse = 1;
								
									Date currentDate = GeneralUtil.getCurrentSqlDate();
							
									/*************************************
									 *     Generando Respuesta
									 *************************************/
									response.setPan				( pan );
									response.setExpiryDate      ( GeneralUtil.formatDate( "dd-MM-yyyy" ,cardholderDto.getCard().getExpirationDate()) );		
									response.setIssueDate		( GeneralUtil.formatDate( "dd-MM-yyyy" , currentDate) );
									response.setCurrentTime     ( GeneralUtil.formatDate( "HH:mm:ss" , currentDate) );	
									response.setType			( "Titular" );
									response.setStatus			( "Inactiva" );					
									response.setCardholderName  ( demographicRequestDto.getName());
								
							}else
								codeResponse = 12;//base no encontrada
						}else
							codeResponse = 4;
					}else
						codeResponse = 4;
													
				}else{
					codeResponse = 3; /** La tarjeta ya fue asignada previamente **/
				}
			}else{
				codeResponse = 10; /** Dato no supero validacion**/
			}
			
		}else{
				codeResponse = 2;	/***tarjeta no registrada**/
		}

		String descripcion = null;
		if(codeResponse == 10)
			descripcion = demograficoValidacion;
		else 
			descripcion =  CatalogUtil.getCatalogAssignmentAccount().get(codeResponse);
		
		response.setCode			( codeResponse );
		response.setAuthorization	( authorization );
		response.setDescription  	( descripcion );
								
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		
		log.debug( "::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog(bin, pan, SecurityUtils.getCurrentUser(), request, codeResponse, response, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_STOCK );
		
		return response;
	}
	
	
	public CardAssignmentResponseDto stockCardAssignment(String account,String pan, DemographicDataRequestDto demographicRequestDto, boolean accountFlag, boolean isActive, boolean isFunction, NewEmployeeRequestDto employee, boolean isSodexo) {
		
		return stockCardAssignment(account, pan,  demographicRequestDto,  accountFlag, null, null, accountFlag, isFunction, employee, isSodexo);
		
	}
	
	public ClientAccountResponseDto stockCardAssignment(String pan, String idClient, String phoneCode, DemographicDataRequestDto demographicRequestDto, boolean accountFlag, String product, String idBanco, String plazaBanjico){
		final  String  pmoralPrducto = "04";
		String account = null;
		String request = "pan["+pan+"]product["+product+"]demographicDto["+demographicRequestDto.toString() +"]";
		ClientAccountResponseDto response = new ClientAccountResponseDto();
		int codeResponse = 0;
		CardAssignmentResponseDto cardResponse = null;
		CardDto card = null;
		String clabe = "0";
		
		List<String> products= productsLevelCaptation();
		boolean isPhone = GeneralUtil.isEmtyString(phoneCode);
		etiqueta:
		if(isPhone){
			codeResponse = 17;
			response.setClientName(demographicRequestDto.getName());
			response.setCode(codeResponse);
			response.setDescription(CatalogUtil.getCatalogAssignmentAccount().get(codeResponse));
			break etiqueta;
		}else{	
			if(!product.equals(pmoralPrducto)){
				if(products.contains(product)){
					String panSub = pan.substring(6, 8);
					if(panSub.equals(product)){
						cardResponse =  stockCardAssignment(account, pan, demographicRequestDto, accountFlag, product, idClient, false, false, null, false);
		
						if(cardResponse != null && cardResponse.getCode() == 1){
							card = cardDao.findByNumberCard(cardResponse.getPan());
							
							if(card != null){
								if(GeneralUtil.isEmtyString(idClient))	
									response.setNoClient(card.getClientId());
								else
									response.setNoClient(GeneralUtil.completaCadena(idClient+"",'0',10,"L"));
								
								//validaciones para que el idbanco y idplaza sean correctos y no se genere mal una clabe
								clabe = GenerateCLABE.generateCLABE(idBanco, plazaBanjico, card.getAccount().substring(8).trim());
								String bin = GeneralUtil.completaCadena(pan.substring(0, 6)+"",'0',10,"L");
								AccountDto accountDto = accountDao.findInfoAccount(bin, card.getAccount());
								phoneCodeService.createPhoneCode(card.getNumberCard().trim(), null, phoneCode);
								if(accountDto != null){
									accountDto.setShortName(clabe);
									accountDao.update(accountDto);
								}
								response.setAuthorization(cardResponse.getAuthorization());
								response.setClientName(cardResponse.getCardholderName());
								response.setCode(cardResponse.getCode());
								response.setCurrentTime(cardResponse.getCurrentTime());
								response.setDescription(cardResponse.getDescription());
								response.setIssueDate(cardResponse.getIssueDate());
								response.setNoCuenta(card.getAccount().substring(8).trim());
								response.setCLABE(clabe);
							}
						
				
						}else if(cardResponse != null && cardResponse.getCode() != 1){
							response.setAuthorization(cardResponse.getAuthorization());
							response.setClientName(cardResponse.getCardholderName());
							response.setCode(cardResponse.getCode());
							response.setCurrentTime(cardResponse.getCurrentTime());
							response.setDescription(cardResponse.getDescription());
							response.setIssueDate(cardResponse.getIssueDate());
							response.setCLABE(clabe);
						}
					}else{
						response.setAuthorization("0");
						response.setClientName(demographicRequestDto.getName());
						response.setCode(14);
						response.setDescription(CatalogUtil.getCatalogAssignmentAccount().get(14));
					}
				}else{
					response.setAuthorization("0");
					response.setClientName(demographicRequestDto.getName());
					response.setCode(14);
					response.setDescription(CatalogUtil.getCatalogAssignmentAccount().get(14));
				}
			}else{
				response.setAuthorization("0");
				response.setClientName(demographicRequestDto.getName());
				response.setCode(16);
				response.setDescription(CatalogUtil.getCatalogAssignmentAccount().get(16));
			}
			
			if( cardResponse != null && cardResponse.getCode() != 1){
				log.debug( "::Almacenando Movimiento en Bitacora::");
				wsUtil = new WSLogUtil( wsLogDao );
				wsUtil.saveLog(pan.substring(0,6), pan, SecurityUtils.getCurrentUser(), request, cardResponse.getCode(), response, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_CONCENTRATOR);

			}else{
	
			log.debug( "::Almacenando Movimiento en Bitacora::");
			wsUtil = new WSLogUtil( wsLogDao );
			wsUtil.saveLog(pan.substring(0,6), pan, SecurityUtils.getCurrentUser(), request, codeResponse, response, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_CONCENTRATOR);
			}
		}
	
		return response;
	}
	
	public CardAssignmentResponseDto stockCardAssignment(String account,String pan, AditionalCardRequestDto aditionalCardRequestDto, boolean accountFlag) {
		int    codeResponse  = 0;
		String authorization = "0";
		String bin = "0";

		String request = "pan["+pan+"]account["+account+"]aditionalCardRequestDto["+aditionalCardRequestDto+"]";
		String accountBuild = null;
		CardAssignmentResponseDto response = new CardAssignmentResponseDto();
		
				
		/** Obteniendo información de la tarjeta a Asignar**/
		CardholderDto cardholderAdicionalDto = cardholderDao.getCardholderInfoByCardNumber(pan);
		
		if(cardholderAdicionalDto != null && cardholderAdicionalDto.getCard() != null && cardholderAdicionalDto.getAccount() != null && cardholderAdicionalDto.getIstrel() != null){

			bin = cardholderAdicionalDto.getIstrel().getBin();
			if( cardholderAdicionalDto.getCard().getStatus() == SystemConstants.STATUS_INACTIVE_CARD ){
				
				/** Verifico que la cuenta sea dummy */
				AccountDto oAccountDto =  cardholderAdicionalDto.getAccount();
				
				/** La cuenta debe existir y debe ser dummy */
				if(  null != oAccountDto  &&  oAccountDto.getAssignmentAccount()!=null && 
						oAccountDto.getAssignmentAccount().trim().equals( SystemConstants.CUENTA_DISPONIBLE_PARA_ASIGNACION )){
					
					/** Obteniendo un numero de Autorizacion **/
					authorization = GeneralUtil.generateAuthorizationNumber(6);
					if(accountFlag){
						
						/** Obtenemos al prefijo de la tabla emisores */
						EmisorDto oEmisorDto = emisorDao.findByBin(cardholderAdicionalDto.getIstrel().getBin());
					
					 	accountBuild  = GeneralUtil.buildPrimaryAccount( account, oEmisorDto.getCuenta().trim() ); 
					}else{	
						accountBuild = cardholderAdicionalDto.getCard().getAccount();
					}
					CardDto titular = cardDao.getTitCardByAccount(accountBuild);
					if(titular != null){
						
						AddressDto titularAddressDto = addressDao.findCardAddress( titular.getNumberCard() );
												
						/** Actualizando los datos personales en ISTCMSCARD**/								
						IstcmscardDto istcmscardTitularDto = istcmscardDao.getIstcmscardInformation(titular.getNumberCard());
						DemographicDataRequestDto demographicDataRequestDto = new DemographicDataRequestDto();

						if (aditionalCardRequestDto != null){
							demographicDataRequestDto.setName(aditionalCardRequestDto.getAdditionalName());
							demographicDataRequestDto.setShortName(aditionalCardRequestDto.getAdditionalShortName());
							demographicDataRequestDto.setAddress(titularAddressDto.getDireccion());
							demographicDataRequestDto.setCellPhone(titularAddressDto.getTelefono3());
							demographicDataRequestDto.setCity(titularAddressDto.getCiudad());
							demographicDataRequestDto.setColony(titularAddressDto.getColonia());
							demographicDataRequestDto.setEmail(titularAddressDto.getEmail());
							demographicDataRequestDto.setHomePhone(titularAddressDto.getTelefono());
							demographicDataRequestDto.setMunicipality(titularAddressDto.getLocalidad());
							demographicDataRequestDto.setRfc(aditionalCardRequestDto.getAdditionalRFC());
							demographicDataRequestDto.setState(titularAddressDto.getEstado());
							demographicDataRequestDto.setWorkPhone(titularAddressDto.getTelefono2());
							demographicDataRequestDto.setZipCode(titularAddressDto.getCp());
						
						}else{
							
							demographicDataRequestDto.setName(titularAddressDto.getNombre());
							demographicDataRequestDto.setShortName(istcmscardTitularDto.getName());
							demographicDataRequestDto.setAddress(titularAddressDto.getDireccion());
							demographicDataRequestDto.setCellPhone(titularAddressDto.getTelefono3());
							demographicDataRequestDto.setCity(titularAddressDto.getCiudad());
							demographicDataRequestDto.setColony(titularAddressDto.getColonia());
							demographicDataRequestDto.setEmail(titularAddressDto.getEmail());
							demographicDataRequestDto.setHomePhone(titularAddressDto.getTelefono());
							demographicDataRequestDto.setMunicipality(titularAddressDto.getLocalidad());
							demographicDataRequestDto.setRfc(titularAddressDto.getRfc());
							demographicDataRequestDto.setState(titularAddressDto.getEstado());
							demographicDataRequestDto.setWorkPhone(titularAddressDto.getTelefono2());
							demographicDataRequestDto.setZipCode(titularAddressDto.getCp());
							
							
						}
						
						CardIssuancePropertiesRequestDto cardIssuancePropertiesRequestDto = new CardIssuancePropertiesRequestDto();
						cardIssuancePropertiesRequestDto.setFormatedAccount(accountBuild);
						cardIssuancePropertiesRequestDto.setPan(pan);
						cardIssuancePropertiesRequestDto.setCardType(titular.getCardType());
						
						
						/**********************************************************************
						 * Almacenando movimiento en la Bitacora de las Tarjetas( ISTCMSACT )
						 **********************************************************************/
						String userid = "";
						
						if(SecurityUtils.getCurrentUser().length() <= 10){
							userid = SecurityUtils.getCurrentUser(); 
						}else{
							userid = SecurityUtils.getCurrentUser().substring(0, 10); 
						}
						
						/**Cambiando a asignada la cuenta**/	

						AccountDto accountDto = cardholderAdicionalDto.getAccount();
						accountDto.setAssignmentAccount( SystemConstants.CUENTA_ASIGNADA );
						//accountDao.update(accountDto);
						
						updateStockCard(cardIssuancePropertiesRequestDto, demographicDataRequestDto, cardholderAdicionalDto, SystemConstants.CARDTYPE_ADICIONAL, false, null);

						
						CardActivityDto cardActivity = new CardActivityDto();
						cardActivity.setPan       ( cardholderAdicionalDto.getCard().getNumberCard() );
						cardActivity.setCardType  ( cardholderAdicionalDto.getCard().getCardType() );
						cardActivity.setUserId    ( userid );
						cardActivity.setSpareChar ( authorization ); 
						cardActivity.setCmsFunc   ( "03" );
						cardActivity.setReplReason( "ALTA" );
						cardActivityDao.create( cardActivity );
												
						codeResponse = 1;		
						
						Date currentDate = GeneralUtil.getCurrentSqlDate();
						
						/*************************************
						 *     Generando Respuesta
						 *************************************/
						response.setPan				( pan );
						response.setExpiryDate      ( GeneralUtil.formatDate( "dd-MM-yyyy" ,cardholderAdicionalDto.getCard().getExpirationDate()) );		
						response.setIssueDate		( GeneralUtil.formatDate( "dd-MM-yyyy" , currentDate) );
						response.setCurrentTime     ( GeneralUtil.formatDate( "HH:mm:ss" , currentDate) );	
						response.setType			( "Adicional" );
						response.setStatus			( "Inactiva" );			
						response.setAuthorization	( authorization );
																								
					}else{
						codeResponse = 5; /** No hay una tarjeta titular */
					}															
				}else{
					codeResponse = 4; /** La cuenta ya se encuentra asignada */ 
				}												
			}
			else{
				codeResponse = 3; /** La tarjeta ya fue asignada previamente */
			}
		}
		
		response.setCode			( codeResponse );
		response.setDescription  	( CatalogUtil.getCatalogAssignmentAccount().get(codeResponse));
								
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		
		log.debug( "::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog(bin, pan, SecurityUtils.getCurrentUser(), request, codeResponse, response, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_STOCK );
		
		return response;			
	}
	
	
	public CardAssignmentResponseDto stockCardAssignment(String account,String pan){
		
		AditionalCardRequestDto aditionalCardRequestDto =  null;
		
		return stockCardAssignment(account, pan, aditionalCardRequestDto);
	}
	
	public CardAssignmentResponseDto stockCardAssignment(String account,String pan, AditionalCardRequestDto aditionalCardRequestDto) {
		
		return stockCardAssignment(account, pan, aditionalCardRequestDto, true );
	}

	public CardAssignmentResponseDto stockCardReplacement(String account,String pan, String lockedCard, DemographicDataRequestDto demographicRequestDto, boolean accountFlag, boolean activation, int issCode, boolean isFunction) throws Exception {
		int    codeResponse  = 0;
		String authorization = "0";
		String bin = "0";
		String typeCard = null;
		String type = null;
		String accountBuild = null;
		IstcmscardDto istcmscardTitularDto = istcmscardDao.getIstcmscardInformation(lockedCard);

		String request = "pan["+pan+"]account["+account+"]"+"locked["+lockedCard+"]";
		
		CardAssignmentResponseDto response = new CardAssignmentResponseDto();				
		
		/** Obteniendo información de la tarjeta a  reemplazar **/
		CardholderDto cardholderReemplazoDto = cardholderDao.getCardholderInfoByCardNumber(lockedCard);
		/**mandamos los datos demograficos a validar**/
		String demograficoValidacion = isValidosDemograficos(demographicRequestDto);
		/** Debe existir la tarjeta a reemplazar */
		empleado:
			if(cardholderReemplazoDto != null && cardholderReemplazoDto.getCard() != null && cardholderReemplazoDto.getAccount() != null && cardholderReemplazoDto.getIstrel() != null){
				if ( demograficoValidacion.equals(SystemMesages.OK) ){
					bin = cardholderReemplazoDto.getIstrel().getBin();
					if( cardholderReemplazoDto.getCard().getStatus() == SystemConstants.STATUS_STOLEN_CARD ||
						cardholderReemplazoDto.getCard().getStatus() == SystemConstants.STATUS_LOST_CARD || 
						cardholderReemplazoDto.getCard().getStatus() == SystemConstants.STATUS_DAMAGED_CARD){
						/**buscamos el tipo de tarjeta que es: titular o adicional*/
						type = istcmscardTitularDto.getCardIndicator();			
						CardholderDto cardholderNewDto = cardholderDao.getCardholderInfoByCardNumber(pan);

						if(cardholderNewDto != null && cardholderNewDto.getCard() != null){
							if( cardholderNewDto.getCard().getStatus() == SystemConstants.STATUS_INACTIVE_CARD ){
								if(cardholderNewDto.getCard().getCardType().equals(cardholderReemplazoDto.getCard().getCardType())){
									AccountDto oAccountDto =  cardholderNewDto.getAccount();
									String userid = (SecurityUtils.getCurrentUser().length() <= 10) ? SecurityUtils.getCurrentUser() : SecurityUtils.getCurrentUser().substring(0, 10);

									if( activation && cardholderNewDto.getCard().getStatus() == SystemConstants.STATUS_INACTIVE_CARD  ){
										cardService.activationCard(cardholderNewDto.getCard(), userid+"_atm");
									}
									/** La cuenta debe existir y debe ser dummy */
									if(  null != oAccountDto  &&  oAccountDto.getAssignmentAccount()!=null &&
											oAccountDto.getAssignmentAccount().trim().equals(SystemConstants.CUENTA_DISPONIBLE_PARA_ASIGNACION)){

										authorization = GeneralUtil.generateAuthorizationNumber(6);

										if(accountFlag){
											/** Obtenemos al prefijo de la tabla emisores */
											EmisorDto oEmisorDto = emisorDao.findByBin(cardholderReemplazoDto.getIstrel().getBin());
											accountBuild       = GeneralUtil.buildPrimaryAccount( account, oEmisorDto.getCuenta().trim() );
										}else{
											accountBuild = cardholderReemplazoDto.getCard().getAccount();
										}

										AccountDto titular = accountDao.findInfoAccount(bin, accountBuild);
										if(titular != null){
											/** Actualizando los datos personales en ISTCMSCARD**/
											CardIssuancePropertiesRequestDto cardIssuancePropertiesRequestDto = new CardIssuancePropertiesRequestDto();
											cardIssuancePropertiesRequestDto.setFormatedAccount(accountBuild);
											cardIssuancePropertiesRequestDto.setPan(pan);
											cardIssuancePropertiesRequestDto.setCardType(cardholderReemplazoDto.getCard().getCardType());

											EmbosserPropertiesDto reemplazo = embosserPropertiesDao.findEmbosserProperiesByPan(lockedCard);
											DemographicDataRequestDto demographicDataRequestDto = new DemographicDataRequestDto();
											if(demographicRequestDto != null){

												demographicDataRequestDto.setName(demographicRequestDto.getName());
												demographicDataRequestDto.setAddress(demographicRequestDto.getAddress());
												demographicDataRequestDto.setCellPhone(demographicRequestDto.getCellPhone());
												demographicDataRequestDto.setCity(demographicRequestDto.getCity());
												demographicDataRequestDto.setColony(demographicRequestDto.getColony());
												demographicDataRequestDto.setEmail(demographicRequestDto.getEmail());
												demographicDataRequestDto.setHomePhone(demographicRequestDto.getHomePhone());
												demographicDataRequestDto.setMunicipality(demographicRequestDto.getMunicipality());
												demographicDataRequestDto.setRfc(demographicRequestDto.getRfc());
												demographicDataRequestDto.setState(demographicRequestDto.getState());
												demographicDataRequestDto.setWorkPhone(demographicRequestDto.getWorkPhone());
												demographicDataRequestDto.setZipCode(demographicRequestDto.getZipCode());
											}

											demographicDataRequestDto.setShortName(istcmscardTitularDto.getName());
											CardLimitDto limitesIndividuales = null;
											if(cardholderReemplazoDto.getCard().getLimitIndicator().equals("1")){
												/** Tiene limites la tarjeta */
												//CardLimitPk cardLimitPk = new CardLimitPk(bin,product);

												CardLimitDto limitesIndividualesTarjetaBloqueada = cardLimitDao.findBypan(bin,cardholderReemplazoDto.getCard().getNumberCard());

												if( limitesIndividualesTarjetaBloqueada != null ){
												    limitesIndividuales = new CardLimitDto( limitesIndividualesTarjetaBloqueada );
													limitesIndividuales.setPan(cardholderNewDto.getCard().getNumberCard());
												}
											}

											if(isFunction){
												/**aquí se mandará agregar el registro en tbl_empleados y a actualizar el de la tarjeta bloqueada y realizar
												 *el reemplazo**/

												NewEmployeeRequestDto employeeRequest = new NewEmployeeRequestDto();
												employeeRequest.setIssCode(issCode);


												int code = updateEmployee( pan,  account, lockedCard, employeeRequest,  true, false);

												if(code != 1){
													codeResponse = 19;
													break empleado;
												}
											}

											
											/** Cambiando el estatus de la Tarjeta a Reemplazada **/
											CardDto cardDto = cardholderReemplazoDto.getCard();
											cardDto.setStatus(SystemConstants.STATUS_REPLACED_CARD);
											cardDao.update(cardDto);

											/**Cambiando a asignada la cuenta**/

											//AccountDto accountDto = cardholderReemplazoDto.getAccount();
											cardholderReemplazoDto.getAccount().setAssignmentAccount( SystemConstants.CUENTA_ASIGNADA );

											AccountDto accountDto = cardholderReemplazoDto.getAccount();
											accountDto.setAssignmentAccount( SystemConstants.CUENTA_ASIGNADA );

											//accountDao.update(accountDto);
											
											updateStockCard(cardIssuancePropertiesRequestDto, demographicDataRequestDto, cardholderNewDto, reemplazo.getCardIndicator(), false, limitesIndividuales);

											/**********************************************************************
											 * Almacenando movimiento en la Bitacora de las Tarjetas( ISTCMSACT )
											 **********************************************************************/
											CardActivityDto cardActivity = new CardActivityDto();
											cardActivity.setPan       ( cardholderNewDto.getCard().getNumberCard() );
											cardActivity.setPanReplace(cardDto.getNumberCard());
											cardActivity.setCardType  ( cardholderNewDto.getCard().getCardType() );
											cardActivity.setUserId    ( userid );
											cardActivity.setSpareChar ( "   r"+authorization );
											cardActivity.setCmsFunc   ( "06" );
											cardActivity.setReplReason( "REEMPLAZO" );
											cardActivityDao.create( cardActivity );

											CardActivityDto cardActivity1 = new CardActivityDto();
											cardActivity1.setPan       ( cardDto.getNumberCard() );
											cardActivity1.setCardType  ( cardDto.getCardType() );
											cardActivity1.setUserId    ( userid );
											cardActivity1.setSpareChar ( authorization );
											cardActivity1.setCmsFunc   ( "50" );
											cardActivity1.setReplReason( "BLOQUEO POR REEMPLAZO" );
											cardActivityDao.create( cardActivity1 );


											Date currentDate = GeneralUtil.getCurrentSqlDate();
											typeCard = (type.equals("0")) ? "Titular" : "Adicional";

											codeResponse = 1;

											response.setPan				( pan );
											response.setExpiryDate      ( GeneralUtil.formatDate( "dd-MM-yyyy" ,cardholderReemplazoDto.getCard().getExpirationDate()) );
											response.setIssueDate		( GeneralUtil.formatDate( "dd-MM-yyyy" , currentDate) );
											response.setCurrentTime     ( GeneralUtil.formatDate( "HH:mm:ss" , currentDate) );
											response.setType			( typeCard );
											response.setStatus			( "Inactiva" );

										}else{
											codeResponse = 5;
										}
									}else
										codeResponse = 4;
								}else
									codeResponse = 18;//productos diferentes
							}else{
								//La tarjeta ya fue asignada previamente
								codeResponse = 3;
							}
						}else{
							codeResponse = 22;
						}


					}else{
						//La tarjeta a reemplzar no cumple el status
						codeResponse = 6;
					}
				}else{
					codeResponse = 10; /** Dato no supero validacion*/
				}
			}else{
				//La tarjeta a reemplzar no existe
				codeResponse = 7;
			}
		
		
		String descripcion = null;
		if(codeResponse == 10)
			descripcion = demograficoValidacion;
		else 
			descripcion =  CatalogUtil.getCatalogAssignmentAccount().get(codeResponse);
		
		/*************************************
		 *     Generando Respuesta
		 ************************************/
		
		response.setCode			( codeResponse );
		response.setDescription  	( descripcion );		
		response.setAuthorization	( authorization );
								
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		
		log.debug( "::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog(bin, pan, SecurityUtils.getCurrentUser(), request, codeResponse, response, SystemConstants.PCODE_LOG_ASIGNMENT_ACCOUNT_STOCK );
		
		return response;
	}
	
	
	@Override
	public CardAssignmentResponseDto stockCardReplacement( String account,String pan, String lockedCard, DemographicDataRequestDto demographicRequestDto, boolean accountFlag, boolean activation ) throws Exception {
	
		return stockCardReplacement(account, pan, lockedCard, demographicRequestDto, accountFlag, activation, 0, false );
	}
	
		public CardAssignmentResponseDto stockCardReplacement( String account, String pan, String lockedCard, boolean accountFlag, boolean activation, int issCode, boolean isFunction) throws Exception{
			DemographicDataRequestDto demographicDataRequestDto = new DemographicDataRequestDto();
			
			AddressDto adicionalAddressDto = addressDao.findCardAddress( lockedCard );
			if(adicionalAddressDto != null){
			demographicDataRequestDto.setAddress(adicionalAddressDto.getDireccion());
			demographicDataRequestDto.setCellPhone(adicionalAddressDto.getTelefono3());
			demographicDataRequestDto.setCity(adicionalAddressDto.getCiudad());
			demographicDataRequestDto.setColony(adicionalAddressDto.getColonia());
			demographicDataRequestDto.setEmail(adicionalAddressDto.getEmail());
			demographicDataRequestDto.setHomePhone(adicionalAddressDto.getTelefono());
			demographicDataRequestDto.setMunicipality(adicionalAddressDto.getLocalidad());
			demographicDataRequestDto.setRfc((adicionalAddressDto.getRfc()!= null) ? adicionalAddressDto.getRfc().trim() : null);
			demographicDataRequestDto.setState(adicionalAddressDto.getEstado());
			demographicDataRequestDto.setWorkPhone(adicionalAddressDto.getTelefono2());
			demographicDataRequestDto.setZipCode(adicionalAddressDto.getCp());
			demographicDataRequestDto.setName(adicionalAddressDto.getNombre());
			
			return stockCardReplacement(account, pan, lockedCard, demographicDataRequestDto, accountFlag, activation, issCode, isFunction);
			
			}else
			
				return stockCardReplacement(account, pan, lockedCard, null, accountFlag, activation, issCode, isFunction);
		
		}
		
		public CardAssignmentResponseDto stockCardReplacement( String account, String pan, String lockedCard, boolean accountFlag, boolean activation) throws Exception{
			
			return stockCardReplacement(account, pan, lockedCard, accountFlag, activation, 0, false);
		}
	@Deprecated
	public CardAssignmentResponseDto stockCardReplacement( String account, String pan, String lockedCard) throws Exception{
		
		DemographicDataRequestDto demographicDataRequestDto = new DemographicDataRequestDto();
		
		AddressDto adicionalAddressDto = addressDao.findCardAddress( lockedCard );
		if(adicionalAddressDto != null){
		demographicDataRequestDto.setAddress(adicionalAddressDto.getDireccion());
		demographicDataRequestDto.setCellPhone(adicionalAddressDto.getTelefono3());
		demographicDataRequestDto.setCity(adicionalAddressDto.getCiudad());
		demographicDataRequestDto.setColony(adicionalAddressDto.getColonia());
		demographicDataRequestDto.setEmail(adicionalAddressDto.getEmail());
		demographicDataRequestDto.setHomePhone(adicionalAddressDto.getTelefono());
		demographicDataRequestDto.setMunicipality(adicionalAddressDto.getLocalidad());
		demographicDataRequestDto.setRfc((adicionalAddressDto.getRfc()!= null) ? adicionalAddressDto.getRfc().trim() : null);
		demographicDataRequestDto.setState(adicionalAddressDto.getEstado());
		demographicDataRequestDto.setWorkPhone(adicionalAddressDto.getTelefono2());
		demographicDataRequestDto.setZipCode(adicionalAddressDto.getCp());
		demographicDataRequestDto.setName(adicionalAddressDto.getNombre());
		
		return stockCardReplacement(account, pan, lockedCard, demographicDataRequestDto, true, false);
		
		}else
		
			return stockCardReplacement(account, pan, lockedCard, null, true, false);
		
	}
	
	
	public List<String> generateCardRange(String bin, String product, int totalCards) {
		List<String> tarjetas = new ArrayList<String>();
		
		for(int i=0; i<totalCards; i++){
			DemographicDataRequestDto demographicDataRequestDto = new DemographicDataRequestDto();
			demographicDataRequestDto.setAddress("Direccion" + i);
			demographicDataRequestDto.setCellPhone("Telefono3:" + i);
			demographicDataRequestDto.setCity("ciudad"+ i);
			demographicDataRequestDto.setColony("colonia" + i);
			demographicDataRequestDto.setEmail("emial" + i);
			demographicDataRequestDto.setHomePhone("telefono1:" +i);
			demographicDataRequestDto.setMunicipality("municipio" + i);
			demographicDataRequestDto.setName("nombre"+ i);
			demographicDataRequestDto.setRfc("rfc" + i);
			demographicDataRequestDto.setShortName("short"+i);
			demographicDataRequestDto.setState("estado"+i);
			demographicDataRequestDto.setWorkPhone("telefono2:"+i);
			demographicDataRequestDto.setZipCode("cp"+i);
			
			String account = "7229399988" + i;
			
			CardAssignmentResponseDto res = embosserCardAssignment(bin,product,account, demographicDataRequestDto);
			
			if(res.getCode() == 1)
				tarjetas.add(res.getPan());
		}
		
		return tarjetas;
	}
	
	
	
	/**
	 * Servicio que actualiza o crea registro de empleado 
	 * @param pan
	 * @param account
	 * @param lockedCard
	 * @param employeeRequest
	 * @param isRemplacement => true si se trata de un reemplazo de tarjeta
	 * @param isSodexo  => configuracion especial para sodexo, si es sodexo guarda la empleadora
	 * 						en campo diferente y valida codigo de domicilio
	 * @return
	 */
	private int updateEmployee(String pan, String account, String lockedCard, NewEmployeeRequestDto employeeRequest, boolean isRemplacement, boolean isSodexo){
		int code = 0;
		TblEmployeeDto newEmployee = null;
		error:
		if(isRemplacement){
			/***busca empleado, falta producto?????**/													//status 0 (xq es titular) y cardtype siempre es 0
			TblEmployeeDto employee = tblEmployeeDao.findEmployee(lockedCard, employeeRequest.getIssCode(), 0, "0" );
			
			if(employee != null){
				
				newEmployee = new TblEmployeeDto();
			
				employee.setStatus(60);
				employee.setReplacementDate(GeneralUtil.getCurrentSqlDate());
				tblEmployeeDao.update(employee);
				
	
				newEmployee.setIssCode(employeeRequest.getIssCode());
				newEmployee.setCompanyCode(employee.getCompanyCode());
				newEmployee.setTiType(employee.getTiType());
				newEmployee.setCardType(employee.getCardType());
				newEmployee.setCodeEmployee(employee.getCodeEmployee());
				newEmployee.setName(employee.getName());
				newEmployee.setAccount(employee.getAccount());
				newEmployee.setPan(pan);
				newEmployee.setExpDate(employee.getExpDate());
				newEmployee.setStatus(0);
				newEmployee.setAssignmentDate(GeneralUtil.getCurrentSqlDate());
				newEmployee.setStatusMaq(employee.getStatusMaq());
				newEmployee.setMaqDate(employee.getMaqDate());
				newEmployee.setAccountType(employee.getAccountType());

				newEmployee.setCompanyCode2(employee.getCompanyCode2());

				newEmployee.setShortNameComp(employee.getShortNameComp());
			
				  tblEmployeeDao.create(newEmployee);
				code = 1;
				
			}else{
				break error;
			}
			
			
		}else{
			
			
			if(isSodexo){
				/***busca que exista el codigo de direccion de la empleadora***/
				EmployerDetailDto employerDetail = employerDetailDao.findEmployer(employeeRequest.getAddressCode(), employeeRequest.getIssCode(), employeeRequest.getProduct());
				if(employerDetail != null){
					//buscar empleadora y producto que existan
					EmployerDto employer = employerDao.findEmployer(employeeRequest.getEmployer(), employeeRequest.getIssCode(), employeeRequest.getProduct());
					
					if(employer != null){
					
						TblEmployeeDto employeeDto = tblEmployeeDao.findEmployee(pan, employeeRequest.getIssCode(), 0, "0");
					
						if(employeeDto == null){
							newEmployee = new TblEmployeeDto();
						
							newEmployee.setIssCode(employeeRequest.getIssCode());
							newEmployee.setAssignmentDate(GeneralUtil.getCurrentSqlDate());
							newEmployee.setPan(pan);
							newEmployee.setAccount(account.substring(11).trim());
							newEmployee.setCompanyCode(employeeRequest.getAddressCode());
							newEmployee.setCompanyCode2(employeeRequest.getEmployer());
							newEmployee.setTiType(employeeRequest.getProduct());
							newEmployee.setCardType("0");//titular o adicional
							newEmployee.setCodeEmployee(employeeRequest.getEmployee());
							newEmployee.setName(employeeRequest.getName());
							newEmployee.setAccountType("0");
							tblEmployeeDao.create(newEmployee);
							code = 1;
						}else{
							code = 2;//empleado ya existe
							break error;
						}
					}else{
						code = 3;//empleadora/producto no valido
						break error;
					}
				}else{
					code = 4; //domicilio de empleadora no existe
					break error;
				}	
			}else{
				//buscar empleadora y producto que existan
				EmployerDto employer = employerDao.findEmployer(employeeRequest.getEmployer(), employeeRequest.getIssCode(), employeeRequest.getProduct());
				
				if(employer != null){
				
					TblEmployeeDto employeeDto = tblEmployeeDao.findEmployee(pan, employeeRequest.getIssCode(), 0, "0");
				
					if(employeeDto == null){
						newEmployee = new TblEmployeeDto();
					
						newEmployee.setIssCode(employeeRequest.getIssCode());
						newEmployee.setAssignmentDate(GeneralUtil.getCurrentSqlDate());
						newEmployee.setPan(pan);
						newEmployee.setAccount(account.substring(11).trim());
						newEmployee.setCompanyCode(employeeRequest.getEmployer());
						newEmployee.setTiType(employeeRequest.getProduct());
						newEmployee.setCardType("0");//titular o adicional
						newEmployee.setCodeEmployee(employeeRequest.getEmployee());
						newEmployee.setName(employeeRequest.getName());
						newEmployee.setAccountType("0");
						tblEmployeeDao.create(newEmployee);
						code = 1;
					}else{
						code = 2;//empleado ya existe
						break error;
					}
				}else{
					code = 3;//empleadora/producto no valido
					break error;
				}
			}
		}
		
		return code;
	}
	
	private String isValidosDemograficos(DemographicDataRequestDto request){
		if(request != null){
			if( request.getZipCode()     !=null && request.getZipCode().length()      >  9 ) return "el campo ZipCode es demasiado largo";//SystemMesages.CP;
			if( request.getColony()      !=null && request.getColony().length()       > 45 ) return "el campo Colony es demasiado largo";//SystemMesages.OK;
			if( request.getAddress()     !=null && request.getAddress().length()      > 45 ) return "el campo Address es demasiado largo";//SystemMesages.ADDRESS;
			if( request.getShortName()   !=null && request.getShortName().length()    > 25 ) return "el campo ShortName es demasiado largo";//SystemMesages.SHORT_NAME;
			if( request.getName()        !=null && request.getName().length()         > 35 ) return "el campo Name es demasiado largo";//SystemMesages.LARGE_NAME; 
			if( request.getRfc()         !=null && request.getRfc().length()          > 16 ) return "el campo RFC es demasiado largo";//SystemMesages.RFC; 
			if( request.getHomePhone()   !=null && request.getHomePhone().length()    > 10 ) return "el campo HomePhone es demasiado largo";//SystemMesages.PHONE;
			if( request.getWorkPhone()   !=null && request.getWorkPhone().length()    > 10 ) return "el campo WorkPhone es demasiado largo";//SystemMesages.PHONE; 
			if( request.getCellPhone()   !=null && request.getCellPhone().length()    > 10 ) return "el campo cellPhone es demasiado largo";
			if( request.getMunicipality()!=null && request.getMunicipality().length() > 45 ) return "el campo Municipality es demasiado largo";//SystemMesages.ENTIDAD;				
			if( request.getCity()        !=null && request.getCity().length()         > 45 ) return "el campo City es demasiado largo";//SystemMesages.ENTIDAD;				
			if( request.getEmail()       !=null && request.getEmail().length()        > 45 ) return "el campo email es demasiado largo";//SystemMesages.ENTIDAD;				
			if( request.getState()       !=null && request.getState().length()        > 45 ) return "el campo state es demasiado largo";//SystemMesages.ENTIDAD;				

		return SystemMesages.OK;
		}
		return "no se encontro tarjeta";
	}


	/**
	 * Valida la longitud de los datos demograficos de un adicional
	 * @return
	 */
	private String isValidosDemograficos(AditionalCardRequestDto request){								
		
		if( request.getAdditionalShortName()   !=null && request.getAdditionalShortName().length()    > 25 ) return "el campo AdditionalShortName es demasiado largo";//SystemMesages.SHORT_NAME;
		if( request.getAdditionalName()        !=null && request.getAdditionalName().length()         > 35 ) return  "el campo AdditionalName es demasiado largo";//SystemMesages.LARGE_NAME; 
		if( request.getAdditionalRFC()         !=null && request.getAdditionalRFC().length()          > 16 ) return "el campo RFC es demasiado largo";//return SystemMesages.RFC; 					
		
		return SystemMesages.OK;
	}

	
	
	/**
	 * Obtine la fecha de expiracion de una tarjeta
	 * @param istCardTypeDto
	 * @return
	 */
	private Date getExpiryDate(IstCardTypeDto istCardTypeDto){
		int expiryMonths = Integer.valueOf(istCardTypeDto.getCardLife()).intValue();				
		java.util.Date expiryDate = GeneralUtil.addMonths(new java.util.Date(), expiryMonths);
		return new Date(expiryDate.getTime());
	}	
	
	
	/**
	 * Inserta una tarjeta TITULAR, ADICONAL, REEMPLAZO
	 * @param embosserCardDto Objeto que contiene 
	 * @param demographicDto
	 * @return
	 */
	private boolean saveEmbosserCard(CardIssuancePropertiesRequestDto cardProperties, DemographicDataRequestDto demographicDto, boolean isTitular, CardLimitDto cardLimitDto){

		/** Guardamos ISTCARD */
		CardFullDto cardDto = new CardFullDto();
		cardDto.setPan        ( (cardProperties.getPan() != null) ? cardProperties.getPan().trim() : null  );
		cardDto.setBase       ( (cardProperties.getFormatedbase() != null) ? cardProperties.getFormatedbase().trim() : null );
		cardDto.setCardType   ( (cardProperties.getCardType() != null) ? cardProperties.getCardType().trim() : null );
		cardDto.setExpiryDate (  cardProperties.getExpiriDate());
		cardDto.setPrimaryAcct( (cardProperties.getFormatedAccount() != null) ? cardProperties.getFormatedAccount().trim() : null);
		cardDto.setOldExpDate ( cardProperties.getExpiriDate());
		
		/** Si tiene limites los guardamos */
		if(cardLimitDto != null){
			cardDto.setLimitiIndicator("1");			
			cardLimitDao.create(cardLimitDto);						
		}
		cardDao.saveNewCard   ( cardDto );
				
		/** Guardamos en ISTCMSCARD */
		EmbosserPropertiesDto embosserPropertiesDto = new EmbosserPropertiesDto(true);
		embosserPropertiesDto.setCardNumber   ((cardProperties.getPan() != null) ? cardProperties.getPan().trim() : null         );
		embosserPropertiesDto.setExpiryDate   (cardProperties.getExpiriDate()   );
		embosserPropertiesDto.setNewExpiryDate(cardProperties.getExpiriDate()   );
		embosserPropertiesDto.setShortName    ((demographicDto.getShortName() != null) ? demographicDto.getShortName().trim() : null    );
		embosserPropertiesDto.setName2        ((demographicDto.getShortName() != null) ? demographicDto.getShortName().trim() : null    );
		embosserPropertiesDto.setMagstripname ((demographicDto.getShortName() != null) ? demographicDto.getShortName().trim() : null    );
		embosserPropertiesDto.setCardIndicator((cardProperties.getCardIndicator() != null) ? cardProperties.getCardIndicator().trim() : null    );
		embosserPropertiesDto.setIssueDate    (cardProperties.getIssueDate()  );			
		embosserPropertiesDao.create(embosserPropertiesDto);
		
		if(isTitular){
			/** Guardamos en ISTDBACCT */
			AccountFullDto accountDto = new AccountFullDto();
			accountDto.setBin    ((cardProperties.getFormatedBin() != null) ? cardProperties.getFormatedBin().trim() : null);
			accountDto.setAccount((cardProperties.getFormatedAccount() != null) ? cardProperties.getFormatedAccount().trim() : null);
			accountDto.setMisid("00");
			/**
			 * Se guarda el nivel de captacion
			 */
			List<String> levels = binsLevelCaptation();
			if( levels.contains(cardProperties.getPan().substring(0,6))){
				String customerType = CatalogUtil.getCatalogCustomerId().get(cardProperties.getCardType());
					accountDto.setCustomerType(customerType);
			}
			accountDao.saveNewAccount(accountDto);
			
			/** Guardamos en ISTBASE */ 
			BaseDto baseDto = new BaseDto(true);
			baseDto.setBin(cardProperties.getFormatedBin());
			baseDto.setBase(cardProperties.getFormatedbase());
			baseDao.create(baseDto);
			
			/** Guardamos en ISTREL la Cuenta*/
			IstrelDto istrelDto = new IstrelDto(true);
			istrelDto.setBin             (cardProperties.getFormatedBin());
			istrelDto.setAccount         (cardProperties.getFormatedAccount());
			istrelDto.setBase            (cardProperties.getFormatedbase());
			istrelDto.setProductCode     ("04");
			istrelDto.setPrimaryIndicator("1");
			istrelDao.create(istrelDto);
		}
		
		/** Guardamos en ISTREL */
		IstrelDto istrelDto = new IstrelDto(true);
		istrelDto.setBin    (cardProperties.getFormatedBin());
		istrelDto.setAccount(cardProperties.getPan());
		istrelDto.setBase   (cardProperties.getFormatedbase());
		istrelDao.create(istrelDto);
		
		/** Guardamos en ISTBASEADDR */
		AddressDto addressDto = new AddressDto(true);
		addressDto.setBin(cardProperties.getFormatedBin());
		addressDto.setIdCliente(cardProperties.getFormatedbase());
		addressDto.setTarjeta  (cardProperties.getPan());
		addressDto.setNombre   ( demographicDto.getName());		
		addressDto.setDireccion( demographicDto.getAddress() );
		addressDto.setColonia  ( demographicDto.getColony() );
		addressDto.setCp       ( demographicDto.getZipCode() );
		addressDto.setCp2       ( demographicDto.getZipCode() );
		addressDto.setTelefono ( demographicDto.getHomePhone() );
		addressDto.setTelefono2( demographicDto.getCellPhone() );
		addressDto.setTelefono3( demographicDto.getWorkPhone());
		addressDto.setCiudad   ( demographicDto.getCity());
		addressDto.setEstado   ( demographicDto.getState());
		addressDto.setLocalidad( demographicDto.getMunicipality());
		addressDto.setRfc	   ( demographicDto.getRfc());
		addressDto.setEmail    (demographicDto.getEmail());
		addressDao.create(addressDto);
		
		return true;
	}


	private List<String> binsLevelCaptation(){
		List<String> levels = new ArrayList<String>();
		levels.add("538984");
		
		return levels;
	}
	
	private List<String> productsLevelCaptation(){
		List<String> levels = new ArrayList<String>();
		levels.add("01");
		levels.add("02");
		levels.add("03");
		levels.add("04");
		levels.add("06");
		return levels;
	}
	
	private boolean updateStockCard(CardIssuancePropertiesRequestDto cardProperties, DemographicDataRequestDto demographicDto, CardholderDto cardholderDto, String type, boolean isTitular, CardLimitDto cardLimitDto){
			
		/** Actualizando los datos personales en ISTCMSCARD**/
		IstcmscardDto istcmscardDto = istcmscardDao.getIstcmscardInformation( cardProperties.getPan() );
		if( istcmscardDto != null ){
			istcmscardDto.setName ( (demographicDto.getShortName() != null) ? demographicDto.getShortName().trim() : null    );
			istcmscardDto.setName1( (demographicDto.getShortName() != null) ? demographicDto.getShortName().trim() : null    );
			istcmscardDto.setName2( (demographicDto.getShortName() != null) ? demographicDto.getShortName().trim() : null    );
			istcmscardDao.sqlUpdate(istcmscardDto);
		}
		
		/** Actualizando la dirección **/
		AddressDto addressDto = addressDao.findCardAddress( cardProperties.getPan() );
		
		if( addressDto!=null){
			addressDto.setDireccion( demographicDto.getAddress() );
			addressDto.setNombre   ( demographicDto.getName() );
			addressDto.setColonia  ( demographicDto.getColony() );
			addressDto.setCp       ( demographicDto.getZipCode() );
			addressDto.setCp2       ( demographicDto.getZipCode() );
			addressDto.setTelefono ( demographicDto.getHomePhone() );
			addressDto.setTelefono2( demographicDto.getCellPhone() );
			addressDto.setTelefono3( demographicDto.getWorkPhone());
			addressDto.setCiudad   ( demographicDto.getCity());
			addressDto.setEstado   ( demographicDto.getState());
			addressDto.setLocalidad( demographicDto.getMunicipality());
			addressDto.setRfc	   ( demographicDto.getRfc());
			addressDto.setEmail    (demographicDto.getEmail());
			addressDao.update( addressDto );
		}
		
		CardDto cardDto = cardholderDto.getCard();
		
		/** Actualizando la cuenta en ISTCARD **/
		CardDto newCardDto = new CardDto();
		newCardDto.setStatus(cardDto.getStatus());
		newCardDto.setCardType(cardProperties.getCardType());					
		newCardDto.setNumberCard(cardDto.getNumberCard());
		newCardDto.setTypeAccount(cardDto.getTypeAccount());
		newCardDto.setClientId((cardProperties.getFormatedbase() != null) ? cardProperties.getFormatedbase() : cardDto.getClientId());
		newCardDto.setExpirationDate(cardDto.getExpirationDate());
		newCardDto.setAccount(cardProperties.getFormatedAccount());
		
		/** Si teiene limites los guardamos */
		if(cardLimitDto != null){
			newCardDto.setLimitIndicator("1");			
			cardLimitDao.create(cardLimitDto);						
		}
		
		cardDao.sqlUpdate(newCardDto);
		
		if(isTitular){
		
			/** Actualizando la cuenta en ISTDBACCT **/		
			AccountDto accountDto = cardholderDto.getAccount();
			int updateAccount= accountDao.accountUpdateFromDummy(accountDto, cardProperties.getFormatedAccount());										
			log.debug("Estatus del update a la cuenta: " + updateAccount);	
		
//			AccountDto accountDto = cardholderDto.getAccount();
//			accountDto.setShortName(accountDto.getAccount().trim());
//			accountDto.setAccount(cardProperties.getFormatedAccount());
			/** Se guarda el nivel de captacion**/
			List<String> levels = binsLevelCaptation();
			if( levels.contains(cardProperties.getPan().substring(0,6))){
				String customerType = CatalogUtil.getCatalogCustomerId().get(cardProperties.getCardType());
					if(customerType != null){
						accountDto.setCustomerType(customerType);	
					}
			}
			accountDao.update(accountDto);
			//int updateAccount= accountDao.accountUpdateFromDummy(accountDto, cardProperties.getFormatedAccount());
			//log.debug("Estatus del update a la cuenta: " + updateAccount);	
	
		}
		
		
		/** Actualizando la cuenta nueva en ISTREL **/
		IstrelDto istrelDto1 = istrelDao.findAccount(cardholderDto.getAccount().getAccount(),cardholderDto.getCard().getClientId());
		istrelDto1.setBase((cardProperties.getFormatedbase() != null) ? cardProperties.getFormatedbase() : cardDto.getClientId());
		int updateIstrel1    = istrelDao.accountUpdate(istrelDto1, cardProperties.getFormatedAccount()); 
		log.debug("Estatus del update a istrel: " + updateIstrel1);
		
		/** Actualizando la tarjeta en ISTREL **/
		IstrelDto istrelDto = cardholderDto.getIstrel();
		istrelDto.setBase((cardProperties.getFormatedbase() != null) ? cardProperties.getFormatedbase() : cardDto.getClientId());
		int updateIstrel    = istrelDao.accountUpdate(istrelDto, cardProperties.getPan()); 
		log.debug("Estatus del update a istrel: " + updateIstrel);
		
		/** Indicamos que es una tarjeta adicional */
		EmbosserPropertiesDto adicional = embosserPropertiesDao.findEmbosserProperiesByPan(cardProperties.getPan());
		adicional.setCardIndicator(type);
		embosserPropertiesDao.update(adicional);
		
		
		return true;
	}

			
}

