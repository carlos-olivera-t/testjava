package com.syc.services.impl;

import java.util.List;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.request.MaquilaRequestDto;
import com.syc.exceptions.OasisException;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dao.LayoutDao;
import com.syc.persistence.dto.CardholderDto;
import com.syc.persistence.dto.LayoutDto;
import com.syc.services.MaquilaService;
import com.syc.services.NIPService;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SystemConstants;

@Service
public class MaquilaServiceImpl implements MaquilaService{
	
private static final transient Log log = LogFactory.getLog(MaquilaServiceImpl.class);
	
	@Autowired
	CardholderDao cardHolderDao;
	
	@Autowired
	LayoutDao layoutDao;
	
	@Autowired
	NIPService cvvService;
	
	TreeMap<String, Integer> map;
	
	String cvv  = null;
	String cvv2 = null;
	
	int idImage = 0;
	
	public void procesaMaquila( MaquilaRequestDto maquilaRequest ){
		
		StringBuffer buffer = new StringBuffer();
		String aux = null;
		String field = null;
		
		String serviceCode = null;
		
		getMapList();
		
		idImage = maquilaRequest.getIdImage();
		
		List<LayoutDto> layoutList = layoutDao.getLayoutByIdEmbosser( maquilaRequest.getIdEmbosser() );
		
		
		/** Obteniendo información de la Cuenta  **/
		CardholderDto cardholderDto = cardHolderDao.getMaquilaCardholderInfo( "5453250300010271" );
		
		try{
			
			/** Obteniendo el CVV2 **/
			cvv2 = cvvService.getCVV( cardholderDto.getCard().getNumberCard(), cardholderDto.getCard().getExpirationDate(), "000" );
			
			/** Obteniendo el CVV **/
			serviceCode = cardholderDto!=null && cardholderDto.getIstcardType()!=null 
								? cardholderDto.getIstcardType().getIccIndicator()+cardholderDto.getIstcardType().getServiceCode()
								: "";
			cvv = cvvService.getCVV( cardholderDto.getCard().getNumberCard(), cardholderDto.getCard().getExpirationDate(), serviceCode);
			
			log.debug("CVV : " + cvv);
			log.debug("CVV2: " + cvv2);
			
			for( LayoutDto fieldLayout: layoutList ){
				
				if( fieldLayout.getDefaultField() != null ){
					field = fieldLayout.getDefaultField().trim();
				}else{
					field = getMapParameter( fieldLayout, cardholderDto );
				}
				
				
				aux = completaCadena( field, fieldLayout.getCharRefill(), fieldLayout.getLengthField(), fieldLayout.getJustified()  );
				buffer.append( aux );
				
			}
		
		}catch(OasisException oe){
			oe.printStackTrace();
			
		}
		log.debug("============================");
		log.debug( buffer.toString() );
		log.debug("============================");
		
	}
	
	private void getMapList(){
		map = new TreeMap<String, Integer>();
		map.put( "pan"          , 1  );
		map.put( "nombre_corto" , 2  );
		map.put( "current_date" , 3  );
		map.put( "expiry_date"  , 4  );
		map.put( "pan+CVV2"     , 5  );
		map.put( "service_code" , 6  );
		map.put( "cvv"          , 7  );
		map.put( "bank_name"    , 8  );
		map.put( "product_name" , 9  );
		map.put( "LF"           , 10 );
		map.put( "card_type"    , 11  );
		map.put( "bank_initals" , 12  );
		map.put( "daily_limit"  , 13  );
		map.put( "id_image"     , 14  );
	}
	
	
	private String getMapParameter( LayoutDto layout, CardholderDto cardHolder ) throws OasisException{
		
		String value = "";
		String aux;
		
		if( layout.getMapField() != null ){
		
			Integer idParam = map.get( layout.getMapField().trim() );
			
			switch( idParam!=null ? idParam : 0 ){
			
			/** Obteniendo el Número de la Tarjeta **/
			case( 1 ):
				/** Si tiene un formato Definido, lo aplica **/
				if( layout.getFormatField() != null )
					value = cardHolder!=null 
								?  GeneralUtil.formatStringCardNumber( cardHolder.getCard().getNumberCard(), layout.getFormatField() ) 
							    : "";
				else
					value = cardHolder!=null ? cardHolder.getCard().getNumberCard() : "";
			break;
			
			/** Obteniendo el Número de la Tarjeta **/
			case( 2 ):
				value = cardHolder!=null && cardHolder.getEmbosserProperties()!=null 
								? cardHolder.getEmbosserProperties().getShortName() 
								: "";
			break;
			
			/** Obtiene la Fecha Actual **/
			case( 3 ):
				aux = layout.getFormatField()!=null 
							? layout.getFormatField().trim() 
							: SystemConstants.FORMATO_FECHA_DEF;
							
				value = GeneralUtil.formatDate(aux, GeneralUtil.getCurrentSqlDate());
			break;
			
			/** Obtiene la Fecha De Expiración **/
			case( 4 ):
				aux = layout.getFormatField()!=null ? layout.getFormatField().trim() : SystemConstants.FORMATO_FECHA_DEF ;
				
				value = cardHolder.getEmbosserProperties()!=null 
							? GeneralUtil.formatDate(aux, cardHolder.getEmbosserProperties().getNewExpiryDate()) 
							:""; 
			break;
			
			case( 5 ):
				/** Si tiene un formato Definido, lo aplica **/
				if( layout.getFormatField() != null ){
					value = cardHolder!=null 
								? GeneralUtil.formatStringCardNumber( cardHolder.getCard().getNumberCard(), layout.getFormatField() ) 
								: "";
				}else{
					value = cardHolder!=null ? cardHolder.getCard().getNumberCard().trim() : "";
				}	
				value += " ";
				value += cvv2;
			break;
			
			/** Obtiene el Service Code **/
			case( 6 ):
				value =  cardHolder!=null && cardHolder.getIstcardType()!=null 
								? cardHolder.getIstcardType().getIccIndicator()+cardHolder.getIstcardType().getServiceCode()
								: "";
			break;
			
			/** Obtiene el CVV **/
			case( 7 ):
				value = cvv;
			break;
			
			/** Obtiene el Nombre del Banco **/
			case( 8 ):
				value =  cardHolder!=null && cardHolder.getIstBin()!=null 
								? cardHolder.getIstBin().getBankName()
								: "";
			break;
			
			/** Obtiene el nombre del Producto **/
			case( 9 ):
				value =  cardHolder!=null && cardHolder.getIstBin()!=null 
								? cardHolder.getIstBin().getProduct()
								: "";
			break;
			
			/** Obtiene el Tipo de Tarjeta **/
			case( 11 ):
				value = "";
					if( cardHolder.getEmbosserProperties()!=null ){
						value = GeneralUtil.isEqualsString(cardHolder.getEmbosserProperties().getCardIndicator(), "0" ) ? "TIT" : "ADIC";
					}
			break;
			
			/** Obtiene las siglas del Banco **/
			case( 12 ):
				value =  cardHolder!=null && cardHolder.getIstBin()!=null 
								? cardHolder.getIstBin().getSiglas()
								: "";
			break;
			
			/** Obtiene el Limite Diario **/
			case( 13 ):
				value =  cardHolder!=null && cardHolder.getIstcardType()!=null 
								? cardHolder.getIstcardType().getCashWdLimit()
								: "";
			break;
			
			/** Obtiene el Id de la Imágen **/
			case( 14 ):
				value =  completaCadena( String.valueOf(idImage), '0', 2, "R"  ) ;
			break;
			/** Procesando Salto de Carro **/
			case( 10 ):
				for( int i=0; i<layout.getLengthField(); i++ ){
					value += "\n";
				}
			break;
			
			default:
				value = " ";
			}
		
		}
		
		return value;
		
		
	}

	/***************************************************************
	 * Rellena de Caracteres una cadena a una Longitud determinada
	 ***************************************************************/
	public static String completaCadena( String cadena, char caracter, int longitud, String posicion ){
		String cadenaFinal = null;
		String aux         = "";
		if( cadena!=null ){
			if( cadena.length() > longitud ){
				cadenaFinal = cadena.substring(0,longitud);
			}else{
				for( int i=0; i<(longitud-cadena.length()); i++ ){
					aux += caracter;				  
				}
				cadenaFinal = posicion.equals("L") ? cadena+aux : aux+cadena;
			}
		}
		return cadenaFinal;
	}
	
}
