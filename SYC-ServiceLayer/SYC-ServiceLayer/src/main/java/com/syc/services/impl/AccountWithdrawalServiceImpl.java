package com.syc.services.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.annotation.CheckPAN;
import com.syc.dto.request.AccountWithDrawalRequestDto;
import com.syc.dto.request.LoadBalanceAccountRequest;
import com.syc.dto.request.LoadBalanceRequest;
import com.syc.dto.response.AccountWithDrawalResponseDto;
import com.syc.dto.response.BalanceMovementAcctRespDto;
import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.dto.response.BalanceMovementRespSodexoDto;
import com.syc.dto.response.BalanceQueryResponseDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.persistence.dao.AccountDao;
import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dao.EmisorDao;
import com.syc.persistence.dao.EmployeeDao;
import com.syc.persistence.dao.SHCLogDao;
import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.AccountDto;
import com.syc.persistence.dto.CardholderDto;
import com.syc.persistence.dto.EmisorDto;
import com.syc.persistence.dto.EmployeeDto;
import com.syc.persistence.dto.SHCLogDto;
import com.syc.services.AccountWithdrawalService;
import com.syc.services.CardService;
import com.syc.services.DepositAccountService;
import com.syc.services.LogService;
import com.syc.services.UserService;
import com.syc.utils.CatalogUtil;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SecurityUtils;
import com.syc.utils.SystemConstants;
import com.syc.utils.ValidatorUtil;
import com.syc.utils.WSLogUtil;

@Service
public class AccountWithdrawalServiceImpl implements AccountWithdrawalService{
	
	private static final transient Log log = LogFactory.getLog(AccountWithdrawalServiceImpl.class);
	
	@Autowired
	CardholderDao cardHolderDao;
	@Autowired
	SHCLogDao     shcLogDao;
	@Autowired
	AccountDao    accountDao;
	@Autowired
	EmisorDao    emisorDao;
	@Autowired
	WSLogDao      wsLogDao;
	@Autowired
	CardService cardService;
	@Autowired
	DepositAccountService depositService;
	@Autowired
	CardDao		  cardDao;
	@Autowired
	EmployeeDao employeeDao;
	
	@Autowired
	LogService logService;
	
	@Autowired
	UserService userService;
	
	
	TreeMap<Integer, Integer> validations;
	TreeMap<Integer, Integer> validations1;
	WSLogUtil wsUtil;

	public BalanceMovementRespDto doWithdrawalAccount(String pan, double amount, String user){
		return doWithdrawalAccount(pan, amount, "Retiro en Ventanilla WS", user, null);
	}
	/**
	 * realiza retiro a una tarjeta determinada
	 */
	public BalanceMovementRespDto doWithdrawalAccount(String pan, double amount, String description, String user, String reference){
		
		String request = "pan["+pan+"],Amount["+amount+"]";
		
		
		String authorization  = "0";
		String bin            = "0";
		
		double currentBalance =  0;
		double balance        =  0;
	
		int codeResponse      =  0;
		
		log.debug("::Procesando un Retiro a la Cuenta::");
		log.debug( "::Request::" + request );
		
		getMapOfValidations();
		
		/** Obteniendo la información de la Cuenta  **/
		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber( pan );
		
		bin     = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getBin().trim() : "0";
		
		log.debug("::Validando Tarjeta::");
		int validatorResult = new ValidatorUtil().validateWebMethod(validations, cardholderDto );

		if (validatorResult > 0) {
			log.debug("::Validaciones no superadas::");
			codeResponse = validatorResult;
		}else{
			log.debug("::Las validaciones fueron superadas correctamente::");
			
			if( amount > 0 ){
				if(description.length() < 41){
				/************************************************
				 *     Generando Transacción en Bitácora
				 ************************************************/
				AccountDto account = cardholderDto.getAccount();
				SHCLogDto shcLog   = new SHCLogDto(21);
				authorization      = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
				
				balance        = account.getAvailableBalance();
				currentBalance = account.getAvailableBalance() - amount;
				
				/** Validando que tenga el Saldo Suficiente **/
				if( currentBalance >= 0 ){
				
					shcLog.setNumberCard  ( pan );
					shcLog.setpCode       ( SystemConstants.PCODE_WINDOW_WITHDRAWAL );
					shcLog.setAmount      ( amount );
					shcLog.setAvalBalance ( currentBalance );
					shcLog.setAuthNum     ( authorization );
					shcLog.setTermId      ( "111111" );
					shcLog.setMsgType     ( 210 );
					shcLog.setFee         ( 0 );
					shcLog.setAcceptorName( description );
					shcLog.setAcctNum     ( account.getAccount() );
					shcLog.setFiller1     ( user );
					shcLog.setFiller3     ( "web_service" );
					shcLog.setRefNum     ( reference );
		
					shcLogDao.create( shcLog ); 
						
					/***************************************
					 * Actualizando el saldo en la Cuenta
					 ***************************************/
					account.setAvailableBalance(currentBalance);
					account.setLedgerBalance   (currentBalance);
					try{
						accountDao.update(account);
			
						codeResponse = 1;
						
						log.debug( "::Recarga Procesada Exitosamente::");
					}catch(Exception e){
						codeResponse = 50;
					}
				}else{
					codeResponse = 5; //Saldo Insuficiente
				}
			
				
				}else{
					codeResponse = 15;//descripcion demasiado largo
				}
			}else{
				codeResponse = 6; //Monto erroneo
			}
		}
		
		/*************************************
		 *     Generando Respuesta
		 *************************************/
		BalanceMovementRespDto response = new BalanceMovementRespDto();
		// se realiza este if para que los saldos no salgan negativos en el response
		if( currentBalance >= 0 ){
			response.setCode(codeResponse);
			response.setAuthorization(authorization);
			response.setDescription( CatalogUtil.getCatalogWithdrawalAccount().get(codeResponse) );
			// se utiliza la funcion math.rint para hacer el redondeo a 2 decimales
			response.setBalance(Math.rint(balance*100)/100);
			response.setCurrentBalance(Math.rint(currentBalance*100)/100);
			
		}else{
			response.setCode(codeResponse);
			response.setAuthorization(authorization);
			response.setDescription( CatalogUtil.getCatalogWithdrawalAccount().get(codeResponse) );
			response.setBalance(0.0);
			response.setCurrentBalance(0.0);
		}
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug( "::Almacenando Movimiento en Bitácora::");
		
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( bin, pan, user, request, codeResponse, response, SystemConstants.PCODE_LOG_WITHDRAWAL_ACCOUNT);
		
		log.debug( "::Devolviendo Respuesta::"+response.toString());

		return response;
	}
	
	private void getMapOfValidations() {
		validations = new TreeMap<Integer, Integer>();
		validations.put(SystemConstants.VALIDATE_EXISTS_ACCOUNT, 1);
		validations.put(SystemConstants.VALIDATE_EXPIRATION_DATE, 1);
		validations.put(SystemConstants.VALIDATE_ACTIVE_CARD, 1);
	}
	
	

	public BasicAuthRespDto transfersCard(String targetCard, String extractCard, double amount, String user) throws Exception{
		
		/**checar que las tarjetas existan
		 * consultar el saldo de la tarjeta a la q se le va a extraer el saldo 
		 * si tiene liquidez se procede a hacer el retiro a extractCard y el deposito a target card
		 * se regresa el saldo de targetCard, codigo 1 en caso de ser exitoso y descripcion**/
		String request = "targetCard["+targetCard+"],extractCard["+extractCard+"],Amount["+amount+"],User["+user+"]";
		
		
		String authorization  = "0";
		
		double currentBalance =  0;
		double balance        =  0;
	
		int codeResponse      =  0;
		String description = null;
		
		getMapOfValidations();
	
		/** Obteniendo la informacion de la Cuenta a la que se le extraera el dinero **/
		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber( extractCard );
		String bin     = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getBin().trim() : "0";

		
		/** Obteniendo la informacion de la Cuenta destino **/
		CardholderDto cardholderDto1 = cardHolderDao.getCardholderInfoByCardNumber( targetCard );
		log.info("::Validando Tarjeta::");
		int validatorResult = new ValidatorUtil().validateWebMethod(validations, cardholderDto );
		int validatorResult1 = new ValidatorUtil().validateWebMethod(validations, cardholderDto1 );
		String desc = "";
		if (validatorResult > 0 || validatorResult1 > 0) {
			
				
			log.debug("::Validaciones no superadas::");
			if(validatorResult == 0){
				codeResponse = validatorResult1;
				 desc = "targetCard ";
			}else{
				codeResponse = validatorResult;
				 desc = "extractCard ";
			}
			
			
				
		} else {
			log.debug("::Las validaciones fueron superadas correctamente::");
		
			if( amount > 0 ){
			/**consultar el saldo de la tarjeta a la que se le extraera el dinero**/
			
					BalanceQueryResponseDto saldo = cardService.balanceQuery(extractCard, user);
		 
					if(saldo.getAvailableAmount() >= amount){
		
						/************************************************
						 *     Generando Transacción en Bitácora
						 ************************************************/
						AccountDto account = cardholderDto.getAccount();
						SHCLogDto shcLog   = new SHCLogDto(21);
						authorization      = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
						
						/***********************************
						 * Se realiza el retiro
						 * ********************************/
						
						balance        = account.getAvailableBalance();
						currentBalance = account.getAvailableBalance() - amount;
						
						/** Validando que tenga el Saldo Suficiente **/
						if( currentBalance >= 0 ){
						
							shcLog.setNumberCard  ( extractCard );
							shcLog.setpCode       ( SystemConstants.PCODE_WINDOW_WITHDRAWAL);
							shcLog.setAmount      ( amount );
							shcLog.setAvalBalance ( currentBalance );
							shcLog.setAuthNum     ( authorization );
							shcLog.setTermId      ( "111111" );
							shcLog.setMsgType     ( 210 );
							shcLog.setFee         ( 0 );
							shcLog.setAcceptorName( desc+description );
							shcLog.setAcctNum     ( account.getAccount() );
							shcLog.setFiller1     ( user );
							shcLog.setFiller3     ( "web_service" );
							shcLogDao.create( shcLog ); 
								
							/***************************************
							 * Actualizando el saldo en la Cuenta
							 ***************************************/
							account.setAvailableBalance(currentBalance);
							account.setLedgerBalance   (currentBalance);
							accountDao.update(account);
							
		
							//**********************************************
							 //* se realiza el deposito a la tarjeta destino
							 //* *********************************************
							
							/************************************************
							 *     Generando Transacción en Bitácora
							 ************************************************/
							AccountDto account1 = cardholderDto1.getAccount();
							SHCLogDto shcLog1   = new SHCLogDto(21);
								
							double currentBalance1 = account1.getAvailableBalance() + amount;
								
							shcLog1.setNumberCard  ( targetCard );
							shcLog1.setpCode       ( SystemConstants.PCODE_WS_RECARGA_SALDO );
							shcLog1.setAmount      ( amount );
							shcLog1.setAvalBalance ( currentBalance1 );
							shcLog1.setAuthNum     ( authorization );
							shcLog1.setTermId      ( "111111" );
							shcLog1.setMsgType     ( 210 );
							shcLog1.setFee         ( 0 );
							shcLog1.setAcceptorName( desc+description );
							shcLog1.setAcctNum     ( account1.getAccount() );
							shcLog1.setFiller1     ( user );
							shcLog1.setFiller3     ( "web_service" );

							shcLogDao.create( shcLog1 ); 
								
							/***************************************
							 * Actualizando el saldo en la Cuenta
							 ***************************************/
							account1.setAvailableBalance(currentBalance1);
							account1.setLedgerBalance   (currentBalance1);
							accountDao.update(account1);
				
							codeResponse = 1;
						
							}else
									/**no se pudo realizar el retiro de la extractCard**/
									codeResponse = 7;
									
					}else
							/**saldo insuficiente en extractCard**/
							codeResponse =5;
			}else
				/**el monto a transferir debe ser mayor a 0.00**/
				codeResponse = 9;
		 
		}
		
			
		/*************************************
		*     Generando Respuesta
		*************************************/
		description = CatalogUtil.getCatalogTransferCards().get(codeResponse);
		BasicAuthRespDto response = new BasicAuthRespDto();
		 response.setCode(codeResponse);
		 response.setAuthorization(authorization);
		 response.setDescription(desc+description);
		 
		 
		 
		 /**************************************************
			 * Almacenando Transaccion en Bitacora del WS de extractCard
			 **************************************************/
			log.debug( "::Almacenando Movimiento en Bitácora::");
			
			wsUtil = new WSLogUtil( wsLogDao );
			wsUtil.saveLog( bin, extractCard, user, request, codeResponse, response, SystemConstants.PCODE_LOG_WITHDRAWAL_TRANSFERS);
			
			log.debug( "::Devolviendo Respuesta::"+response.toString());
		
		return response;
	}
	
public AccountWithDrawalResponseDto AccountWithDrawal( AccountWithDrawalRequestDto request ){
		
		String requestString = request.toString();//"bin["+request.getBin()+"],account["+request.getAccount()+"], transactionType["+request.getTransactionType()+"], referencia["+request.getReferencia()+"], monto["+request.getAmountWithDrawal()+"], comision["+request.getMontoComision()+"], moneda["+request.getCurrency()+"]";
		
		int codeResponse      =  0;
		String authorization  = "0";
		double currentBalance =  0.0;
		double logMonto =  0.0;
		double logcomision =  0.0;
		double balance        =  0.0;			
		String numberCard     = "0";
		EmployeeDto employeeDto      = null;
		
		
		String binString = GeneralUtil.rellenaCadena( request.getBin(), '0', 10, "L" );		
		//String accountString = GeneralUtil.rellenaCadena( request.getAccount(), '0', 19, "L" );
		
		AccountWithDrawalResponseDto response = new AccountWithDrawalResponseDto();
		
		getMapOfValidationsAccount();
		
		employeeDto      = employeeDao.findEmployeeByAccount( request.getAccount() );
		/**validar que el pcode este dentro de la lista*/
		List<String> pcodeAmouunt = codeAmountConstants();
		List<String> pcodeComission = codeComissionConstants();
	if(pcodeComission.contains(request.getTransactionCodeCommission())){	
		if(pcodeAmouunt.contains(request.getTransactionCodeAmount())){
			if(employeeDto != null){
				/** Obteniendo la informacion de la Cuenta donde se realizara el cargo **/
				CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber(employeeDto.getPan());
		
		
				try{			
					
					balance = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getAvailableBalance() : 0;
					numberCard = cardholderDto !=null ? cardholderDto.getCard().getNumberCard() : "0";
			
					if (cardholderDto != null) {
						int validatorResult = new ValidatorUtil().validateWebMethod(validations1, cardholderDto);
				
						if (validatorResult > 0) {
							codeResponse = validatorResult;
						} else {
							log.debug("Las validaciones fueron superadas correctamente...");
					
							/**se valida que el monto sea mayor a 0**/
							if( request.getAmountWithDrawal() > 0 ){
								AccountDto account = cardholderDto.getAccount();
								double montoTotal = request.getAmountWithDrawal() + request.getComissionAmount();
								
								if(account.getAvailableBalance() >= montoTotal){
									currentBalance = (account.getAvailableBalance() - montoTotal);
									logMonto = (account.getAvailableBalance() - request.getAmountWithDrawal());
									logcomision = (logMonto - request.getComissionAmount());
					
									if( currentBalance >= 0 ){
										/************************************************
										 * Almacenando transaccion en Bitacora SHCLog
										 * del monto a retirar
										 ************************************************/
										account = cardholderDto.getAccount();
										SHCLogDto shcLog   = new SHCLogDto(21);
										authorization      = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
										// pcode = Integer.parseInt(CatalogUtil.getCatalogpCodeAccount().get(request.getTransactionCodeAmount()));
										shcLog.setNumberCard  ( cardholderDto.getCard().getNumberCard() );
										shcLog.setpCode       ( Integer.parseInt(request.getTransactionCodeAmount()) );
										shcLog.setAmount      ( request.getAmountWithDrawal() );
										shcLog.setAvalBalance ( logMonto );
										shcLog.setAuthNum     ( authorization );
										shcLog.setTermId      ( "111111" );
										shcLog.setMsgType     ( 210 );
										shcLog.setAcceptorName( "Cargo Pago de Servicios y Tiempo Aire" );
										shcLog.setAcctNum     ( account.getAccount() );
										shcLog.setFiller1     ( SecurityUtils.getCurrentUser());
										shcLog.setFiller3     ( "web_service" );
										//shcLog.setRefNum     ( request.getTransactionCodeAmount() );
			
											shcLogDao.create( shcLog );
											/************************************************
											 * Almacenando transaccion en Bitacora SHCLog
											 * del monto de comision
											 ************************************************/
											SHCLogDto shcLog2   = new SHCLogDto(21);
										//	int pcode1 = Integer.parseInt(CatalogUtil.getCatalogpCodeComission().get(request.getTransactionCodeCommission()));

											shcLog2.setNumberCard  ( cardholderDto.getCard().getNumberCard() );
											shcLog2.setpCode       ( Integer.parseInt(request.getTransactionCodeCommission()));
											shcLog2.setAmount      ( request.getComissionAmount() );
											shcLog2.setAvalBalance ( logcomision );
											shcLog2.setAuthNum     ( authorization );
											shcLog2.setTermId      ( "111111" );
											shcLog2.setMsgType     ( 210 );
											shcLog2.setAcceptorName( "Cargo Pago de Servicios y Tiempo Aire" );
											shcLog2.setAcctNum     ( account.getAccount() );
											shcLog2.setFiller1     ( SecurityUtils.getCurrentUser() );
											shcLog2.setFiller3     ( "web_service" );
											//shcLog2.setRefNum     ( transactionCodeCom );
											shcLogDao.create( shcLog2 );
									
											/***************************************
											 * Actualizando el saldo en la Cuenta
											 ***************************************/
											account.setAvailableBalance(currentBalance);
											account.setLedgerBalance(currentBalance);
											int updated = accountDao.sqlUpdate(account);// update(account);		
											if(updated == 1){
												codeResponse = 1;
											    log.debug( "::Recarga Procesada Exitosamente::");
											}else{
												codeResponse = 50;
												log.debug( "::Recarga no procesada, vuelva a intentar");
											}
						
									
									}else{
										codeResponse = 5; //Saldo Insuficiente
									}
							}else{
									codeResponse = 5;//saldo insuficiente
							}
						}else{
							codeResponse = 6; //Monto erroneo
						}
				
					}
				
				}else{
					codeResponse = 8; //tarjeta no existe
				}
	
		
		}catch(Exception e){
			wsUtil = new WSLogUtil( wsLogDao );
			e.printStackTrace();
			wsUtil.saveLog(request.getBin(), cardholderDto.getCard().getNumberCard(), SystemConstants.WS_USER_NAME, requestString, -1,"error al procesar solicitud, verifique parámetros de entrada", "1234");
		}
		}else
			codeResponse = 3;// cuenta no existe	
		
		}else{
			codeResponse = 20;//pcode amount incorrecto
		}
		
	}else{
		codeResponse = 21;//pcode comission incorrecto
	}
		/*************************************
		 *     Generando Respuesta
		 *************************************/			
		response.setCode(codeResponse);
		response.setAuthorization(authorization);
		response.setBalance(balance);
		response.setCurrentBalance(currentBalance);
		response.setTransactionCodeAmount(Integer.parseInt(request.getTransactionCodeAmount()));
		response.setDescription(CatalogUtil.getCatalogWithdrawalAccount().get(codeResponse));
		
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( binString, numberCard, "ws_monex", requestString, codeResponse, response, SystemConstants.PCODE_MONEX_COMMISION);

		return response;
		
	}	

	
	public BalanceMovementRespDto doDevAccount( String pan, double amount, int pCode, String description, String user, String comments )throws Exception{
		String request = "pan["+pan+"],Amount["+amount+"], pCode["+pCode+"], description["+description+"]";
		
		
		String authorization  = "0";
		String bin            = "0";
		
		double currentBalance =  0;
		double balance        =  0;
		double currbal = 0;
		int codeResponse      =  0;
		
		log.debug("::Procesando un Retiro a la Cuenta::");
		log.debug( "::Request::" + request );
		
		getMapOfValidations();
		
		/** Obteniendo la información de la Cuenta  **/
		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber( pan );
		
		bin     = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getBin().trim() : "0";
		
		log.debug("::Validando Tarjeta::");
		int validatorResult = new ValidatorUtil().validateWebMethod(validations, cardholderDto );

		if (validatorResult > 0) {
			log.debug("::Validaciones no superadas::");
			codeResponse = validatorResult;
		}else{
			log.debug("::Las validaciones fueron superadas correctamente::");
			
			if( amount > 0 ){
		
				/************************************************
				 *     Generando Transacción en Bitácora
				 ************************************************/
				AccountDto account = cardholderDto.getAccount();
				SHCLogDto shcLog   = new SHCLogDto(21);
				authorization      = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
				
				balance        = account.getAvailableBalance();
				currbal = account.getAvailableBalance() - amount;
				
				/** Validando que tenga el Saldo Suficiente **/
				if( currbal >= 0 ){
				
					currentBalance = account.getAvailableBalance() - amount;
					shcLog.setNumberCard  ( pan );
					shcLog.setpCode       ( pCode );
					shcLog.setAmount      ( amount );
					shcLog.setAvalBalance ( currentBalance );
					shcLog.setAuthNum     ( authorization );
					shcLog.setTermId      ( "111111" );
					shcLog.setMsgType     ( 210 );
					shcLog.setFee         ( 0 );
					shcLog.setAcceptorName( description );
					shcLog.setAcctNum     ( account.getAccount() );
					shcLog.setFiller1     ( user );
					shcLog.setFiller3     ( "web_service" );
					
		
					shcLogDao.create( shcLog ); 
						
					/***************************************
					 * Actualizando el saldo en la Cuenta
					 ***************************************/
					account.setAvailableBalance(currentBalance);
					account.setLedgerBalance   (currentBalance);
					int updated = accountDao.sqlUpdate(account);
					if(updated == 1){
						codeResponse = 1;

					log.debug( "::Recarga Procesada Exitosamente::");
					}else{
						codeResponse = 50;
						
						log.debug( "::Recarga no procesada, vuelva a intentar");
					}
				}else{
					codeResponse = 5; //Saldo Insuficiente
				}
			}else{
				codeResponse = 6; //Monto erroneo
			}
		}
		
		/*************************************
		 *     Generando Respuesta
		 *************************************/
		BalanceMovementRespDto response = new BalanceMovementRespDto();
		response.setCode(codeResponse);
		response.setAuthorization(authorization);
		response.setDescription( CatalogUtil.getCatalogWithdrawalAccount().get(codeResponse) );
		response.setBalance(balance);
		response.setCurrentBalance(currentBalance);

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug( "::Almacenando Movimiento en Bitácora::");
		
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( bin, pan, user, request, codeResponse, response, SystemConstants.PCODE_LOG_WITHDRAWAL_ACCOUNT);
		
		log.debug( "::Devolviendo Respuesta::"+response.toString());

		return response;

	}
	
	
	
	/**servicio que realiza retiros a determinado numero de tarjetas**/
	public List<BalanceMovementRespSodexoDto> listwithDrawal( List<LoadBalanceRequest> request, String user, boolean isValidaBin) throws Exception{

		List<BalanceMovementRespSodexoDto> lista =  new ArrayList<BalanceMovementRespSodexoDto>();
		//String pan = null;
		
		if (request != null){
			
			for(int i = 0; i<=request.size() -1; i++){
				BalanceMovementRespSodexoDto registro = new BalanceMovementRespSodexoDto();
				//pan = request.get(i).getPan();
				//System.out.println(pan);
				
				//validar que la tarjeta pertenezca a sodexo o servibonos
				//con el usuario hacer la consulta a user_bin y sacar los bines permitidos 
				//en la tabla agregar los bines de servibonos a sodexo y viseversa
				//si no es un bin valido, llenar el objeto de respuesta y seguir con la siguiente tarjeta
				boolean isPermitido = true;
				
				if(isValidaBin)
				
				 isPermitido = userService.getBinesPermitidos(user, request.get(i).getPan().substring(0, 8));
				
				if(isPermitido){
					 BalanceMovementRespDto response = doWithdrawalAccount( request.get(i).getPan(), request.get(i).getAmount(), "sodexo_user" );
					 registro.setCode(response.getCode());
					 registro.setAuthorization(response.getAuthorization());
					 registro.setDescription(response.getDescription());
					 registro.setPan(request.get(i).getPan());
					 registro.setBalance(response.getBalance());
					 registro.setCurrentBalance(response.getCurrentBalance());
				
					 lista.add(registro);
				}else{
					 registro.setCode(30);
					 registro.setAuthorization("0");
					 registro.setDescription("operacion no permitida sobre el BIN [ " + request.get(i).getPan()+ " ] ");
					 registro.setPan(request.get(i).getPan());
					 registro.setBalance(0);
					 registro.setCurrentBalance(0);
				
					 lista.add(registro);
				}
				 
			}		 
		
			//System.out.println(lista.toString());

		}
		
		return lista;
		

	}
	
public List<BalanceMovementAcctRespDto> withdrawalAccount( List<LoadBalanceAccountRequest> request, String user, int clave_emisor ) throws Exception{
		
		List<BalanceMovementAcctRespDto> lista 		 =  new ArrayList<BalanceMovementAcctRespDto>();
		EmployeeDto employeeDto      				 = null;

		if (request != null){
			
			for(int i = 0; i<=request.size() -1; i++){
				
				BalanceMovementAcctRespDto registro = new BalanceMovementAcctRespDto();
				
				//aquí va el código para traer la tarjeta a partir de la cuenta
				
				employeeDto      = employeeDao.findEmployeeByAccount( request.get(i).getAccount(), clave_emisor );

				if(employeeDto != null){
					
					BalanceMovementRespDto response = null;
					try{
						response = doWithdrawalAccount( employeeDto.getPan(), request.get(i).getAmount(), user );
							registro.setCode(response.getCode());
							registro.setAuthorization(response.getAuthorization());
							registro.setDescription(response.getDescription());
							registro.setAccount(request.get(i).getAccount());
							registro.setBalance(response.getBalance());
							registro.setCurrentBalance(response.getCurrentBalance());
					}catch(Exception e){
							registro.setCode(50);
							registro.setAuthorization("0");
							registro.setDescription("error al procesar la solicitud, intentelo mas tarde");
							registro.setAccount(request.get(i).getAccount());
							registro.setBalance(response.getBalance());
							registro.setCurrentBalance(0);
					}
				}else{
					registro.setCode(10);
					registro.setAuthorization("0");
					registro.setDescription("cuenta no registrada");
					registro.setAccount(request.get(i).getAccount());
					registro.setBalance(0.0);
					registro.setCurrentBalance(0.0);
				} 
				 lista.add(registro);
			}		 
		
		
		}
		

		return lista;
		
	}	
	
	
	private void getMapOfValidationsAccount() {
		validations1 = new TreeMap<Integer, Integer>();
		validations1.put(SystemConstants.VALIDATE_EXISTS_ACCOUNT, 1);
		validations1.put(SystemConstants.VALIDATE_ACCOUNT_ACTIVE, 1);
		
	}
	
private List<String> codeAmountConstants(){
		
		List<String> pcodes = new ArrayList<String>();
		
		pcodes.add("900100");
		pcodes.add("900300");
		pcodes.add("900500");
		pcodes.add("900700");
		pcodes.add("900900");
		pcodes.add("901100");
		pcodes.add("901300");
		pcodes.add("901500");
		pcodes.add("901700");
		pcodes.add("901900");
		pcodes.add("902100");
		pcodes.add("902300");
		pcodes.add("902500");
		pcodes.add("902700");

		return pcodes;
	}
	
	private List<String> codeComissionConstants(){
		List<String> pcodesc = new ArrayList<String>();
		pcodesc.add("900200");
		pcodesc.add("900400");
		pcodesc.add("900600");
		pcodesc.add("900800");
		pcodesc.add("901000");
		pcodesc.add("901200");
		pcodesc.add("901400");
		pcodesc.add("901600");
		pcodesc.add("901800");
		pcodesc.add("902000");
		pcodesc.add("902200");
		pcodesc.add("902400");
		pcodesc.add("902600");
		pcodesc.add("902800");
		pcodesc.add("902900");
		return pcodesc;
	}
	@Override
	public BalanceMovementRespDto doWithdrawalAccount(String bin, String account, double amount, int pCode, String description, String user) throws Exception {
		
		String requestString = "bin["+bin+"],account["+account+"], monto["+amount+"]";
		
		int codeResponse      =  0;
		String authorization  = "0";
		double currentBalance =  0.0;
		double balance        =  0.0;			
		String pan = null;
		
		 bin = GeneralUtil.rellenaCadena( bin, '0', 10, "L" );		
		//String accountString = GeneralUtil.rellenaCadena( request.getAccount(), '0', 19, "L" );
		
		BalanceMovementRespDto response = new BalanceMovementRespDto();
		
		getMapOfValidationsAccount();
		EmisorDto emisorDto = emisorDao.findByBin(bin);
		
		if(emisorDto != null){
				String buildAccount = GeneralUtil.buildPrimaryAccount(account, emisorDto.getCuenta());
				/** Obteniendo la informacion de la Cuenta donde se realizara el cargo **/
				CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByAccount(bin, buildAccount);
				
					balance = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getAvailableBalance() : 0;
					if (cardholderDto != null) {
						pan = cardholderDto.getCard().getNumberCard();
					
						int validatorResult = new ValidatorUtil().validateWebMethod(validations1, cardholderDto);
				
						if (validatorResult > 0) {
							codeResponse = validatorResult;
						} else {
							log.debug("Las validaciones fueron superadas correctamente...");
					
							/**se valida que el monto sea mayor a 0**/
							if( amount > 0 ){
								AccountDto accountDto = cardholderDto.getAccount();
								if(accountDto.getAvailableBalance() >= amount){
									currentBalance = (accountDto.getAvailableBalance() - amount);
									if( currentBalance >= 0 ){
										/************************************************
										 * Almacenando transaccion en Bitacora SHCLog
										 * del monto a retirar
										 ************************************************/
										SHCLogDto shcLog   = new SHCLogDto(21);
										authorization      = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
										// pcode = Integer.parseInt(CatalogUtil.getCatalogpCodeAccount().get(request.getTransactionCodeAmount()));
										shcLog.setNumberCard  ( cardholderDto.getCard().getNumberCard() );
										shcLog.setpCode       ( pCode);
										shcLog.setAmount      ( amount);
										shcLog.setAvalBalance ( currentBalance );
										shcLog.setAuthNum     ( authorization );
										shcLog.setTermId      ( "111111" );
										shcLog.setMsgType     ( 210 );
										shcLog.setAcceptorName( description );
										shcLog.setAcctNum     ( accountDto.getAccount() );
										shcLog.setFiller1     ( user);
										shcLog.setFiller3     ( "web_service" );
										//shcLog.setRefNum     ( request.getTransactionCodeAmount() );
			
											shcLogDao.create( shcLog );
											/***************************************
											 * Actualizando el saldo en la Cuenta
											 ***************************************/
											accountDto.setAvailableBalance(currentBalance);
											accountDto.setLedgerBalance(currentBalance);
											try{
												accountDao.update(accountDto);		
												log.debug( "::Recarga Procesada Exitosamente::");
												codeResponse = 1;
											}catch(Exception e){
												codeResponse = 50;
											}
						
									
									}else{
										codeResponse = 5; //Saldo Insuficiente
									}
								}else{
									codeResponse = 5;//saldo insuficiente
								}
							}else{
								codeResponse = 6; //Monto erroneo
							}
						}
				
					}else{
						codeResponse = 8; //tarjeta no existe
					}
		}else{
			codeResponse = 22;//prefijo no encontrado
		}
		
		/*************************************
		 *     Generando Respuesta
		 *************************************/			
		response.setCode(codeResponse);
		response.setAuthorization(authorization);
		response.setBalance(balance);
		response.setCurrentBalance(currentBalance);
		response.setDescription(CatalogUtil.getCatalogWithdrawalAccount().get(codeResponse));
		
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( bin, pan, user, requestString, codeResponse, response, SystemConstants.PCODE_LOG_WITHDRAWAL_ACCOUNT_A);
		
		return response;

	}
	
}


