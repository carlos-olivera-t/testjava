package com.syc.services.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.response.*;
import com.syc.persistence.dao.*;
import com.syc.persistence.dto.*;
import com.syc.persistence.dto.mapper.BalanceDto;
import com.syc.services.*;
import com.syc.utils.*;


@Service
public class AccountServiceImpl implements AccountService{

	
	private static final transient Log log = LogFactory.getLog(AccountServiceImpl.class);
	
	@Autowired
	CardholderDao cardholderDao;
	@Autowired
	AccountDao    accountDao;
	@Autowired
	PCodeDao    pCodeDao;
	@Autowired
	EmisorDao    emisorDao;
	@Autowired
	CardDao		  cardDao;
	@Autowired
	CardService cardService;
	@Autowired
	DepositAccountService depositAccountService;
	@Autowired
	AccountWithdrawalService accountWithdrawalService;
	@Autowired
	IstCardTypeDao istCardTypeDao;
	
	@Autowired
	CatchmentLevelsService catchmentLevelsService;
	
	@Autowired
	WSLogDao wsLogDao;
	WSLogUtil wsUtil;

	TreeMap<Integer, Integer> validations;

	
	@Override
	public BasicAuthRespDto accountLock(String account, String bin)
			throws Exception {
		int codeResponse = 0; //Operacion realizada con exito.
        String folio = "";
        String request = "account[" + account + "] bin[ " + bin + " ]";
        String user = SecurityUtils.getCurrentUser();
        
        bloqueaCuenta:{
            folio = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
            
            if( !(account != null && account.length() == 11) ){
                codeResponse = 1; //Cuenta no válida
                break bloqueaCuenta;
            }
            
            String binFormateado = GeneralUtil.rellenaCadena(bin, '0', 10, "I");
            EmisorDto emisorDto = emisorDao.findByBin(binFormateado);
            String cuentaFormateda = GeneralUtil.buildPrimaryAccount(account, emisorDto.getCuenta());
            
            AccountDto cuentaObtenida = accountDao.findInfoAccount(emisorDto.getBin(), cuentaFormateda);
            if(cuentaObtenida == null){
                codeResponse = 2; //No se encontraron datos para la cuenta 
                break bloqueaCuenta;

            }
                        
            if( !cuentaObtenida.getStatus().equals(SystemConstants.STATUS_ACTIVE_ACCOUNT) ){
                codeResponse = 3; //La cuenta no esta activa
                break bloqueaCuenta;
            }
            
            if(cuentaObtenida.getAvailableBalance() > 0){
            	codeResponse = 4; //Saldo disponible mayor a cero
                break bloqueaCuenta;
            }
            cuentaObtenida.setStatus(SystemConstants.STATUS_CANCEL_ACCOUNT);
            accountDao.createOrUpdate(cuentaObtenida);
            
            
            List<CardDto> listaTarjetas = cardDao.findByAccount(cuentaFormateda);
            if(listaTarjetas != null && listaTarjetas.size() > 0){
                for(int i=0; i<listaTarjetas.size(); i++){
                    CardDto oCardDto = listaTarjetas.get(i);                    
                    cardService.cardLock(oCardDto.getNumberCard(), user, SystemConstants.STATUS_CANCEL_CARD);                                        
                }
            }                    
        }
        
        BasicAuthRespDto response = new BasicAuthRespDto();
        response.setCode(codeResponse);        
        response.setDescription(CatalogUtil.getCatalogAccountLock().get(codeResponse));
        
        if(codeResponse == 0){
        	response.setAuthorization(folio);
        }
        
        wsUtil = new WSLogUtil(wsLogDao);
        wsUtil.saveLog("0", "", user, request, codeResponse, response,SystemConstants.PCODE_LOG_LOCK_ACCOUNT);

        return response;

	}
	@Override
	
	public BalanceMovementRespDto doAdjustment(String id, String bin, String account, double amount, String user) throws Exception {
		String request = "id["+id+"], account["+account+"], amount["+amount+"]";
		BalanceMovementRespDto response = null;
		PCodeDto pCodeDto = pCodeDao.findIdPCode(id);
		int codeResponse = 0;
		String description = null;
		double balance = 0.0;
		double currentBalance = 0.0;
		String pan = null;
		bin     = GeneralUtil.completaCadena(bin, '0', 10, "L");
		EmisorDto emisorDto = emisorDao.findByBin(bin);
		exit:
		if(emisorDto != null){
				String buildAccount = GeneralUtil.buildPrimaryAccount(account, emisorDto.getCuenta());
				CardholderDto cardholderDto = cardholderDao.getCardholderInfoByAccount( bin, buildAccount );
				
					if(cardholderDto != null){
						pan = cardholderDto.getCard().getNumberCard();
						if(pCodeDto != null){
							String operation = pCodeDto.getDescConcept();
							if(operation.equals("CARGO")){
								response = accountWithdrawalService.doWithdrawalAccount(bin, account, amount, pCodeDto.getpCode(), pCodeDto.getDescEdoCta(), user);
							}else if(operation.equals("ABONO")){
								
								/** Invocando Niveles de Captacion**/
								
								LevelCaptationResponseDto levelCaptation = catchmentLevelsService.levelsCaptation(cardholderDto.getCard().getNumberCard(), amount, user );
								if(levelCaptation.getCode() == 1){
									String edoCtaDescription = (pCodeDto.getDescEdoCta().length() < 41) ? pCodeDto.getDescEdoCta() :pCodeDto.getDescEdoCta().substring(0, 39);
									response = depositAccountService.doDepositAccount(bin, account, amount, pCodeDto.getpCode(), edoCtaDescription, user, levelCaptation);  
								}else{
									codeResponse = levelCaptation.getCode();
									description  = levelCaptation.getDescription();
									break exit;
								}
					
							}else{
								codeResponse = 25;//tipo de operacion no reconocida
								description = "tipo de operacion no reconocida";
							}
						}else{
							codeResponse = 26;//tipo de operacion no reconocida
							description = "id no encontrado";
						}
					}else{
						codeResponse = 3;//tipo de operacion no reconocida
						description = "cuenta no encontrada";
			
					}
		}else{
			codeResponse = 9;
			description = "prefijo no encontrado";
		}
					if(response == null){
						response = new BalanceMovementRespDto();
						response.setAuthorization("0");
						response.setBalance(balance);
						response.setCode(codeResponse);
						response.setCurrentBalance(currentBalance);
						response.setDescription(description);
			
					}
					wsUtil = new WSLogUtil( wsLogDao );
					wsUtil.saveLog( bin, pan, user, request, codeResponse, response, SystemConstants.PCODE_LOG_ADJUSTMENT);
		
		
					return response;
		}	
	
	
	private void getMapOfValidations() {
		validations = new TreeMap<Integer, Integer>();
		validations.put(SystemConstants.VALIDATE_EXISTS_ACCOUNT, 1);
		validations.put(SystemConstants.VALIDATE_ACCOUNT_ACTIVE, 1);

	}
	@Override
	public BasicAuthRespDto temporaryLockAccount(String bin, String account, int operationType, String user) {
		
		final int OPERATION_TYPE_LOCK = 1;
		final int OPERATION_TYPE_UNLOCK = 2;

		String request = "bin[" + bin + "], account["+account+"]operationType["+operationType+"]";

		log.info("Procesando Bloqueo de Cuenta");
		log.info("Request::" + request);

		String folio = "0";
		String description = null;
		String cmsfunc = null;
		int codeResponse = 0;
		String accountStatus = null;
		String compareStatus = null;
		String accountBuild = null;
		String binSyc = GeneralUtil.completaCadena(bin, '0', 10, "L");
		/** Validando que el tipo de Operación sea permitido. **/
		if (operationType <= 2) {
			EmisorDto oEmisorDto = emisorDao.findByBin(binSyc);
			getMapOfValidations();
			if (oEmisorDto != null) {
				 accountBuild = GeneralUtil.buildPrimaryAccount(account, oEmisorDto.getCuenta().trim());
				if(accountBuild != null){
					AccountDto accountDto = accountDao.findInfoAccount(binSyc,accountBuild);
					if (operationType == OPERATION_TYPE_LOCK) {
						accountStatus = SystemConstants.STATUS_TEMPORARY_LOCK_ACCOUNT;
						description = "BLOQUEO TEMPORAL";
						cmsfunc = "15";
						compareStatus = SystemConstants.STATUS_ACTIVE_ACCOUNT;
					} else if(operationType == OPERATION_TYPE_UNLOCK){
						accountStatus = SystemConstants.STATUS_ACTIVE_ACCOUNT;
						description = "DESBLOQUEO TEMPORAL";
						cmsfunc = "16";
						compareStatus = SystemConstants.STATUS_TEMPORARY_LOCK_ACCOUNT;
					}else
						codeResponse = 4;
					
					if (accountDto != null) {
						if (accountDto.getStatus().equals(compareStatus)) {
							folio = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
							accountStatus = (operationType == OPERATION_TYPE_LOCK) ? SystemConstants.STATUS_TEMPORARY_LOCK_ACCOUNT
									: SystemConstants.STATUS_ACTIVE_ACCOUNT;
							description = (operationType == OPERATION_TYPE_LOCK) ? "BLOQUEO TEMPORAL CUENTA"
									: "DESBLOQUEO CUENTA";

							/*****************************************
							 * Actualizando Estatus de la Tarjeta
							 *****************************************/
							accountDto.setStatus(accountStatus);
							accountDao.update(accountDto);

							codeResponse = 1; // Operacion Exitosa
						} else
							codeResponse = 3; // La cuenta tiene que estar Activa
					} else 
						codeResponse = 2; // cuenta Inexistente
				}else
					codeResponse = 2;
			}else
				codeResponse = 9;//prefijo no encontrado
		} else 
			codeResponse = 4;
				
				/*************************************
				 * Generando Respuesta a Devolver
				 *************************************/
				BasicAuthRespDto response = new BasicAuthRespDto();
				response.setCode(codeResponse);
				response.setAuthorization(folio);
				response.setDescription(CatalogUtil.getCatalogAccountTemLock().get(codeResponse));

				/**************************************************
				 * Almacenando Transaccion en Bitacora del WS
				 **************************************************/

				log.debug("::Almacenando Movimiento en Bitácora::");
				wsUtil = new WSLogUtil(wsLogDao);
				wsUtil.saveLog(binSyc, accountBuild, user, request, codeResponse, response,SystemConstants.PCODE_LOG_TEMPORARY_LOCK_ACCOUNT);

	return response;
					
	}
	@Override
	public CardsBalanceAccountResponseDto getCardsBalanceByAccount(String account, String bin, String user) {
		int codeResponse = 0;
		String description = null;
		Date currentDate = GeneralUtil.getCurrentSqlDate();
		List<Object[]> cards = null;
		List<BalanceDto> pans = null;
		if (bin != null && account != null && user != null) {
			/** El bin en las tablas esta a 10 posiciones */
			String binSyc = GeneralUtil.completaCadena(bin, '0', 10, "L");

			/** Obtenemos al prefijo de la tabla emisores */
			EmisorDto oEmisorDto = emisorDao.findByBin(binSyc);

			/** Generamos el numero de cuenta */
			String accountBuild = GeneralUtil.buildPrimaryAccount(account,
					oEmisorDto.getCuenta().trim());
			CardholderDto cardholderDto = cardholderDao.getCardholderInfoByAccount(binSyc, accountBuild);
			if(cardholderDto != null){
				if (accountBuild != null) {
					cards = cardDao.findCardsByAccount(bin, accountBuild);
					if (cards != null && cards.size() > 0) {
						pans = new ArrayList<BalanceDto>();
						for (int i = 0; i < cards.size(); i++) {
							BalanceDto tar = new BalanceDto();
							tar.setNumberCard((String) cards.get(i)[0]);
							tar.setStatus((Integer) cards.get(i)[1]);
							tar.setCardIndicator((cards.get(i)[2].toString()
								.equals("0")) ? "Titular" : "Adicional");
							tar.setAvalBalance(cardholderDto.getAccount().getAvailableBalance());
							pans.add(tar);
						}
						codeResponse = 1;
						description = "Tarjetas registradas";
					} else {
						codeResponse = 2;
						description = "no se encontraron tarjetas asociadas";
					}
				} else {
					codeResponse = 3;
					description = "error al procesar la cuenta";

				}
			}else{
				codeResponse = 5;
				description = "cuenta no existe";
			}
		} else {
			codeResponse = 4;
			description = "valores invalidos para la operación";
		}

		CardsBalanceAccountResponseDto response = new CardsBalanceAccountResponseDto();

		response.setCodeResponse(codeResponse);
		response.setDescription(description);
		response.setIssueDate(GeneralUtil.formatDate("dd-MM-yyyy", currentDate));
		response.setCurrentTime(GeneralUtil.formatDate("HH:mm:ss", currentDate));
		response.setCards(pans);

		return response;
	}
}
