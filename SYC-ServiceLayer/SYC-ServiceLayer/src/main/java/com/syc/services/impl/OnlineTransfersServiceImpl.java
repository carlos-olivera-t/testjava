package com.syc.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.request.TransferOnlineRequestDto;
import com.syc.dto.response.LevelCaptationResponseDto;
import com.syc.dto.response.TransferSpeiResponseDto;
import com.syc.persistence.dao.*;
import com.syc.persistence.dto.*;
import com.syc.services.CatchmentLevelsService;
import com.syc.services.OnlineTransfersService;
import com.syc.utils.*;

@Service
public class OnlineTransfersServiceImpl implements OnlineTransfersService {

	private static final transient Log log = LogFactory
			.getLog(OnlineTransfersServiceImpl.class);

	@Autowired
	CardDao cardDao;
	@Autowired
	AccountDao accountDao;
	@Autowired
	CardholderDao cardholderDao;
	@Autowired
	SHCLogDao shcLogDao;
	@Autowired
	IstCardTypeDao istCardTypeDao;
	@Autowired
	EmisorDao emisorDao;
	@Autowired
	WSLogDao wsLogDao;
	@Autowired
	CatchmentLevelsService catchmentLevelsService;

	WSLogUtil wsUtil;
	TreeMap<Integer, Integer> validations;

	@Override
	public List<TransferSpeiResponseDto> transfersSpei(List<TransferOnlineRequestDto> request, String bin, String user) {
		List<TransferSpeiResponseDto> response = new ArrayList<TransferSpeiResponseDto>();
		int codeResponse = 0;
		double newAvailableBalanceSource = 0.0;
		double newAvailableBalanceDest = 0.0;
		double avalBalanceSource = 0.0;
		double avalBalanceDest = 0.0;
		String authorization = "0";
		getMapOfValidations();
		String panSource = "0";
		String requestOp = null;

		if (request != null && request.size() > 0) {
			TransferSpeiResponseDto ob1 = null;
			for (TransferOnlineRequestDto r : request) {
				requestOp = "sourceAccount [" + r.getSourceAccount()+ "], destinationAccount [" + r.getDestinationAccount()+ "], amount [" + r.getAmount() + "]";
				String binSyc = GeneralUtil.completaCadena(bin, '0', 10, "L");
				EmisorDto oEmisorDto = emisorDao.findByBin(binSyc);
				if (oEmisorDto != null) {
					String accountBuildSource = GeneralUtil.buildPrimaryAccount(r.getSourceAccount(),oEmisorDto.getCuenta().trim());
					String accountBuildDest = GeneralUtil.buildPrimaryAccount(r.getDestinationAccount(), oEmisorDto.getCuenta().trim());
					if (accountBuildSource != null) {
						if (accountBuildDest != null) {
							CardDto cardDtoSource = cardDao.findPanByAccount(binSyc, accountBuildSource);
							CardDto cardDtoDestination = cardDao.findPanByAccount(binSyc, accountBuildDest);
							if (!r.getDestinationAccount().equals(r.getSourceAccount())) {
								if (cardDtoSource != null) {
									if (cardDtoDestination != null) {
										panSource = cardDtoSource.getNumberCard();
										CardholderDto cardholderDtoSource = cardholderDao.getCardholderInfoByCardNumber(cardDtoSource.getNumberCard());
										CardholderDto cardholderDtoDestination = cardholderDao.getCardholderInfoByCardNumber(cardDtoDestination.getNumberCard());
										if (cardholderDtoDestination != null&& cardholderDtoSource != null) {
											log.debug("::Validando Cuenta::");
											int validatorResult = new ValidatorUtil().validateWebMethod(validations, cardholderDtoSource);
											int validatorResult1 = new ValidatorUtil().validateWebMethod(validations, cardholderDtoDestination);
											if (validatorResult > 0 || validatorResult1 > 0) {
												log.debug("::Validaciones no superadas::");
												
												codeResponse = (validatorResult > 0) ? validatorResult : validatorResult1;

											} else if (validatorResult == 0 && validatorResult1 == 0) {
												log.debug("::Las validaciones fueron superadas correctamente::");
												AccountDto accountDtoDest = cardholderDtoDestination.getAccount();
												AccountDto accountDtoSource = cardholderDtoSource.getAccount();

												/*******************************************
												 * se realiza el retiro de la cuenta origen
												 *******************************************/
												avalBalanceSource = accountDtoSource.getAvailableBalance();
												newAvailableBalanceSource = avalBalanceSource - r.getAmount();
												/*************************************************
												 * se realiza el deposito a la  cuenta destino
												 ***********************************************/
												avalBalanceDest = accountDtoDest.getAvailableBalance();
												newAvailableBalanceDest = avalBalanceDest + r.getAmount();
												
												/*******************************************************************************
												 *  Si el saldo disponible es menos a 0, la cuenta no tiene fondos suficientes
												 *******************************************************************************/
												if( newAvailableBalanceSource >= 0 ){
													
													LevelCaptationResponseDto levels = catchmentLevelsService.levelsCaptation(cardholderDtoDestination.getCard().getNumberCard(), r.getAmount(), user );
													
													if(levels.getCode() == 1){
														SHCLogDto shcLogSource = new SHCLogDto(21);
														SHCLogDto shcLogDestination = new SHCLogDto(21);
														authorization = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);

														shcLogSource.setNumberCard(cardDtoSource.getNumberCard());
														shcLogSource.setpCode(SystemConstants.PCODE_MONEX_TRANSFER_CC_TO_PAN_CARGO);
														shcLogSource.setAmount(r.getAmount());
														shcLogSource.setAvalBalance(newAvailableBalanceSource);
														shcLogSource.setAuthNum(authorization);
														shcLogSource.setTermId("111111");
														shcLogSource.setMsgType(210);
														shcLogSource.setFee(0);
														shcLogSource.setAcceptorName("TRANSFERENCIA ENTRE CUENTAS");
														shcLogSource.setAcctNum(accountDtoSource.getAccount());
														shcLogSource.setFiller1(user);
														shcLogSource.setFiller3("web_service");

														shcLogDao.create(shcLogSource);

														shcLogDestination.setNumberCard(cardDtoDestination.getNumberCard());
														shcLogDestination.setpCode(SystemConstants.PCODE_MONEX_TRANSFER_CC_TO_PAN_ABONO);
														shcLogDestination.setAmount(r.getAmount());
														shcLogDestination.setAvalBalance(newAvailableBalanceDest);
														shcLogDestination.setAuthNum(authorization);
														shcLogDestination.setTermId("111111");
														shcLogDestination.setMsgType(210);
														shcLogDestination.setFee(0);
														shcLogDestination.setAcceptorName("TRANSFERENCIA ENTRE CUENTAS");
														shcLogDestination.setAcctNum(accountDtoDest.getAccount());
														shcLogDestination.setFiller1(user);
														shcLogDestination.setFiller3("web_service");

														shcLogDao.create(shcLogDestination);

														/******************************************************
														 * Actualizar el cash_dep_used y la fecha en istdbacct y el
														 * saldo disponible de cuenta destino
														 ******************************************************/
														accountDtoDest.setCash_dep_used((-1) * levels.getMonthlyAmount());
														accountDtoDest.setCash_dep_date(GeneralUtil.getCurrentSqlDate());
														accountDtoDest.setAvailableBalance(newAvailableBalanceDest);
														accountDtoDest.setLedgerBalance(newAvailableBalanceDest);
														accountDao.update(accountDtoDest);

														/*******************************************************
														 * Actualizamos el saldo disponible de cuenta origen
														 *******************************************************/
														accountDtoSource.setAvailableBalance(newAvailableBalanceSource);
														accountDtoSource.setLedgerBalance(newAvailableBalanceSource);
														
														accountDao.update(accountDtoSource);

														codeResponse = 1;

														log.info("::Transferencia realizada con exito::");
													}else if(levels != null)
														codeResponse = levels.getCode();
													
												}else{
													codeResponse = 7; // Fondos Insuficientes en cuenta origen
												}
											}
										} else
											codeResponse = 6;// cuentas noasociadas
									} else
										codeResponse = 3;// no se encontro registro de cuenta destino
								} else
									codeResponse = 2;// no se encontro registro de cuenta origen
							} else
								codeResponse = 8;// cuentas no deben ser iguales
						} else
							codeResponse = 3;// cuenta destino no existe
					} else
						codeResponse = 2;// cuenta origen no existe
				} else
					codeResponse = 9;// no hay registro en tbl_emisores

				ob1 = new TransferSpeiResponseDto();
				ob1.setCode(codeResponse);
				ob1.setDescription(CatalogUtil.getCatalogTransfersSpei().get(codeResponse));
				
				if( codeResponse == 1 ){
					ob1.setDestinationAvailableBalance(newAvailableBalanceDest);
					ob1.setDestinationPreviousBalance(avalBalanceDest);
					
					ob1.setSourceAvailableBalance(newAvailableBalanceSource);
					ob1.setSourcePreviousBalance(avalBalanceSource);

				}else{
					ob1.setDestinationAvailableBalance(avalBalanceDest);
					ob1.setDestinationPreviousBalance(avalBalanceDest);
					
					ob1.setSourceAvailableBalance(avalBalanceSource);
					ob1.setSourcePreviousBalance(avalBalanceSource);
				}
				
				ob1.setAuthorization(authorization);
				response.add(ob1);

				/**************************************************
				 * Almacenando Transaccion en Bitacora del WS
				 **************************************************/
				log.debug("::Almacenando Movimiento en Bitacora::");
				wsUtil = new WSLogUtil(wsLogDao);
				wsUtil.saveLog(binSyc, panSource, SystemConstants.WS_USER_NAME,requestOp, codeResponse, response,SystemConstants.PCODE_BALANCE_QUERY_BY_ACCOUNT);
			}
		}

		return response;
	}

	private void getMapOfValidations() {
		validations = new TreeMap<Integer, Integer>();
		validations.put(SystemConstants.VALIDATE_EXISTS_ACCOUNT, 1);
		validations.put(SystemConstants.VALIDATE_ACCOUNT_ACTIVE, 1);

	}

}
