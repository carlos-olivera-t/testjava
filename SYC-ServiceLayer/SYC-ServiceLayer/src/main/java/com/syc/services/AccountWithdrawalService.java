package com.syc.services;

import java.util.List;

import com.syc.dto.request.AccountWithDrawalRequestDto;
import com.syc.dto.request.LoadBalanceAccountRequest;
import com.syc.dto.request.LoadBalanceRequest;
import com.syc.dto.response.AccountWithDrawalResponseDto;
import com.syc.dto.response.BalanceMovementAcctRespDto;
import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.dto.response.BalanceMovementRespSodexoDto;
import com.syc.dto.response.BasicAuthRespDto;

public interface AccountWithdrawalService {
	

	/**
	 * Realiza un retiro por numero de tarjeta
	 * @param pan
	 * @param amount
	 * @param user
	 * @return
	 */
	public BalanceMovementRespDto doWithdrawalAccount(String pan, double amount, String user);
	
	/**
	 * Realiza un retiro por numero de tarjeta
	 * @param pan
	 * @param amount
	 * @param description
	 * @param user
	 * @param reference
	 * @return
	 */
	public BalanceMovementRespDto doWithdrawalAccount(String pan, double amount, String description, String user, String reference);
	
	
	/**
	 * servicio que tiene como proposito realizar una transferencia de saldo entre tarjetas
	 * @param targetCard
	 * @param extractCard
	 * @param amount
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public BasicAuthRespDto transfersCard(String targetCard, String extractCard, double amount, String user) throws Exception;
	
	/**
	 * Servicio de Cargo de Saldo para Si Vale
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public AccountWithDrawalResponseDto AccountWithDrawal( AccountWithDrawalRequestDto request ) throws Exception;
	/**
	 * Servicio que realiza un retiro (MAQUILAS)
	 * @param pan
	 * @param amount
	 * @param activationCard
	 * @param pCode
	 * @param description
	 * @param user
	 * @param comments
	 * @return
	 * @throws Exception
	 */
	public BalanceMovementRespDto doDevAccount( String pan, double amount, int pCode, String description, String user, String comments )throws Exception;
	/**
	 * Servicio que realiza retiros a determinado numero de tarjetas
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public List<BalanceMovementRespSodexoDto> listwithDrawal( List<LoadBalanceRequest> request, String user, boolean isValidaBin) throws Exception;
	
	/**
	 * Servicio que tiene como proposito realizar retiros x numero de cuenta
	 * @param request lista de cuenta- monto q
	 * @param user
	 * @param clave_emisor ISS_CODE que se encuentra en tbl_empleados el cual es un identificador de cliente
	 * @return
	 * @throws Exception
	 */
	public List<BalanceMovementAcctRespDto> withdrawalAccount( List<LoadBalanceAccountRequest> request, String user, int clave_emisor ) throws Exception;
	
	/**
	 * Servicio que realiza un retiro por cuenta
	 * @param bin
	 * @param account
	 * @param amount
	 * @param pCode
	 * @param description
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public BalanceMovementRespDto doWithdrawalAccount( String bin, String account, double amount, int pCode, String description, String user)throws Exception;
	

}
