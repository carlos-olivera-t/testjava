package com.syc.services.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.request.ChequeDto;
import com.syc.dto.request.LoadBalanceAccountRequest;
import com.syc.dto.request.LoadBalanceRequest;
import com.syc.dto.response.BalanceMovementAcctRespDto;
import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.dto.response.BalanceMovementRespSodexoDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.LevelCaptationResponseDto;
import com.syc.persistence.dao.AccountDao;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dao.EmisorDao;
import com.syc.persistence.dao.EmployeeDao;
import com.syc.persistence.dao.SHCLogDao;
import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.AccountDto;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.CardholderDto;
import com.syc.persistence.dto.EmisorDto;
import com.syc.persistence.dto.EmployeeDto;
import com.syc.persistence.dto.SHCLogDto;
import com.syc.services.CardService;
import com.syc.services.CatchmentLevelsService;
import com.syc.services.DepositAccountService;
import com.syc.services.NotificationsService;
import com.syc.services.OnlineTransfersService;
import com.syc.services.UserService;
import com.syc.utils.CatalogUtil;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SystemConstants;
import com.syc.utils.ValidatorUtil;
import com.syc.utils.WSLogUtil;

@Service
public class DepositAccountServiceImpl implements DepositAccountService{
	
	private static final transient Log log = LogFactory.getLog(DepositAccountServiceImpl.class);
	
	@Autowired
	EmployeeDao   employeeDao;
	@Autowired
	CardholderDao cardHolderDao;
	@Autowired
	SHCLogDao     shcLogDao;
	@Autowired
	AccountDao    accountDao;
	@Autowired
	WSLogDao      wsLogDao;
	@Autowired
	CardService   cardServiceDao;
	@Autowired
	EmisorDao    emisorDao;
	@Autowired
	OnlineTransfersService   onlineTransfersService;
	@Autowired
	CatchmentLevelsService catchmentLevelsService;
	
	@Autowired
	NotificationsService notificationsService;
	

	@Autowired
	UserService userService;
	
	TreeMap<Integer, Integer> validations;
	TreeMap<Integer, Integer> validations1;
	WSLogUtil wsUtil;
	
	static final double LIMITE_GENERAL = 20000.00;
	static final double LIMITE_DIARIO = 10000.00;
		
	private double limiteDiario(String pan, Date currentDate){
		double limite = 0.0;
		String limited = null;
		Object limiteDiario = shcLogDao.getLimitsByCardNumber(pan, currentDate);
		
		 if(limiteDiario != null){
			 limited = String.valueOf(limiteDiario);
			 limite = Double.parseDouble(limited);
		 }else
			 limite = 0.0;
		 return limite;
	}
	
		public BalanceMovementRespDto doDepositLimits( String pan, double amount, boolean activationCard, String user )throws Exception{
			int codeResponse = 0;
			double limiteTransaccion = 0.0;
			String bin = pan.substring(0, 6); 
			String request = "pan["+pan+"],Amount["+amount+"], user["+user+"]";
			Date currentDate   = GeneralUtil.getCurrentSqlDate();
			CardholderDto limiteGeneral = cardHolderDao.getCardholderInfoByCardNumber(pan);
			if(limiteGeneral != null){
				limiteTransaccion = limiteGeneral.getAccount().getAvailableBalance() + amount;
				double limiteDiario = limiteDiario(pan, currentDate)+ amount;
				if( limiteTransaccion != 0.0 && limiteTransaccion <= LIMITE_GENERAL){
					if(limiteDiario != 0.0 && limiteDiario <= LIMITE_DIARIO){
						return doDepositAccount(pan, amount, activationCard, SystemConstants.PCODE_WS_RECARGA_SALDO, "Transferencia WS", user);
					}else
						codeResponse = 18;
				}else
					codeResponse = 17;
			}else
				codeResponse = 8;
			BalanceMovementRespDto response = new BalanceMovementRespDto();
			response.setAuthorization("0");
			response.setCode(codeResponse);
			response.setDescription(CatalogUtil.getCatalogDepositAccount().get(codeResponse));
			
			wsUtil = new WSLogUtil( wsLogDao );
			wsUtil.saveLog( bin, pan, user, request, codeResponse, response, String.valueOf(SystemConstants.PCODE_WS_RECARGA_SALDO));
			
			log.debug( "::Devolviendo Respuesta::"+response.toString());
			
			return response;
	}
	
	
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, String user )throws Exception{
		return doDepositAccount(pan, amount, activationCard, SystemConstants.PCODE_WS_RECARGA_SALDO, "Transferencia WS", user);
	}
	
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, String description, String user, String reference )throws Exception{
		int codeResponse = 15;
		if(description.length() < 41){
			if(reference.length() < 13 || reference == null){
				return doDepositAccount(pan, amount, activationCard, SystemConstants.PCODE_WS_RECARGA_SALDO, description, user, null, reference, false);
		
			}else{
				codeResponse = 16;
			}
			}else{
				codeResponse = 15;
			}
			BalanceMovementRespDto response = new BalanceMovementRespDto();
			response.setAuthorization("0");
			response.setCode(codeResponse);
			response.setDescription("el campo description es demasiado largo");
			
			return response;
		
	}
	
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, int pCode, String description, String user )throws Exception{
		return doDepositAccount(pan, amount, activationCard, pCode, description, user, null);
	}
	
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, int pCode, String description, boolean checkLevels, String user )throws Exception{
		return doDepositAccount(pan, amount, activationCard, pCode, description, user, null, null, checkLevels);
	}
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, int pCode, String description, String user, String comments )throws Exception{
		return doDepositAccount(pan, amount, activationCard, pCode, description, user, comments, null, false);
	}
		
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, int pCode, String description, String user, String comments, String reference, boolean checkLevels )throws Exception{
		
		String request = "pan["+pan+"],Amount["+amount+"]";
		String authorization  = "0";
		String bin            = "0";
		String bin1 = pan.substring(0, 6);
		double currentBalance =  0;
		double balance        =  0;
		String desc 	  = null;
		int codeResponse      =  0;
		BasicAuthRespDto activationRespDto = null;;
		
		log.debug("::Procesando una Recarga de Saldo::");
		log.debug( "::Request::" + request );
		getMapOfValidations();
		
		/** Obteniendo la informacion de la Cuenta  **/
		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber( pan );
		bin     = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getBin().trim() : "0";
		
		log.debug("::Validando Tarjeta::");
		int validatorResult = new ValidatorUtil().validateWebMethod(validations, cardholderDto );

		if (validatorResult > 0) {	
			log.debug("::Validaciones no superadas::");
			codeResponse = validatorResult;
		} else {
			log.debug("::Las validaciones fueron superadas correctamente::");
				
		if(amount > 0){
			if(description.length() < 41){
				
			
			/***********************************************************************
			 *  Activando la tarjeta siempre que el parametro activationCard==true,
			 *            y la tarjeta se encuentre Inactiva 
			 ***********************************************************************/
				
			CardDto cardDto = cardholderDto.getCard();
				
			if( activationCard && cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD  ){
				activationRespDto = cardServiceDao.activationCard(cardDto, user+"_atm");
			}
				
			if( cardDto.getStatus() == SystemConstants.STATUS_ACTIVE_CARD || (cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD && bin1.equals("474646"))){
					
				balance = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getAvailableBalance() : 0;
				
				if( comments != null && comments.length() > 25 ){
					comments = comments.substring(0,25);
				}
				
				LevelCaptationResponseDto levels = null;
				if(checkLevels){
					
					 levels = catchmentLevelsService.levelsCaptation(pan, amount, user);

					 if( levels != null ){
						 levels.setCode( levels.getCode() + 20  );
					 }

				}

				if((checkLevels && levels.getCode() == 21) || !checkLevels){
						/************************************************
						 *     Generando Transacción en Bitácora
						 ************************************************/
						AccountDto account = cardholderDto.getAccount();
						SHCLogDto shcLog   = new SHCLogDto(21);
						authorization      = activationRespDto!=null ? activationRespDto.getAuthorization() 
										: GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
					
										currentBalance = account.getAvailableBalance() + amount;
					
						shcLog.setNumberCard  ( pan );
						shcLog.setpCode       ( pCode );
						shcLog.setAmount      ( amount );
						shcLog.setAvalBalance ( currentBalance );
						shcLog.setAuthNum     ( authorization );
						shcLog.setTermId      ( "111111" );
						shcLog.setTermLoc     (comments);
						shcLog.setMsgType     ( 210 );
						shcLog.setFee         ( 0 );
						shcLog.setAcceptorName( description );
						shcLog.setAcctNum     ( account.getAccount() );
						shcLog.setFiller1     ( user );
						shcLog.setFiller3     ( "web_service" );
						shcLog.setRefNum      ( reference );

						shcLogDao.create( shcLog ); 
					
						/***************************************
						 * Actualizando el saldo en la Cuenta
						 ***************************************/
						account.setAvailableBalance(currentBalance);
						account.setLedgerBalance   (currentBalance);

						if(checkLevels && levels.getCode() == 21){
							account.setCash_dep_used((-1) * levels.getMonthlyAmount());
							account.setCash_dep_date(GeneralUtil.getCurrentSqlDate());
						}
						try{
							accountDao.sqlUpdate(account);

								codeResponse = 1;
	
							log.debug( "::Recarga Procesada Exitosamente::");
							
				  		}catch(Exception exp){
							codeResponse = 50;
							
							log.debug( "::Recarga no procesada, vuelva a intentar");
					
						}
				}else{
					if(levels != null ){
						codeResponse =  levels.getCode();
						desc = levels.getDescription();
					
					}
				}
			}else{
				codeResponse = 2;
			}
				
				
			}else{
				codeResponse = 15;//descripcion demasiado largo
			}
		}else{
			codeResponse = 6; //Monto erroneo
		}
			
		}
		
		
		if(codeResponse == 1 && bin1.equals("474646")){
			/** Enviando Petición al socket de notificaciones**/
	       notificationsService.sendRequestNotification("SYCNOTDEPOSIT"+pan+amount);
		}
		/*************************************
		 *     Generando Respuesta
		 *************************************/
		BalanceMovementRespDto response = new BalanceMovementRespDto();
		response.setCode(codeResponse);
		response.setAuthorization(authorization);
		response.setDescription( (desc != null) ? desc : CatalogUtil.getCatalogDepositAccount().get(codeResponse) );
		response.setBalance(Math.rint(balance*100)/100);
		response.setCurrentBalance(Math.rint(currentBalance*100)/100);
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug( "::Almacenando Movimiento en Bitácora::");
		
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( bin, pan, user, request, codeResponse, response, String.valueOf(pCode));
		
		log.debug( "::Devolviendo Respuesta::"+response.toString());

		return response;
	}
	
	
	
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, List<ChequeDto> cheques) throws Exception {
		String request = "pan["+pan+"],Amount["+amount+"]";
		
		boolean activationCard = false;
		int pCode = SystemConstants.PCODE_WS_RECARGA_SALDO;
		String description = "Recarga saldo Web 2.0";
		String comments = "Recarga saldo Web2.0";
//		String user = SecurityUtils.getCurrentUser();//aqui se queda y manda la excepcion
		String user = "cardman_user";
		
		String authorization  = "0";
		String bin            = "0";
		
		double currentBalance =  0;
		double balance        =  0;
	
		int codeResponse      =  0;
		
		BasicAuthRespDto activationRespDto = null;;
		
		log.debug("::Procesando una Recarga de Saldo::");
		log.debug( "::Request::" + request );
		
		getMapOfValidations();
		
		/** Obteniendo la informacion de la Cuenta  **/
		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber( pan );
		
		bin     = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getBin().trim() : "0";
		
		log.debug("::Validando Tarjeta::");
		int validatorResult = new ValidatorUtil().validateWebMethod(validations, cardholderDto );

		if (validatorResult > 0) {
				
			log.debug("::Validaciones no superadas::");
			codeResponse = validatorResult;
				
		} else {
			log.debug("::Las validaciones fueron superadas correctamente::");
				
			/***********************************************************************
			 *  Activando la tarjeta siempre que el parametro activationCard==true,
			 *            y la tarjeta se encuentre Inactiva 
			 ***********************************************************************/
				
			CardDto cardDto = cardholderDto.getCard();
				
			if( activationCard && cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD  ){
				activationRespDto = cardServiceDao.activationCard(cardDto, user+"_atm");
			}
				
			if( cardDto.getStatus() == SystemConstants.STATUS_ACTIVE_CARD ){
					
				balance = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getAvailableBalance() : 0;
				
				if( comments != null && comments.length() > 25 ){
					comments = comments.substring(0,25);
				}
				
				
				/************************************************
				 *     Generando Transacción en Bitácora
				 ************************************************/
				AccountDto account = cardholderDto.getAccount();
				SHCLogDto shcLog   = new SHCLogDto(21);
				authorization      = activationRespDto!=null 
										? activationRespDto.getAuthorization() 
										: GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
				if(amount > 0){	
				currentBalance = account.getAvailableBalance() + amount;
				shcLog.setAmount      ( amount );
				shcLog.setFiller1     ( amount + "" );	
				}	
				
													
				double c1=0, c2=0;
					if(cheques != null && cheques.size() > 0){
						//type 1 para salvo buen cobro y type 0 para cheques en firme
						for(int i = 0; i < cheques.size(); i++){							
							if(cheques.get(i).getType() == 1){
								shcLog.setFiller1     ( 0 + "" );
								 c1 = 0;
							}
							else if(cheques.get(i).getType() == 0){
								shcLog.setFiller1     ( 0 + "" );
								 c2 = cheques.get(i).getImporte()+c2;
							}
							
							if(i==0){
								shcLog.setFiller2     ( cheques.get(i).getImporte() + "," + cheques.get(i).getBanco() + "," + cheques.get(i).getReferencia());
							}
							else if(i==1){
								shcLog.setFiller3     ( cheques.get(i).getImporte() + "," + cheques.get(i).getBanco() + "," + cheques.get(i).getReferencia());
							}
							else if(i==2){
								shcLog.setFiller4     ( cheques.get(i).getImporte() + "," + cheques.get(i).getBanco() + "," + cheques.get(i).getReferencia());
							}
						}
						double monto = c1+c2;
						shcLog.setAmount   (monto);
						currentBalance = account.getAvailableBalance()+monto;
					}										
				
					shcLog.setNumberCard  ( pan );
					shcLog.setpCode       ( pCode );
					shcLog.setAvalBalance ( currentBalance );
					shcLog.setAuthNum     ( authorization );
					shcLog.setTermId      ( "111111" );
					shcLog.setTermLoc     (comments);
					shcLog.setMsgType     ( 210 );
					shcLog.setFee         ( 0 );
					shcLog.setAcceptorName( description );
					shcLog.setAcctNum     ( account.getAccount() );	
					
				
				shcLogDao.create( shcLog ); 
					
				/***************************************
				 * Actualizando el saldo en la Cuenta
				 ***************************************/
				account.setAvailableBalance(currentBalance);
				account.setLedgerBalance   (currentBalance);
				try{
					accountDao.sqlUpdate(account);

					
						codeResponse = 1;

					log.debug( "::Recarga Procesada Exitosamente::");
					
		  		}catch(Exception exp){
					codeResponse = 50;
					
					log.debug( "::Recarga no procesada, vuelva a intentar");
			
				}
				
			}else{
				codeResponse = 2;
			}
			
		}
		
		/*************************************
		 *     Generando Respuesta
		 *************************************/
		BalanceMovementRespDto response = new BalanceMovementRespDto();
		response.setCode(codeResponse);
		response.setAuthorization(authorization);
		response.setDescription( CatalogUtil.getCatalogDepositAccount().get(codeResponse) );
		response.setBalance(balance);
		response.setCurrentBalance(currentBalance);
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug( "::Almacenando Movimiento en Bitácora::");
		
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( bin, pan, user, request, codeResponse, response, SystemConstants.PCODE_LOG_DEPOSIT_CHEQUE);
		
		log.debug( "::Devolviendo Respuesta::"+response.toString());

		return response;
	}
	
	public List<BalanceMovementRespSodexoDto> loadBalance( List<LoadBalanceRequest> request, String user, boolean isValidBin) throws Exception{
		
		
		List<BalanceMovementRespSodexoDto> lista =  new ArrayList<BalanceMovementRespSodexoDto>();
		//String pan = null;
		
		if (request != null){
			
			for(int i = 0; i<=request.size() -1; i++){
				
				BalanceMovementRespSodexoDto registro = new BalanceMovementRespSodexoDto();
				//pan = request.get(i).getPan();
				//System.out.println(pan);
				boolean isPermitido = true;
				
				if(isValidBin)
				
				 isPermitido = userService.getBinesPermitidos(user, request.get(i).getPan().substring(0, 8));
				
				if(isPermitido){
					BalanceMovementRespDto response = doDepositAccount( request.get(i).getPan(), request.get(i).getAmount(), true, user );
					 registro.setCode(response.getCode());
					 registro.setAuthorization(response.getAuthorization());
					 registro.setDescription(response.getDescription());
					 registro.setPan(request.get(i).getPan());
					 registro.setBalance(response.getBalance());
					 registro.setCurrentBalance(response.getCurrentBalance());
				}else{
					 registro.setCode(30);
					 registro.setAuthorization("0");
					 registro.setDescription("operacion no permitida sobre el BIN [ " + request.get(i).getPan()+ " ] ");
					 registro.setPan(request.get(i).getPan());
					 registro.setBalance(0);
					 registro.setCurrentBalance(0);
				
					 
				}
				
				lista.add(registro);
			}		 
		
			//System.out.println(lista.toString());

		}
		
		return lista;
		

	}
	
	
	public List<BalanceMovementAcctRespDto> depositAccount( List<LoadBalanceAccountRequest> request, String user, int clave_emisor, boolean isActivation ) throws Exception{
		
		List<BalanceMovementAcctRespDto> lista 		 =  new ArrayList<BalanceMovementAcctRespDto>();
		EmployeeDto employeeDto      				 = null;

		if (request != null){
			
			for(int i = 0; i<=request.size() -1; i++){
				
				BalanceMovementAcctRespDto registro = new BalanceMovementAcctRespDto();
				
				//aquí va el código para traer la tarjeta a partir de la cuenta
				
				employeeDto      = employeeDao.findEmployeeByAccount( request.get(i).getAccount(), clave_emisor );

				if(employeeDto != null){
					BalanceMovementRespDto response = null;
					try{
						response = doDepositAccount( employeeDto.getPan(), request.get(i).getAmount(), isActivation,SystemConstants.PCODE_WS_RECARGA_SALDO_ACCOUNT, "Transferencia WS", user );
						
						registro.setCode(response.getCode());
						registro.setAuthorization(response.getAuthorization());
						registro.setDescription(response.getDescription());
						registro.setAccount(request.get(i).getAccount());
						registro.setBalance(response.getBalance());
						registro.setCurrentBalance(response.getCurrentBalance());
					}catch(Exception e){
						registro.setCode(50);
						registro.setAuthorization("0");
						registro.setDescription("error al procesar la solicitud, intentelo mas tarde");
						registro.setAccount(request.get(i).getAccount());
						registro.setBalance(0.0);
						registro.setCurrentBalance(0);
					}
					
				}else{
					registro.setCode(10);
					registro.setAuthorization("0");
					registro.setDescription("cuenta no registrada");
					registro.setAccount(request.get(i).getAccount());
					registro.setBalance(0.0);
					registro.setCurrentBalance(0.0);
				} 
			
				 lista.add(registro);
			}		 
		}
		
		return lista;
		
	}	

	private void getMapOfValidations() {
		validations = new TreeMap<Integer, Integer>();
		validations.put(SystemConstants.VALIDATE_EXISTS_ACCOUNT, 1);
		validations.put(SystemConstants.VALIDATE_EXPIRATION_DATE, 1);
	}

	private void getMapOfValidationsAccount() {
		validations = new TreeMap<Integer, Integer>();
		validations.put(SystemConstants.VALIDATE_EXISTS_ACCOUNT, 1);
		validations.put(SystemConstants.VALIDATE_ACCOUNT_ACTIVE, 1);

	}
	@Override
	public BalanceMovementRespDto doDepositAccount(String bin, String account, double amount, int pCode, String description, String user, LevelCaptationResponseDto levelsCaption)
		{
			int codeResponse = 0;
			BalanceMovementRespDto response = new BalanceMovementRespDto();
//			try{
				
				String request = "bin["+bin+"],Account["+account+"Amount["+amount+"]";
				/**aqui checar las claves en los pcodes para saber si se hace cargo o abono**/
				
				String authorization  = "0";
				double currentBalance =  0;
				double balance        =  0;
				String pan = null;
				
				BasicAuthRespDto activationRespDto = null;;
				
				log.debug("::Procesando una Recarga de Saldo::");
				log.debug( "::Request::" + request );
				
				getMapOfValidationsAccount();
				bin     = GeneralUtil.completaCadena(bin, '0', 10, "L");
				EmisorDto emisorDto = emisorDao.findByBin(bin);
				if(emisorDto != null){
					String buildAccount = GeneralUtil.buildPrimaryAccount(account, emisorDto.getCuenta());
				/** Obteniendo la informacion de la Cuenta  **/
				CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByAccount( bin, buildAccount );
				
				if(cardholderDto != null){
					
					log.debug("::Validando Tarjeta::");
					int validatorResult = new ValidatorUtil().validateWebMethod(validations, cardholderDto );
		
					if (validatorResult > 0) {
						
						log.debug("::Validaciones no superadas::");
						codeResponse = validatorResult;
						
					} else {
						log.debug("::Las validaciones fueron superadas correctamente::");
							if(amount > 0){
								if(description.length() < 41){
									AccountDto accountDto = cardholderDto.getAccount();
									if( accountDto.getStatus().equals(SystemConstants.STATUS_ACTIVE_ACCOUNT)){
							
										balance = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getAvailableBalance() : 0;
						
										/************************************************
										 *     Generando Transacción en Bitácora
										 ************************************************/
						
										SHCLogDto shcLog   = new SHCLogDto(21);
										authorization      = activationRespDto!=null 
												? activationRespDto.getAuthorization() 
												: GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
							
												currentBalance = accountDto.getAvailableBalance() + amount;
												pan = cardholderDto.getCard().getNumberCard();
												shcLog.setNumberCard  ( cardholderDto.getCard().getNumberCard() );
												shcLog.setpCode       ( pCode );
												shcLog.setAmount      ( amount );
												shcLog.setAvalBalance ( currentBalance );
												shcLog.setAuthNum     ( authorization );
												shcLog.setTermId      ( "111111" );
												shcLog.setMsgType     ( 210 );
												shcLog.setFee         ( 0 );
												shcLog.setAcceptorName( description );
												shcLog.setAcctNum     ( accountDto.getAccount() );
												shcLog.setFiller1     ( user );
												shcLog.setFiller3     ( "web_service" );
		
												shcLogDao.create( shcLog ); 
							
												/***************************************
												 * Actualizando el saldo en la Cuenta
												 ***************************************/
												if( levelsCaption != null && levelsCaption.getCode() == 1 ){
													accountDto.setCash_dep_used( (-1) * levelsCaption.getMonthlyAmount() );
												}
												
												accountDto.setCash_dep_date(GeneralUtil.getCurrentSqlDate());
												accountDto.setAvailableBalance(currentBalance);
												accountDto.setLedgerBalance   (currentBalance);
												try{
													accountDao.sqlUpdate(accountDto);
				
													
														codeResponse = 1;
							
													log.debug( "::Recarga Procesada Exitosamente::");
													
										  		}catch(Exception exp){
													codeResponse = 50;
													
													log.debug( "::Recarga no procesada, vuelva a intentar");
											
												}
												
									}else{
										codeResponse = 19;//cuenta no esta activa
									}	
								}else{
									codeResponse = 15;//descripcion demasiado largo
								}
							}else
								codeResponse = 6; //Monto erroneo
					}
				}else
					codeResponse = 3;//cuenta no encontrada
				}else{
					codeResponse = 22;//prefijo no encontrado
				}
				
				response.setAuthorization(authorization);
				response.setBalance(balance);
				response.setCurrentBalance(currentBalance);
				response.setDescription(CatalogUtil.getCatalogDepositAccount().get(codeResponse));
				response.setCode(codeResponse);
				
				
				wsUtil = new WSLogUtil( wsLogDao );
				wsUtil.saveLog( bin, pan, user, request, codeResponse, response, SystemConstants.PCODE_LOG_DEPOSIT_ACCOUNT);
//			}catch(StaleObjectStateException tw){
//				codeResponse = 50;
//				
//				log.debug( "::Recarga no procesada, vuelva a intentar");
//			
//			}
		return response;
	}

}
