package com.syc.services.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.response.BasicDateResponseDto;
import com.syc.dto.response.BasicResponseDto;
import com.syc.exceptions.OasisException;
import com.syc.persistence.dao.AddressDao;
import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.AddressDto;
import com.syc.persistence.dto.CardDto;
import com.syc.services.NIPService;
import com.syc.services.NotificationsService;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SecurityUtils;
import com.syc.utils.SystemConstants;
import com.syc.utils.WSLogUtil;

@Service
public class NIPServiceImpl implements NIPService{

	private static final transient Log log = LogFactory.getLog(NIPServiceImpl.class);
	private static final String OASIS_OFFLINE = "-1";
	
	private static final String SERVICE_PIN_VALIDATOR  = "SYCNIPV";
	private static final String SERVICE_PIN_ASSIGNMENT = "SYCNIPN";
	private static final String SERVICE_CVV            = "SYCCVVC";
	
	
	@Autowired
	WSLogDao      wsLogDao;
	
	@Autowired
	AddressDao datos;
	
	@Autowired
	NotificationsService notificationsService;
	
	@Autowired
	private CamelContext context;
	private ProducerTemplate template;
	
	WSLogUtil wsUtil;
	
	@Autowired
	CardDao cardDao;
	
	public BasicResponseDto pinValidator(String pan, String pin) {
		int codeResponse =0;
		CardDto cardDto = cardDao.findByNumberCard(pan);
		List<String> bines = binsNIPInactive();
		/**validando que la tarjeta este activa***/
		if (cardDto != null) {
			if (cardDto.getStatus() == SystemConstants.STATUS_ACTIVE_CARD || ((bines.contains(pan.substring(0, 6)) && cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD))) {

				codeResponse = processRequest(SERVICE_PIN_VALIDATOR, pan, pin);
		
			}else
				codeResponse = 6;
		}else
		
		codeResponse = 4;
		/*****************************
		 *   Generando Respuesta
		 *****************************/
		BasicResponseDto response = new BasicResponseDto();
		response.setCodigo(codeResponse);
		response.setDescripcion( pinValidatorCatalog().get(codeResponse) );
		

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug( "::Almacenando Movimiento en Bitácora::");
		
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( "", pan, "", "Pan["+pan+"]", codeResponse, response, SystemConstants.PCODE_LOG_PIN_VALIDATOR);
		
		log.debug( "::Devolviendo Respuesta::"+response.toString());
		
		return response;
	}
	
	public BasicResponseDto pinAssignment(String pan, String pin, String user) {
		int codeResponse = 0;
		CardDto cardDto = cardDao.findByNumberCard(pan);
		List<String> bines = binsNIPInactive();
		/**validando que la tarjeta este activa***/
		if (cardDto != null) {
			if (cardDto.getStatus() == SystemConstants.STATUS_ACTIVE_CARD || ((bines.contains(pan.substring(0, 6)) && cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD))) {
	
					codeResponse = processRequest(SERVICE_PIN_ASSIGNMENT, pan, pin);
					
					String bin = pan.substring(0,6);
					//System.out.println(bin+"\n\n\n");
					if(codeResponse == 1){
						if(bin.equals("460068") || bin.equals("474646")){
							AddressDto addressDto = datos.findCardAddress(pan);
							if(addressDto != null){
								// verificando que el campo mail contenga un correo
								if (addressDto.getEmail() != null && addressDto.getEmail().contains("@")) {
									try{
										String cadena = "SYCCAMBIONIP "+pan;
										notificationsService.sendRequestNotification(cadena);
									}catch(Exception e){
										log.warn("excepcion al enviar notificacion");
									}
				
								} else
									log.info("no se pudo enviar el mail");
						
							}else
								log.info("no se encontraron demograficos de la tarjeta");
						}else
							log.info("bin invalido");
					}else
						log.info("codigo de respuesta invalido");
					
				}else{
					codeResponse = 6;
				}
		}else{
			codeResponse = 4;
		}

		/*****************************
		 *   Generando Respuesta
		 *****************************/
		BasicResponseDto response = new BasicResponseDto();
		response.setCodigo(codeResponse);
		response.setDescripcion( pinAssignment().get(codeResponse) );
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug( "::Almacenando Movimiento en Bitácora::");
		
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( "", pan, user, "Pan["+pan+"]", codeResponse, response, SystemConstants.PCODE_LOG_PIN_ASSIGNMENT);
		
		log.debug( "::Devolviendo Respuesta::"+response.toString());
		
		return response;
		 
	}
	

	public BasicResponseDto pinAssignment(String pan, String pin) {
		
		return pinAssignment(pan, pin, null);
	}

	private List<String> binsNIPInactive(){
		List<String> levels = new ArrayList<String>();
		levels.add("538984");
		
		return levels;
	}
	public BasicResponseDto pinChange(String pan, String pin, String newPin, String user) {
		
		int codeResponse      = 0;
		int codePINValidator  = 0;
		int codePINAssignment = 0;
		
		/***********************
		 * Validando el NIP
		 ***********************/
		List<String> bines = binsNIPInactive();
		CardDto cardDto = cardDao.findByNumberCard(pan);
		/**validando que la tarjeta este activa***/
		if (cardDto != null) {
			if (cardDto.getStatus() == SystemConstants.STATUS_ACTIVE_CARD || ((bines.contains(pan.substring(0, 6)) && cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD))) {
		
					codePINValidator = processRequest(SERVICE_PIN_VALIDATOR, pan, pin);
		
					if( codePINValidator == 1 ){
						/*********************************
						 * Solicitando un Cambio de NIP
						 *********************************/
						codePINAssignment = processRequest(SERVICE_PIN_ASSIGNMENT, pan, newPin);
			
						if( codePINAssignment == 1 ){
						
								codeResponse = 1;//Cambio de NIP realizado con éxito;
						}else if( codePINAssignment == 2 ){
						
							codeResponse = 4; //Cambio de NIP Rechazado;
						}else
						
							codeResponse = codePINAssignment; //Sin conexion con autorizador
					
						}
			
			}else
				codeResponse = 6;
		}else
			codeResponse= 7;
		
		/*****************************
		 *   Generando Respuesta
		 *****************************/
		BasicResponseDto response = new BasicResponseDto();
		response.setCodigo(codeResponse);
		response.setDescripcion( pinChange().get(codeResponse) );
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug( "::Almacenando Movimiento en Bitácora::");
		
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( "", pan, "", "Pan["+pan+"]", codeResponse, response, SystemConstants.PCODE_LOG_PIN_CHANGE);
		
		log.debug( "::Devolviendo Respuesta::"+response.toString());

		
		return response;
	}
	
	public BasicResponseDto pinChange(String pan, String pin, String newPin){
		
		return  pinChange(pan,  pin, newPin, SecurityUtils.getCurrentUser());
	}
	
	@Override
	public BasicDateResponseDto changeNIP(String pan, String oldNip, String newNip, String user) {

		 String currentDate = GeneralUtil.formatDate( "dd-MM-yyyy" , GeneralUtil.getCurrentSqlDate() );
		 String currentTime = GeneralUtil.getCurrentTime("HHmmss");
		  BasicResponseDto resp = pinChange(pan,  oldNip, newNip, SecurityUtils.getCurrentUser());
		  
		  BasicDateResponseDto response = new BasicDateResponseDto();
		  
		  response.setCodigo(resp.getCodigo());
		  response.setDescripcion(resp.getDescripcion());
		  response.setDate(currentDate);
		  response.setTime(currentTime); 
		  
		return response;
	}
	
	public String getCVV( String pan, Date expiryDate, String serviceCode ) throws OasisException{
		String response = null;
		
					pan = pan.trim();
		
					StringBuffer buffer = new StringBuffer();
					buffer.append( SERVICE_CVV );
					buffer.append( pan.trim() );
					buffer.append( GeneralUtil.formatDate("yyMM", expiryDate) );
					buffer.append( serviceCode );
					
					int codeResponse =0;
					CardDto cardDto = cardDao.findByNumberCard(pan);
					/**validando que la tarjeta este activa***/
					if (cardDto != null) {
						if (cardDto.getStatus() == SystemConstants.STATUS_ACTIVE_CARD) {
		
								response = sendRequestOASIS( buffer.toString() );
						
										
					if( response != null && response.length() >= 26){
			
						String header       = response.substring(0,5);
						String numberCard   = response.substring(5, 21);
					    codeResponse = Integer.parseInt(response.substring(21, 23));
						String racalCVV     = response.substring(23, 26);
			
						log.debug("Header      : " +  header);
						log.debug("numberCard  : " +  numberCard);
						log.debug("codeResponse: " +  codeResponse);
						log.debug("racalCVV    : " +  racalCVV);
			
						if( (header.equals("CVVCR") && numberCard.equals(pan) && codeResponse == 0 ) ){
				
									return racalCVV;
						}else{
							throw new OasisException("Error en la respuesta de OASIS::pan["+pan+"],RETURN_PAN["+numberCard+"],codeResponse["+codeResponse+"]");
						}
					}else{
						throw new OasisException("Error al Obtener el CVV::pan["+pan+"],expiryDate["+expiryDate+"],serviceCode["+serviceCode+"]");
					}
					
						}else
							codeResponse = 6;
						throw new OasisException("Error, la tarjeta debe estar activada::pan["+pan+"],codeResponse["+codeResponse+"]");

					}else{
						
						codeResponse = 4;//tarjeta no registrada
						
						throw new OasisException("Error, tarjeta no encontrada::pan["+pan+"],codeResponse["+codeResponse+"]");
					}

	}
	
	private int processRequest( String service, String pan, String pin ){
		
		int codeResponse = 0;
		
		String oasisResponse = null;

		StringBuffer buffer = new StringBuffer();
		buffer.append(service);
		buffer.append(pan);
		buffer.append(pin);
		
		/********************************
		 * Enviando Solicitud al OASIS
		 ********************************/
		List<String> bines = binsNIPInactive();
		CardDto cardDto = cardDao.findByNumberCard(pan);
		/**validando que la tarjeta este activa***/
		if (cardDto != null) {
			if (cardDto.getStatus() == SystemConstants.STATUS_ACTIVE_CARD || ((bines.contains(pan.substring(0, 6)) && cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD))) {
		oasisResponse = sendRequestOASIS(buffer.toString());
			}else
				codeResponse = 6;
		}
		
		/**************************
		 * Procesando Respuesta
		 **************************/
		if (!GeneralUtil.isEmtyString(oasisResponse)) {
	
			if (!GeneralUtil.isEqualsString(oasisResponse, OASIS_OFFLINE)) {
				
				if (oasisResponse.contains( pan.length()>16?pan.substring(0,16):pan )) {
					int lenght = oasisResponse.length();
					log.debug( "OASIS codeResponse[ " + oasisResponse.substring(lenght - 2, lenght) + " ]" );
					int code = Integer.parseInt(oasisResponse.substring(lenght - 2, lenght));

					if (code == 0){
						codeResponse = 1; // Valido
					}else if( code == 2 ){
						codeResponse = 4; // Tarjeta No encontrada
					}else
						codeResponse = 2; // Invalido
				}

			} else {
				codeResponse = 3; // Sin conexion con OASIS
			}
		} 
		
		return codeResponse;

	}
	
	/******************************************
	 * Método Privado que envía una solicitud a OASIS
	 ******************************************/
	private String sendRequestOASIS(String request) {

		String response = null;

		try {
			template = context.createProducerTemplate();
			
			log.debug("SocketRequest[ " + request + " ]");
			
			/*********************************
			 * Enviando Solicitud Asincrona
			 *********************************/
//			Future<String> future = template.asyncRequestBody("seda:oasis",	request, String.class);
//			response = future.get();
			
			response = template.requestBody("seda:oasis",    request, String.class);
			
			log.debug("SocketResponse[ " + response + " ]");
			
		}catch(Exception ex){
			log.debug("Error en el envío del Socket[ "+ex.getMessage() +" ]");
			return OASIS_OFFLINE;
		}
		return response;
	}

	
	/*******************************************
	 * Devuelve el catálogo del servicio del 
	 *        validador de NIP
	 *******************************************/
	
	private static TreeMap<Integer,String> pinValidatorCatalog(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(0), "Respuesta Autorizador Inválida.");
		cat.put(new Integer(1), "NIP Válido");
		cat.put(new Integer(2), "NIP Inválido");
		cat.put(new Integer(3), "Sin conexión con el Autorizador.");
		cat.put(new Integer(4), "Tarjeta no encontrada.");
		cat.put(new Integer(6), "La tarjeta debe estar activada.");
		return cat;
	}
	
	/*******************************************
	 *   Devuelve el catálogo del servicio 
	 *        asignación de NIP
	 *******************************************/
	
	private static TreeMap<Integer,String> pinAssignment(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(0), "Cadena de respuesta Inválida.");
		cat.put(new Integer(1), "Asignación realizada con éxito.");
		cat.put(new Integer(2), "Asignación Rechazada por Autorizador.");
		cat.put(new Integer(3), "Sin conexión con Autorizador.");
		cat.put(new Integer(4), "Tarjeta no encontrada.");
		cat.put(new Integer(6), "La tarjeta debe estar activada.");
		return cat;
	}
	

	/*******************************************
	 *   Devuelve el catálogo del servicio 
	 *        cambio de NIP
	 *******************************************/
	
	private static TreeMap<Integer,String> pinChange(){
		TreeMap<Integer,String> cat = new TreeMap<Integer,String>();
		cat.put(new Integer(0), "Respuesta OASIS Inválida.");
		cat.put(new Integer(1), "NIP Actualizado.");
		cat.put(new Integer(2), "NIP Inválido");
		cat.put(new Integer(3), "Sin conexión con OASIS.");
		cat.put(new Integer(4), "Actualización Rechazada.");
		cat.put(new Integer(6), "La tarjeta debe estar activa.");
		cat.put(new Integer(7), "Tarjeta no encontrada");
		return cat;
	}

	
	
}
