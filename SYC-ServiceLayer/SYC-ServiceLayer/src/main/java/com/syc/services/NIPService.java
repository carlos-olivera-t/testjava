package com.syc.services;

import java.sql.Date;

import com.syc.dto.response.BasicDateResponseDto;
import com.syc.dto.response.BasicResponseDto;
import com.syc.exceptions.OasisException;

public interface NIPService {
	
	/**
	 * Servicio que invoca al cardser y valida el pin
	 * @param pan
	 * @param pin
	 * @return
	 */
	public BasicResponseDto pinValidator(String pan, String pin) ;
	/**servicio que cambia un NIP
	 * @pan no. de tarjeta
	 * @pin NIP
	 * @newPin nuevo NIP
	 * @User usuario que realizo la operacion
	 * */
	public BasicResponseDto pinChange( String pan, String pin, String newPin, String user );
	/**
	 * Servicio que realiza el cambio de nip
	 * @param pan
	 * @param oldNip
	 * @param newNip
	 * @param user
	 * @return
	 */
	public BasicDateResponseDto changeNIP(String pan, String oldNip, String newNip, String user);
	/**
	 * Servicio que realiza el cambio de nip
	 * @param pan
	 * @param pin
	 * @param newPin
	 * @return
	 */
	public BasicResponseDto pinChange( String pan, String pin, String newPin );
	/**servicio que asigna un NIP
	 * @pan no. de tarjeta
	 * @pin NIP
	 * @User usuario que realizo la operacion
	 * */
	public BasicResponseDto pinAssignment( String pan, String pin, String user);
	/**servicio que asigna un NIP
	 * @pan no. de tarjeta
	 * @pin NIP
	 * */
	public BasicResponseDto pinAssignment( String pan, String pin);
	/**
	 * Servicio que devuelve el CVV de una Tarjeta
	 * @param pan
	 * @param expiryDate
	 * @param serviceCode
	 * @return
	 * @throws OasisException
	 */
	public String getCVV( String pan, Date expiryDate, String serviceCode )throws OasisException;
	
	
}
