package com.syc.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.response.BasicResponseDto;
import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dao.PhoneCodeDao;
import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.PhoneCodeDto;
import com.syc.services.PhoneCodeService;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SecurityUtils;
import com.syc.utils.SystemConstants;
import com.syc.utils.WSLogUtil;

@Service("phoneCodeService")
public class PhoneCodeServiceImpl implements PhoneCodeService {
	
	@Autowired
	PhoneCodeDao phoneCodeDao;
	@Autowired
    private WSLogDao    wsLogDao;
    private WSLogUtil    wsUtil;
	@Autowired
	CardDao cardDao;
	
	private static final String PAGATODO_PREFIX = "061";
	private static final String ESTATUS_ACTIVO  = "A";
	private static final int    BLOQUEO         = 1;
	private static final int    DESBLOQUEO      = 2;

	public BasicResponseDto createPhoneCode(String pan, String account, String nip) {
		Respuesta respuestaServicio = Respuesta.NIP_CREADO;
        String request = "pan["+pan+"]account["+account+"]nip["+nip+"]"+"]";
		Respuesta validaciones = doValidaInput(pan, account, nip);
		String baseNip = null;    		
		crear: {
			if(validaciones != Respuesta.VALIDACIONES_OK){
				respuestaServicio = validaciones;
				break crear;
			}
        
			baseNip = getBase(pan, account);
            
            if(baseNip == null){
                respuestaServicio = Respuesta.SIN_DATOS;
                break crear;
            }
			
//			if(phoneCodeDao.read(accountNip.trim()) != null){
//				respuestaServicio = Respuesta.CREADO;
//				break crear;
//			}
						
			PhoneCodeDto phoneCodeDto = new PhoneCodeDto(baseNip.trim(), nip, SecurityUtils.getCurrentUser());
			phoneCodeDao.createOrUpdate(phoneCodeDto);			
		}
		
		BasicResponseDto response = new BasicResponseDto();
		response.setCodigo       ( respuestaServicio.getCode() );		
		response.setDescripcion  ( respuestaServicio.getDescription()   );
		
		wsUtil = new WSLogUtil( wsLogDao );
        wsUtil.saveLog(baseNip, "", SecurityUtils.getCurrentUser(), request, respuestaServicio.getCode(), response, SystemConstants.PCODE_ASIGNMENT_PHONE_CODE);

	
		return response;
	}
	
	public BasicResponseDto validatePhoneCode(String pan, String account, String nip, boolean nipFlag) {
		Respuesta respuestaServicio = Respuesta.NIP_VALIDO;
		String request = "pan["+pan+"]account["+account+"]nip["+nip+"]"+"]";
		Respuesta validaciones = doValidaInput(pan, account, nip);
		String baseNip = null;
		validar: {

			if(validaciones != Respuesta.VALIDACIONES_OK){
				respuestaServicio = Respuesta.NIP_NO_VALIDO;
				break validar;
			}
			baseNip = getBase(pan, account);
            
            if(baseNip == null){
                respuestaServicio = Respuesta.NIP_NO_VALIDO;
                break validar;
            }
			
			PhoneCodeDto oPhoneCodeDto = phoneCodeDao.read(baseNip.trim());
			
			if(oPhoneCodeDto == null){
				respuestaServicio = Respuesta.NIP_NO_VALIDO;
				break validar;
			}
			
			if(!nipFlag){
				if(!oPhoneCodeDto.getNip().equals(nip)){
					respuestaServicio = Respuesta.NIP_NO_VALIDO;
					break validar;
				}
			}
			
			if(!oPhoneCodeDto.getStatus().equals(ESTATUS_ACTIVO)){
				respuestaServicio = Respuesta.NIP_INACTIVO;
				break validar;
			}
			
		}
		
		BasicResponseDto response = new BasicResponseDto();
		response.setCodigo       ( respuestaServicio.getCode() );		
		response.setDescripcion  ( respuestaServicio.getDescription()   );
	
		wsUtil = new WSLogUtil( wsLogDao );
        wsUtil.saveLog(baseNip, "", SecurityUtils.getCurrentUser(), request, respuestaServicio.getCode(), response, SystemConstants.PCODE_VALIDATE_PHONE_CODE);
        

		
		return response;
	}

	public BasicResponseDto changePhoneCode(String pan, String account,String oldNip, String newNip, boolean nipFlag) {
		Respuesta respuestaServicio = Respuesta.NIP_CAMBIO_OK;
        String request = "pan["+pan+"]account["+account+"]oldNip["+oldNip+"]newNip["+newNip+"]";
		BasicResponseDto response = validatePhoneCode(pan, account, oldNip, nipFlag);
		String baseNip = null;
		cambiar:{
			
			if(response.getCodigo() != 0){
				respuestaServicio = Respuesta.NIP_NO_VALIDO_CAMBIO;
				break cambiar;
			}
			
			if( !(doValidaNuevoNip(newNip) == Respuesta.VALIDACIONES_OK) ){
				respuestaServicio = Respuesta.NIP_NUEVO_INVALIDO;
				break cambiar;
			}
			baseNip = getBase(pan, account);
			
			PhoneCodeDto oPhoneCodeDto = phoneCodeDao.read(baseNip.trim());
			
			String nipsOld[] = new String[4];
			nipsOld[0] = oPhoneCodeDto.getNip();
			nipsOld[1] = oPhoneCodeDto.getOldNip1();
			nipsOld[2] = oPhoneCodeDto.getOldNip2();
			nipsOld[3] = oPhoneCodeDto.getOldNip3();
			
			
			/** TODO: Validar vs Nips anteriores **/
			boolean nip = true;
			
			error:
			for(String arr : nipsOld){
				if(arr == null ){
					nip =true;
					break error;
				}else if(arr.equals(newNip)){
					nip = false;
					break error;
				}
			}
			if(nip){
				oPhoneCodeDto.setNip(newNip);
				oPhoneCodeDto.setOldNip1(  nipsOld[0]  );
				oPhoneCodeDto.setOldNip2(  nipsOld[1]  );
				oPhoneCodeDto.setOldNip3(  nipsOld[2]  );
				oPhoneCodeDto.setOldNip4(  nipsOld[3]  );
				
				phoneCodeDao.createOrUpdate(oPhoneCodeDto);	
			}else{
				
				respuestaServicio = Respuesta.NIP_REPETIDO;
			}
				
		}
		
		
		response = new BasicResponseDto();
		response.setCodigo       ( respuestaServicio.getCode() );		
		response.setDescripcion  ( respuestaServicio.getDescription()   );
	
		wsUtil = new WSLogUtil( wsLogDao );
        wsUtil.saveLog(baseNip, "", SecurityUtils.getCurrentUser(), request, respuestaServicio.getCode(), response, SystemConstants.PCODE_CHANGE_PHONE_CODE);
        
		return response;		
	}

	public BasicResponseDto changeStatus(String pan, String account, int operation) {		
		Respuesta respuestaServicio = Respuesta.ESTAUS_NO_MODIFICADO;
        String request = "pan["+pan+"]account["+account+"]operation["+operation+"]";
		Respuesta validaciones = doValidaInput(pan, account, "11");
		String baseNip = null;
		cambiarStatus:{			
			if(validaciones != Respuesta.VALIDACIONES_OK){
				respuestaServicio = validaciones;
				break cambiarStatus;
			}
			
			baseNip = getBase(pan, account);
            
            if(baseNip == null){
                respuestaServicio = Respuesta.SIN_DATOS_ESTATUS;
                break cambiarStatus;
            }
            
			PhoneCodeDto oPhoneCodeDto = phoneCodeDao.read(baseNip.trim());
			
			if(oPhoneCodeDto == null){
				respuestaServicio = Respuesta.SIN_DATOS_ESTATUS;
				break cambiarStatus;
			}			
			
			if(operation == BLOQUEO){
				oPhoneCodeDto.setStatus("C");
				phoneCodeDao.createOrUpdate(oPhoneCodeDto);
				respuestaServicio = Respuesta.NIP_BLOQUEADO;
			}else if (operation == DESBLOQUEO) {
				oPhoneCodeDto.setStatus("A");
				phoneCodeDao.createOrUpdate(oPhoneCodeDto);
				respuestaServicio = Respuesta.NIP_DESBLOQUEADO;
			}			
		}
	
		BasicResponseDto response = new BasicResponseDto();
		response.setCodigo       ( respuestaServicio.getCode() );		
		response.setDescripcion  ( respuestaServicio.getDescription()   );
	
		wsUtil = new WSLogUtil( wsLogDao );
        wsUtil.saveLog(baseNip, "", SecurityUtils.getCurrentUser(), request, respuestaServicio.getCode(), response, SystemConstants.PCODE_CHANGE_STATUS_PHONE_CODE);
    

		return response;				
	}		
	
	private Respuesta doValidaInput(String pan, String account, String nip) {
		Respuesta response = Respuesta.VALIDACIONES_OK;

		valida: {
			if (pan == null && account == null) {
				response = Respuesta.DATOS_INVALIDOS;
				break valida;
			}
			if(nip == null){
				response = Respuesta.NIP_INVALIDO;
				break valida;
			}			
		}

		return response;
	}

	private Respuesta doValidaNuevoNip(String nuevoNip) {
		Respuesta response = Respuesta.VALIDACIONES_OK;

		if(nuevoNip == null){
			response = Respuesta.NIP_INVALIDO;
		}
		
		return response;
	}
	
	private String getBase(String pan, String account){
        String base      = null;
        CardDto oCardDto = null;
        
        if(account != null && account.length() == 11){
            String cuentaFormateda = GeneralUtil.buildPrimaryAccount(account, PAGATODO_PREFIX);
            List<CardDto> listaTarjetas = cardDao.findByAccount(cuentaFormateda);
        
            if(listaTarjetas.size() > 0){
                oCardDto = listaTarjetas.get(0);
            }            
        }
        
        if(pan != null && pan.length() == 16){
             oCardDto = cardDao.findByNumberCard(pan);
        }
        
        
        if(oCardDto != null){
            base = oCardDto.getClientId();
        }
                
        return base;
    }

	enum Respuesta {
		/** createPhoneCode **/
		NIP_CREADO     (0, "Nip telefónico creado"),		
		VALIDACIONES_OK(1, "Validaciones superadas"), 
		DATOS_INVALIDOS(2, "Tiene que proporcionar la cuenta o tarjeta"),
		NIP_INVALIDO   (3, "El campo nip telefónico no es válido"),
		SIN_DATOS      (4, "No se encontró la tarjeta/cuenta"),
		CREADO         (5, "El código telefónico ya fue creado previamente"),
		NIP_FORMATO_INV(6, "El formato del nip no es correcto"),
		
		/** validatePhoneCode **/
		NIP_VALIDO     (0, "El nip telefónico es válido"),
		NIP_NO_VALIDO  (1, "El nip telefónico no es válido"),
		NIP_INACTIVO   (2, "Nip telefónico inactivo"),
		
		/** changePhoneCode **/
		NIP_CAMBIO_OK  	  		(0, "El nip telefónico fue modificado"),
		NIP_NO_VALIDO_CAMBIO    (1, "El nip telefónico no es válido"),
		NIP_NUEVO_INVALIDO      (2, "El nuevo nip no es válido"),
		NIP_REPETIDO      		(3, "Código repetido"),
		
		/** Cambio estatus bloque / desbloqueo **/
		NIP_BLOQUEADO           (0, "Nip telefónico bloqueado"),
		NIP_DESBLOQUEADO        (0, "Nip telefónico activado"),
		SIN_DATOS_ESTATUS       (1, "No se encontró la tarjeta/cuenta"),
		ESTAUS_NO_MODIFICADO    (2, "No se pudo cambiar el estatus"),
		;

		private Respuesta(int code, String description) {
			this.code = code;
			this.description = description;
		}

		int code;
		String description;
		
		public int getCode() {
			return code;
		}		
		public void setCode(int code) {
			this.code = code;
		}		
		public String getDescription() {
			return description;
		}		
		public void setDescription(String description) {
			this.description = description;
		}				
	}
	
}