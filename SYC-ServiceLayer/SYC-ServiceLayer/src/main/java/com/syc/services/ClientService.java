package com.syc.services;

import java.util.List;

import com.syc.dto.response.BasicAcctResponseDto;
import com.syc.dto.response.LockClientRespDto;


public interface ClientService {
	
	/**
	 * Servicio que bloquea temporalmente un cliente
	 * @param bin
	 * @param account
	 * @param operationType
	 * @param user
	 * @return
	 */
	LockClientRespDto temporaryLockClient(String bin, String idClient, int operationType, String user);

}
