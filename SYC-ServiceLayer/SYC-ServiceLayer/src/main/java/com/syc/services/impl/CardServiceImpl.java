package com.syc.services.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.request.UpdateLimitCardDto;
import com.syc.dto.response.*;
import com.syc.exceptions.OasisException;
import com.syc.persistence.dao.*;
import com.syc.persistence.dto.*;
import com.syc.persistence.dto.mapper.BalanceDto;
import com.syc.persistence.dto.mapper.PanDto;
import com.syc.persistence.dto.mapper.TransactionDto;
import com.syc.persistence.dto.mapper.TransactionModDto;
import com.syc.services.CardService;
import com.syc.services.NIPService;
import com.syc.services.UserService;
import com.syc.utils.*;
//import com.syc.ws.client.akala.AkalaWService;
//import com.syc.ws.client.akala.impl.SaldoDto;

@Service()
public class CardServiceImpl implements CardService {

	private static final transient Log log = LogFactory.getLog(CardServiceImpl.class);

	@Autowired
	CardholderDao cardHolderDao;
	@Autowired
	NIPService nipService;
	@Autowired
	PhoneCodeDao phoneCodeDao;
	@Autowired
	CardDao cardDao;
	@Autowired
	AccountDao accountDao;
	@Autowired
	SHCLogDao shcLogDao;
	@Autowired
	CardActivityDao cardActivityDao;
	@Autowired
	WSLogDao wsLogDao;
	@Autowired
	TransactionDao transactionDao;
	@Autowired
	CardLimitDao cardLimitDao;
	@Autowired
	IstcmscardDao istcmscardDao;
	@Autowired
	IstCardTypeDao istCardTypeDao;
	@Autowired
	AddressDao addressDao;
	@Autowired
	private EmisorDao emisorDao;
	@Autowired
	UserService userService;

	WSLogUtil wsUtil;

	public BasicAuthRespDto activationCard(String pan, String user) throws Exception {

		String request = "pan[" + pan + "]";

		log.debug("Procesando Activación de Tarjeta");
		log.debug("Request::" + request);

		String folio = "0";
		int codeResponse = 0;

		CardDto cardDto = cardDao.findByNumberCard(pan);

		if (cardDto != null) {
			if (cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD) {

				folio = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);

				/*****************************************
				 * Actualizando Estatus de la Tarjeta
				 *****************************************/
				cardDto.setStatus(SystemConstants.STATUS_ACTIVE_CARD);
				cardDao.update(cardDto);

				/**********************************************************************
				 * Almacenando movimiento en la Bitácora de las Tarjetas(
				 * ISTCMSACT )
				 **********************************************************************/
				CardActivityDto cardActivity = new CardActivityDto();
				cardActivity.setPan(cardDto.getNumberCard());
				cardActivity.setCardType(cardDto.getCardType());
				cardActivity.setUserId(user);
				cardActivity.setSpareChar(folio);
				cardActivity.setCmsFunc("03");
				cardActivity.setReplReason("ACTIVACION");

				cardActivityDao.create(cardActivity);

				log.debug("::Activación Procesada Exitosamente::");

				codeResponse = 1; // Operacion Exitosa
			} else {
				log.debug("::La tarjeta [" + pan + "] NO se encuentra Activa::");
				codeResponse = 3; // La tarjeta tiene que estar Ináctiva
			}
		} else {
			log.debug("::La tarjeta [" + pan + "] no existe::");
			codeResponse = 2; // Tarjeta Inexistente
		}

		/*************************************
		 * Generando Respuesta
		 *************************************/
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode(codeResponse);
		response.setAuthorization(folio);
		response.setDescription(CatalogUtil.getCatalogActivationCard().get(
				codeResponse));

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/

		log.debug("::Almacenando Movimiento en Bitácora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog("0", pan, user, request, codeResponse, response,
				SystemConstants.PCODE_LOG_ACTIVACION);

		log.debug("::Devolviendo Respuesta::" + response.toString());

		return response;
	}

	public BasicAuthRespDto activationCard(String pan, String user, int code)
			throws Exception {
		BasicAuthRespDto response = new BasicAuthRespDto();
		int codeResponse = 0;
		String request = "pan[" + pan + "]";
		CardDto cardDto = cardDao.findByNumberCard(pan);

		int status = cardDto.getStatus();
		if (code == SystemConstants.STATUS_ACTIVE_CARD) {
			if (status == SystemConstants.STATUS_INACTIVE_CARD) {
				return activationCard(pan, user);
			}

			codeResponse = 3;
			response.setCode(codeResponse);
			response.setAuthorization("-00000");
			response.setDescription(CatalogUtil.getCatalogActivationCard().get(codeResponse));

			/**************************************************
			 * Almacenando Transaccion en Bitacora del WS
			 **************************************************/

			log.debug("::Almacenando Movimiento en Bitácora::");
			wsUtil = new WSLogUtil(wsLogDao);
			wsUtil.saveLog("0", pan, user, request, codeResponse, response,SystemConstants.PCODE_LOG_ACTIVACION);

			log.debug("::Devolviendo Respuesta::" + response.toString());
		} else {
			response.setCode(codeResponse);
			response.setAuthorization("-00000");
			response.setDescription("codigo incorrecto");

			/**************************************************
			 * Almacenando Transaccion en Bitacora del WS
			 **************************************************/

			log.debug("::Almacenando Movimiento en Bitácora::");
			wsUtil = new WSLogUtil(wsLogDao);
			wsUtil.saveLog("0", pan, user, request, codeResponse, response,
					SystemConstants.PCODE_LOG_ACTIVACION);

			log.debug("::Devolviendo Respuesta::" + response.toString());

		}

		return response;

	}

	public BasicAuthRespDto cardLock(String pan, String user, int status) throws Exception {

		String request = "pan[" + pan + "]";

		log.debug("Procesando Bloqueo de Tarjeta");
		log.debug("Request::" + request);

		String folio = "0";
		int codeResponse = 0;

		CardDto cardDto = cardDao.findByNumberCard(pan);

		if (cardDto != null) {
			if (cardDto.getStatus() == SystemConstants.STATUS_ACTIVE_CARD) {
				folio = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);

				/*****************************************
				 * Actualizando Estatus de la Tarjeta
				 *****************************************/
				cardDto.setStatus(status);
				cardDao.update(cardDto);

				/**********************************************************************
				 * Almacenando movimiento en la Bitácora de las Tarjetas(
				 * ISTCMSACT )
				 **********************************************************************/
				CardActivityDto cardActivity = new CardActivityDto();
				cardActivity.setPan(cardDto.getNumberCard());
				cardActivity.setCardType(cardDto.getCardType());
				cardActivity.setUserId(user);
				cardActivity.setSpareChar(folio);
				cardActivity.setCmsFunc("05");
				cardActivity.setReplReason("BLOQUEO");

				cardActivityDao.create(cardActivity);

				log.debug("::Activación Procesada Exitosamente::");

				codeResponse = 1; // Operacion Exitosa
			} else {
				log.debug("::La tarjeta [" + pan + "] NO se encuentra Activa::");
				codeResponse = 3; // La tarjeta tiene que estar Activa
			}
		} else {
			log.debug("::La tarjeta [" + pan + "] no existe::");
			codeResponse = 2; // Tarjeta Inexistente
		}

		/*************************************
		 * Generando Respuesta a Devolver
		 *************************************/
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode(codeResponse);
		response.setAuthorization(folio);
		response.setDescription(CatalogUtil.getCatalogCardLock().get(
				codeResponse));

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/

		log.debug("::Almacenando Movimiento en Bitácora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog("0", pan, user, request, codeResponse, response.toString(),
				SystemConstants.PCODE_LOG_LOCK);

		log.debug("::Devolviendo Respuesta::" + response.toString());

		return response;
	}

	public BasicAuthRespDto cardLock(String pan, String user) throws Exception {

		return cardLock(pan, user, SystemConstants.STATUS_LOST_CARD);
	}

	public BasicAuthRespDto temporaryLock(String pan, int operationType,
			String user)

					throws Exception {

		final int OPERATION_TYPE_LOCK = 1;

		String request = "pan[" + pan + "]";

		log.debug("Procesando Bloqueo de Tarjeta");
		log.debug("Request::" + request);

		String folio = "0";
		String description = null;
		String cmsfunc = null;
		int codeResponse = 0;
		int cardStatus = 0;
		int comapreStatus = 0;

		/** Validando que el tipo de Operación sea permitido. **/
		if (operationType <= 2) {
			CardDto cardDto = cardDao.findByNumberCard(pan);

			if (operationType == OPERATION_TYPE_LOCK) {
				cardStatus = SystemConstants.STATUS_TEMPORARY_LOCK;
				description = "BLOQUEO TEMPORAL";
				cmsfunc = "15";
				comapreStatus = SystemConstants.STATUS_ACTIVE_CARD;
			} else {
				cardStatus = SystemConstants.STATUS_ACTIVE_CARD;
				description = "DESBLOQUEO TEMPORAL";
				cmsfunc = "16";
				comapreStatus = SystemConstants.STATUS_TEMPORARY_LOCK;
			}

			if (cardDto != null) {
				if (cardDto.getStatus() == comapreStatus) {

					folio = GeneralUtil
							.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);

					cardStatus = (operationType == OPERATION_TYPE_LOCK) ? SystemConstants.STATUS_TEMPORARY_LOCK
							: SystemConstants.STATUS_ACTIVE_CARD;
					description = (operationType == OPERATION_TYPE_LOCK) ? "BLOQUEO TEMPORAL"
							: "DESBLOQUEO";

					/*****************************************
					 * Actualizando Estatus de la Tarjeta
					 *****************************************/
					cardDto.setStatus(cardStatus);
					cardDao.update(cardDto);

					/**********************************************************************
					 * Almacenando movimiento en la Bitácora de las Tarjetas(
					 * ISTCMSACT )
					 **********************************************************************/
					CardActivityDto cardActivity = new CardActivityDto();
					cardActivity.setPan(cardDto.getNumberCard());
					cardActivity.setCardType(cardDto.getCardType());
					cardActivity.setUserId(user);
					cardActivity.setSpareChar(folio);
					cardActivity.setCmsFunc(cmsfunc);
					cardActivity.setReplReason(description);

					cardActivityDao.create(cardActivity);

					codeResponse = 1; // Operacion Exitosa
				} else {
					log.debug("::La tarjeta [" + pan
							+ "] NO se encuentra Activa::");
					codeResponse = 3; // La tarjeta tiene que estar Activa
				}
			} else {
				log.debug("::La tarjeta [" + pan + "] no existe::");
				codeResponse = 2; // Tarjeta Inexistente
			}
		} else {
			log.info("Código de Bloqueo no Permitido [" + operationType + "]");
			codeResponse = 4;
		}
		/*************************************
		 * Generando Respuesta a Devolver
		 *************************************/
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode(codeResponse);
		response.setAuthorization(folio);
		response.setDescription(CatalogUtil.getCatalogCardLock().get(
				codeResponse));

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/

		log.debug("::Almacenando Movimiento en Bitácora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog("0", pan, user, request, codeResponse, response.toString(),
				SystemConstants.PCODE_LOG_TEMPORARYLOCK);

		log.debug("::Devolviendo Respuesta::" + response.toString());

		return response;
	}

	public BasicAuthRespDto temporaryLock(String pan, int operationType)
			throws Exception {

		return temporaryLock(pan, operationType, "ws_user");

	}
	
	
	public BasicAuthRespDto unLockNIP(String pan, String user) throws Exception {

		String request = "pan[" + pan + "]";

		log.debug("Procesando desbloqueo de Tarjeta");
		log.debug("Request::" + request);

		String folio = "0";
		String description = "DESBLOQUEO INTENTOS DE NIP";
		String cmsfunc = "20";//definir para desbloqueo x nips invalidos
		int codeResponse = 0;
		int cardStatus = 50;
		

			CardDto cardDto = cardDao.findByNumberCard(pan);

			if (cardDto != null) {
				if (cardDto.getStatus() == SystemConstants.STATUS_LOCK_BY_NIP_CARD) {
						
						folio = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);

						/*****************************************
						 * Actualizando Estatus de la Tarjeta
						 *****************************************/
						cardDto.setStatus(cardStatus);
						/**
						 * actualizar todos los campos a 0 de los contadores en ISTCARD*/
						

						cardDto.setPinRetry(0);
						cardDto.settPinRetry(0);
					
						cardDao.update(cardDto);
						/**********************************************************************
						 * Almacenando movimiento en la Bitácora de las Tarjetas(ISTCMSACT )
						 **********************************************************************/
						CardActivityDto cardActivity = new CardActivityDto();
						cardActivity.setPan(cardDto.getNumberCard());
						cardActivity.setCardType(cardDto.getCardType());
						cardActivity.setUserId(user);
						cardActivity.setSpareChar(folio);
						cardActivity.setCmsFunc(cmsfunc);
						cardActivity.setReplReason(description);

						cardActivityDao.create(cardActivity);
						
						
						codeResponse = 1; // Operacion Exitosa
				} else {
					codeResponse = 5; // La tarjeta tiene que estar bloqueada x nips
				}
			} else {
				codeResponse = 2; // Tarjeta Inexistente
			}
		/*************************************
		 * Generando Respuesta a Devolver
		 *************************************/
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode(codeResponse);
		response.setAuthorization(folio);
		response.setDescription(CatalogUtil.getCatalogCardLock().get(codeResponse));

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/

		log.debug("::Almacenando Movimiento en Bitácora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog("0", pan, user, request, codeResponse, response,
				SystemConstants.PCODE_LOG_NIPLOCK);

		log.debug("::Devolviendo Respuesta::" + response.toString());

		return response;
	}

	public BasicAuthRespDto activationCard(CardDto cardDto, String user) throws Exception {

		String request = "pan[" + cardDto.getNumberCard() + "]";

		log.debug("Procesando Activación de Tarjeta");
		log.debug("Request::" + request);

		String folio = "0";
		int codeResponse = 0;

		if (cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD) {

			folio = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);

			/*****************************************
			 * Actualizando Estatus de la Tarjeta
			 *****************************************/
			cardDto.setStatus(SystemConstants.STATUS_ACTIVE_CARD);
			cardDao.update(cardDto);

			/**********************************************************************
			 * Almacenando movimiento en la Bitácora de las Tarjetas( ISTCMSACT
			 * )
			 **********************************************************************/
			CardActivityDto cardActivity = new CardActivityDto();
			cardActivity.setPan(cardDto.getNumberCard());
			cardActivity.setCardType(cardDto.getCardType());
			cardActivity.setUserId(user);
			cardActivity.setSpareChar(folio);
			cardActivity.setCmsFunc("03");
			cardActivity.setReplReason("ACTIVACION");

			cardActivityDao.create(cardActivity);

			log.debug("::Activación Procesada Exitosamente::");

			codeResponse = 1; // Operacion Exitosa
		} else {
			log.debug("::La tarjeta [" + cardDto.getNumberCard()
					+ "] NO se encuentra Activa::");
			codeResponse = 3; // La tarjeta tiene que estar Ináctiva
		}

		/*************************************
		 * Generando Respuesta a Devolver
		 *************************************/
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode(codeResponse);
		response.setAuthorization(folio);
		response.setDescription(CatalogUtil.getCatalogActivationCard().get(
				codeResponse));

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/

		log.debug("::Almacenando Movimiento en Bitácora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog("0", cardDto.getNumberCard(), user, request,
				codeResponse, response.toString(), SystemConstants.PCODE_LOG_ACTIVACION);

		log.debug("::Devolviendo Respuesta::" + response.toString());

		return response;
	}

	public List<BalanceQueryResponseSodexoDto> BalanceQuery(List<String> cardNumber, String user, boolean isValidaBin) {

		List<BalanceQueryResponseSodexoDto> lista = new ArrayList<BalanceQueryResponseSodexoDto>();
		String pan = null;
		
		if (cardNumber != null) {

			for (int i = 0; i <= cardNumber.size() - 1; i++) {
				BalanceQueryResponseSodexoDto registro = new BalanceQueryResponseSodexoDto();
				pan = cardNumber.get(i);
				
				boolean isPermitido = true;
				
				if(isValidaBin)
					isPermitido = userService.getBinesPermitidos(user, pan.substring(0, 8));
				
				if(isPermitido){
				BalanceQueryResponseDto response = balanceQuery(pan, user);
				registro.setCode(response.getCode());
				registro.setAvailableAmount(response.getAvailableAmount());
				registro.setDescription(response.getDescription());
				registro.setPan(cardNumber.get(i));

				}else{
					registro.setCode(30);
					registro.setAvailableAmount(0);
					registro.setDescription("Operacion no permitida sobre bin ["+pan.substring(0, 8)+"]");
					registro.setPan(cardNumber.get(i));
				
				}
				
				lista.add(registro);
			}

			// System.out.println(lista.toString());

		}

		return lista;
	}

	public BalanceQueryResponseDto balanceQuery(String cardNumber, boolean isInactive, boolean isActivation, String user) {

		String bin;

		CardholderDto clientDto = cardHolderDao
				.getCardholderInfoByCardNumber(cardNumber);

		bin = (clientDto != null && clientDto.getAccount() != null) ? clientDto
				.getAccount().getBin() : "0";

				int code = 0;
				String description = "Tarjeta no registrada.";
				double amount = 0;

				if (clientDto != null && clientDto.getAccount() != null) {
					if (clientDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD || isInactive) {
						code = 1;
						description = "Tarjeta Registrada.";
						amount = clientDto.getAccount().getAvailableBalance();
					} else {
						code = 1;
						description = "Tarjeta Inactiva.";
					}

				}

				
				BalanceQueryResponseDto response = new BalanceQueryResponseDto();
				response.setCode(code);
				response.setDescription(description);
				response.setAvailableAmount(amount);

				/**************************************************
				 * Almacenando Transaccion en Bitacora del WS
				 **************************************************/
				log.debug("::Almacenando Movimiento en Bitacora::");
				wsUtil = new WSLogUtil(wsLogDao);
				wsUtil.saveLog(bin, cardNumber, user, "PAN["+ cardNumber + "]", code, response, SystemConstants.PCODE_BALANCE_QUERY);

				return response;
	}

	public BalanceQueryResponseDto balanceQuery(String cardNumber, String user){
		return balanceQuery(cardNumber, false, false, user);
	}
	public BalanceQueryStatusResponseDto balanceQueryStatus(String cardNumber, String user) {

		String bin;
		int status = 0;

		CardholderDto clientDto = cardHolderDao.getCardholderInfoByCardNumber(cardNumber);

		bin = (clientDto != null && clientDto.getAccount() != null) ? clientDto.getAccount().getBin() : "0";

				int code = 0;
				String description = "Tarjeta no registrada.";
				double amount = 0;

				if (clientDto != null && clientDto.getAccount() != null) {
					if (clientDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD) {
						code = 1;
						status = clientDto.getCard().getStatus();
						amount = clientDto.getAccount().getAvailableBalance();
					} else {
						code = 2;
						status = clientDto.getCard().getStatus();

					}

				}

				BalanceQueryStatusResponseDto response = new BalanceQueryStatusResponseDto();
				response.setCode(code);
				response.setStatus(status);
				response.setDescription((status != 0) ? CatalogUtil.getCatalogCardStatus().get(status) : description);
				response.setAvailableAmount(amount);

				/**************************************************
				 * Almacenando Transaccion en Bitacora del WS
				 **************************************************/
				log.debug("::Almacenando Movimiento en Bitacora::");
				wsUtil = new WSLogUtil(wsLogDao);
				wsUtil.saveLog(bin, cardNumber, user, "PAN["+ cardNumber + "]", code, response.toString(),
						SystemConstants.PCODE_BALANCE_QUERY);

				return response;
	}

	public BalanceQueryResponseDto balanceQueryWhitCommision(String pan, double commisionAmount) {
	
		String bin;
		CardholderDto clientDto = cardHolderDao
				.getCardholderInfoByCardNumber(pan);
		bin = (clientDto != null && clientDto.getAccount() != null) ? clientDto
				.getAccount().getBin() : "0";
				int code = 0;
				String description = "Tarjeta no registrada.";
				double amount = 0;

				if (clientDto != null && clientDto.getAccount() != null) {
					if (clientDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD) {
						code = 1;
						description = "Tarjeta Registrada.";
						amount = clientDto.getAccount().getAvailableBalance();

						/** Almacenando registro en SHCLOG **/
						SHCLogDto shcLog = new SHCLogDto(21);

						shcLog.setNumberCard(pan);
						shcLog.setpCode(SystemConstants.PCODE_MONEX_BALANCE_QUERY_BATCH_COMMISION);
						shcLog.setAmount(commisionAmount);
						shcLog.setAvalBalance(amount);
						shcLog.setAuthNum(GeneralUtil
								.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT));
						shcLog.setTermId("111111");
						shcLog.setMsgType(210);
						shcLog.setFee(0);
						shcLog.setAcceptorName("COMISION POR CONSULTA DE SALDO IVR");
						shcLog.setAcctNum(clientDto.getAccount().getAccount());
						shcLog.setFiller3("web_service");

						shcLogDao.create(shcLog);

					} else {
						code = 1;
						description = "Tarjeta Inactiva.";
						log.info("tarjeta inactiva");
					}

				}

				BalanceQueryResponseDto response = new BalanceQueryResponseDto();
				response.setCode(code);
				response.setDescription(description);
				response.setAvailableAmount(amount);

				/**************************************************
				 * Almacenando Transaccion en Bitacora del WS
				 **************************************************/
				log.debug("::Almacenando Movimiento en Bitacora::");
				wsUtil = new WSLogUtil(wsLogDao);
				wsUtil.saveLog(bin, pan, SystemConstants.WS_USER_NAME, "PAN[" + pan
						+ "]", code, response.toString(),
						SystemConstants.PCODE_MONEX_IVR_BALANCE_QUERY);
				// log.info(response);
				return response;

	}

	public BasicResponseDto cardStatus(String pan, String user) throws Exception {

		int codeResponse = 0;

		/** Obteniendo la Información del Tarjetahabiente **/
		CardholderDto cardHolderDto = cardHolderDao.getCardholderInfoByCardNumber(pan);
		String bin = (cardHolderDto != null && cardHolderDto.getAccount() != null) ? cardHolderDto.getAccount().getBin() : "0";

				if (cardHolderDto != null && cardHolderDto.getCard() != null) {
					codeResponse = cardHolderDto.getCard().getStatus();
				}

				/** Generando Respuesta **/
				BasicResponseDto response = new BasicResponseDto();
				response.setCodigo(codeResponse);
				response.setDescripcion(CatalogUtil.getCatalogCardStatus().get(codeResponse));

				/**************************************************
				 * Almacenando Transaccion en Bitacora del WS
				 **************************************************/
				wsUtil = new WSLogUtil(wsLogDao);
				wsUtil.saveLog(bin, pan, SystemConstants.WS_USER_NAME, "pan[" + pan+ "]", codeResponse, response,SystemConstants.PCODE_STATUS_CARD);

				return response;
	}

	public TransactionsListResponseDto lastestTransactions(String pan, int numMaxOfRows, int operationType, boolean isapproved, int emisorCode) {
		int codeResponse = 0;
		String description = null;
		List<TransactionDto> list = null;
		int size = 0;
		String request = "pan [" + pan + "], numMaxOfRows [" + numMaxOfRows
				+ "], operationType[" + operationType + "]";

		CardholderDto cardholderDto = cardHolderDao
				.getCardholderInfoByCardNumber(pan);

		if (cardholderDto != null) {

			if (cardholderDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD) {

				/** Obteniendo el Listado de Transacciones **/
				list = transactionDao.getTransactionsList(pan, numMaxOfRows, operationType, isapproved, emisorCode);

				if (list != null && list.size() > 0) {
					codeResponse = 1;
					size = list.size();
				} else {
					codeResponse = 0;
				}
			} else {
				codeResponse = 2;
			}
		} else {
			codeResponse = 3;
		}

		TransactionsListResponseDto response = new TransactionsListResponseDto();
		response.setCode(codeResponse);
		response.setDescription(CatalogUtil.getCatalogTransactions().get(
				codeResponse));
		response.setLastestTransactions(list);

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		 log.debug("::Almacenando Movimiento en Bitacora::");
		 wsUtil = new WSLogUtil(wsLogDao);
		 wsUtil.saveLog(pan.substring(0,7), pan, SystemConstants.WS_USER_NAME,request, codeResponse, response.toString()+ " registros["+ size+"]" , SystemConstants.PCODE_LOG_TRANSACTIONS_LIST);

		return response;
	}

	public TransactionsListResponseDto lastestTransactions(String pan, int numMaxOfRows, int operationType, int emisorCode) {

		return lastestTransactions(pan, numMaxOfRows, operationType, false, emisorCode);
	}

	public TransactionsListResponseDto getTransactionsByAccountOrPan(String pan, String bin, String account, int numMaxOfRows,
			int operationType, int emisorCode) {
		int codeResponse = 0;

		TransactionsListResponseDto response = new TransactionsListResponseDto();
		if (GeneralUtil.isEmtyString(pan) && account.length() == 11) {
			String binSyc = GeneralUtil.completaCadena(bin, '0', 10, "L");
			EmisorDto oEmisorDto = emisorDao.findByBin(binSyc);
			if (oEmisorDto != null) {
				String accountBuild = GeneralUtil.buildPrimaryAccount(account,
						oEmisorDto.getCuenta().trim());
				/**
				 * Busca todas las tarjetas asociadas a la cuenta y envia para busqueda de transacciones
				 * la titular que no tenga estatus 57
				 * Si encuentra más de una tarjeta, devuelve codigo de que tiene varias tarjetas asociadas
				 * al numero de cuenta, que realice la busqueda por numero de tarjeta
				 */
				
				List<Object[]> tarjetas = cardDao.findCardsByAccount(binSyc, accountBuild);
				//hacer servicio que te devuelva las tarjetas asociadas ya mapeadas
				
				 List<PanDto> pans =  getPansMappedByAccount(tarjetas);
				 //puede haber mas de un remplazo, iterar lista, llenarla con las tarjetas con estatus
				 //distinto a remplazado
				 pans = getPansNotReplacement(pans);
				 if(pans != null){
					 if( pans.size() == 1){
						 return lastestTransactions(pans.get(0).getNumberCard(), numMaxOfRows, operationType, emisorCode);
					 }else{
						 codeResponse = 6; //tiene mas de 2 tarjetas asignadas al numero de cuenta, favor de realizar la consulta por cuenta.	
					 }
				 }else{
					 codeResponse = 3;//tarjeta no registrada	 
				 }
			} else
				codeResponse = 5;// no hay registro en tbl_emisores
		} else if (GeneralUtil.isEmtyString(account) && pan.length() == 16) {
			return lastestTransactions(pan, numMaxOfRows, operationType, emisorCode);
		} else
			codeResponse = 4;// parametros invalidos

		response.setCode(codeResponse);
		response.setDescription(CatalogUtil.getCatalogTransactions().get(
				codeResponse));
		return response;
	}
	/**
	 * Devuelve una lista que contienen tarjetas asociadas a una cuenta
	 * mapeadas a un dto
	 * @param tarjetas
	 * @return
	 */
	private List<PanDto> getPansMappedByAccount(List<Object[]> tarjetas){
		List<PanDto> pans = null;	
		if(tarjetas != null && tarjetas.size() > 0){
			pans = new ArrayList<PanDto>();
			for (int i = 0; i < tarjetas.size(); i++) {
				PanDto tar = new PanDto();
				tar.setNumberCard((String) tarjetas.get(i)[0]);
				tar.setStatus((Integer) tarjetas.get(i)[1]);
				tar.setCardIndicator((tarjetas.get(i)[2].toString()
						.equals("0")) ? "Titular" : "Adicional");
				pans.add(tar);
			}
		}

		return pans;
	}
	/**
	 * Busca tarjetas que no esten con estatus de bloqueo
	 * @param tarjetas
	 * @return
	 */
	private List<PanDto> getPansNotReplacement(List<PanDto> tarjetas){
		List<PanDto> pansNotReplacement = null;
		if(tarjetas != null && tarjetas.size() > 0){
			pansNotReplacement = new ArrayList<PanDto>();
			for (int i = 0; i < tarjetas.size(); i++) {
				PanDto tar = tarjetas.get(i);
				if(tar.getStatus() != SystemConstants.STATUS_REPLACED_CARD)
					pansNotReplacement.add(tar);
			}
		}
				
		
		return pansNotReplacement;
	}

	public TransactionsListResponseDto periodTransactions(String pan, int period, int operationType, int emisorCode) {
		int codeResponse = 0;
		String description = null;
		List<TransactionDto> list = null;
		int size = 0;
		String request = "pan [" + pan + "], period [" + period
				+ "], operationType[" + operationType + "]";

		CardholderDto cardholderDto = cardHolderDao
				.getCardholderInfoByCardNumber(pan);

		if (period <= 3 && period != 0) {

			if (cardholderDto != null) {

				if (cardholderDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD) {

					/** Obteniendo el Listado de Transacciones **/
					list = transactionDao.getTransactionsPeriodList(pan, period, operationType, emisorCode);

					if (list != null && list.size() > 0) {
						codeResponse = 1;
						size = list.size();
						description = "Movimientos Registrados.";
					} else {
						codeResponse = 0;
						description = "No se encontraron Movimientos con esta Tarjeta";
					}
				} else {
					codeResponse = 2;
					description = "Tarjeta Inactiva";
				}
			} else {
				codeResponse = 3;
				description = "Tarjeta no registrada";
			}
		} else {
			codeResponse = 4;
			description = "Periodo fuera de rango";
		}

		TransactionsListResponseDto response = new TransactionsListResponseDto();
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setLastestTransactions(list);

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug("::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(pan.substring(0, 7), pan, SystemConstants.WS_USER_NAME, request, codeResponse, response.getCode() +" "+response.getDescription() +" registros["+ size+"]", SystemConstants.PCODE_LOG_TRANSACTIONS_PERIOD);

		return response;
	}

	public SHCLogDto getLogDetails(String pan, String tranDate, int msgType,
			String authNum) {

		Date tran = GeneralUtil.fechaToString(tranDate, "dd-MM-yyyy");

		SHCLogDto shclogDto = shcLogDao.getLogByCardNumberDetails(pan, tran,
				msgType, authNum);

		return shclogDto;
	}

	public BasicAuthRespDto updateCardLimit( String pan, IndividualLimitsDto request, String user) {

		int codeResponse = 3;
		String description = "tarjeta no registrada";
		String authorization = null;
		CardLimitDto limits = null;
		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber(pan);
		boolean existlimit = true;
		String request1 = "request[ pan [ "+pan+" ]limits[ "+request+" ]user[ "+user+" ]]";

		if(cardholderDto != null ){
			String bin = cardholderDto.getAccount().getBin();
			limits = cardLimitDao.findBypan(bin, pan);

			if (limits == null) {
				existlimit = false;
				limits = new CardLimitDto(); 
				
				limits.setBin(cardholderDto.getAccount().getBin());
				limits.setPan(pan);
				/**Indica en istcard que tenemos limites individuales**/
				CardDto card = cardholderDto.getCard();
				card.setLimitIndicator("1");
				cardDao.update(card);
			
			}
			
			/** Compras cantidad e importe */
			limits.setPurchLimit(request.getMaxBuysAmount());
			limits.setPurchCount(request.getMaxBuysNumber());
			/** Retiros nacional cantidad e importe */
			limits.setCashWdLimit(request.getMaxWithdrawalAmount());
			limits.setCashWdCount(request.getMaxWithdrawalNumber());
			/** Retiros internacional cantidad e importe */
			limits.setIcashWdLimitInt(request.getMaxWithdrawalAmountInt());
			limits.setIcashWdCountInt(request.getMaxWithdrawalNumberInt());
			

			if (existlimit){
				cardLimitDao.update(limits);
				description = "limites individuales actualizados";
			}else{
				cardLimitDao.create(limits);
				description = "limites individuales creados";
			}
			authorization = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
			codeResponse = 1;
		}
			

		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setAuthorization(authorization);
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		 log.debug("::Almacenando Movimiento en Bitacora::");
		 wsUtil = new WSLogUtil(wsLogDao);
		 wsUtil.saveLog(pan.substring(0,7), pan, user,request1, codeResponse, response, SystemConstants.PCODE_LOG_LIMITS_CREATE_UPDATE);


		return response;
	}

	public CardLimitsResponseDto getLimitsDetails(String pan, String user) {
		
		int codeResponse = 3;
		String description = "tarjeta no registrada";
		CardLimitDto limits = null;
		IndividualLimitsDto cardLimits = null;
		String request = "request[ pan [ "+pan+" ]user[ "+user+" ]]";
		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber(pan);

		if(cardholderDto != null ){
			String bin = cardholderDto.getAccount().getBin();

			 limits = cardLimitDao.findBypan(bin, pan);
		
			if(limits != null){
				codeResponse = 1;
				description = "limites individuales encontrados";
				cardLimits = new IndividualLimitsDto();
				/** Compras cantidad e importe */
				cardLimits.setMaxBuysAmount(limits.getPurchLimit());
				cardLimits.setMaxBuysNumber(limits.getPurchCount());
				/** Retiros nacional cantidad e importe */
				cardLimits.setMaxWithdrawalAmount(limits.getCashWdLimit());
				cardLimits.setMaxWithdrawalNumber(limits.getCashWdCount());
				/** Retiros internacional cantidad e importe */
				cardLimits.setMaxWithdrawalAmountInt(limits.getIcashWdLimitInt());
				cardLimits.setMaxWithdrawalNumberInt(limits.getIcashWdCountInt());
			}else{
				codeResponse = 2;
				description = "no se encontraron limites individuales";
			}
		}
		CardLimitsResponseDto response = new CardLimitsResponseDto();
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setCardLimits(cardLimits);

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		 log.debug("::Almacenando Movimiento en Bitacora::");
		 wsUtil = new WSLogUtil(wsLogDao);
		 wsUtil.saveLog(pan.substring(0,7), pan, user,request, codeResponse, response, SystemConstants.PCODE_LOG_LIMITS_QUERY);


		return response;

	}
	
	public BasicAuthRespDto deleteCardLimit(String pan, String user){
		
		int codeResponse = 3;
		String description = "tarjeta no registrada";
		String authorization = null;
		CardLimitDto limits = null;
		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber(pan);
		String request = "request[ pan [ "+pan+" ]user[ "+user+" ]]";
		if(cardholderDto != null ){
			String bin = cardholderDto.getAccount().getBin();
			limits = cardLimitDao.findBypan(bin, pan);

			if (limits != null) {
				cardLimitDao.delete(limits);
				/**Indica en istcard que no se tienen limites individuales**/
				CardDto card = cardholderDto.getCard();
				card.setLimitIndicator("0");
				cardDao.update(card);
				codeResponse = 1;
				description = "limites individuales borrados";
			}else{
				codeResponse = 2;
				description = "limites individuales no encontrados";
			}
		}
		
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setAuthorization(authorization);

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		 log.debug("::Almacenando Movimiento en Bitacora::");
		 wsUtil = new WSLogUtil(wsLogDao);
		 wsUtil.saveLog(pan.substring(0,7), pan, user,request, codeResponse, response, SystemConstants.PCODE_LOG_LIMITS_DELETE);

		return response;
	}

	public UserQueryResponseDto findByUserOrPan(String pan, String webUser, String user, String bin) {

		String request = "pan["+pan+"]webUser["+webUser+"]user["+user+"]bin["+bin+"]";
		UserQueryResponseDto response = new UserQueryResponseDto();

		List<IstcmscardDto> lista = new ArrayList<IstcmscardDto>();
		int codeResponse = 0;

		if (!pan.equals("") && pan.length() == 16)
			webUser = "";
		else if (!pan.equals("") && pan.length() != 16)
			pan = "";

		if ((!pan.equals("") && pan.length() == 16) && webUser.equals("")) {
			// hacemos la consulta x tarjeta

			IstcmscardDto resp = istcmscardDao.getIstcmscardInformation(pan);
			if (resp != null)
				lista.add(resp);
		}

		else if (pan.equals("") && !webUser.equals("")) {
			// hacemos la consulta x usuario y bin

			List<IstcmscardDto> resp = istcmscardDao.getInformationByName(
					webUser, bin);
			if (resp.size() >= 0)
				for (int j = 0; j < resp.size(); j++) {
					lista.add(resp.get(j));
				}
		} else {

			codeResponse = 4;// los parametros no pueden ser nulos
		}
		if (lista.size() > 0)
			codeResponse = 1;
		else
			codeResponse = (codeResponse == 0) ? 2 : codeResponse;

		response.setCode(codeResponse);
		response.setDescription(CatalogUtil.getCatalogSearchClient().get(
				codeResponse));
		response.setCliente(lista);
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		 log.debug("::Almacenando Movimiento en Bitacora::");
		 wsUtil = new WSLogUtil(wsLogDao);
		 wsUtil.saveLog(pan.substring(0,7), pan, user,request, codeResponse, response, SystemConstants.PCODE_LOG_FIND_BY_USER_PAN);


		return response;
	}

	@Override
	public CardAccountResponseDto getCardsByAccount(String account, String bin, String user) {
		int codeResponse = 0;
		String description = null;
		Date currentDate = GeneralUtil.getCurrentSqlDate();
		List<Object[]> cards = null;
		List<PanDto> pans = null;
		if (bin != null && account != null && user != null) {
			/** El bin en las tablas esta a 10 posiciones */
			String binSyc = GeneralUtil.completaCadena(bin, '0', 10, "L");

			/** Obtenemos al prefijo de la tabla emisores */
			EmisorDto oEmisorDto = emisorDao.findByBin(binSyc);

			/** Generamos el numero de cuenta */
			String accountBuild = GeneralUtil.buildPrimaryAccount(account,
					oEmisorDto.getCuenta().trim());

			if (accountBuild != null) {
				cards = cardDao.findCardsByAccount(bin, accountBuild);
				if (cards != null && cards.size() > 0) {
					pans = new ArrayList<PanDto>();
					for (int i = 0; i < cards.size(); i++) {
						PanDto tar = new PanDto();
						tar.setNumberCard((String) cards.get(i)[0]);
						tar.setStatus((Integer) cards.get(i)[1]);
						tar.setCardIndicator((cards.get(i)[2].toString()
								.equals("0")) ? "Titular" : "Adicional");
						pans.add(tar);
					}
					codeResponse = 1;
					description = "Tarjetas registradas";
				} else {
					codeResponse = 2;
					description = "no se encontraron tarjetas asociadas";
				}
			} else {
				codeResponse = 3;
				description = "error al procesar la cuenta";

			}
		} else {
			codeResponse = 4;
			description = "valores invalidos para la operación";
		}

		CardAccountResponseDto response = new CardAccountResponseDto();

		response.setCodeResponse(codeResponse);
		response.setDescription(description);
		response.setIssueDate(GeneralUtil.formatDate("dd-MM-yyyy", currentDate));
		response.setCurrentTime(GeneralUtil.formatDate("HH:mm:ss", currentDate));
		response.setCards(pans);

		return response;
	}

	public BalanceQueryStatusResponseDto balanceQueryAccount(String bin,
			String account, String user) {
		BalanceQueryStatusResponseDto response = new BalanceQueryStatusResponseDto();
		String binSyc = GeneralUtil.completaCadena(bin, '0', 10, "L");
		EmisorDto oEmisorDto = emisorDao.findByBin(binSyc);
		double avalBalance = 0.0;
		int codeResponse = 0;
		String pan = "0";
		int status = 99;
		String request = "bin[ " + bin + " ], account[ " + account + " ]";
		if (oEmisorDto != null) {
			String accountBuild = GeneralUtil.buildPrimaryAccount(account,
					oEmisorDto.getCuenta().trim());
			CardDto cardDto = cardDao.findPanByAccount(binSyc, accountBuild);
			if (cardDto != null) {
				pan = cardDto.getNumberCard();
				CardholderDto cardHolderDto = cardHolderDao
						.getCardholderInfoByCardNumber(cardDto.getNumberCard());
				if (cardHolderDto != null && cardHolderDto.getAccount() != null
						&& cardHolderDto.getCard() != null
						&& cardHolderDto.getIstrel() != null) {
					// if(cardHolderDto.getAccount().getStatus().equals(SystemConstants.STATUS_ACTIVE_ACCOUNT)){
					avalBalance = cardHolderDto.getAccount()
							.getAvailableBalance();
					codeResponse = 1;
					status = Integer.parseInt(cardHolderDto.getAccount()
							.getStatus());

					// }else
					// codeResponse = 2;//cuenta inactiva
				} else
					codeResponse = 3;// cuenta no asociada

			} else
				codeResponse = 5;// cuenta no existente
		} else
			codeResponse = 4;// registro en tbl_emisores no existe

		response.setCode(codeResponse);
		response.setDescription(CatalogUtil.getCatalogBalanceQueryByAccount()
				.get(codeResponse));
		response.setAvailableAmount(avalBalance);
		response.setStatus(status);
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug("::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(bin, pan, user, request, codeResponse, response,
				SystemConstants.PCODE_BALANCE_QUERY_BY_ACCOUNT);

		return response;
	}

	public BalanceQueryResponseDto balanceQueryByAccountOrPan(String pan, String bin, String account, String user) {
		BalanceQueryResponseDto response = new BalanceQueryResponseDto();
		int codeResponse = 0;
		String description = null;
		double avalBalance = 0.0;
		if (pan.equals("") && account.length() == 11) {

			BalanceQueryStatusResponseDto resp = balanceQueryAccount(bin, account, user);
			if (resp != null) {
				response.setCode(resp.getCode());
				response.setDescription(resp.getDescription());
				response.setAvailableAmount(resp.getAvailableAmount());
				return response;

			}
		} else if (account.equals("") && pan.length() == 16) {
			return balanceQuery(pan, user);
		} else {
			codeResponse = 8;// parametros no validos
			description = "parámetros invalidos";
		}
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setAvailableAmount(avalBalance);

		return response;
	}

	public IVRInfoClientResponse findIVRCardInfo(String pan, String account,
			String bin) {

		CardDto cardDto = null;
		EmisorDto emisorDto = null;
		CardholderDto cardholder = null;
		int codeResponse;
		boolean isFindByPan = false;
		
		String request = "pan[" + pan + "],account[" + account + "],bin[" + bin
				+ "]";

		isFindByPan = !GeneralUtil.isEmtyString(pan);

		if (isFindByPan) {

			/** La busqueda a realizar es por tarjeta **/
			cardDto = cardDao.findByNumberCard(pan);

		} else {
			/** La busqueda a realizar es por cuenta **/
			bin = GeneralUtil.completaCadena(bin, '0', 10, "L");
			emisorDto = emisorDao.findByBin(bin);

			if (emisorDto != null) {

				/** Armamos la cuenta con el prefijo **/
				String refillAccount = GeneralUtil.buildPrimaryAccount(account,
						emisorDto.getCuenta().trim());

				/** Realizamos la busqueda por cuenta y bin **/
				cardDto = cardDao.findPanByAccount(bin, refillAccount);
				cardholder = cardHolderDao.getCardholderInfoByAccount(bin, refillAccount);
				
			}
		}
		/** Generando Respuesta **/
		IVRInfoClientResponse response = new IVRInfoClientResponse();
		if (cardDto != null) {
			PhoneCodeDto oPhoneCodeDto = phoneCodeDao.read(cardDto.getClientId().trim());
			if(oPhoneCodeDto != null){
				response.setCodePhoneStatus((oPhoneCodeDto.getStatus().equals("A") ? "Activo" : "Bloqueado"));
			}else
				response.setCodePhoneStatus("Codigo no existe");
			int product = GeneralUtil.convertStringToInt(cardDto.getCardType());

			/** Tarjeta [Solo opera con productos 1,2,3,4] **/
			if (isFindByPan) {

				if (product <= 3) {
					codeResponse = 1;// Tarjetahabiente encontrado
					response.setProduct(cardDto.getCardType());
					response.setStatus(cardDto.getStatus());
				} else {
					codeResponse = 3; // Este prodcto opera solo con cuenta
				}

				/** Cuenta [Solo opera con producto 4] **/
			} else {

				if (product == 4) {
					codeResponse = 1;// Tarjetahabiente encontrado
					response.setProduct(cardDto.getCardType());
					response.setStatus(Integer.parseInt(cardholder.getAccount()
							.getStatus()));
				} else {
					codeResponse = 4; // Este producto solo opera con tarjeta
				}
			}
		} else {
			codeResponse = 2; // Tarjetahabiente no encontrado
		}
		response.setCode(codeResponse);
		response.setDescription(CatalogUtil.getCatalogIVRCardInfo().get(
				codeResponse));

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(bin, pan, SystemConstants.WS_USER_NAME, request,
				response.getCode(), response,
				SystemConstants.PCODE_IVR_INFO_CLIENT);

		return response;
	}

	@Override
	public AccountResponseDto findAccountByPan(String pan, String user) {
		StringBuilder request = new StringBuilder();
		request.append("pan[").append(pan).append("]user[").append(user)
		.append("]");
		AccountResponseDto response = new AccountResponseDto();
		CardholderDto cardDto = cardHolderDao
				.getCardholderInfoByCardNumber(pan);
		String bin = GeneralUtil.completaCadena(pan.substring(0, 6), '0', 10,
				"L");
		int codeResponse = 0;
		String description = "cuenta no encontrada";

		if (cardDto != null) {
			if (cardDto.getCard() != null && cardDto.getAccount() != null
					&& cardDto.getIstrel() != null) {
				response.setAccount(cardDto.getAccount().getAccount()
						.substring(8));
				response.setProduct(cardDto.getCard().getCardType());
				response.setStatus(cardDto.getCard().getStatus());
				codeResponse = 1;
				description = "cuenta registrada";

			} else
				codeResponse = 2;
		} else {
			codeResponse = 3;
			description = "la tarjeta no existe";
		}
		response.setCode(codeResponse);
		response.setDescription(description);

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(bin, pan, user, request.toString(), response.getCode(),
				response, SystemConstants.PCODE_SEARCH_ACCOUNT_BY_PAN);

		return response;
	}

	@Override
	public FindPanResponseDto findDetailPan(String pan, String cvv2, String expirationDate, String rfc, String user) throws OasisException {

		StringBuffer request = new StringBuffer();
		request.append("[").append(pan).append("],").append("[").append(cvv2).append("],").append("[").append(expirationDate).append("],").append("[").append(user).append("],");
		int codeResponse = 0;
		List<Object[]> cards = null;
		List<PanDto> pans = null;
		String clientName = null;
		String account = null;
		String bin = null;
		Date expiryDate = null;
		String findCvv = null;
		String shaFindCvv = null;
		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber(pan);
		etiqueta:
		if(cardholderDto != null){
			bin = GeneralUtil.completaCadena(pan.substring(0, 6),'0',10,"L");
			
			IstCardTypeDto cardType = istCardTypeDao.findPrefixByBin(cardholderDto.getIstrel().getBin(), cardholderDto.getCard().getCardType());
			AddressDto addressDto = addressDao.findCardAddress(pan);
			if(addressDto != null){
				String rfcClient = (addressDto.getRfc() != null) ? addressDto.getRfc().trim() : null;
				if(rfc.equals(rfcClient) || GeneralUtil.isEmtyString(rfc)){
					if(cardType != null){
						if(!GeneralUtil.isEmtyString(expirationDate)){
							 expiryDate = GeneralUtil.convertStringToSqlDate(expirationDate, "dd-MM-yyyy");
							 String serviceCode =  cardType.getIccIndicator() + cardType.getServiceCode();
							  findCvv = nipService.getCVV( cardholderDto.getCard().getNumberCard(),  expiryDate,  serviceCode );
							  if(findCvv != null){
									 shaFindCvv = GenerateSHA.getStringMessageDigest(findCvv, GenerateSHA.SHA384);
									
							  }else{
									codeResponse = 5;//cvv inválido
									break etiqueta;
							  }

						}
							  if( cvv2.equals(shaFindCvv) || GeneralUtil.isEmtyString(cvv2)){
							  if(cardholderDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD){

									IstcmscardDto istcmsCard = istcmscardDao.getIstcmscardInformation(pan);
									if(istcmsCard != null){
										clientName = istcmsCard.getName();
										account = cardholderDto.getAccount().getAccount();
										pans = new ArrayList<PanDto>();
										if(istcmsCard.getCardIndicator().equals("0")){
											cards = cardDao.findCardsByAccount(cardholderDto.getIstrel().getBin(), cardholderDto.getAccount().getAccount());
											if(cards != null && cards.size() > 0){
												for(int i = 0; i < cards.size(); i++){
													PanDto tar = new PanDto();
													tar.setNumberCard((String) cards.get(i)[0]);
													tar.setStatus((Integer)cards.get(i)[1]);
													tar.setCardIndicator((cards.get(i)[2].toString().equals("0")) ? "Titular" : "Adicional");
													pans.add(tar);
												}

												codeResponse = 1;
											}
										}else{
											PanDto tar = new PanDto();
											tar.setNumberCard(cardholderDto.getCard().getNumberCard());
											tar.setStatus(cardholderDto.getCard().getStatus());
											tar.setCardIndicator((istcmsCard.getCardIndicator().equals("0")) ? "Titular" : "Adicional");
											pans.add(tar);
											codeResponse = 1;
										}
									}else
										codeResponse = 9;//no se encontro registro en istcmscard
								}else
									codeResponse = 8;//estatus tarjeta no es activa
							
						}else
							codeResponse = 5;//cvv no encontrado
					}else
						codeResponse = 6;//bin no encontrado
				}else
					codeResponse = 10;//rfc invalido
			}else
				codeResponse = 11;//demograficos no encontrados
		}else
			codeResponse = 7;//tarjeta no encontrada

		FindPanResponseDto response = new FindPanResponseDto();

		response.setCards(pans);
		response.setCode(codeResponse);
		response.setDateTime(GeneralUtil.formatDate( "dd-MM-yyyy  HH:mm:SS" , GeneralUtil.getCurrentSqlDate()));
		response.setDescription(CatalogUtil.getCatalogFindDetailPans().get(codeResponse));
		response.setClientName(clientName);
		response.setNoCuenta(account);

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(bin, pan, user, request.toString(), response.getCode(), response, SystemConstants.PCODE_LOG_DETAIL_PANS);


		return response;
	}

	@Override
	public BalanceCardsResponseDto balanceQueryCards(String pan, String user) {

		StringBuffer request = new StringBuffer();
		request.append("[").append(pan).append("],").append("[").append(user)
		.append("],");
		int codeResponse = 0;
		List<Object[]> cards = null;
		List<BalanceDto> pans = null;
		String bin = null;
		String clientName = null;
		String account = null;
		CardholderDto cardholderDto = cardHolderDao
				.getCardholderInfoByCardNumber(pan);

		if (cardholderDto != null) {
			if (cardholderDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD) {

				IstcmscardDto istcmsCard = istcmscardDao
						.getIstcmscardInformation(pan);
				if (istcmsCard != null) {
					pans = new ArrayList<BalanceDto>();
					clientName = istcmsCard.getName();
					account = cardholderDto.getAccount().getAccount();
					if (istcmsCard.getCardIndicator().equals("0")) {
						cards = cardDao.findCardsByAccount(cardholderDto
								.getIstrel().getBin(), cardholderDto
								.getAccount().getAccount());
						if (cards != null && cards.size() > 0) {
							for (int i = 0; i < cards.size(); i++) {
								BalanceDto tar = new BalanceDto();
								tar.setNumberCard((String) cards.get(i)[0]);
								tar.setStatus((Integer) cards.get(i)[1]);
								tar.setCardIndicator((cards.get(i)[2]
										.toString().equals("0")) ? "Titular"
												: "Adicional");
								tar.setAvalBalance(cardholderDto.getAccount()
										.getAvailableBalance());
								pans.add(tar);
							}

							codeResponse = 1;
						}
					} else {
						BalanceDto tar = new BalanceDto();
						tar.setNumberCard(cardholderDto.getCard()
								.getNumberCard());
						tar.setStatus(cardholderDto.getCard().getStatus());
						tar.setCardIndicator((istcmsCard.getCardIndicator()
								.equals("0")) ? "Titular" : "Adicional");
						tar.setAvalBalance(cardholderDto.getAccount()
								.getAvailableBalance());
						pans.add(tar);
						codeResponse = 1;
					}
				} else
					codeResponse = 9;// no hay registro en istcmscard
			} else
				codeResponse = 8;// tarjeta no esta activa
		} else
			codeResponse = 7;// no se encontro registro de tarjeta

		BalanceCardsResponseDto response = new BalanceCardsResponseDto();

		response.setBalanceCards(pans);
		response.setCode(codeResponse);
		response.setClientName(clientName);
		response.setNoCuenta(account);
		response.setDescription(CatalogUtil.getCatalogFindDetailPans().get(
				codeResponse));
		response.setDateTime(GeneralUtil.formatDate("dd-MM-yyyy  HH:mm:SS",
				GeneralUtil.getCurrentSqlDate()));

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(bin, pan, user, request.toString(), response.getCode(),
				response, SystemConstants.PCODE_LOG_DETAIL_PANS);

		return response;
	}

	@Override
	public TransactionModListDto lastestTransactions(String pan, String initialDay, String finalDay, int operationType, int emisorCode) {

		int codeResponse = 0;
		String description = null;
		List<TransactionModDto> list = null;
		int size = 0;
		String request = "pan [" + pan + "], initialDay [" + initialDay+ "], finalDay[" + finalDay + "]";

		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber(pan);

		if (cardholderDto != null) {

			if (cardholderDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD) {
				java.util.Date actualDate = null;
				java.util.Date lastDate = null;
				try{
				TreeMap<String, java.util.Date> map = com.syc.persistence.dao.impl.GeneralUtil.getPeriodTime();

				 actualDate = map.get("FechaInicial");
				/** ultima fecha disponible 3 meses atras **/
				 lastDate = map.get("FechaFinal");
				
				/**
				 * comparar la fecha final con la inicial que mandan del
				 * endpoint
				 **/
				java.util.Date firstDay = GeneralUtil.convertStringToSqlDate(initialDay, "dd/MM/yyyy");
				java.util.Date lastDay = GeneralUtil.convertStringToSqlDate(finalDay, "dd/MM/yyyy");

				if (firstDay.after(lastDate) && lastDay.after(lastDate)) {

					list = transactionDao.getTransactionsDateList(pan,firstDay, lastDay, operationType, emisorCode);

					if (list != null && list.size() > 0) {
						codeResponse = 1;
						size = list.size();
						description = "Movimientos Registrados.";
					} else {
						codeResponse = 0;
						description = "No se encontraron Movimientos con esta Tarjeta";
					}
				} else {
					codeResponse = 4;
					description = "Fechas fuera de rango";
				}
				}catch(Exception e){
					codeResponse = 5;
					description = "formato de fecha incorrecto";
				}
			} else {
				codeResponse = 2;
				description = "Tarjeta Inactiva";
			}
		} else {
			codeResponse = 3;
			description = "Tarjeta no registrada";
		}

		Date currentDate = GeneralUtil.getCurrentSqlDate();
		TransactionModListDto response = new TransactionModListDto();
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setLastestTransactions(list);
		response.setDate(GeneralUtil.formatDate( "dd/MM/yyyy" , currentDate));

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug("::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(pan.substring(0, 6), pan, SystemConstants.WS_USER_NAME,request, codeResponse, response.toString() + " registros["+ size+"]", SystemConstants.PCODE_LOG_TRANSACTIONS_PERIOD);

		return response;

	}
	
	@Override
	public TransactionsListResponseDto lastestTransactions(String pan, String initialDay, String finalDay, int operationType, int numMaxOfRows, int emisorCode) {

		int codeResponse = 0;
		String description = null;
		List<TransactionDto> list = null;
		int size = 0;
		String request = "pan [" + pan + "], initialDay [" + initialDay+ "], finalDay[" + finalDay + "]";

		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber(pan);

		if (cardholderDto != null) {

			if (cardholderDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD) {
				java.util.Date actualDate = null;
				java.util.Date lastDate = null;
				try{
				TreeMap<String, java.util.Date> map = com.syc.persistence.dao.impl.GeneralUtil.getPeriodTime();

				 actualDate = map.get("FechaInicial");
				/** ultima fecha disponible 3 meses atras **/
				 lastDate = map.get("FechaFinal");
				
				/**
				 * comparar la fecha final con la inicial que mandan del
				 * endpoint
				 **/
				java.util.Date firstDay = GeneralUtil.convertStringToSqlDate(initialDay, "dd/MM/yyyy");
				java.util.Date lastDay = GeneralUtil.convertStringToSqlDate(finalDay, "dd/MM/yyyy");

				if (firstDay.after(lastDate) && lastDay.after(lastDate)) {

					list = transactionDao.getTransactionsDateList(pan,firstDay, lastDay, operationType, numMaxOfRows, emisorCode);

					if (list != null && list.size() > 0) {
						codeResponse = 1;
						size = list.size();
						description = "Movimientos Registrados.";
					} else {
						codeResponse = 0;
						description = "No se encontraron Movimientos con esta Tarjeta";
					}
				} else {
					codeResponse = 4;
					description = "Fechas fuera de rango";
				}
				}catch(Exception e){
					codeResponse = 5;
					description = "formato de fecha incorrecto";
				}
			} else {
				codeResponse = 2;
				description = "Tarjeta Inactiva";
			}
		} else {
			codeResponse = 3;
			description = "Tarjeta no registrada";
		}

		Date currentDate = GeneralUtil.getCurrentSqlDate();
		TransactionsListResponseDto response = new TransactionsListResponseDto();
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setLastestTransactions(list);
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug("::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(pan.substring(0, 6), pan, SystemConstants.WS_USER_NAME,request, codeResponse, response.toString()+ " registros["+ size+"]", SystemConstants.PCODE_LOG_TRANSACTIONS_PERIOD);

		return response;

	}
	
	@Override
	public TransactionsListResponseDto lastestTransactions(String pan, String initialDay, String finalDay, int operationType, int numMaxOfRows, boolean isValid, int emisorCode) {

		int codeResponse = 0;
		String description = null;
		List<TransactionDto> list = null;

		String request = "pan [" + pan + "], initialDay [" + initialDay+ "], finalDay[" + finalDay + "]";

		CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber(pan);

		if (cardholderDto != null) {

			if (cardholderDto.getCard().getStatus() == SystemConstants.STATUS_ACTIVE_CARD || isValid ) {
				java.util.Date actualDate = null;
				java.util.Date lastDate = null;
				try{
				TreeMap<String, java.util.Date> map = com.syc.persistence.dao.impl.GeneralUtil.getPeriodTime();

				 actualDate = map.get("FechaInicial");
				/** ultima fecha disponible 3 meses atras **/
				 lastDate = map.get("FechaFinal");
				
				/**
				 * comparar la fecha final con la inicial que mandan del
				 * endpoint
				 **/
				java.util.Date firstDay = GeneralUtil.convertStringToSqlDate(initialDay, "dd/MM/yyyy");
				java.util.Date lastDay = GeneralUtil.convertStringToSqlDate(finalDay, "dd/MM/yyyy");

				if (firstDay.after(lastDate) && lastDay.after(lastDate)) {

					list = transactionDao.getTransactionsDateList(pan,firstDay, lastDay, operationType, numMaxOfRows, emisorCode);

					if (list != null && list.size() > 0) {
						codeResponse = 1;
						description = "Movimientos Registrados.";
					} else {
						codeResponse = 0;
						description = "No se encontraron Movimientos con esta Tarjeta";
					}
				} else {
					codeResponse = 4;
					description = "Fechas fuera de rango";
				}
				}catch(Exception e){
					codeResponse = 5;
					description = "formato de fecha incorrecto";
				}
			} else {
				codeResponse = 2;
				description = "Tarjeta Inactiva";
			}
		} else {
			codeResponse = 3;
			description = "Tarjeta no registrada";
		}

		TransactionsListResponseDto response = new TransactionsListResponseDto();
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setLastestTransactions(list);
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug("::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(pan.substring(0, 6), pan, SystemConstants.WS_USER_NAME,request, codeResponse, response, SystemConstants.PCODE_LOG_TRANSACTIONS_PERIOD);

		return response;

	}

	@Override
	public StatusResponseDto cardStatusDate(String pan, String user) throws Exception {
		
		StatusResponseDto response = new StatusResponseDto();
			BasicResponseDto status = cardStatus(pan, user);
			
			if(status.getCodigo() != 0){
				Object[] activity = cardActivityDao.findByPan(pan);
				
					response.setTrandate(GeneralUtil.formatDate("dd/MM/yyyy", (Date) activity[0]));
					response.setTrantime((String) activity[1]);
			}
			response.setCodigo(status.getCodigo());
			response.setDescripcion(status.getDescripcion());
			
			
			
		return response;
	}

}
