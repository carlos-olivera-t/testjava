package com.syc.services.monex;

import com.syc.dto.request.TransferRequestDto;
import com.syc.dto.response.TransferResponseDto;

public interface TransfersService {
	
	/**
	 * Metodo que realiza Transpasos entre cuentas Concentradoras y Tarjetas Asociadas
	 * activa la tarjeta destino en caso de ser la primer dispersion
	 * @param transferRequest
	 * @param emisor
	 * @return
	 */
	public TransferResponseDto onlineTransfers( TransferRequestDto transferRequest, int emisor );

}
