package com.syc.services;

import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.dto.response.BalanceQueryResponseDto;

public interface AkalaService {

	/**
	 * Servicio que invoca a Akala para obtener el saldo de una tarjeta
	 * @param pan
	 * @return
	 */
	public BalanceQueryResponseDto balanceQuery( String pan );
	
	/**
	 * Metodo que invoca a Akala para realizar un deposito x cuenta
	 * @param pan
	 * @param amount
	 * @param user
	 * @return
	 */
	public BalanceMovementRespDto doDepositAccount(String pan, double amount);
	
}
