package com.syc.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.response.BasicAcctResponseDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.LockClientRespDto;
import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dto.CardDto;
import com.syc.services.AccountService;
import com.syc.services.ClientService;
import com.syc.utils.GeneralUtil;

@Service
public class ClientServiceImpl implements ClientService{

	
	@Autowired
	CardDao cardDao;
	@Autowired
	AccountService accountService;
	
	@Override
	public LockClientRespDto temporaryLockClient(String bin, String idClient, int operationType, String user) {
		/**
		 * buscar el no de cliente y todas sus cuentas asociadas e ir mandando de una en una al de bloqueo de cuenta temporal
		 */
		int codeResponse = 0;
		LockClientRespDto response = new LockClientRespDto();
		List<BasicAcctResponseDto>  listResp = null;
		String description = "numero de cliente no encontrado";
		bin =  GeneralUtil.completaCadena(bin, '0', 10, "L");
		idClient = GeneralUtil.completaCadena(idClient, '0', 10, "L");
		List<CardDto> cards = cardDao.findIdClient(bin, idClient);
		
		if( cards != null && cards.size() > 0){
			 listResp = new ArrayList<BasicAcctResponseDto>();
			for(int i = 0; i< cards.size(); i++){
				
				 BasicAuthRespDto account = accountService.temporaryLockAccount(bin, cards.get(i).getAccount().substring(8).trim(), 1, user);
				 BasicAcctResponseDto resp = new BasicAcctResponseDto();
				 resp.setCode(account.getCode());
				 resp.setDescription(account.getDescription());
				 resp.setAuthorization(account.getAuthorization());
				 resp.setAccount(cards.get(i).getAccount().substring(8));
				 listResp.add(resp);
				 codeResponse = 1;
				 description = "cuentas encontradas";
				 

			}
		}
			
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setAccounts(listResp);
		
		return response;
	}

}
