/**********************************
 * Author: Angel Contreras Torres
 * Fecha : 4-Oct-2010
 * Descripci�n: Clase encargada de modificar 
 * 			      datos de las Tarjetas
 **********************************/
package com.syc.services;

import java.util.List;

import com.syc.dto.request.UpdateLimitCardDto;
import com.syc.dto.response.AccountResponseDto;
import com.syc.dto.response.BalanceCardsResponseDto;
import com.syc.dto.response.BalanceQueryResponseDto;
import com.syc.dto.response.BalanceQueryStatusResponseDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.BasicResponseDto;
import com.syc.dto.response.CardAccountResponseDto;
import com.syc.dto.response.CardLimitsResponseDto;
import com.syc.dto.response.FindPanResponseDto;
import com.syc.dto.response.IVRInfoClientResponse;
import com.syc.dto.response.IndividualLimitsDto;
import com.syc.dto.response.StatusResponseDto;
import com.syc.dto.response.TransactionModListDto;
import com.syc.dto.response.TransactionsListResponseDto;
import com.syc.dto.response.UserQueryResponseDto;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.CardLimitDto;
import com.syc.persistence.dto.SHCLogDto;
import com.syc.dto.response.BalanceQueryResponseSodexoDto;
import com.syc.exceptions.OasisException;

public interface CardService {
	
	/**
	 * metodo que devuelve el saldo de una lista de tarjetas, por el momento solo se usa
	 * para sodexo
	 * @param cardNumber
	 * @return
	 */
	public List<BalanceQueryResponseSodexoDto> BalanceQuery( List<String> cardNumber, String user, boolean isValidaBin );
	
	/***
	 * metodo que devuelve el saldo de una tarjeta y su estatus
	 * @param cardNumber
	 * @return
	 */
	public BalanceQueryStatusResponseDto balanceQueryStatus(String cardNumber, String user);
	
	/**
	 * Método que Activa una Tarjeta
	 * @param pan
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public BasicAuthRespDto activationCard( String pan, String user ) throws Exception;
	
	/**
	 * Método Invocado en la Activación Automática
	 * @param cardDto
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public BasicAuthRespDto activationCard( CardDto cardDto, String user ) throws Exception;
	/**
	 * Método Invocado en la Activación Automática para monex
	 * @param pan
	 * @param user
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public BasicAuthRespDto activationCard(String pan, String user, int code) throws Exception;
	
	
	/**
	 * Método que bloquea una Tarjeta
	 * @param pan
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public BasicAuthRespDto cardLock( String pan, String user ) throws Exception;
	
	/**
	 * Metodo que bloquea una tarjeta
	 * @param pan
	 * @param user
	 * @param status
	 * @return
	 * @throws Exception
	 */
	public BasicAuthRespDto cardLock( String pan, String user, int status ) throws Exception;
	/**
	 *  Método que bloquea y desbloquea una Tarjeta de manera Temporal
	 * @param pan
	 * @param operationType
	 * @return
	 * @throws Exception
	 */
	public BasicAuthRespDto temporaryLock(String pan, int operationType) throws Exception;	
	/**
	 * Metodo que bloquea y desbloquea una tarjeta de manera temporal 
	 * @param pan
	 * @param operationType
	 * @param user
	 * @return
	 * @throws Exception
	 */
	
	public BasicAuthRespDto temporaryLock(String pan, int operationType, String user) throws Exception;
	
	/**
	 * Desbloqueo de tarjetas x intentos de NIP invalidos
	 * @param pan
	 * @param operationType
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public BasicAuthRespDto unLockNIP(String pan, String user) throws Exception;
	
	/**
	 *  Método que Obtiene el Saldo de una Tarjeta.
	 * @param cardNumber
	 * @param isInactive
	 * @return
	 */
	public BalanceQueryResponseDto balanceQuery( String cardNumber, boolean isInactive, boolean isActivation, String user );
	
	/**
	 * Método que Obtiene el Saldo de una Tarjeta.
	 * @param cardNumber
	 * @return BalanceQueryResponseDto
	 */
	public BalanceQueryResponseDto balanceQuery( String cardNumber, String user);
	
	
	/**
	 * Método que Obtiene el Saldo de una cuenta.
	 * @param bin
	 * @param account
	 * @param user
	 * @return BalanceQueryStatusResponseDto
	 */
	public BalanceQueryStatusResponseDto balanceQueryAccount( String bin, String account , String user);
	
	/**
	 * Metodo que Obtiene el Saldo de una Tarjeta y cobra comision.
	 * @param pan
	 * @param commisionAmount
	 * @return BalanceQueryResponseDto
	 */
	public BalanceQueryResponseDto balanceQueryWhitCommision( String pan, double commisionAmount );
	
	/**
	 * Servicio que manda la peticion de consulta de saldo por cuenta o por tarjeta
	 * @param pan
	 * @param bin
	 * @param account
	 * @return
	 */
	public BalanceQueryResponseDto balanceQueryByAccountOrPan(String pan, String bin, String account, String user);
	/**
	 * Metodo que Obtiene el Estatus de una Tarjeta
	 * @param pan
	 * @return BasicResponseDto
	 * @throws Exception
	 */
	public BasicResponseDto cardStatus( String pan, String user ) throws Exception;
	
	/**
	 * Metodo que Obtiene el Estatus de una Tarjeta
	 * @param pan
	 * @return BasicResponseDto
	 * @throws Exception
	 */
	public StatusResponseDto cardStatusDate( String pan, String user ) throws Exception;
	
	/**
	 * Metodo que Obtiene el listado de transacciones de una Tarjeta
	 * @param pan
	 * @param numMaxOfRows
	 * @param operationType
	 * @return TransactionsListResponseDto
	 */
	public TransactionsListResponseDto lastestTransactions( String pan, int numMaxOfRows, int operationType, int emisorCode);
	
	/**
	 * Servicio que busca las ultimas transacciones de una tarjeta o cuenta
	 * @param pan
	 * @param bin
	 * @param account
	 * @param numMaxOfRows
	 * @param operationType
	 * @return
	 */
	public TransactionsListResponseDto getTransactionsByAccountOrPan( String pan, String bin, String account, int numMaxOfRows, int operationType, int emisorCode);
	
	/**
	 * Metodo que Obtiene el listado de transacciones de una Tarjeta
	 * se utiliza isapproved para mostrar solo las transacciones aprobadas
	 * @param pan
	 * @param numMaxOfRows
	 * @param operationType
	 * @param isapproved
	 * @return TransactionsListResponseDto
	 */
	public TransactionsListResponseDto lastestTransactions( String pan, int numMaxOfRows, int operationType, boolean isapproved, int emisorCode);

	/**
	 * Metodo que Obtiene el listado de transacciones de una Tarjeta en un periodo determinado
	 * @param pan
	 * @param period
	 * @param operationType
	 * @return TransactionsListResponseDto
	 */
	public TransactionsListResponseDto periodTransactions(String pan, int period, int operationType, int emisorCode);
	
	/**
	 * Metodo que obtiene los detalles de una tarjeta
	 * @param pan
	 * @param tranDate
	 * @param msgType
	 * @param authNum
	 * @return SHCLogDto
	 */
	public SHCLogDto getLogDetails(String pan, String tranDate, int msgType, String  authNum );
	
	/**
	 * Metodo que obtiene los limites individuales de determinada tarjeta
	 * @param pan
	 * @return CardLimitDto
	 */
	public CardLimitsResponseDto getLimitsDetails(String pan, String user);
	
	/**
	 * Servicio que crea o actualiza los limites individuales de una tarjeta
	 * @param pan
	 * @param limits
	 * @return
	 */
	public BasicAuthRespDto updateCardLimit(String pan, IndividualLimitsDto limits, String user);
	
	/**
	 * Servicio que se encarga de borrar los limites individuales de una tarjeta
	 * @param pan
	 * @return
	 */
	public BasicAuthRespDto deleteCardLimit(String pan, String ser);
	
	/**
	 * Servicio que tiene como proposito realizar la busqueda de un tarjeta-habiente mediante su nombre
	 * o numero de tarjeta
	 * @param user
	 * @param pan
	 * @param webUser
	 * @return UserQueryResponseDto
	 */
	public UserQueryResponseDto findByUserOrPan( String pan, String webUser, String user, String bin);
	
	/**
	 * Servicio que realiza una busqueda de tarjetas asociadas a una cuenta
	 * @param account
	 * @param bin
	 * @param user
	 * @return CardAccountResponseDto
	 */
	public CardAccountResponseDto getCardsByAccount(String account, String bin, String user);
	
	/**
	 * Servicio que tiene como proposito realizar la busqueda de un tarjeta-habiente mediante su cuenta
	 * o numero de tarjeta. Si la la busqueda es por cuenta, sera necesario recibir el BIN
	 * @param pan
	 * @param account
	 * @param bin
	 * @return IVRInfoClientResponse
	 */
	public IVRInfoClientResponse findIVRCardInfo(String pan, String account, String bin);
	
	/**
	 * Servicio que tiene como proposito la consulta de una cuenta por tarjeta
	 * @param pan
	 * @return AccountResponseDto
	 */
	public AccountResponseDto findAccountByPan(String pan, String user);
	/**
	 * Valida que una tarjeta exista y este activa, si es titular devuelve todas las tarjetas asociadas
	 * @param pan
	 * @param bornDay
	 * @param cvv2
	 * @param expirationDate
	 * @param rfc
	 * @param user
	 * @return
	 * @throws OasisException 
	 */
	public FindPanResponseDto findDetailPan(String pan, String cvv2, String expirationDate, String rfc, String user) throws OasisException;
	/**
	 * Devuelve el saldo, en caso de que sea titular devuelve tambien las tarjetas adicionales
	 * @param pan
	 * @param user
	 * @return
	 */
	public BalanceCardsResponseDto balanceQueryCards( String pan, String user);
	
	/**
	 * Devuelve un listado de transacciones por fecha de inicio y fin, maximo 3 meses
	 * @param pan
	 * @param initialDay
	 * @param finalDay
	 * @return
	 */
	public TransactionModListDto lastestTransactions(String pan, String initialDay, String finalDay, int operationType, int emisorCode);
	
	/**
	 * Devuelve un listado de transacciones por fecha de inicio y fin, maximo 3 meses y un numero determinado
	 * de transacciones
	 * @param pan
	 * @param initialDay
	 * @param finalDay
	 * @param operationType
	 * @param numMaxOfRows
	 * @return
	 */
	public TransactionsListResponseDto lastestTransactions(String pan, String initialDay, String finalDay, int operationType, int numMaxOfRows, int emisorCode);
	
	/**
	 * Devuelve un listado de transacciones por fecha de inicio y fin, maximo 3 meses y un numero determinado
	 * de transacciones
	 * @param pan
	 * @param initialDay
	 * @param finalDay
	 * @param operationType
	 * @param numMaxOfRows
	 * @param isValid  --> si es true devuelve las transacciones sin importar el estatus de la tarjeta 
	 * @return
	 */
	public TransactionsListResponseDto lastestTransactions(String pan, String initialDay, String finalDay, int operationType, int numMaxOfRows, boolean isValid, int emisorCode);
	
	
}
