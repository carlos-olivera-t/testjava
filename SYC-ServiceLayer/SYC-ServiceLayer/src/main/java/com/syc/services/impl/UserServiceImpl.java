package com.syc.services.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.persistence.dao.UserBinDao;
import com.syc.services.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	
	@Autowired
	private UserBinDao userBinDao;
	private static final transient Log log = LogFactory.getLog(UserServiceImpl.class);

	@Override
	public boolean getBinesPermitidos(String userName, String bin) {
		
		List<String> binesPermitidos = userBinDao.getBinesByUser(userName);

		boolean isPermitido = false;

		for (int i = 0; i < binesPermitidos.size(); i++) {
			if (binesPermitidos.get(i).equals(bin)) {
				isPermitido = true;
				break;
			}
		}
		
		return isPermitido;
	}

}
