package com.syc.services.monex;

import com.syc.dto.response.AccountStatementResponseDto;

public interface AccountStatementService {
	
	/********************************************
	 * Interfaz para obtener los estados de  
	 *              Cuenta
	 * 
	 * Autor: Angel Contreras Torres.
	 * Fecha: 29/12/2011
	 ********************************************/
	
	public AccountStatementResponseDto getPostgresAccountStatement( String employer, String product );
	
	public AccountStatementResponseDto monexAccountStatement( String employer, String product );

}
