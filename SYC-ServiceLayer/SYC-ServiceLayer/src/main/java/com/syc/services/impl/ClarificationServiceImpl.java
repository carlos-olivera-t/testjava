package com.syc.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.request.ClarificationRequestDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.BasicFolioResponseDto;
import com.syc.persistence.dao.AclaracionesDao;
import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.AclaracionDto;
import com.syc.services.ClarificationService;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SystemConstants;
import com.syc.utils.WSLogUtil;

@Service
public class ClarificationServiceImpl implements ClarificationService {
	
	
	@Autowired
	AclaracionesDao aclaracionesDao;
	@Autowired
	WSLogDao wsLogDao;
	WSLogUtil wsUtil;

	public static final int NEW_CLARIFICATION = 0;
	public static final int END_CLARIFICATION = 1;

	@Override
	public BasicFolioResponseDto createClarification(ClarificationRequestDto request, String user) {
		int codeResponse = 1;
		String description = "aclaración agregada exitosamente";
		String bin = request.getPan().substring(0, 6);
		long folio = 0;
		AclaracionDto aclaracion = new AclaracionDto();
		int num = GeneralUtil.convertStringToInt(request.getpCode());
		if( num != -1){
			
			aclaracion.setPan(request.getPan());
			aclaracion.setPcode(request.getpCode());
			aclaracion.setMonto(request.getAmount());
			aclaracion.setFecha(GeneralUtil.convertStringToSqlDate(request.getTrandate(), "dd/MM/yyyy"));
			aclaracion.setAutorizacion( request.getAuthorization() );
			aclaracion.setComercio(request.getCommerce());
			aclaracion.setStatus(NEW_CLARIFICATION);
			aclaracion.setFechaAlta(GeneralUtil.getCurrentSqlDate());
			folio = aclaracion.getFolio();
			aclaracionesDao.create(aclaracion);
		}else{
			codeResponse = 2;
			description = "pCode debe ser numerico";
		}
		BasicFolioResponseDto response = new BasicFolioResponseDto();
		response.setCode(codeResponse);
		response.setDescription(description);
		response.setFolio(folio);
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(GeneralUtil.completaCadena(bin,'0',10,"L"), request.getPan(), user, request.toString(), response.getCode(), response, SystemConstants.PCODE_NEW_CLARIFICATION);
		
		
		return response;
	}

	@Override
	public BasicAuthRespDto endClarification(long folio, String user) {
		int codeResponse = 0;
		String authorization = "0";
		String description = "aclaración no encontrada";
		String pan = null;
		String bin = null;
		String request = "folio["+folio+"]user["+user+"]";
		AclaracionDto aclaracion = aclaracionesDao.findByFolio(folio);
		if(aclaracion != null){
			if(aclaracion.getStatus() == NEW_CLARIFICATION){
				aclaracion.setFechaModificacion(GeneralUtil.getCurrentSqlDate());
				aclaracion.setStatus(END_CLARIFICATION);
				pan = aclaracion.getPan();
				bin = GeneralUtil.completaCadena(pan.substring(0,6),'0',10,"L");
				aclaracionesDao.update(aclaracion);
				authorization = GeneralUtil.generateAuthorizationNumber(6);
				codeResponse = 1;
				description = "aclaración cerrada con éxito";
			}else{
				codeResponse = 3;//la aclaración ya fue cerrada con anterioridad
				description = "aclaración ya fue cerrada con anterioridad";
			}
		}else
			codeResponse = 2;//aclaracion no encontrada
		
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setAuthorization(authorization);
		response.setCode(codeResponse);
		response.setDescription(description);
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(bin, pan, user, request, response.getCode(), response, SystemConstants.PCODE_END_CLARIFICATION);
		
		return response;
	}

}
