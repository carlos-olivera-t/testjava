package com.syc.services;

import java.util.List;

import com.syc.dto.request.DemographicDataRequestDto;
import com.syc.dto.request.NewEmployeeRequestDto;
import com.syc.dto.response.CardAssignmentResponseDto;
import com.syc.dto.response.ClientAccountResponseDto;

public interface CardIssuanceService {
	
	/**
	 * Servicio de proposito General que se encarga de Asignar una tarjeta Embosada TITULAR  
	 * @param bin
	 * @param product => producto de la tarjeta
	 * @param account => numero de cuenta
	 * @param demographicRequestDto => datos demograficos
	 * @param idClient => numero de cliente (base)
	 * @return
	 */
	CardAssignmentResponseDto embosserCardAssignment( String bin, String product, String account, DemographicDataRequestDto demographicRequestDto, String idClient);
	/**
	 * Servicio de proposito General que se encarga de Asignar una tarjeta Embosada TITULAR  
	 * @param bin Bin al que pertenece la tarjeta
	 * @param product El tipo de producto
	 * @param account El numero de cuenta al cual asiganar la tarjeta
	 * @param demographicRequestDto Datos demograficos de la tarjeta
	 * @return
	 */
	CardAssignmentResponseDto embosserCardAssignment( String bin, String product, String account, DemographicDataRequestDto demographicRequestDto);
	
	/**
	 * Proceso que genera una cuenta concentradora y una clabe
	 * @param bin
	 * @param product
	 * @param demographicRequestDto
	 * @param idBanco
	 * @param plazaBanjico
	 * @return
	 */
	ClientAccountResponseDto accountAssignment( String bin, String product, String phoneCode, String idClient, DemographicDataRequestDto demographicRequestDto, String idBanco, String plazaBanjico);

	
	/**
     * Servicio de proposito General que se encarga de Asignar una tarjeta Embosada ADICONAL  
	 * @param aditionalCardRequestDto Dto que contiene los datos para generar adicional
	 * @return
	 */
	CardAssignmentResponseDto aditionalCardAssignment(AditionalCardRequestDto aditionalCardRequestDto);
	
	
	/**
	 * Servicio de proposito General que se encarga de remplazar una tarjeta Embosada TITULAR/ADICONAL
	 * @param lockedCard => tarjeta bloqueada
	 * @return
	 */
	CardAssignmentResponseDto replaceCard(String lockedCard);
	
	/************************************************************************** 
	 * Servicio de proposito General que se encarga de Asignar una tarjeta
	 *   de STOCK TITULAR  
	 **************************************************************************/
	
		
	/**
	 * Servicio de proposito General que se encarga de Asignar una tarjeta de STOCK TITULAR  
	 * @param pan
	 * @param demographicRequestDto => datos demograficos del tarjeta-habiente
	 * @param accountFlag => nos permite saber si la cuenta la generamos nosotros(true) o tomamos la que ya existe(false)
	 * @param product
	 * @param idClient
	 * @param isActive  => nos permite saber si tenemos que activar la tarjeta (true)
	 * @param isFunction => nos permite saber si se va a realizar el proceso de asignacion de empleadora(true)
	 * @param employee  => datos de la empleadora para poder dar de alta el registro en tbl_empleados
	 * @param isSodexo => true si se trata de sodexo y se va a buscar que exista el codigo de direccion en tbl_empleadoras_detail
	 * @return
	 */
	CardAssignmentResponseDto stockCardAssignment( String account, String pan, DemographicDataRequestDto demographicRequestDto, boolean accountFlag, String product, String idClient, boolean isActive, boolean isFunction, NewEmployeeRequestDto employee, boolean isSodexo);
	/**
	 * Servicio de proposito General que se encarga de Asignar una tarjeta de STOCK TITULAR  
	 * @param account
	 * @param pan
	 * @param demographicRequestDto => Datos demograficos de la tarjeta
	 * @param accountFlag => nos permite saber si la cuenta la generamos nosotros(true) o tomamos la que ya existe(false); true si la cuenta la maneja el cliente y false si la cuenta la manejamos nosotros
	 * @param isActivation => nos permite saber si tenemos que activar la tarjeta (true)
	 * @param isFunction => nos permite saber si se va a realizar el proceso de asignacion de empleadora(true)
	 * @param employee  => datos de la empleadora para poder dar de alta el registro en tbl_empleados
	 * @param isSodexo => true si se trata de sodexo y se va a buscar que exista el codigo de direccion en tbl_empleadoras_detail
	 * @return
	 */
	CardAssignmentResponseDto stockCardAssignment( String account, String pan, DemographicDataRequestDto demographicRequestDto, boolean accountFlag, boolean isActivation, boolean isFunction, NewEmployeeRequestDto employee, boolean isSodexo);
	/**
	 * Servicio de proposito General que se encarga de Asignar una tarjeta de STOCK TITULAR
	 * y clabe interbancaria
	 * @param pan
	 * @param demographicRequestDto
	 * @param accountFlag  => nos permite saber si la cuenta la generamos nosotros(true) o tomamos la que ya existe(false)
	 * @param product
	 * @param idBanco => clave del banco que se utiliza para generar la CLABE
	 * @param plazaBanjico => numero de plaza banjico que fue asignada, se utiliza también para generar la CLABE
	 * @return
	 */
	ClientAccountResponseDto stockCardAssignment( String pan, String idClient, String codePhone, DemographicDataRequestDto demographicRequestDto, boolean accountFlag, String product, String idBanco, String plazaBanjico);
	/**
	 * Servicio de proposito General que se encarga de Asignar una tarjeta de STOCK ADICIONAL
	 * @param account El numero de cuenta
	 * @param pan El numero de tarjeta ADICONAL a asignar
	 * @return
	 */
	CardAssignmentResponseDto stockCardAssignment( String account, String pan );
	/**
	 * Servicio de proposito General que se encarga de Asignar una tarjeta de STOCK ADICIONAL
	 * @param account El numero de cuenta
	 * @param pan El numero de tarjeta ADICONAL a asignar
	 * @param aditionalCardRequestDto (name, shortName, rfc)
	 * @return
	 */
	
	CardAssignmentResponseDto stockCardAssignment( String account, String pan, AditionalCardRequestDto aditionalCardRequestDto );
	/**
	 * Servicio de proposito General que se encarga de Asignar una tarjeta de STOCK ADICIONAL
	 * @param account El numero de cuenta
	 * @param pan El numero de tarjeta ADICONAL a asignar
	 * @param aditionalCardRequestDto (name, shortName, rfc)
	 * @param account flag true si la cuenta la maneja el cliente y false si la cuenta la manejamos nosotros
	 * @return
	 */
	CardAssignmentResponseDto stockCardAssignment( String account, String pan, AditionalCardRequestDto aditionalCardRequestDto, boolean accountFlag );
	
	/** 
	 * Servicio generado para reemplazar una tarjeta de stock ya sea titular o adicional
	 * @param pan
	 * @param account
	 * @param lockedCard
	 * @param demographicRequestDto
	 * @param accountFlag   true si la cuenta la maneja el cliente, false si la cuenta la manejamos nosotros
	 * @throws Exception 
	 **/
	CardAssignmentResponseDto stockCardReplacement( String account, String pan, String lockedCard, DemographicDataRequestDto demographicRequestDto, boolean accountFlag,  boolean activation, int issCode, boolean isFunction) throws Exception;
	
	/**
	 * Servicio de proposito General que se encarga de Reemplzar una tarjeta de STOCK
	 * en donde el cliente maneja las cuentas
	 * @param account
	 * @param pan
	 * @param lockedCard
	 * @return
	 * @throws Exception 
	 */
	@Deprecated
	public CardAssignmentResponseDto stockCardReplacement( String account, String pan, String lockedCard) throws Exception;

	/**
	 * * Servicio de proposito General que se encarga de Reemplzar una tarjeta de STOCK
	 * en donde el cliente maneja las cuentas
	 * @param account
	 * @param pan
	 * @param lockedCard
	 * @param accountFlag  => nos permite saber si la cuenta la generamos nosotros(true) o tomamos la que ya existe(false)
	 * @param activation => nos permite saber si se activará la tarjeta (true)
	 * @param issCode  => numero que identifica a cada empresa, se utiliza en el proceso de asignacion de empleadora
	 * @param isFunction => nos permite saber si se va a realizar el proceso de asignacion de empleadora(true)
	 * @return
	 * @throws Exception
	 */
	public CardAssignmentResponseDto stockCardReplacement( String account, String pan, String lockedCard, boolean accountFlag, boolean activation, int issCode, boolean isFunction) throws Exception;

	
	public CardAssignmentResponseDto stockCardReplacement( String account, String pan, String lockedCard, boolean accountFlag, boolean activation) throws Exception;
	/**
	 * Servicio que se encarga de realizar validaciones para poder hacer un reemplazo stock
	 * @param account
	 * @param pan
	 * @param lockedCard
	 * @param product
	 * @param activation => nos permite saber si se activará la tarjeta (true)
	 * @return
	 * @throws Exception
	 */
	public CardAssignmentResponseDto stockCardReplacement( String account,String pan, String lockedCard, DemographicDataRequestDto demographicRequestDto, boolean accountFlag, boolean activation ) throws Exception;

	/**
	 * Genera un rango de tarjetas del bin y producto indicado
	 * @param bin
	 * @param product
	 */
	List<String>  generateCardRange(String bin, String product, int totalCards);

}
