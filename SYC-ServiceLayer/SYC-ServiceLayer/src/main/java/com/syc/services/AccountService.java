package com.syc.services;

import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.CardsBalanceAccountResponseDto;

public interface AccountService {
	
	/**
	 * Servicio que bloquea una cuenta
	 * @param account
	 * @param bin
	 * @return BasicAuthRespDto
	 * @throws Exception
	 */
	BasicAuthRespDto accountLock( String account, String bin) throws Exception ;
	
	/**
	 * Servicio que realiza un cargo o abono dependiendo del id que se mande
	 * @param id
	 * @param bin
	 * @param account
	 * @param amount
	 * @param user
	 * @return BasicAuthRespDto
	 * @throws Exception 
	 */
	BalanceMovementRespDto doAdjustment(String id, String bin, String account,  double amount, String user) throws Exception;
	
	/**
	 * Servicio que bloquea temporalmente una cuenta
	 * @param bin
	 * @param account
	 * @param operationType
	 * @param user
	 * @return
	 */
	BasicAuthRespDto temporaryLockAccount(String bin, String account, int operationType, String user);
	
	/**
	 * Servicio que realiza una busqueda de tarjetas asociadas a una cuenta y devuelve su saldo
	 * @param account
	 * @param bin
	 * @param user
	 * @return CardAccountResponseDto
	 */
	public CardsBalanceAccountResponseDto getCardsBalanceByAccount(String account, String bin, String user);
	

}
