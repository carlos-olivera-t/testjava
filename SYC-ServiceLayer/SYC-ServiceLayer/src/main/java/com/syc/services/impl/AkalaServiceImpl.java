package com.syc.services.impl;

import java.rmi.RemoteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.dto.response.BalanceQueryResponseDto;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.CardholderDto;
import com.syc.services.AkalaService;
import com.syc.services.CardService;
import com.syc.services.LogService;
import com.syc.utils.CatalogUtil;
import com.syc.utils.SecurityUtils;
import com.syc.utils.SystemConstants;
import com.syc.ws.client.akala.AkalaWebService;
import com.syc.ws.client.akala.IModuleServiceOperacion_Autorizacion_ServiceFaultFault_FaultMessage;
import com.syc.ws.client.akala.IModuleServiceOperacion_Saldo_ServiceFaultFault_FaultMessage;
import com.syc.ws.client.akala.impl.BalanceDto;
import com.syc.ws.client.akala.impl.SaldoDto;

@Service
public class AkalaServiceImpl implements AkalaService {

	private static final transient Log log = LogFactory.getLog(AkalaServiceImpl.class);

	@Autowired
	AkalaWebService akalaWebService;

	@Autowired
	WSLogDao wsLogDao;

	@Autowired
	LogService logService;

	@Autowired
	CardholderDao cardHolderDao;

	@Autowired
	CardService cardServiceDao;

	public BalanceQueryResponseDto balanceQuery(String pan) {
		final String WS_USER = SecurityUtils.getCurrentUser();

		SaldoDto wsResponseDto;
		int codeResponse = 0;

		try {
			wsResponseDto = akalaWebService.saldo(pan);
		} catch (RemoteException e) {			
			logService.saveLog("0", pan, WS_USER, "pan[" + pan + "]",-1, "AkalaRemoteException: "+e.getMessage(),SystemConstants.PCODE_WS_REQ_AKALA);									
			return errorResponseBalance(CatalogUtil.getAkalaSaldo().get(codeResponse));			
		} catch (IModuleServiceOperacion_Saldo_ServiceFaultFault_FaultMessage e) {
			logService.saveLog("0", pan, WS_USER, "pan[" + pan + "]",-1, "AkalaServiceAkalaException: "+e.getMessage(),SystemConstants.PCODE_WS_REQ_AKALA);									
			return errorResponseBalance(CatalogUtil.getAkalaSaldo().get(codeResponse));			
		}

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug("::Almacenando Respuesta de akala ::");
		logService.saveLog("0", pan, WS_USER, "pan[" + pan + "]",wsResponseDto.getStatus(),"AkalaResponse:" + wsResponseDto.toString(),SystemConstants.PCODE_WS_REQ_AKALA);

		codeResponse = wsResponseDto.getStatus();

		/*************************************
		 * Generando Respuesta a Devolver
		 *************************************/
		BalanceQueryResponseDto response = new BalanceQueryResponseDto();
		response.setCode(codeResponse);
		response.setAvailableAmount(wsResponseDto.getBalance());
		response.setDescription(CatalogUtil.getAkalaSaldo().get(codeResponse));

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug("::Almacenando Movimiento en Bitacora::");
		logService.saveLog("0", pan, WS_USER, "pan[" + pan + "]",response.getCode(), "SYCResponse:" + response.toString(),SystemConstants.PCODE_BALANCE_QUERY_CASH_AME);

		
		return response;
	}

	public BalanceMovementRespDto doDepositAccount(String pan, double amount) {
		final String WS_USER = SecurityUtils.getCurrentUser();

		BalanceMovementRespDto oBalanceMovementRespDto = new BalanceMovementRespDto();
		int codeResponse = 0;
		
			BalanceDto oBalanceDto;
			try {
				oBalanceDto = akalaWebService.autorizacion(pan, amount);
				
				/**************************************************
				 * Almacenando Transaccion en Bitacora del WS
				 **************************************************/
				log.debug("::Almacenando Respuesta de akala ::");
				logService.saveLog("0", pan, WS_USER,  "pan[" + pan + "] amount[" + amount + "]"  ,oBalanceMovementRespDto.getCode(),"AkalaResponse:" + oBalanceDto.toString(),SystemConstants.PCODE_DEPOSIT_AKALA);
				
				oBalanceMovementRespDto.setAuthorization(oBalanceDto.getNumeroAutorizacion() + "");
				oBalanceMovementRespDto.setBalance(oBalanceDto.getMonto());
				oBalanceMovementRespDto.setCurrentBalance(oBalanceDto.getMonto());

				codeResponse = oBalanceDto.getCodigoEstado();
				oBalanceMovementRespDto.setDescription(CatalogUtil.getAkalaDepositAccount().get(codeResponse));
				oBalanceMovementRespDto.setCode(codeResponse);

				/** Obteniendo la informacion de la Cuenta **/
				CardholderDto cardholderDto = cardHolderDao.getCardholderInfoByCardNumber(pan);
				CardDto cardDto = cardholderDto.getCard();

				/** Si la tarjeta esta inactiva la damos de alta */
				if (cardDto.getStatus() == SystemConstants.STATUS_INACTIVE_CARD) {

					/** Solo si la respuesta del servicio de akala es OK */
					if (codeResponse == 1) {
						cardServiceDao.activationCard(cardDto, WS_USER);
					}

				}

				
			} catch (RemoteException e) {
				logService.saveLog("0", pan, WS_USER, "pan[" + pan + "] amount[" + amount + "]"  ,-1,"AkalaRemoteException: "+e.getMessage(),SystemConstants.PCODE_DEPOSIT_CASH_AME_EXC);
				return errorResponseDeposit(CatalogUtil.getAkalaSaldo().get(codeResponse));									
			} catch (IModuleServiceOperacion_Autorizacion_ServiceFaultFault_FaultMessage e) {
				logService.saveLog("0", pan, WS_USER,  "pan[" + pan + "] amount[" + amount + "]"  ,-1,"AkalaServiceAkalaException: "+e.getMessage(),SystemConstants.PCODE_DEPOSIT_CASH_AME_EXC);
				return errorResponseDeposit(CatalogUtil.getAkalaSaldo().get(codeResponse));									
			} catch (Exception e) {
				logService.saveLog("0", pan, WS_USER,  "pan[" + pan + "] amount[" + amount + "]"  ,-1,"GenericException: "+e.getMessage(),SystemConstants.PCODE_DEPOSIT_CASH_AME_EXC);
				return errorResponseDeposit(CatalogUtil.getAkalaSaldo().get(codeResponse));									
			}
			
		

		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/

		log.debug("::Almacenando Movimiento en Bitacora::");
		logService.saveLog("0", pan, WS_USER,  "pan[" + pan + "] amount[" + amount + "]"  ,oBalanceMovementRespDto.getCode(),"SYCResponse:" + oBalanceMovementRespDto.toString(),SystemConstants.PCODE_DEPOSIT_CASH_AME);

		return oBalanceMovementRespDto;
	}

	
	private BalanceQueryResponseDto errorResponseBalance(String description){
		BalanceQueryResponseDto errorResponse = new BalanceQueryResponseDto();
		errorResponse.setCode(-1);
		errorResponse.setDescription(description);
		errorResponse.setAvailableAmount(0);
		
		return errorResponse;
	}
	
	private BalanceMovementRespDto errorResponseDeposit(String description){
		BalanceMovementRespDto errorResponse = new BalanceMovementRespDto();
		errorResponse.setCode(-1);
		errorResponse.setDescription(description);
		errorResponse.setBalance(0);
		errorResponse.setCurrentBalance(0);
		
		return errorResponse;
	}
}
