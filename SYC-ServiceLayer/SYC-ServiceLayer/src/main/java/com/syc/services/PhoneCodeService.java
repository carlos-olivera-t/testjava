package com.syc.services;

import com.syc.dto.response.BasicResponseDto;

public interface PhoneCodeService {

		/**
		 * Creacion de codigo telefonico
		 * @param pan
		 * @param account
		 * @param nip
		 * @return
		 */
		BasicResponseDto createPhoneCode(String pan, String account, String nip);	
		/**
		 * valida que un codigo telefonico sea correcto
		 * @param pan
		 * @param account
		 * @param nip
		 * @param nipFlag
		 * @return
		 */
		BasicResponseDto validatePhoneCode(String pan, String account, String nip, boolean nipFlag );	
		/**
		 * realiza el cambio de codigo telefonico
		 * @param pan
		 * @param account
		 * @param oldNip
		 * @param newNip
		 * @param nipFlag
		 * @return
		 */
		BasicResponseDto changePhoneCode( String pan, String account, String oldNip, String newNip, boolean nipFlag );
		/**
		 * Devuelve el estatus actual de un codigo telefonico
		 * @param pan
		 * @param account
		 * @param operation
		 * @return
		 */
		BasicResponseDto changeStatus( String pan, String account, int operation);
		
}


