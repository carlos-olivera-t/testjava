package com.syc.services;

import com.syc.dto.request.ClarificationRequestDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.BasicFolioResponseDto;

public interface ClarificationService {
	
	/**
	 * Servicio que crea una aclaracion
	 * @param request
	 * @param user
	 * @return
	 */
	BasicFolioResponseDto createClarification(ClarificationRequestDto request, String user);
	
	/**
	 * Servicio que pone estatus de aclaracion como terminada
	 * @param folio
	 * @param user
	 * @return
	 */
	BasicAuthRespDto endClarification(long folio, String user);

}
