/************************************************
 * Author: Angel Contreras Torres
 * Fecha : 12-Enero-2011
 * Descripción: Clase encargada de modificar 
 * 			    información del Tarjetahabiente
 ************************************************/
package com.syc.services;

import java.util.Map;

import com.syc.dto.request.BitMatchInfoClientRequestDto;
import com.syc.dto.request.CardholderInfoRequestDto;
import com.syc.dto.request.DemographicDataRequestDto;
import com.syc.dto.response.AddresRespDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.BasicDateResponseDto;
import com.syc.dto.response.CardAssignmentResponseDto;
import com.syc.persistence.dto.CatBinDto;

/*************************************************
 *   Interfaz para los servicios relacionados
 * a el alta datos Generales de un Tarjetahabiente 
 * 
 * Autor: Angel Contreras Torres.
 * Fecha: 9/11/2011
 *************************************************/
public interface CardHolderService {
	
	/***************************************************** 
	 * Servicio que almacena informacion del cliente 
	 * para una posterior asociacion en el Cardman-Web  
	 *****************************************************/
	public BasicAuthRespDto saveInformationClientBitMatch(BitMatchInfoClientRequestDto clientInfoReq, String emisor);
	
	/** Servicio que asocia una tarjeta de Stock a un Tarjetahabiente con una cuenta especifica **/
	public BasicAuthRespDto assignmentAccount(String pan, BitMatchInfoClientRequestDto clientInfoReq);
	
	/** Servicio que carga los datos generales de un tarjetahabiente **/
	public BasicAuthRespDto cardholderRegistration( CardholderInfoRequestDto cardHolderInfo );
	
	/** Servicio que obtiene los datos generales **/
	public AddresRespDto cardholderInfo(String pan);
	
	/************************************************************************** 
	 * Servicio de proposito General que se encarga de Asignar una tarjeta
	 *   de STOCK TITULAR  
	 **************************************************************************/
	
	/**
	 * Método que guarda datos demograficos 
	 * @param addressDto
	 * @return
	 */
	BasicDateResponseDto updateDemographicData(String pan, DemographicDataRequestDto demographicDataRequestDto);
	
	/**
	 * Metodo que guarda datos demograficos 
	 * @param bin
	 * @param idCLient
	 * @param addressDto
	 * @return
	 */
	public BasicDateResponseDto updateDemographicDataClient(String bin, String idClient, DemographicDataRequestDto demographicDataRequestDto);
	
	/**
	 * Obtine los datos de las tablas ISTREL, ISTCARD, ITDBACCT
	 * @param pan El numero de tarjeta
	 * @return
	 */
	Map<String,Object> getCardHolderInfo(String pan);
	
	
	
	CatBinDto getProductProperties(String bin, String producto);
}
