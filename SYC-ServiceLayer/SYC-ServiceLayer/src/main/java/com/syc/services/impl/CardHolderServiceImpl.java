package com.syc.services.impl;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.syc.dto.request.BitMatchInfoClientRequestDto;
import com.syc.dto.request.CardholderInfoRequestDto;
import com.syc.dto.request.DemographicDataRequestDto;
import com.syc.dto.response.AddresRespDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.BasicDateResponseDto;
import com.syc.dto.response.CardAssignmentResponseDto;
import com.syc.persistence.dao.AccountDao;
import com.syc.persistence.dao.AddressDao;
import com.syc.persistence.dao.BitacoraMatchDao;
import com.syc.persistence.dao.CardActivityDao;
import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dao.CatBinDao;
import com.syc.persistence.dao.EmbosserPropertiesDao;
import com.syc.persistence.dao.EmisorDao;
import com.syc.persistence.dao.IstcmscardDao;
import com.syc.persistence.dao.IstrelDao;
import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.AccountDto;
import com.syc.persistence.dto.AddressDto;
import com.syc.persistence.dto.BitacoraMatchDto;
import com.syc.persistence.dto.CardActivityDto;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.CardholderDto;
import com.syc.persistence.dto.CatBinDto;
import com.syc.persistence.dto.CatBinDto.CatBinPk;
import com.syc.persistence.dto.EmbosserPropertiesDto;
import com.syc.persistence.dto.EmisorDto;
import com.syc.persistence.dto.IstcmscardDto;
import com.syc.persistence.dto.IstrelDto;
import com.syc.services.CardHolderService;
import com.syc.utils.CatalogUtil;
import com.syc.utils.GeneralUtil;
import com.syc.utils.PropertiesUtil;
import com.syc.utils.SecurityUtils;
import com.syc.utils.SystemConstants;
import com.syc.utils.SystemMesages;
import com.syc.utils.WSLogUtil;

@Repository
public class CardHolderServiceImpl implements CardHolderService{
	
	private static final String DUMMY_ACCOUNT = "9999999";
	
//	@SuppressWarnings("unused")
	private static final transient Log log = LogFactory.getLog(CardHolderServiceImpl.class);
	
	@Autowired
	private BitacoraMatchDao bitacoraMatchDao;
	
	@Autowired
	private IstcmscardDao    istcmscardDao; 
	
	@Autowired
	private AddressDao       addressDao;
	
	@Autowired
	private CardholderDao    cardholderDao;
	
	@Autowired
	private CardDao          cardDao;
	
	@Autowired
	private AccountDao       accountDao;
	
	@Autowired
	private IstrelDao        istrelDao;
	
	@Autowired
	private CardActivityDao cardActivityDao;
	
	@Autowired
	private WSLogDao        wsLogDao;
	
	@Autowired
	private EmisorDao 		emisorDao;
	
	@Autowired
	private EmbosserPropertiesDao embosserPropertiesDao;
	
	@Autowired
	private CatBinDao catBinDao; 
	
	
	WSLogUtil wsUtil;
	
	public BasicAuthRespDto saveInformationClientBitMatch( BitMatchInfoClientRequestDto clientInfoReq, String emisor ) {
		
		final String CUENTA_ASOCIADA  = "B";
		final String CUENTA_CANCELADA = "C";
		
		int    codeResponse  = 0;
		String description   = "Los Datos no fueron Almacenados Correctamente";
		String authorization = GeneralUtil.generateAuthorizationNumber(6);
		
		
		
		String account = GeneralUtil.completaCadena(clientInfoReq.getCuenta(), '0', 11, "L");
		
		/** Localizando cuentas existentes **/
		BitacoraMatchDto bitacoraDto = bitacoraMatchDao.findByAccount( account, emisor );
		
		if( bitacoraDto!=null ){
			/** Actualizando los datos de la Solicitud **/
			
			
			/** Procesando actualización con cuenta Asociada **/
			if( bitacoraDto.getStatusCuenta().equals(CUENTA_ASOCIADA) ){
				
				/** Cancelando Cuenta Actual**/
				bitacoraDto.setStatusCuenta(CUENTA_CANCELADA);
				bitacoraMatchDao.update( bitacoraDto );

				/** Generando un nuevo registro**/
				BitacoraMatchDto newBitacoraDto =   getInfo( clientInfoReq, new BitacoraMatchDto(emisor) );
				newBitacoraDto.setCuenta       ( account );
				newBitacoraDto.setAuthorization( authorization );
				newBitacoraDto.setTarjeta      ( bitacoraDto.getTarjeta() );
				newBitacoraDto.setStatusCuenta ( CUENTA_ASOCIADA );
				newBitacoraDto.setStatus       ( 9 );
				bitacoraMatchDao.create ( newBitacoraDto );

				/** Actualizando los datos personales en ISTCMSCARD**/
				IstcmscardDto istcmscardDto = istcmscardDao.getIstcmscardInformation(newBitacoraDto.getTarjeta());
				if( istcmscardDto != null ){
					istcmscardDto.setName( newBitacoraDto.getNombreCorto() );
					istcmscardDto.setName2( newBitacoraDto.getNombreLargo() );
					istcmscardDao.sqlUpdate(istcmscardDto);
				}
				
				/** Actualizando la direccion **/
				AddressDto addressDto = addressDao.findCardAddress( newBitacoraDto.getTarjeta() );
				log.debug(ReflectionToStringBuilder.toString(addressDto));
				if( addressDto!=null){
					addressDto.setDireccion( newBitacoraDto.getDireccion() );
					addressDto.setNombre   ( newBitacoraDto.getNombreLargo() );
					addressDto.setColonia  ( newBitacoraDto.getColonia() );
//					addressDto.setEstado   ( newBitacoraDto.getEntidadFederativa() );
//					addressDto.setRfc      ( newBitacoraDto.getRfc() );
					addressDto.setCp       ( newBitacoraDto.getCodigoPostal() );
					addressDto.setTelefono ( newBitacoraDto.getTelefono() );
					addressDto.setTelefono2( newBitacoraDto.getTelefono2() );
					
					log.debug(ReflectionToStringBuilder.toString(addressDto));
					addressDao.update( addressDto );
				}	
			
			/** Procesando actualización cuando la cuenta aún no esta Asociada **/
			}else{
				getInfo( clientInfoReq, bitacoraDto );
				authorization = bitacoraDto.getAuthorization();
				bitacoraMatchDao.update( bitacoraDto );
				
			}
			
			/** Proceso finalizado, Seteando Respuesta. **/
			codeResponse  = 1;
			description   = "La información fue Actualizada Correctamente";

		}else{
			
			/** Generando Registro en la BD **/
			bitacoraDto = getInfo( clientInfoReq, new BitacoraMatchDto(emisor) );
			bitacoraDto.setCuenta        ( account );
			bitacoraDto.setEmisor       (emisor);
			bitacoraDto.setAuthorization(authorization);
			/** Persistiendo la Nueva cuenta en BD **/
			log.debug(ReflectionToStringBuilder.toString(bitacoraDto));
			bitacoraMatchDao.create( bitacoraDto );
			
			codeResponse = 1;
			description   = "La información fue Almacenada Correctamente";
			
		}
		
		/*************************************
		 *     Generando Respuesta
		 *************************************/
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode         (codeResponse);
		response.setAuthorization( codeResponse==1 ? authorization : "0" );
		response.setDescription  ( description  );

		
		return response;
	}
	
	
	public BasicAuthRespDto assignmentAccount(String pan, BitMatchInfoClientRequestDto clientInfoReq){
		
		int    codeResponse  = 0;
		String authorization = "0";
		String account       = null;
		
		String request = "pan["+pan+"]"+clientInfoReq.toString();
		
		/** Obteniendo informacion de la tarjeta a Asignar**/
		CardholderDto cardholderDto = cardholderDao.getCardholderInfoByCardNumber(pan);
		
		/** Realizando las validaciones necesarias **/
		if( cardholderDto != null ){
			/** Validando que la tarjeta este inactiva **/
			if( cardholderDto.getCard().getStatus() == SystemConstants.STATUS_INACTIVE_CARD ){
				
				/** Validando la cuenta y su longitud **/
				EmisorDto emisorDto = emisorDao.findByBin(cardholderDto.getIstrel().getBin());
				
				/** Armando la cuenta **/
				account = GeneralUtil.buildPrimaryAccount( clientInfoReq.getCuenta(), (emisorDto!=null ? emisorDto.getCuenta() : "0") );
				
				if( account.length() <= 19 ){
					
					/** Obteniendo el listado de tarjetas asociadas con la cuenta solicitada **/
					List<CardDto> list = cardDao.findByAccount(account);
				
					/** Validando que la cuenta no haya sido asignada Previamente **/
					if( list.size() == 0 ){
						
						/** Obteniendo un numero de Autorizacion **/
						authorization = GeneralUtil.generateAuthorizationNumber(6);
						
						/** Iniciando Asignacion **/
						BitacoraMatchDto newBitacoraDto = getInfo( clientInfoReq, new BitacoraMatchDto("") );
						
						/** Actualizando los datos personales en ISTCMSCARD**/
						IstcmscardDto istcmscardDto = istcmscardDao.getIstcmscardInformation( pan );
						if( istcmscardDto != null ){
							istcmscardDto.setName( newBitacoraDto.getNombreCorto() );
							istcmscardDto.setName2( newBitacoraDto.getNombreCorto() );
							istcmscardDao.sqlUpdate(istcmscardDto);
						}
						
						/** Actualizando la dirección **/
						AddressDto addressDto = addressDao.findCardAddress( pan );
						if( addressDto!=null){
							addressDto.setNombre   ( newBitacoraDto.getNombreLargo() );
							addressDto.setDireccion( newBitacoraDto.getDireccion() );
							addressDto.setColonia  ( newBitacoraDto.getColonia() );
							addressDto.setEstado   ( newBitacoraDto.getEntidadFederativa() );
							addressDto.setCp       ( newBitacoraDto.getCodigoPostal() );
							addressDto.setRfc      ( newBitacoraDto.getRfc());
							addressDto.setTelefono ( newBitacoraDto.getTelefono() );
							addressDto.setTelefono3( newBitacoraDto.getTelefono2() );
							log.debug(addressDto.toString());
							addressDao.update( addressDto );
						}
						
						/** Actualizando la cuenta en ISTCARD **/
						CardDto cardDto = cardholderDto.getCard();
						cardDto.setAccount(account);
						cardDao.sqlUpdate(cardDto);
						
						/** Actualizando la cuenta en ISTDBACCT **/
						AccountDto accountDto = cardholderDto.getAccount();
						int updateAccount= accountDao.accountUpdate(accountDto, account);
						log.debug("Estatus del update a la cuenta: " + updateAccount);
	
						/** Actualizando la cuenta en ISTREL **/
						IstrelDto istrelDto = cardholderDto.getIstrel();
						int updateIstrel    = istrelDao.accountUpdate(istrelDto, pan); 
						log.debug("Estatus del update a istrel: " + updateIstrel);
						
						/**********************************************************************
						 * Almacenando movimiento en la Bitacora de las Tarjetas( ISTCMSACT )
						 **********************************************************************/
						CardActivityDto cardActivity = new CardActivityDto();
						cardActivity.setPan       ( cardholderDto.getCard().getNumberCard() );
						cardActivity.setCardType  ( cardholderDto.getCard().getCardType() );
						cardActivity.setUserId    ( SystemConstants.WS_USER_NAME );
						cardActivity.setSpareChar ( authorization );
						cardActivity.setCmsFunc   ( "03" );
						cardActivity.setReplReason( "ALTA" );
						cardActivityDao.create( cardActivity );
						
						codeResponse = 1;
					}else{
						codeResponse = 4;//La cuenta ya se encuentra asignada.
					}
				}else{
					codeResponse = 21; //La cuenta es demasiado larga	
				}
			}else{
				codeResponse = 3;//La tarjeta a asignar ya fué asignada con anterioridad
			}
		}else{
			codeResponse = 2;//La tarjeta a asignar no existe
		}
		
		/*************************************
		 *     Generando Respuesta
		 *************************************/
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode         (codeResponse);
		response.setAuthorization( authorization );
		response.setDescription  ( CatalogUtil.getCatalogAssignmentAccount().get(codeResponse) );
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		
		log.debug( "::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog("0", pan, SystemConstants.WS_USER_NAME, request, codeResponse, response, SystemConstants.PCODE_LOG_CARDHOLDER_REG   );
		
		return response;
	}
	
	@SuppressWarnings("unused")
	public BasicAuthRespDto cardholderRegistration( CardholderInfoRequestDto cardHolderInfo ){
		
		String bin           = "0";
		String authorization = "0";
		int    codeResponse  = 0;//Tarjeta Inexistente
		
		
		if( cardHolderInfo.getName().length() <= 25 ){
			
			/***************************************************
			 * Obteniendo la Informacion Personal de la Tarjeta 
			 ***************************************************/
			CardholderDto cardholderDto = cardholderDao.getCardholderInfoByCardNumber(cardHolderInfo.getPan());
			bin                         = ( cardholderDto !=null && cardholderDto.getAccount() != null) ? cardholderDto.getAccount().getBin() : "0";
			
			if( cardholderDto != null ){
				
				CardDto cardDto = cardholderDto.getCard();

				/** Validando que la tarjeta se encuentre inactiva. **/
				if( true ){
					
					AddressDto adressDto = addressDao.findCardAddress(cardDto.getNumberCard());
					authorization = GeneralUtil.generateAuthorizationNumber(6);
					
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getName()) )         { adressDto.setNombre( cardHolderInfo.getName().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getAddress()) )      { adressDto.setDireccion( cardHolderInfo.getAddress().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getAddress2()) )     { adressDto.setDireccion2( cardHolderInfo.getAddress2().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getColony()) )       { adressDto.setColonia( cardHolderInfo.getColony().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getDelegation()) )   { adressDto.setLocalidad( cardHolderInfo.getDelegation().toUpperCase() ); }
	//				if( !GeneralUtil.isEmtyString(cardHolderInfo.getFederalEntity()) ){ cardholderInfoDto.setFederalEntity( cardHolderInfo.getFederalEntity().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getState()) )        { adressDto.setEstado( cardHolderInfo.getState().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getZipCode()) )      { adressDto.setCp( cardHolderInfo.getZipCode().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getHomePhone()) )    { adressDto.setTelefono( cardHolderInfo.getHomePhone().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getCellPhone()) )    { adressDto.setTelefono2( cardHolderInfo.getCellPhone().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getWorkPhone()) )    { adressDto.setTelefono3( cardHolderInfo.getWorkPhone().toUpperCase() ); }
					if( !GeneralUtil.isEmtyString(cardHolderInfo.getRfc()) )          { adressDto.setRfc( cardHolderInfo.getRfc().toUpperCase() ); }
					
					/***************************************************
					 *  Actualizando Información del Tarjeta Habiente 
					 ***************************************************/
					
					addressDao.update( adressDto );
					
					/***************************************************
					 *  Actualizando Información de ISTCMSCARD 
					 ***************************************************/
					
					IstcmscardDto istcmscardDto = istcmscardDao.getIstcmscardInformation( cardDto.getNumberCard() );
					istcmscardDto.setName ( cardHolderInfo.getName().toUpperCase() );
					istcmscardDto.setName2( cardHolderInfo.getName().toUpperCase() );
					
					istcmscardDao.sqlUpdate(istcmscardDto);
					
					/***************************************
					 *        Activando Tarjeta
					 ***************************************/
					cardDto.setStatus(SystemConstants.STATUS_ACTIVE_CARD);
					cardDao.update(cardDto);
					
					/**********************************************************************
					 * Almacenando movimiento en la Bitácora de las Tarjetas( ISTCMSACT )
					 **********************************************************************/
					CardActivityDto cardActivity = new CardActivityDto();
					cardActivity.setPan       ( cardDto.getNumberCard() );
					cardActivity.setCardType  ( cardDto.getCardType() );
					cardActivity.setUserId    ( SystemConstants.WS_USER_NAME );
					cardActivity.setSpareChar ( authorization );
					cardActivity.setCmsFunc   ( "05" );
					cardActivity.setReplReason( "ACTIVACION" );
						
					cardActivityDao.create( cardActivity );
					
					codeResponse = 1;
				}else{
					codeResponse = 3; //La tarjeta ya se encuentra Activa.
				}
			}
				
		}else{
				codeResponse = 2; // El nombre excede la longitud máxima permitida
		}
		
		/************************************
		*      Generando Respuesta 
		************************************/	
		BasicAuthRespDto response = new BasicAuthRespDto();
		response.setCode(codeResponse);
		response.setAuthorization(authorization);
		response.setDescription(CatalogUtil.getCardholderRegistration().get(codeResponse));
		
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		
		log.debug( "::Almacenando Movimiento en Bitácora::");
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog( bin, cardHolderInfo.getPan(), SystemConstants.WS_USER_NAME, cardHolderInfo.toString(), codeResponse, response, SystemConstants.PCODE_LOG_CARDHOLDER_REG);
		
		return response;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public AddresRespDto cardholderInfo(String pan){

		AddressDto addresDto = addressDao.findCardAddress(pan);
		
		AddresRespDto addressResponse = new AddresRespDto();
		
		if(addresDto != null){
			addressResponse.setCode(1);
									
			if( !GeneralUtil.isEmtyString(addresDto.getTarjeta()) )         { addressResponse.setTarjeta(addresDto.getTarjeta());}
			if( !GeneralUtil.isEmtyString(addresDto.getNombre()) )          { addressResponse.setNombre(addresDto.getNombre());}
			if( !GeneralUtil.isEmtyString(addresDto.getDireccion()) )       { addressResponse.setDireccion(addresDto.getDireccion());}
			if( !GeneralUtil.isEmtyString(addresDto.getDireccion2()) )      { addressResponse.setDireccion2(addresDto.getDireccion2());}
			if( !GeneralUtil.isEmtyString(addresDto.getColonia()) )         { addressResponse.setColonia(addresDto.getColonia());}
			if( !GeneralUtil.isEmtyString(addresDto.getLocalidad()) )       { addressResponse.setLocalidad(addresDto.getLocalidad());}
			if( !GeneralUtil.isEmtyString(addresDto.getEstado()) )          { addressResponse.setEstado(addresDto.getEstado());}
			if( !GeneralUtil.isEmtyString(addresDto.getCp()) )              { addressResponse.setCp(addresDto.getCp());}
			if( !GeneralUtil.isEmtyString(addresDto.getTelefono()) )        { addressResponse.setTelefono(addresDto.getTelefono());}
			if( !GeneralUtil.isEmtyString(addresDto.getRfc()) )             { addressResponse.setRfc(addresDto.getRfc());}
			
			
		}else{
			addressResponse.setCode(3);
			addressResponse.setDescription("No existe la tarjeta");
		}
						
		return addressResponse;
	}
	
	
	
	/** {@inheritDoc}
	 * @see com.syc.services.CardService#saveDemographic(com.syc.persistence.dto.AddressDto)
	 */	
	public BasicDateResponseDto updateDemographicData(String pan, DemographicDataRequestDto demographicDataRequestDto) {
		int    codeResponse  = 1;	
		String description = SystemMesages.SERVICE_OK;
		
		StringBuilder request = new StringBuilder();				
		request.append("pan[").append(pan).append("]").append("demographicDataRequestDto[").append(demographicDataRequestDto).append("]");
		String demograficosValid = null;
		CardholderDto cardholderDto = cardholderDao.getCardholderInfoByCardNumber(pan);
						
		if(cardholderDto != null && cardholderDto.getIstrel() != null && cardholderDto.getCard() != null){																	
			demograficosValid = isValidosDemograficos(demographicDataRequestDto);
			
			if( demograficosValid.equals(SystemMesages.OK) ){								
					updateAddress(pan,demographicDataRequestDto, null);	
					updatePersonalData( pan, demographicDataRequestDto.getShortName());
					
			}else{
				codeResponse = 2;
				description = demograficosValid;
			}						
		}else{
			codeResponse = 3;
			description = SystemMesages.IVALIDA_CARD;
		}
		
		
		if(codeResponse == 2){
			description = demograficosValid; 
		}else{
			description = PropertiesUtil.getProperty(description);
		}
		
		BasicDateResponseDto response = new BasicDateResponseDto();
		response.setCodigo			( codeResponse );		
		response.setDescripcion     ( description);
		response.setDate			( GeneralUtil.formatDate( "dd-MM-yyyy" , GeneralUtil.getCurrentSqlDate() ) );
		response.setTime     		( GeneralUtil.formatDate( "HH:mm:ss" , GeneralUtil.getCurrentSqlDate() ) );	
		
		
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
				
		
		log.debug( "::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil( wsLogDao );
		wsUtil.saveLog("0", pan, SecurityUtils.getCurrentUser(), request.toString(), codeResponse, response, SystemConstants.PCODE_UPDATE_DEMOGRAPHIC_DATA);  
		
		return response;						
	}

	
	/** {@inheritDoc}
	 * @see com.syc.services.CardService#saveDemographic(com.syc.persistence.dto.AddressDto)
	 */	
	public BasicDateResponseDto updateDemographicDataClient(String bin, String idClient, DemographicDataRequestDto demographicDataRequestDto) {
		
		
		String sBin = GeneralUtil.completaCadena(bin, '0', 10, "L");
		String sBase = GeneralUtil.completaCadena(idClient, '0', 10, "L");
		String pan = "";
		BasicDateResponseDto basicDateResponseDto = null;
		List<CardDto> tarjetas = cardDao.findIdClientBus(sBin, sBase);
		
		if(tarjetas != null){
			for(CardDto tarjeta : tarjetas){
				pan =  tarjeta.getNumberCard();
				basicDateResponseDto = updateDemographicData(pan, demographicDataRequestDto);
			}
		}
		return basicDateResponseDto;
	}
	
	
	/* (non-Javadoc)
	 * @see com.syc.services.CardHolderService#getCardHolderInfo(java.lang.String)
	 */	
	public Map<String,Object> getCardHolderInfo(String pan) {	
		Map<String,Object> result = new HashMap<String, Object>();
		
		result.put("cardHolder", cardholderDao.getCardholderInfoByCardNumber(pan));
		result.put("addres",  addressDao.findCardAddress(pan) );
		result.put("embosserProperties", embosserPropertiesDao.findEmbosserProperiesByPan(pan));		
		return result;
	}

	
	public CatBinDto getProductProperties(String bin, String producto) {
		StringBuilder request = new StringBuilder();				
		request.append("bin[").append(bin).append("]").append("producto[").append(producto).append("]");
		
		
		CatBinPk catBinPk = new CatBinDto().getCatBinPk();
		
		catBinPk.setBin(bin);
		catBinPk.setProducto(producto);
		
		
		return catBinDao.read(catBinPk);		
	}
	

	
	private void updatePersonalData(String pan, String shortName){
		/** Actualizando los datos personales en ISTCMSCARD**/
		IstcmscardDto istcmscardDto = istcmscardDao.getIstcmscardInformation( pan );
		if( istcmscardDto != null ){
			if(shortName.length() < 26){
				
				istcmscardDto.setName( shortName);
				istcmscardDto.setName2( shortName );
				istcmscardDto.setName1(shortName);
				istcmscardDao.sqlUpdate(istcmscardDto);
				
			}
		}
	}
	
	

    /**
     * Actualiza la direccion segun el parametro enviado DemographicDataRequestDto o AddressDto
     * @param pan Numero de tarjeta al que se van actulizar los datos
     * @param demographicDto Entrada de servicio que contiene un objeto DemographicDataRequestDto
     * @param originalAddressDto Entrada de servicio que contiene un objeto tipo AddressDto
     */
	private void updateAddress(String pan, DemographicDataRequestDto demographicDto, AddressDto originalAddressDto ){
		/** Actualizando la dirección **/
		AddressDto addressDto = addressDao.findCardAddress( pan );
		
		if(originalAddressDto == null){				
			if( addressDto!=null){
				addressDto.setDireccion( demographicDto.getAddress() );
				addressDto.setNombre   ( demographicDto.getName() );
				addressDto.setColonia  ( demographicDto.getColony() );
				addressDto.setCp       ( demographicDto.getZipCode() );
				addressDto.setCp2       ( demographicDto.getZipCode() );
				addressDto.setTelefono ( demographicDto.getHomePhone() );
				addressDto.setTelefono2( demographicDto.getCellPhone() );
				addressDto.setTelefono3( demographicDto.getWorkPhone());
				addressDto.setCiudad   ( demographicDto.getCity());
				addressDto.setEstado   ( demographicDto.getState());
				addressDto.setLocalidad( demographicDto.getMunicipality());
				addressDto.setRfc	   ( demographicDto.getRfc());
				addressDto.setEmail(demographicDto.getEmail());
				addressDao.update( addressDto );
			}
		}else if(demographicDto == null){
			addressDto.setDireccion( originalAddressDto.getDireccion() );
			addressDto.setNombre   ( originalAddressDto.getNombre()  );
			addressDto.setColonia  ( originalAddressDto.getColonia() );
			addressDto.setCp       ( originalAddressDto.getCp());
			addressDto.setCp2       ( originalAddressDto.getCp() );
			addressDto.setTelefono ( originalAddressDto.getTelefono() );
			addressDto.setTelefono2( originalAddressDto.getTelefono2() );								
			addressDto.setTelefono3( originalAddressDto.getTelefono3());
			addressDto.setCiudad   ( originalAddressDto.getCiudad());
			addressDto.setEstado   ( originalAddressDto.getEstado());
			addressDto.setLocalidad( originalAddressDto.getLocalidad());
			addressDto.setRfc	   ( originalAddressDto.getRfc());
			addressDto.setEmail    (originalAddressDto.getEmail());
			addressDao.update( addressDto );
		}
	}
	
	
	
	private BitacoraMatchDto getInfo( BitMatchInfoClientRequestDto clientInfoReq, BitacoraMatchDto bitacoraDto ){
		
		if(clientInfoReq.getCodigoPostal()!=null)      bitacoraDto.setCodigoPostal     (clientInfoReq.getCodigoPostal());
		if(clientInfoReq.getColonia()!=null)           bitacoraDto.setColonia          (clientInfoReq.getColonia());
		if(clientInfoReq.getDireccion()!=null)         bitacoraDto.setDireccion        (clientInfoReq.getDireccion());
		if(clientInfoReq.getEntidadFederativa()!=null) bitacoraDto.setEntidadFederativa(clientInfoReq.getEntidadFederativa());
		if(clientInfoReq.getNombreCorto()!=null)       bitacoraDto.setNombreCorto      (clientInfoReq.getNombreCorto());
		if(clientInfoReq.getNombreLargo()!=null)       bitacoraDto.setNombreLargo      (clientInfoReq.getNombreLargo());
		if(clientInfoReq.getRfc()!=null)               bitacoraDto.setRfc              (clientInfoReq.getRfc());
		if(clientInfoReq.getTelefono()!=null)          bitacoraDto.setTelefono         (clientInfoReq.getTelefono());
		if(clientInfoReq.getTelefono2()!=null)         bitacoraDto.setTelefono2        (clientInfoReq.getTelefono2());
		if(clientInfoReq.getSucursal()!=null)          bitacoraDto.setSucursalCliente  (clientInfoReq.getSucursal());
		if(clientInfoReq.getProducto()!=null)          bitacoraDto.setProducto         (clientInfoReq.getProducto());
		
		return bitacoraDto;
	}

	
	/**
	 * Valida la longitud de los datos demograficos
	 * @return
	 */
	
	private String isValidosDemograficos(DemographicDataRequestDto request){								
		if( request.getZipCode()     !=null && request.getZipCode().length()      >  9 ) return "el campo ZipCode es demasiado largo";//SystemMesages.CP;
		if( request.getColony()      !=null && request.getColony().length()       > 45 ) return "el campo Colony es demasiado largo";//SystemMesages.OK;
		if( request.getAddress()     !=null && request.getAddress().length()      > 45 ) return "el campo Address es demasiado largo";//SystemMesages.ADDRESS;
		if( request.getShortName()   !=null && request.getShortName().length()    > 25 ) return "el campo ShortName es demasiado largo";//SystemMesages.SHORT_NAME;
		if( request.getName()        !=null && request.getName().length()         > 35 ) return "el campo Name es demasiado largo";//SystemMesages.LARGE_NAME; 
		if( request.getRfc()         !=null && request.getRfc().length()          > 16 ) return "el campo RFC es demasiado largo";//SystemMesages.RFC; 
		if( request.getHomePhone()   !=null && request.getHomePhone().length()    > 10 ) return "el campo HomePhone es demasiado largo";//SystemMesages.PHONE;
		if( request.getWorkPhone()   !=null && request.getWorkPhone().length()    > 10 ) return "el campo WorkPhone es demasiado largo";//SystemMesages.PHONE; 
		if( request.getCellPhone()   !=null && request.getCellPhone().length()    > 10 ) return "el campo cellPhone es demasiado largo";
		if( request.getMunicipality()!=null && request.getMunicipality().length() > 45 ) return "el campo Municipality es demasiado largo";//SystemMesages.ENTIDAD;				
		if( request.getCity()        !=null && request.getCity().length()         > 45 ) return "el campo City es demasiado largo";//SystemMesages.ENTIDAD;				
		if( request.getEmail()       !=null && request.getEmail().length()        > 45 ) return "el campo email es demasiado largo";//SystemMesages.ENTIDAD;				
		if( request.getState()       !=null && request.getState().length()        > 45 ) return "el campo state es demasiado largo";//SystemMesages.ENTIDAD;				

		return SystemMesages.OK;
	}

}
