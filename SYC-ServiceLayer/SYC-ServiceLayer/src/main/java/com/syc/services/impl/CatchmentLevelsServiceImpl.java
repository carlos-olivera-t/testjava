package com.syc.services.impl;

import java.sql.Date;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.response.LevelCaptationResponseDto;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dao.IstCardTypeDao;
import com.syc.persistence.dao.UdisDao;
import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.AccountDto;
import com.syc.persistence.dto.CardholderDto;
import com.syc.persistence.dto.IstCardTypeDto;
import com.syc.persistence.dto.UdisDto;
import com.syc.services.CatchmentLevelsService;
import com.syc.utils.CatalogUtil;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SystemConstants;
import com.syc.utils.ValidatorUtil;
import com.syc.utils.WSLogUtil;


@Service
public class CatchmentLevelsServiceImpl implements CatchmentLevelsService{
	
	@Autowired
	CardholderDao cardholderDao;
	@Autowired
	WSLogDao wsLogDao;
	@Autowired
	UdisDao udisDao;
	@Autowired
	IstCardTypeDao istCardTypeDao;
	WSLogUtil wsUtil;
	TreeMap<Integer, Integer> validations;
	
	
	private static final transient Log log = LogFactory.getLog(OnlineTransfersServiceImpl.class);
	private static final String BIN_PAGATODO = "0000538984";

	@Override
	public LevelCaptationResponseDto levelsCaptation(String pan, double amount, String user) {
		int codeResponse = 0;
		String binSyc = null;
		double montoMensual = 0.0;
		double montoTotal = 0;
		double montoMaxMensual = 0;
		
		double limite_mensual = 0;
		double limite_general = 0;
		
		getMapOfValidations();
		
		CardholderDto cardholderDto = cardholderDao.getCardholderInfoByCardNumber(pan);
		if (cardholderDto != null) {
			log.debug("::Validando Cuenta::");
			binSyc = GeneralUtil.completaCadena(pan.substring(0, 6), '0', 10,"L");
			/**
			 * poner validaciones para la tarjeta
			 */
			int validatorResult = new ValidatorUtil().validateWebMethod(validations, cardholderDto);
			if (validatorResult > 0) {
				log.debug("::Validaciones no superadas::");
				codeResponse = validatorResult;

			} else if (validatorResult == 0) {
				log.debug("::Las validaciones fueron superadas correctamente::");
				AccountDto accountDto = cardholderDto.getAccount();
				if (amount > 0) {
					//if ((accountDto.getAvailableBalance() - amount) >= 0) {
						String cardType = null;

						if( cardholderDto.getAccount().getBin().trim().equals(BIN_PAGATODO) ){
							cardType = GeneralUtil.completaCadena(accountDto.getCustomerType(), '0', 4, "L");
						}else{
							cardType = cardholderDto.getCard().getCardType();
						}

						/**hacer generica la busqueda**/
						IstCardTypeDto limitsLevel = istCardTypeDao.findPrefixByBin(binSyc,cardType);
						if (limitsLevel != null) {
							montoTotal = accountDto.getAvailableBalance() + amount;
							montoMaxMensual = (-1) * accountDto.getCash_dep_used();
							montoMensual = amount;
							
							limite_general = limitsLevel.getMaxAvailableBalance();
							limite_mensual = limitsLevel.getMaxBalanceByMonth();
									
							String lastDeposit = String.valueOf(accountDto.getCash_dep_date()).substring(0, 7).replace("-", "");
							String actualDate = String.valueOf(GeneralUtil.getCurrentSqlDate()).substring(0, 7).replace("-", "");
							if (Integer.parseInt(lastDeposit) < Integer.parseInt(actualDate)) {
								montoMaxMensual = 0;
							}
							montoMensual += montoMaxMensual;
							/**************************************************************
							 * se hace la validacion de que el monto no rebase
							 * los niveles de captacion mensualesy generales
							 *************************************************************/
							if (montoTotal <= limitsLevel.getMaxAvailableBalance()) {
								if (montoMensual <= limitsLevel.getMaxBalanceByMonth()) {
									codeResponse = 1;// niveles superados
								} else
									codeResponse = 5;// saldo sobrepasa limite
														// mensual
							} else
								codeResponse = 4;// saldo disponible sobrepasa
													// limite general
						} else
							codeResponse = 10;// cuenta destino no tiene nivel
												// de captacion asignado
					//} else
						//codeResponse = 7;// saldo insuficiente en cuenta origen
				} else
					codeResponse = 13;// monto debe ser mayor a 0
			}

		} else
			codeResponse = 3;// no se encontro tarjeta

		LevelCaptationResponseDto response = new LevelCaptationResponseDto();
		response.setCode(codeResponse);
		response.setDescription(CatalogUtil.getCatalogTransfersSpei().get(codeResponse));
		response.setMonthlyAmount(montoMensual);
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		String request = "request[pan[" + pan + "]amount[" + amount + "]saldoDisp["+(montoTotal-amount)+"]saldo_acum["+montoMaxMensual+"]lim_mens["+limite_mensual+"]lim_gral["+limite_general+"]";

		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(binSyc, pan, user, request,codeResponse, response, SystemConstants.PCODE_LOG_CHECK_LEVEL_CAP);

		return response;
	}

	@Override
	public LevelCaptationResponseDto maxAmount(String pan, double amount, String user) {


		int codeResponse = 0;
		String request = "request[pan[" + pan + "]amount[" + amount + "]user["+ user + "]";
		String binSyc = null;
		double montoMensual = 0.0;
		getMapOfValidations();
		CardholderDto cardholderDto = cardholderDao.getCardholderInfoByCardNumber(pan);
		if (cardholderDto != null) {
			log.debug("::Validando Cuenta::");
			binSyc = GeneralUtil.completaCadena(pan.substring(0, 6), '0', 10,"L");
			/**
			 * poner validaciones para la tarjeta
			 */
			int validatorResult = new ValidatorUtil().validateWebMethod(validations, cardholderDto);
			if (validatorResult > 0) {
				log.debug("::Validaciones no superadas::");
				codeResponse = validatorResult;

			} else if (validatorResult == 0) {
				log.debug("::Las validaciones fueron superadas correctamente::");
				AccountDto accountDto = cardholderDto.getAccount();
				if (amount > 0) {
					if ((accountDto.getAvailableBalance() - amount) >= 0) {
						/***hacer la consulta para saber el valor de la udi**/
							String bin = pan.substring(0, 8);	
							Date lastDay = udisDao.findLastDayUdi(bin, "0");
							String lastTime = udisDao.findLastHourUdi(bin, "0", lastDay);
							UdisDto udis = udisDao.findLastUdi(bin, "0", lastDay, lastTime);
							
							double maxAvailable = 1500 * udis.getUdi();
							double montoMaxMensual = (-1) * accountDto.getCash_dep_used();
							 montoMensual = amount;
							String lastDeposit = String.valueOf(accountDto.getCash_dep_date()).substring(0, 7).replace("-", "");
							String actualDate = String.valueOf(GeneralUtil.getCurrentSqlDate()).substring(0, 7).replace("-", "");
							if (Integer.parseInt(lastDeposit) < Integer.parseInt(actualDate)) {
								montoMaxMensual = 0;
							}
							montoMensual += montoMaxMensual;
							/**************************************************************
							 * se hace la validacion de que el monto no rebase
							 * los niveles de captacion mensualesy generales
							 *************************************************************/
								if (montoMensual <= maxAvailable) {
									codeResponse = 1;// niveles superados
								} else
									codeResponse = 5;// saldo sobrepasa limite mensual
					} else
						codeResponse = 7;// saldo insuficiente en cuenta origen
				} else
					codeResponse = 13;// monto debe ser mayor a 0
			}

		} else
			codeResponse = 3;// no se encontro tarjeta

		LevelCaptationResponseDto response = new LevelCaptationResponseDto();
		response.setCode(codeResponse);
		response.setDescription(CatalogUtil.getCatalogTransfersSpei().get(codeResponse));
		response.setMonthlyAmount(montoMensual);
		/**************************************************
		 * Almacenando Transaccion en Bitacora del WS
		 **************************************************/
		log.debug("::Almacenando Movimiento en Bitacora::");
		wsUtil = new WSLogUtil(wsLogDao);
		wsUtil.saveLog(binSyc, pan, user, request,codeResponse, response, SystemConstants.PCODE_LOG_CHECK_LEVEL_CAP);

		return response;
	}

	private void getMapOfValidations() {
		validations = new TreeMap<Integer, Integer>();
		validations.put(SystemConstants.VALIDATE_EXISTS_ACCOUNT, 1);
		validations.put(SystemConstants.VALIDATE_ACCOUNT_ACTIVE, 1);

	}

}
