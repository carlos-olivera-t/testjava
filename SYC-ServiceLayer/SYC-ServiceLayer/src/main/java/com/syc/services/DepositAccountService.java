package com.syc.services;

import java.util.List;



import com.syc.dto.request.ChequeDto;
import com.syc.dto.request.LoadBalanceAccountRequest;
import com.syc.dto.request.LoadBalanceRequest;
import com.syc.dto.response.BalanceMovementAcctRespDto;
import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.dto.response.BalanceMovementRespSodexoDto;
import com.syc.dto.response.LevelCaptationResponseDto;

public interface DepositAccountService {
	
	/***********************************************
	 * Metodos que realizan un deposito a una cuenta por tarjeta
	 ***********************************************/
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, String user )throws Exception;
	
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, String description, String user, String reference )throws Exception;
	
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, int pCode, String description, String user )throws Exception;
	
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, int pCode, String description, boolean checkLevels, String user )throws Exception;
			
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, int pCode, String description, String user, String comments )throws Exception;
	/**
	 * Servicio que realiza un deposito a cuenta por tarjeta
	 * @param pan
	 * @param amount
	 * @param activationCard
	 * @param pCode
	 * @param description
	 * @param user
	 * @param comments
	 * @param reference
	 * @param checkLevels
	 * @return
	 * @throws Exception
	 */
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, boolean activationCard, int pCode, String description, String user, String comments, String reference, boolean checkLevels )throws Exception;
	
	public BalanceMovementRespDto doDepositAccount( String pan, double amount, List<ChequeDto> cheques) throws Exception;
	
	/**
	 * Servicio que realiza depositos para SODEXO
	 * @param request
	 * @param isValidBin
	 * @return
	 * @throws Exception
	 */
	public List<BalanceMovementRespSodexoDto> loadBalance( List<LoadBalanceRequest> request, String user, boolean isValidBin) throws Exception;
	
	/**
	 * Servicio que tiene por objetivo realizar un deposito por cuenta
	 * @param request
	 * @param user
	 * @param clave_emisor
	 * @param isActivation
	 * @return
	 * @throws Exception
	 */
	public List<BalanceMovementAcctRespDto> depositAccount( List<LoadBalanceAccountRequest> request, String user, int clave_emisor, boolean isActivation ) throws Exception;
	
	/**
	 * Metodo que realiza la validacion de limite diario y limite general de
	 * tarjetas 
	 * @param pan
	 * @param amount
	 * @param activationCard
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public BalanceMovementRespDto doDepositLimits( String pan, double amount, boolean activationCard, String user )throws Exception;

	/**
	 * Servicio que realiza un deposito por cuenta
	 * @param bin
	 * @param account
	 * @param amount
	 * @param pCode
	 * @param description
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public BalanceMovementRespDto doDepositAccount( String bin, String account, double amount, int pCode, String description, String user, LevelCaptationResponseDto levelsCaption)throws Exception;
	
}
