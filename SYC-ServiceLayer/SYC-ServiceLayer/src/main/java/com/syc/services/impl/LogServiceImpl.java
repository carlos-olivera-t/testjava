package com.syc.services.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.WSLogDto;
import com.syc.services.LogService;

@Service
public class LogServiceImpl implements LogService{
	
	private static final transient Log log = LogFactory.getLog(LogServiceImpl.class);
	
	@Autowired
	WSLogDao wsLogDao;
	
	public void saveLog(String bin, String numberCard, String username,String request, int code, Object object) {
		request = (request!=null && request.length()>400) ? request.substring(0,399) : request;
		
		WSLogDto wsLog = new WSLogDto();
		wsLog.setBin( bin );
		wsLog.setNumberCard(numberCard.trim());
		wsLog.setUserName(username);
		wsLog.setCommand( object.getClass().getSimpleName().replace("ResponseDto", "Command") );
		wsLog.setRequest(request);
		wsLog.setCodeResponse(code);
		wsLog.setResponse( (object.toString()!=null && object.toString().length()>400) ? object.toString().substring(0,399) : object.toString());
		
		log.info(wsLog.toString());		
		
		wsLogDao.create( wsLog );
	}

	public void saveLog(String bin, String numberCard, String username,String request, int code, Object object, String command) {
		request  = (request!=null && request.length()>400) ? request.substring(0,399) : request;
		String response = (object!=null && object.toString().length()>400) ? object.toString().substring(0,399) : object.toString();
		
		WSLogDto wsLog = new WSLogDto();
		wsLog.setBin( bin );
		wsLog.setNumberCard(numberCard!=null?numberCard.trim():"0");
		wsLog.setUserName(username);
		wsLog.setCommand( command );
		wsLog.setRequest(request);
		wsLog.setCodeResponse(code);
		wsLog.setResponse(response);

		log.info(wsLog.toString());		
		
		wsLogDao.create( wsLog );
	}

	
}
