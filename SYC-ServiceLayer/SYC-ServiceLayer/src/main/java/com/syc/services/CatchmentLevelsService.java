package com.syc.services;

import com.syc.dto.response.LevelCaptationResponseDto;

public interface CatchmentLevelsService {
	
	/**
	 * Servicio que checa los niveles de captacion para ver si se realiza el deposito
	 * @param pan
	 * @param amount
	 * @param user
	 * @return
	 */
	public LevelCaptationResponseDto levelsCaptation(String pan, double amount, String user);
	
	/**
	 * Servicio que revisa el monto maximo permitido por mes 1500  UDIS
	 * @param pan
	 * @param amount
	 * @param user
	 * @return
	 */
	public LevelCaptationResponseDto maxAmount(String pan, double amount, String user);
}
