package com.syc.services;

import com.syc.dto.request.MaquilaRequestDto;

public interface MaquilaService {
	
	public void procesaMaquila( MaquilaRequestDto maquilaRequest );
	
}
