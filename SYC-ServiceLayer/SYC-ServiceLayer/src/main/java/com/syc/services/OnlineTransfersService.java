package com.syc.services;

import java.util.List;

import com.syc.dto.request.TransferOnlineRequestDto;
import com.syc.dto.response.BasicResponseDto;
import com.syc.dto.response.LevelCaptationResponseDto;
import com.syc.dto.response.TransferSpeiResponseDto;



public interface OnlineTransfersService {
	
	/**
	 * Servicio que realiza transferencias entre cuentas
	 * @param request => lista de cuentas origen-destino para realizar transferencias
	 * @param bin
	 * @param user
	 * @return
	 */
	List<TransferSpeiResponseDto>   transfersSpei(List<TransferOnlineRequestDto> request, String bin, String user);


}
