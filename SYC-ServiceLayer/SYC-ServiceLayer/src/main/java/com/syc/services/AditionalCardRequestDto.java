package com.syc.services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
	"account",
    "cardholder",
    "additionalName",
    "additionalShortName",
    "additionalRFC",
    "additionalMail"
})
public class AditionalCardRequestDto {
	
	String account;
	String cardholder;
	String additionalName;
	String additionalShortName;
	String additionalRFC;
	String additionalMail;
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}

	public String getCardholder() {
		return cardholder;
	}
	public void setCardholder(String cardholder) {
		this.cardholder = cardholder;
	}

	public String getAdditionalName() {
		return additionalName;
	}
	public void setAdditionalName(String additionalName) {
		this.additionalName = additionalName;
	}

	public String getAdditionalShortName() {
		return additionalShortName;
	}
	public void setAdditionalShortName(String additionalShortName) {
		this.additionalShortName = additionalShortName;
	}

	public String getAdditionalRFC() {
		return additionalRFC;
	}
	public void setAdditionalRFC(String additionalRFC) {
		this.additionalRFC = additionalRFC;
	}

	public String getAdditionalMail() {
		return additionalMail;
	}
	public void setAdditionalMail(String additionalMail) {
		this.additionalMail = additionalMail;
	}

	public String toString(){
		String response = null;
		response  = "account["+account+"],";
		response += "cardholder["+cardholder+"]";
		response += "additionalName["+additionalName+"]";
		response += "additionalShortName["+additionalShortName+"]";
		response += "additionalRFC["+additionalRFC+"]";
		response += "additionalMail["+additionalMail+"]";
		return response;
	}
}
