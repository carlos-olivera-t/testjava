package com.syc.services.monex.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.dto.request.TransferRequestDto;
import com.syc.dto.response.TransferResponseDto;
import com.syc.persistence.dao.AccountDao;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dao.EmployeeDao;
import com.syc.persistence.dao.SHCLogDao;
import com.syc.persistence.dao.WSLogDao;
import com.syc.persistence.dto.AccountDto;
import com.syc.persistence.dto.CardholderDto;
import com.syc.persistence.dto.EmployeeDto;
import com.syc.persistence.dto.SHCLogDto;
import com.syc.services.CardService;
import com.syc.services.monex.TransfersService;
import com.syc.utils.CatalogUtil;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SystemConstants;
import com.syc.utils.WSLogUtil;

@Service
public class TransfersServiceImpl implements TransfersService{
	
	private static final transient Log log = LogFactory.getLog(TransfersServiceImpl.class);
	
	@Autowired
	EmployeeDao   employeeDao;
	
	@Autowired
	CardholderDao cardHolderDao;
	
	@Autowired
	SHCLogDao     shcLogDao;
	
	@Autowired
	AccountDao   accountDao;
	
	@Autowired
	WSLogDao     wsLogDao;
	
	@Autowired
	CardService cardService;
	
	WSLogUtil    wsUtil;
	
	public TransferResponseDto onlineTransfers( TransferRequestDto request, int clave_emisor){
		
		final int CONCENTRATOR_TO_PAN                = 1;
		final int PAN_TO_PAN                         = 2;
		final int PAN_TO_CONCENTRATOR_PARTIAL_AMOUNT = 3;
		final int PAN_TO_CONCENTRATOR_TOTAL_AMOUNT   = 4;
		final int MONEX_CLAVE 		 				 = 0;
		
		final int ACCOUNT_CONCENTRATOR_CODE          = 3;
		
		EmployeeDto sourceEmployeeDto      = null;
		EmployeeDto destinationEmployeeDto = null;
		
		int codeResponse = 16;
		int movementType = 0;
		
		String sAuthorization = "0";
		String dAuthorization = "0";
		
		String bin = "0";
		
		TransferResponseDto response = new TransferResponseDto();
		
		double currentBalanceSource       = 0;
		double previousBalanceSource      = 0;
		double currentBalanceDestination  = 0;
		double previousBalanceDestination = 0;
		double transferAmount             = 0;
		
		int    sourceAccountType          = -1;
		int    destinationAccountType     = -1;
		
		int    pCodeCargo                 = 0;
		int    pCodeAbono                 = 0;
		
		try{
			//cambiar valor por la clave de Monex
			if(clave_emisor == MONEX_CLAVE){
			
				if( request.getSourceAccount() != null ){
					sourceEmployeeDto      = employeeDao.findEmployeeByAccount( request.getSourceAccount() );
					if( request.getAccountDestination() != null ){
					destinationEmployeeDto = employeeDao.findEmployeeByAccount( request.getAccountDestination() );
					}
				}
			}else{
				
				if( request.getSourceAccount() != null ){
					sourceEmployeeDto      = employeeDao.findEmployeeByAccount( request.getSourceAccount(), clave_emisor );
					if( request.getAccountDestination() != null ){
						destinationEmployeeDto = employeeDao.findEmployeeByAccount( request.getAccountDestination(), clave_emisor );
					}
				}
			}
	
			/********************************************
			 *  REALIZANDO LAS VALIDACIONES REQUERIDAS
			 ********************************************/
			
			if(!request.getSourceAccount().equalsIgnoreCase(request.getAccountDestination())){
			
			/** Validando que la cuenta origen Exista **/
			if( sourceEmployeeDto != null ){
				
				/** Validando que la cuenta Destino Exista **/
				if( destinationEmployeeDto != null ){
					
					/** Validando que la cuenta origen este Activa **/
					if( sourceEmployeeDto.getStatus() != SystemConstants.STATUS_CANCEL_CARD ){
						
						/** Validando que la cuenta destino este Activa **/
						if(destinationEmployeeDto.getStatus() != SystemConstants.STATUS_CANCEL_CARD){
							
							/** Validando que ambas cuentas pertenezcan al mismo Producto **/
							if( GeneralUtil.areTheSameProducts(sourceEmployeeDto.getPan(), destinationEmployeeDto.getPan() ) ){
								
								
								/** Efectuando las Validaciones Especificas de Acuerdo al Tipo de Movimiento **/
								movementType           = GeneralUtil.convertStringToInt( request.getMovementType() );
								sourceAccountType      = GeneralUtil.convertStringToInt( sourceEmployeeDto.getAccountType() );
								destinationAccountType = GeneralUtil.convertStringToInt( destinationEmployeeDto.getAccountType() );
								
								switch( movementType ){
								
									/** De Cuenta Concentradora a Tarjeta**/
									case CONCENTRATOR_TO_PAN:
										if( sourceAccountType == ACCOUNT_CONCENTRATOR_CODE ){
											if( destinationAccountType == ACCOUNT_CONCENTRATOR_CODE ){
												codeResponse = 15; //LA CUENTA DESTINO ES CONCENTRADORA
											}
										}else{
											codeResponse = 1; //LA CUENTA ORIGEN NO ES CUENTA CONCENTRADORA
										}
										
										/** Estableciendo pCodes **/
										pCodeCargo = SystemConstants.PCODE_MONEX_TRANSFER_CC_TO_PAN_CARGO;
										pCodeAbono = SystemConstants.PCODE_MONEX_TRANSFER_CC_TO_PAN_ABONO;
										
									break;
									
									/** De Tarjeta a tarjeta **/
									case PAN_TO_PAN:
										if( sourceAccountType != ACCOUNT_CONCENTRATOR_CODE ){
											
											if( destinationAccountType == ACCOUNT_CONCENTRATOR_CODE ){
												codeResponse = 15; //LA CUENTA DESTINO ES CONCENTRADORA
											}
										}else{
											codeResponse = 14; //LA CUENTA ORIGEN ES CUENTA CONCENTRADORA
										}
										
										/** Estableciendo pCodes **/
										pCodeCargo = SystemConstants.PCODE_MONEX_TRANSFER_PAN_TO_PAN_CARGO;
										pCodeAbono = SystemConstants.PCODE_MONEX_TRANSFER_PAN_TO_PAN_ABONO;
										
									break;
									
									/** De Tarjeta a Concentradora **/
									case PAN_TO_CONCENTRATOR_PARTIAL_AMOUNT :
										if( sourceAccountType != ACCOUNT_CONCENTRATOR_CODE ){
											
											if( destinationAccountType != ACCOUNT_CONCENTRATOR_CODE ){
												codeResponse = 4; //LA CUENTA DESTINO NO ES CONCENTRADORA
											}
										}else{
											codeResponse = 14; //LA CUENTA ORIGEN ES CUENTA CONCENTRADORA
										}
										
										/** Estableciendo pCodes **/
										pCodeCargo = SystemConstants.PCODE_MONEX_TRANSFER_PAN_TO_CC_CARGO;
										pCodeAbono = SystemConstants.PCODE_MONEX_TRANSFER_PAN_TO_CC_ABONO;
									break;
									
									/** De Tarjeta a Concentradora **/
									case PAN_TO_CONCENTRATOR_TOTAL_AMOUNT :
										if( sourceAccountType != ACCOUNT_CONCENTRATOR_CODE ){
											
											if( destinationAccountType != ACCOUNT_CONCENTRATOR_CODE ){
												codeResponse = 4; //LA CUENTA DESTINO NO ES CONCENTRADORA
											}
										}else{
											codeResponse = 14; //LA CUENTA ORIGEN ES CUENTA CONCENTRADORA
										}
										
										/** Estableciendo pCodes **/
										pCodeCargo = SystemConstants.PCODE_MONEX_TRANSFER_PAN_TO_CC_CARGO;
										pCodeAbono = SystemConstants.PCODE_MONEX_TRANSFER_PAN_TO_CC_ABONO;
										
									break;
									
									default:
										codeResponse = 13; //TIPO DE MOVIMIENTO INVALIDO
									break;
								}
								
								
							}else{
								codeResponse = 6; //LAS CUENTAS SON DE DISTINTOS PRODUCTOS
							}
						}else{
							codeResponse = 5; //CUENTA DESTINO CANCELADA
						}	
					
					}else{
						codeResponse = 2; //CUENTA ORIGEN CANCELADA
					}
					
					
					//FIN
				}else{
					codeResponse = 11; //LA CUENTA DESTINO NO EXISTE
				}
			}else{
				codeResponse = 9; //LA CUENTA ORIGEN NO EXISTE
			}
			}else{
				codeResponse = 12; //ORIGEN Y DESTINO NO PUEDEN SER IGUALES
			}
			
			/**************************************
			 *     Generando Movimientos 
			 **************************************/
			
			if( codeResponse == SystemConstants.VALIDACIONES_SUPERADAS ){
				log.info("Las validaciones fueron superadas, Procesando Saldos... ");	
				
				/** Obteniendo información del los tarjetahabientes **/
				CardholderDto sourceCardHolder      = cardHolderDao.getCardholderInfoByCardNumber( sourceEmployeeDto.getPan() );
				CardholderDto destinationCardHolder = cardHolderDao.getCardholderInfoByCardNumber( destinationEmployeeDto.getPan() );
				
				bin     = ( sourceCardHolder !=null && sourceCardHolder.getAccount() != null) ? sourceCardHolder.getAccount().getBin().trim() : "0";
				
				/****************************************************************
				 *  Generando transacción por el Importe de la Transferencia
				 ****************************************************************/
				AccountDto accountSource = sourceCardHolder.getAccount();
				SHCLogDto shcLog = new SHCLogDto(21);
				sAuthorization = GeneralUtil.generateAuthorizationNumber(SystemConstants.RANDOM_LENGHT);
				
				transferAmount = (movementType == PAN_TO_CONCENTRATOR_TOTAL_AMOUNT) ? accountSource.getAvailableBalance() : request.getSourceAmount();
				
				if( movementType == PAN_TO_CONCENTRATOR_TOTAL_AMOUNT && transferAmount == 0 ){
					codeResponse = 7;
				}else{
					
					if( accountSource.getAvailableBalance() >= transferAmount ){
						
						currentBalanceSource  = (accountSource.getAvailableBalance() - transferAmount);
						previousBalanceSource = accountSource.getAvailableBalance();
						
						shcLog.setNumberCard  ( sourceCardHolder.getCard().getNumberCard() );
						shcLog.setpCode       ( pCodeCargo );
						shcLog.setAmount      ( transferAmount );
						shcLog.setAvalBalance ( currentBalanceSource );
						shcLog.setAuthNum     ( sAuthorization );
						shcLog.setAcceptorName( "Ajuste por transferencia." );
						shcLog.setAcctNum     ( accountSource.getAccount() );
			
						shcLogDao.create(shcLog);
						
						/***********************************************
						 *  Actualizando el saldo en la Cuenta Origen
						 ***********************************************/
						accountSource.setAvailableBalance(currentBalanceSource);
						accountSource.setLedgerBalance   (currentBalanceSource);
						int updated = accountDao.sqlUpdate(accountSource);
						
						if(updated == 1){
							codeResponse = 1;

								log.debug( "::Recarga Procesada Exitosamente::");
								
								/****************************************************************
								 *  Generando transacción por el Abono a la cuenta Destino
								 ****************************************************************/
								AccountDto accountDestination = destinationCardHolder.getAccount();
								shcLog = new SHCLogDto(21);
								dAuthorization = sAuthorization; 
									
								currentBalanceDestination  = accountDestination.getAvailableBalance() + transferAmount;
								previousBalanceDestination = accountDestination.getAvailableBalance();
									
								shcLog.setNumberCard  ( destinationCardHolder.getCard().getNumberCard() );
								shcLog.setpCode       ( pCodeAbono );
								shcLog.setAmount      ( transferAmount );
								shcLog.setAvalBalance ( currentBalanceDestination );
								shcLog.setAuthNum     ( dAuthorization );
								shcLog.setAcceptorName( "Abono por transferencia" );
								shcLog.setAcctNum     ( accountDestination.getAccount() );
					
								shcLogDao.create(shcLog);
								
								/**************************************************
								 *  Actualizando el saldo en la Cuenta Destino
								 **************************************************/
								accountDestination.setAvailableBalance(currentBalanceDestination);
								accountDestination.setLedgerBalance   (currentBalanceDestination);
								int updated2 =accountDao.sqlUpdate(accountDestination);
				
								if(updated2 == 1){
									codeResponse = 1;

									log.debug( "::Recarga Procesada Exitosamente::");
									
									/** Seteando los valores en la respuesta **/
									response.setSourceAuthorization        ( sAuthorization );
									response.setDestinationAuthorization   ( dAuthorization );
									response.setSourceAvailableBalance     ( currentBalanceSource );
									response.setSourcePreviousBalance      ( previousBalanceSource );
									response.setDestinationAvailableBalance( currentBalanceDestination );
									response.setDestinationPreviousBalance ( previousBalanceDestination );
								
								}else{
									codeResponse = 50;
									
									log.debug( "::Recarga no procesada, vuelva a intentar");
								}
						
						}else{
							codeResponse = 50;
							
							log.debug( "::Recarga no procesada, vuelva a intentar");
						}
						/** Activando Tarjeta en caso que sea primera dispersion. **/
						if( destinationCardHolder.getCard().getStatus() == SystemConstants.STATUS_INACTIVE_CARD  ){
							cardService.activationCard(destinationCardHolder.getCard(), "OnlineTransfers");	
						}
						
						codeResponse = 0;
					}else{
						codeResponse = 3;
					}
				}
			}
			
			/***************************** 
			 *   Generando Respuesta.
			 *****************************/
			response.setCode(GeneralUtil.completaCadena(String.valueOf(codeResponse), '0', 3, "L"));
			response.setDescription(CatalogUtil.getCatalogTransfer().get(codeResponse));
			
			/**************************************************
			 * Almacenando Transaccion en Bitacora del WS
			 **************************************************/
			wsUtil = new WSLogUtil( wsLogDao );
			wsUtil.saveLog(bin, request.getSourceAccount(), "ws_monex", request.toString(), codeResponse, response, SystemConstants.PCODE_LOG_MONEX_TRANSFERS);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return response;
	}

	
}
















