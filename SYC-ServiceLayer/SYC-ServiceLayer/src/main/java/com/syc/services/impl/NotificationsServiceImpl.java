package com.syc.services.impl;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.syc.services.NotificationsService;

/**
 * @author Yessica Gomez
 * 
 * @description Servicio que se encarga de enviar notificaciones a ciBanco si se actualiza un status o si se cambia un NIP
 * @version 1.0
 * 
 *
 */


@Repository( "notificationsService")
public class NotificationsServiceImpl implements NotificationsService{
	
	@Autowired
	private CamelContext context;
	private ProducerTemplate template;
	
	private static final transient Log log = LogFactory.getLog(NotificationsServiceImpl.class);
	
	public Object sendRequestNotification(String in ){
		
		String response = null;

		try {
			template = context.createProducerTemplate();
			
			log.debug("SocketRequest[ " + in + " ]");
			
			/*********************************
			 * Enviando Solicitud Asincrona
			 *********************************/
//			Future<String> future = template.asyncRequestBody("seda:notifications",	in, String.class);
//			response = future.get();
			
			response = template.requestBody("seda:notifications",    in, String.class);
			
			log.debug("SocketResponse[ " + response + " ]");
			
		}catch(Exception ex){
			log.debug("Error en el envío del Socket[ "+ex.getMessage() +" ]");
		}
		return response;
		
		
	}
}

