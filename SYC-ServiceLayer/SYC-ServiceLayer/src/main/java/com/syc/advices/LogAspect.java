package com.syc.advices;

import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import  org.apache.commons.logging.*;

import com.syc.utils.GeneralUtil;
import com.syc.utils.SecurityUtils;

@Aspect
public class LogAspect {
		
		private static final transient Log log = LogFactory.getLog(LogAspect.class);
		
		@Pointcut("execution(* *com.syc.services..*.*(..))")
		public void logDebug() {
		}
		
		/** Aspecto que tiene por objetivo controlar el debug de los servicios **/
		@Around( "logDebug()")
		public Object LogAOP(ProceedingJoinPoint call) throws Throwable {
			
			long delta = System.currentTimeMillis();
			StringBuilder buffer = new StringBuilder();
			Object response = null;
			
			buffer.append( "-------------------->\n" );
			buffer.append( "service  ==> " + call.getSignature().getName() );
			buffer.append( "\nuser     ==> " + SecurityUtils.getCurrentUser() );
			buffer.append( "\nip       ==> " + SecurityUtils.getRemoteIP() );

			/** Obteniendo los parametros de entrada **/
		  	for(int i = 0 ; i <= call.getArgs().length -1 ; i++){
		  		buffer.append( "\nparam["+i+"] ==> " + (call.getArgs()[i] !=null ? call.getArgs()[i].toString() : "null"));
		  	}
			
		  	try{
		  		/** Invocando el servicio solicitado **/
				response = call.proceed();
				
		  	}catch(Exception msg){
		  		msg.printStackTrace();
		  		String idTicket = GeneralUtil.getFolioErrorApp();
		  		printTraceError(msg, idTicket , buffer );
		  		throw new BusinessException("Ocurrio un problema al procesar su peticion, su id de Reporte es: "+ idTicket);
		  		
		  	}
		  	
		  	if(response != null){
				buffer.append( "\nresponse ==> " + response.toString() );
				buffer.append( "\ntime     ==> " + ( System.currentTimeMillis() - delta ) );
				buffer.append( " ms.\n<--------------------" );
			  	}else{
			  		buffer.append(" no devuelve nada!");
			  	}
			
			log.info( buffer.toString() );
			return response;
		}
		
		private void printTraceError(Exception ex, String ticket, StringBuilder buffer ){
			
			buffer.append( "\nTicket de Error: " + ticket );
			
			/** Imprimiendo en el log de la actividad diaria **/
			log.info( buffer.toString() +"\n<--------------------");
			
			log.error( buffer.toString());
			log.error( "",ex );
			log.error("\n<--------------------");
			
		}

	}



