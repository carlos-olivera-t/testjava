package com.syc.advices;

import org.springframework.core.NestedRuntimeException;

public class BusinessException extends NestedRuntimeException {

	public BusinessException(String msg) {
		super(msg);
	}

	public BusinessException(String mensaje, Throwable causa) {
		super(mensaje, causa);
	}

	/** 
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
