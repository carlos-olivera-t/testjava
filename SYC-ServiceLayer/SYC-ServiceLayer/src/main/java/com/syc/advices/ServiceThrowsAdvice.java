package com.syc.advices;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.syc.exceptions.OasisException;
import com.syc.persistence.dao.AccountDao;
import com.syc.persistence.dto.AccountDto;



@Aspect
@Component
public class ServiceThrowsAdvice {
	

	@AfterThrowing(pointcut = "execution(* com.syc.services..*.*(..))", throwing = "dataAccesEx")
	public void doRecoveryRuntimeException(DataAccessException dataAccesEx)
			throws BusinessException {
		throw new BusinessException("", dataAccesEx);
	}
	
	@AfterThrowing(pointcut = "execution(* com.syc.services..*.*(..))", throwing = "runtimeEx")
	public void doRecoveryRuntimeException(RuntimeException runtimeEx)
			throws BusinessException {
		throw new BusinessException("",runtimeEx);
	}
	
	@AfterThrowing(pointcut = "execution(* com.syc.services..*.*(..))", throwing = "oasisEx")
	public void doRecoveryRuntimeException(OasisException oasisEx)	
			throws BusinessException {
		throw new BusinessException("", oasisEx);
	}
	
	@AfterThrowing(pointcut = "execution(* com.syc.services..*.*(..))", throwing = "ex")
	public void doRecoveryActionDataAccess(Exception ex)
			throws BusinessException {
		throw new BusinessException("Error General: ", ex);
	}
	
	

}
