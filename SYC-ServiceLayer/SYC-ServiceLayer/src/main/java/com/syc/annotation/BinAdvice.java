package com.syc.annotation;

import com.syc.persistence.dao.UserBinDao;
import com.syc.utils.BinsConstants;
import com.syc.utils.SecurityUtils;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

@org.aspectj.lang.annotation.Aspect
public class BinAdvice {

	
	@Autowired
	private UserBinDao userBinDao;
	
	private static final transient Log log = LogFactory.getLog(BinAdvice.class);
	
	@org.aspectj.lang.annotation.Around("@annotation(CheckPAN)")
	public Object seleccionBin(ProceedingJoinPoint call)
		    throws Throwable
		  {
		    log.info("\n ===============> Entrando a BinAdvice <===============");
		    Method metodoSolicitado = getMetodoSolicitado(call.getSignature());
		    
		    int pos = ((CheckPAN)metodoSolicitado.getAnnotation(CheckPAN.class)).idParameter();
		    Class<?> clazz = ((CheckPAN)metodoSolicitado.getAnnotation(CheckPAN.class)).type();
		    
		    Object param = call.getArgs()[(pos - 1)];
		    String pan = discoverPAN(param, clazz);
		    boolean isPermitido = false;
		    String bin = null;
		    if ((pan.isEmpty()) || (pan == null)) {
		      return call.proceed();
		    }
		    if (pan.length() > 8)
		    {
		      bin = pan.substring(0, 8);
		      String currentUser = SecurityUtils.getCurrentUser();
		      log.info("request => [" + pan + "," + bin + "," + currentUser + "]");

		      List<String> usuariosPermitidos = new BinsConstants().getUsuarios(bin);
		      
		      int users = usuariosPermitidos.size();
		      for (int i = 0; i < users; i++) {
		        if ((((String)usuariosPermitidos.get(i))).equals(currentUser))
		        {
		          isPermitido = true;
		          break;
		        }
		      }
		      
//				List<String> binesPermitidos = userBinDao.getBinesByUser(currentUser);
		      //
//		      		boolean isPermitido = false;
		      //
//		      		for (int i = 0; i < binesPermitidos.size(); i++) {
//		      			if (binesPermitidos.get(i).equals(bin)) {
//		      				isPermitido = true;
//		      				break;
//		      			}
//		      		}
		    }
		    else
		    {
		      log.info("Longitud no permitida[ " + pan + " ] ");
		      throw new AccessDeniedException("Longitud no permitida [ " + pan + " ] ");
		    }
		    if (!isPermitido)
		    {
		      log.info("operacion NO permitida sobre el BIN [ " + bin + " ] ");
		      throw new AccessDeniedException("operacion no permitida sobre el BIN [ " + bin + " ] ");
		    }
		    log.info("operacion permitida sobre el BIN [ " + bin + " ] ");
		    

		    return call.proceed(call.getArgs());
		  }

	/**
	 * Clase que extrae el metodo de la frima solicitada
	 * 
	 * @param firmaMetodo
	 * @return
	 */
	private Method getMetodoSolicitado(Signature firmaMetodo) {
		Class<?> c = firmaMetodo.getDeclaringType();

		String methodName = firmaMetodo.getName();
		Method[] metodos = c.getMethods();
		Method metodoSolicitado = null;

		for (int i = 0; i < metodos.length; i++) {
			if (metodos[i].getName().equals(methodName)) {
				metodoSolicitado = metodos[i];
				break;
			}
		}
		return metodoSolicitado;
	}

	/**
	 * Método que obtiene el PAN
	 * 
	 * @param param
	 * @param clazz
	 * @return
	 */
	private String discoverPAN(Object param, Class<?> clazz) {
		String pan = null;
		
		
		if (param instanceof String) {
			pan = (String) param;
		} else {
			try {
				Method[] metodos = clazz.getDeclaredMethods();

				for (int i = 0; i < metodos.length; i++) {
					String methodName = metodos[i].getName();

					if (methodName.equalsIgnoreCase("getPan")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					} else if (methodName.equalsIgnoreCase("getTarjeta")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					} else if (methodName.equalsIgnoreCase("getNumberCard")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					} else if (methodName.equalsIgnoreCase("getCardNumber")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					} else if (methodName.equalsIgnoreCase("getNumeroTarjeta")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					
					} else if (methodName.equalsIgnoreCase("getCard")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
					break;
				}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return pan;
	}

}