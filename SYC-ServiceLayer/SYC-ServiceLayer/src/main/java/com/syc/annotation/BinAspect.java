package com.syc.annotation;

import java.lang.reflect.Method;
import java.util.List;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Around;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;



import com.syc.persistence.dao.UserBinDao;
//import com.syc.persistence.dao.UserBinDao;
import com.syc.utils.SecurityUtils;


//@Aspect
public class BinAspect {

	@Autowired
	private UserBinDao userBinDao;

	//@Pointcut("execution(public * com.syc.ws.endpoint.cibanco.impl.*.*(..))")
	// && !execution(* com.syc.ws.endpoint.cibanco.impl.embosser*.*(..))
	public void webServiceCIBanco() {
	}

	//@Around("webServiceCIBanco()")
	public Object binCIBanco(ProceedingJoinPoint call) throws Throwable {

		Object param = null;
		// String servicio =
		// discoverService(call.getSignature().getDeclaringType());

		if (call.getArgs().length >= 1) {
			param = call.getArgs()[0];
		}
		
		if(call.getSignature().getName().contains("embosser")){
			return call.proceed(call.getArgs());
		}

		String pan = discoverPAN(param, param.getClass());

		if (pan != null) {
			String bin = pan.substring(0, 8);
			
			if(call.getSignature().getName().contains("stockCardHolderAssignment") || call.getSignature().getName().equals("asignaNIP")){
				if(bin.contains("460068") || bin.contains("474646")){
					return call.proceed(call.getArgs());
				}else
					throw new org.springframework.security.access.AccessDeniedException(
							"operacion no permitida sobre el BIN [ " + bin + " ] ");
			}

			String currentUser = SecurityUtils.getCurrentUser();
			List<String> binesPermitidos = userBinDao.getBinesByUser(currentUser);

			boolean isPermitido = false;

			for (int i = 0; i < binesPermitidos.size(); i++) {
				if (binesPermitidos.get(i).equals(bin)) {
					isPermitido = true;
					break;
				}
			}

			if (!isPermitido) {
				throw new org.springframework.security.access.AccessDeniedException(
						"operacion no permitida sobre el BIN [ " + bin + " ] ");
			}
		}

		return call.proceed(call.getArgs());

	}

	/**
	 * Método que obtine el servicio que ejecuta el AOP
	 * 
	 * @param clazz
	 * @return
	 * 
	 *         private String discoverService(Class<?> clazz) { String service =
	 *         null;
	 * 
	 *         Package userPackage = clazz.getPackage(); String packageName =
	 *         userPackage.getName();
	 * 
	 *         if(packageName.contains("com.syc.ws.endpoint.")){ service =
	 *         packageName.replace("com.syc.ws.endpoint.", ""); }
	 * 
	 *         return service; }
	 */

	/**
	 * Método que obtiene el PAN
	 * 
	 * @param param
	 * @param clazz
	 * @return
	 */
	private String discoverPAN(Object param, Class<?> clazz) {
		String pan = null;
		String cuenta = null;
		

		if (param instanceof String) {
			pan = (String) param;
		} else {
			try {
				Method[] metodos = clazz.getDeclaredMethods();

				for (int i = 0; i < metodos.length; i++) {
					String methodName = metodos[i].getName();

					if (methodName.equalsIgnoreCase("getPan")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					} else if (methodName.equalsIgnoreCase("getTarjeta")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					} else if (methodName.equalsIgnoreCase("getNumberCard")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					} else if (methodName.equalsIgnoreCase("getCardNumber")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					} else if (methodName.equalsIgnoreCase("getNumeroTarjeta")) {
						Method m = metodos[i];

						pan = (String) m.invoke(param);
						break;
					} else if (methodName.equalsIgnoreCase("getCuenta")) {
						Method m = metodos[i];

						cuenta = (String) m.invoke(param);
						break;
					}
				}

				if (cuenta != null) {
					//log.info("Es una cuenta" + cuenta);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return pan;
	}

}
