package com.syc.exceptions;

public class OasisException extends Exception{
	
	/**
	 * @Author Angel Contreras Torres
	 * @Fecha
	 * @Description Excepción que se lanza cuando existe algún problema 
	 *              al enviar una petición al OASIS
	 */
	private static final long serialVersionUID = 1L;

	public OasisException(String description) {
		super(description);
	}

	public OasisException(String description, Throwable throwable) {
		super(description, throwable);
	}


}
