package com.syc.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.services.AccountWithdrawalService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
									"/persistence-app-TEST-ctx.xml", 
									"/services-app-ctx.xml",									
									"/transaction-app-ctx.xml",
									"/camel-routes-ctx.xml"})
public class AccountWhitdrawalTest {
	
	private static final int RECARGA_EXITOSA       = 1; 
	private static final int TARJETA_NO_ENCONTRADA = 8;
	private static final int FONDOS_INSUFICIENTES  = 5;
	private static final int IMPORTE_ERRONEO       = 6;
	
	
	@Autowired
	AccountWithdrawalService withdrawalService;
	
	String pan  = "5887711000110889735";
	String user = "angelctmex";
	
	BalanceMovementRespDto response;

	@Ignore
	@Test
	public void isNotNull(){
		assertNotNull( withdrawalService );
	}
	@Ignore
	@Test 
	public void tarjetaNoEncontrada(){
		response = withdrawalService.doWithdrawalAccount(pan+"123", 1, user);
		assertEquals(response.getCode(), TARJETA_NO_ENCONTRADA);
	}
	
	@Test 
	public void fondosInsuficientes(){
		response = withdrawalService.doWithdrawalAccount(pan, 10000, user);
		assertEquals(response.getCode(), FONDOS_INSUFICIENTES);
	}
	@Ignore
	@Test 
	public void importeErroneo(){
		response = withdrawalService.doWithdrawalAccount(pan, 0, user);
		assertEquals(response.getCode(), IMPORTE_ERRONEO);
	}

	@Ignore
	@Test 
	public void realizaRetiro(){
		response = withdrawalService.doWithdrawalAccount(pan, 1, user);
		assertEquals(response.getCode(), RECARGA_EXITOSA);
	}
	
	

}
