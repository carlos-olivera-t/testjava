package com.syc.service.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.syc.dto.request.ChequeDto;
import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.CardholderDto;
import com.syc.services.DepositAccountService;
import com.syc.utils.SystemConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
									"/persistence-app-TEST-ctx.xml", 
									"/services-app-ctx.xml",									
									"/transaction-app-ctx.xml",
									"/camel-routes-ctx.xml"})
public class DepositosTest {
	
	private static final int RECARGA_EXITOSA       = 1; 
	private static final int TARJETA_INACTIVA      = 2;
	private static final int TARJETA_NO_ENCONTRADA = 8;
	
	
	@Autowired
	DepositAccountService depositAccountService;
	
	@Autowired
	CardDao cardDao;
	
	@Autowired
	CardholderDao cardHolderDao;
	
	CardholderDto cardHolderDto;
	
	String pan  = "5887711000110889735";
	String user = "angelctmex";
	
	double depositAmount       = 30;
	double availabletBalanceDB =  0.0;
	
	boolean activationCard;
	@Ignore
	@Test
	public void isNotNull(){
		assertNotNull( depositAccountService );
	}
	
	@Ignore
	@Test
	public void init() throws Exception{
		
		/***************************
		 *   Ináctivando Tarjeta
		 ***************************/
		CardDto cardDto = cardDao.findByNumberCard(pan);
		cardDto.setStatus( SystemConstants.STATUS_INACTIVE_CARD );
		cardDao.sqlUpdate( cardDto );
		
	}
	@Ignore
	@Test
	public void depositoConTarjetaInactiva() throws Exception{
		
		/***************************************************
		 * Realizando un Depósito con una cuenta Inactiva
		 *  con el parámetro activationCard en falso
		 ***************************************************/
		
		BalanceMovementRespDto response = depositAccountService.doDepositAccount(pan, depositAmount, activationCard, user);
		
//		assertEquals(response.getCode(), TARJETA_INACTIVA);
//		assertEquals(response.getAuthorization(), "0");
		
	}
	@Ignore
	@Test
	public void depositoConActivacion() throws Exception{
		
		/******************************************
		 *   Obteniendo el Saldo de la Cuenta
		 ******************************************/
		cardHolderDto       = cardHolderDao.getCardholderInfoByCardNumber(pan);
		availabletBalanceDB = cardHolderDto.getAccount().getAvailableBalance();
		
		/***************************************************
		 * Realizando un Depósito con una cuenta Inactiva
		 *  con el parámetro activationCard en true
		 ***************************************************/
		
		activationCard = true;
		
		BalanceMovementRespDto response = depositAccountService.doDepositAccount(pan, depositAmount, activationCard, user);
		
		assertEquals(response.getCode(), RECARGA_EXITOSA);
		assertTrue( Integer.parseInt( response.getAuthorization() ) > 0 );
		assertEquals( new Double(response.getCurrentBalance()) , new Double(availabletBalanceDB+depositAmount));
		assertEquals( new Double(response.getBalance()) , new Double(availabletBalanceDB));
		
	}
	@Ignore
	@Test
	public void cuentaInexistente() throws Exception{
		
		/*******************************************
		 * Enviando una cuenta Inexistente para 
		 * 		  validar la Respuesta
		 *******************************************/
		
		pan = "5800011000110889735";
		
		BalanceMovementRespDto response = depositAccountService.doDepositAccount(pan, depositAmount, activationCard, user);
		
		assertEquals(response.getCode(), TARJETA_NO_ENCONTRADA);
		assertEquals(response.getAuthorization(), "0");
		
	}
	@Ignore
	@Test
	public void depositoCheque() throws Exception{
		double amount=1245.00;
		double importe=2245.00;
		String banco="cibanco";
		String referencia="referencia";
		int type=1;
		String pan = "5453250100087008";
		
		ChequeDto cheque = new ChequeDto();

		cheque.setImporte(importe);
		cheque.setBanco(banco);
		cheque.setReferencia(referencia);
		cheque.setType(type);

		List<ChequeDto> cheques = new ArrayList<ChequeDto>();

		cheques.add(cheque);
		
		BalanceMovementRespDto response = depositAccountService.doDepositAccount(pan, amount, cheques);
		
		System.out.println(response.getCode()+ response.getDescription());

		
		
	}
	
	@Test
	public void maximoDepositoDiario() throws Exception{
		
		
	}
	

	@Test
	public void saldoMaximo() throws Exception{
		
		
	}
}
