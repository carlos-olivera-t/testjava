package com.syc.service.test;



import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.syc.dto.request.DemographicDataRequestDto;
import com.syc.dto.response.CardAssignmentResponseDto;
import com.syc.services.CardHolderService;

@ContextConfiguration(locations = { 
		"/camel-routes-ctx.xml"})
public class CardHolderServiceTest extends BaseTest{
	
	@Autowired
	CardHolderService cardHolderService;
	
	//@Ignore
	@Test
	public void updateDemographicData001(){
		DemographicDataRequestDto demographicRequestDto = new DemographicDataRequestDto();
		demographicRequestDto.setAddress("Pva Mextli Mz A");
		demographicRequestDto.setCellPhone("55156955");
		demographicRequestDto.setCity("Mexico");
		demographicRequestDto.setColony("Cuahutemoc");
		demographicRequestDto.setHomePhone("13834577");
		demographicRequestDto.setName("Paco Reseniz");
		demographicRequestDto.setShortName("Paco");
		demographicRequestDto.setZipCode("55678");
		demographicRequestDto.setMunicipality("Ecatepunk");
		demographicRequestDto.setRfc("recf820720");
		demographicRequestDto.setState("mex");
		demographicRequestDto.setWorkPhone("111111");
		
		login("master", "koala");
		
		cardHolderService.updateDemographicDataClient("538984","868915", demographicRequestDto);
		//cardHolderService.updateDemographicData("5063030100000314", demographicRequestDto);
	}
	
	
}
