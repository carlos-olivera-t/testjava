package com.syc.service.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


import com.syc.dto.response.LevelCaptationResponseDto;
import com.syc.services.CatchmentLevelsService;


public class CatchmentLevelsServiceTest extends BaseTest{
	
	@Autowired
	CatchmentLevelsService catchmentLevelsService;
	
	
	@Test
	public void isNotNull(){
		assertNotNull( catchmentLevelsService );
	}
	
	
	@Test
	public void validateAmount(){
		login("test", "test");
		LevelCaptationResponseDto response = catchmentLevelsService.maxAmount("5339870900000016   ", 250, "ws_test");
		
		
		System.out.println("\n\n\n"+response.toString()+"\n\n\n");
		
	}

}
