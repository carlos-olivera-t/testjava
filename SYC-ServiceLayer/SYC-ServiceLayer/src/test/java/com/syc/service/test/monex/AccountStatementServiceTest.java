package com.syc.service.test.monex;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.syc.dto.response.AccountStatementResponseDto;
import com.syc.persistence.dto.mapper.AccountStatementDto;
import com.syc.services.monex.AccountStatementService;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
									"/persistence-app-TEST-ctx.xml", 
									"/ws-clients-app-ctx.xml",
									"/services-app-ctx.xml",									
									"/transaction-app-ctx.xml",
									"/camel-routes-ctx.xml"})
public class AccountStatementServiceTest {
	
	private static final transient Log log = LogFactory.getLog(AccountStatementServiceTest.class);
	
	@Autowired
	AccountStatementService accountStatementService;
	@Ignore
	@Test
	public void isNotNull(){
		assertNotNull( accountStatementService );
	}
	@Ignore
	@Test
	public void getPostgresInfo(){
		AccountStatementResponseDto response = accountStatementService.getPostgresAccountStatement("B130300001", "01");
		
		log.debug("*****************************************************");
		for( AccountStatementDto row: response.getListAccountStatement()){
			log.debug(row.toString());
		}
		log.debug("*****************************************************\n");
	}
	
	
//	@Test
//	public void getMonexInfo(){
//		AccountStatementResponseDto response = accountStatementService.getMonexAccountStatement("101400001", null);
//		
//		log.debug("*****************************************************");
//		for( AccountStatementDto row: response.getListAccountStatement()){
//			log.debug(row.toString());
//		}
//		log.debug("*****************************************************\n");
//	}

}
