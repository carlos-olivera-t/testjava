package com.syc.service.test;

import static org.junit.Assert.*;

import java.sql.Date;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.syc.dto.response.BasicResponseDto;
import com.syc.utils.GeneralUtil;
import com.syc.services.NIPService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
									"/persistence-app-TEST-ctx.xml", 
									"/services-app-ctx.xml",
									"/transaction-app-ctx.xml",
									"/camel-routes-ctx.xml"})
public class NIPTest {
	
//	private static final transient Log log = LogFactory.getLog(NIPTest.class);
	
	private static final int VALIDACION_EXITOSA = 1;
	private static final int VALIDACION_FALLIDA = 2;
	
	@Autowired
	NIPService nipService;
	
	String pan  = "6395590000000018";
	String pin  = "6AD64BDF26AD7CB4";
	
	@Test
	public void isNotNull(){
		assertNotNull( nipService );
	}
	
	@Ignore
	@Test
	public void validaNIPInCorrecto() throws Exception{
		
		/***********************************
		 *    Validando un nip Correcto
		 ***********************************/
		
		BasicResponseDto response = nipService.pinValidator(pan, pin);
		System.out.println("=============================");
		System.out.println( response.toString() );
		System.out.println("=============================");
//		assertEquals(response.getCodigo(), VALIDACION_EXITOSA);
		
	}
	@Ignore
	@Test
	public void validaCVV() throws Exception{
		
		/***********************************
		 *    Validando CVV
		 ***********************************/
		
		java.util.Date utilDate = GeneralUtil.getDateFormat("dd/MM/yyyy", "11/06/2013"); 
		Date expiryDate = new Date( utilDate.getTime() );
		Thread.sleep(30000);
		String response;
		System.out.println("=============================");
		for( int i=0; i<5; i++ ){
			response  = nipService.getCVV(pan, expiryDate, "000");
			System.out.println( response );
		}
		System.out.println("=============================");
		
//		String response  = nipService.getCVV(pan, expiryDate, "000");
//		String response2 = nipService.getCVV(pan, expiryDate, "000");
//		System.out.println("=============================");
//		System.out.println( response );
//		System.out.println( response2 );
//		System.out.println("=============================");
//		assertEquals(response.getCodigo(), VALIDACION_EXITOSA);
		
	}
	@Ignore
	@Test
	public void validaNIPCorrecto() throws Exception{
		
		/***********************************
		 *    Validando un nip Erroneo
		 ***********************************/
		
		pin = "7845";
		
		BasicResponseDto response = nipService.pinValidator(pan, pin);
		System.out.println("=============================");
		System.out.println( response.toString() );
		System.out.println("=============================");
		assertEquals(response.getCodigo(), VALIDACION_FALLIDA);
		
	}
	@Ignore
	@Test
	public void asignacionDeNIP() throws Exception{
		
		/***************************************************
		 * Realizando un Depésito con una cuenta Inactiva
		 *  con el parámetro activationCard en falso
		 ***************************************************/
		
		BasicResponseDto response = nipService.pinAssignment(pan, pin);
//		BasicResponseDto response = nipService.pinAssignment("111", "6AD64BDF26AD7CB3");
		System.out.println("=============================");
		System.out.println( response.toString() );
		System.out.println("=============================");
		
	}
	
	@Ignore
	@Test
	public void ChangeNIP() throws Exception{
		
		/***************************************************
		 * Realizando un Depésito con una cuenta Inactiva
		 *  con el parámetro activationCard en falso
		 ***************************************************/
		
		BasicResponseDto response = nipService.pinChange(pan, pin, "123456789");
//		BasicResponseDto response = nipService.pinAssignment("111", "6AD64BDF26AD7CB3");
		System.out.println("=============================");
		System.out.println( response.toString() );
		System.out.println("=============================");
		
	}
	

}
