package com.syc.service.test.monex;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.syc.dto.request.TransferRequestDto;
import com.syc.dto.response.TransferResponseDto;
import com.syc.persistence.dao.CardholderDao;
import com.syc.persistence.dao.EmployeeDao;
import com.syc.persistence.dto.CardholderDto;
import com.syc.persistence.dto.EmployeeDto;
import com.syc.service.test.BaseTest;
import com.syc.services.monex.TransfersService;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SystemConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/camel-routes-ctx.xml"})

public class TransfersTest extends BaseTest {
	
	@Autowired
	TransfersService transfersService;
	
	@Autowired
	EmployeeDao employeeDao;
	
	@Autowired
	CardholderDao cardHolderDao;
	
	/** Tipos de Movimiento **/
	final String CONCENTRATOR_TO_PAN                = "01";
	final String PAN_TO_PAN                         = "02";
	final String PAN_TO_CONCENTRATOR_PARTIAL_AMOUNT = "03";
	final String PAN_TO_CONCENTRATOR_TOTAL_AMOUNT   = "04";
	
	/** CÓDIGOS DE RESPUESTA **/
	final int TRANSFERENCIA_EXITOSA              = 0;//1; 
	final int CUENTA_ORIGEN_INEXISTE             = 9;//2; 
	final int CUENTA_DESTINO_INEXISTE            = 11;//3;
	final int CUENTAS_CON_DISTINTOS_PRODUCTOS    = 6;//4;
	final int CUENTA_ORIGEN_NO_ES_CONCENTRADORA  = 1;//5;
	final int CUENTA_DESTINO_ES_CONCENTRADORA    = 15;//6;
	final int CUENTA_ORIGEN_ES_CONCENTRADORA     = 14;//7;
	final int CUENTA_DESTINO_NO_ES_CONCENTRADORA = 4;//8;
	final int MOVIMIENTO_INVALIDO                = 13;//9;
	final int CUENTA_ORIGEN_CANCELADA            = 2;//10
	final int FONDOS_INSUFICIENTES               = 3;//11;
	final int CUENTA_ORIGEN_DESTINO_IGUALES 	 = 12;
	
	/**  **/
	final String CONCENTRADORA_ALPHA = "98870727"; //CONCENTRADORA 1 5453250100087008   
	final String CONCENTRADORA_BETA  = "98872255"; //CONCENTRADORA 2 5452900100029118   
	
	final String ACCOUNT_ZEUS        = "90001960"; //GRUPO 1
	final String ACCOUNT_FENIX       = "90001952"; //GRUPO 1
	
	final String ACCOUNT_JAVA        = "90002053"; //GRUPO 2
	final String ACCOUNT_GROOVY      = "90001949"; //GRUPO 2
	final String ACCOUNT_DIFERENT    = "90000973";
	
	int currentStatus = 0;
	
	int clave_emisor = 0;
	@Test
	public void isNotNull(){
		assertNotNull( transfersService );
		assertNotNull( employeeDao );
	}
	
	/** Validación de una Cuenta Destino Inexistente & Vacia **/
	@Test
	public void concentradoraToNull(){
		
		TransferRequestDto request = new TransferRequestDto(CONCENTRADORA_ALPHA, null);
		request.setSourceAmount(1);
		request.setMovementType( CONCENTRATOR_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request,  clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTA_DESTINO_INEXISTE);
	}
	
	/** Validación de una Cuenta Origen Inexistente & Vacia **/
	@Ignore
	@Test
	public void NullToConcentradora(){
		
		TransferRequestDto request = new TransferRequestDto(null, CONCENTRADORA_BETA);
		request.setSourceAmount(1);
		request.setMovementType( CONCENTRATOR_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTA_ORIGEN_INEXISTE);
	}

	/** De cuenta concentradora a Cuenta Normal, pero diferente producto **/
	@Ignore
	@Test
	public void distintosProductos(){
		
		TransferRequestDto request = new TransferRequestDto(CONCENTRADORA_ALPHA, ACCOUNT_DIFERENT);
		request.setSourceAmount(1);
		request.setMovementType( CONCENTRATOR_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTAS_CON_DISTINTOS_PRODUCTOS);
		
	}
	
	/** De cuenta concentradora Cancelada a Pan **/
	@Ignore
	@Test
	public void concentradoraCanceladaToPan(){
		
		/** Cancelando temporalmente la cuenta Origen**/
		EmployeeDto accountSource = employeeDao.findEmployeeByAccount(CONCENTRADORA_BETA);
		currentStatus = accountSource.getStatus();
		accountSource.setStatus(SystemConstants.STATUS_CANCEL_CARD);
		employeeDao.update(accountSource);
		
		
		TransferRequestDto request = new TransferRequestDto(CONCENTRADORA_BETA, ACCOUNT_GROOVY);
		request.setSourceAmount(1);
		request.setMovementType( CONCENTRATOR_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
//		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTA_ORIGEN_CANCELADA);
		
		/** Devolviendo el estatus original **/ 
		accountSource.setStatus(currentStatus);
		employeeDao.update(accountSource);
		
	}
	
	/************************************************************
	 * Validaciones de Tipos de Movimientos con Cuentas Erroneas IIIII 
	 ************************************************************/
	@Ignore
	@Test
	public void ConToPanC_C(){
		
		TransferRequestDto request = new TransferRequestDto(CONCENTRADORA_ALPHA, CONCENTRADORA_BETA);
		request.setSourceAmount(1);
		request.setMovementType( CONCENTRATOR_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTA_DESTINO_ES_CONCENTRADORA);
	}
	@Ignore
	@Test
	public void PanToPanC_P(){
		
		TransferRequestDto request = new TransferRequestDto(CONCENTRADORA_ALPHA, ACCOUNT_FENIX);
		request.setSourceAmount(1);
		request.setMovementType( PAN_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTA_ORIGEN_ES_CONCENTRADORA);
	}
	@Ignore
	@Test
	public void PanToPanP_C(){
		
		TransferRequestDto request = new TransferRequestDto(ACCOUNT_FENIX, CONCENTRADORA_ALPHA);
		request.setSourceAmount(1);
		request.setMovementType( PAN_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTA_DESTINO_ES_CONCENTRADORA);
	}
	@Ignore
	@Test
	public void PanToConC_P(){
		
		TransferRequestDto request = new TransferRequestDto(CONCENTRADORA_ALPHA, ACCOUNT_ZEUS);
		request.setSourceAmount(10);
		request.setMovementType( PAN_TO_CONCENTRATOR_TOTAL_AMOUNT );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTA_ORIGEN_ES_CONCENTRADORA);
	}
	@Ignore
	@Test
	public void PanToConP_P(){
		
		TransferRequestDto request = new TransferRequestDto(ACCOUNT_JAVA, ACCOUNT_GROOVY);
		request.setSourceAmount(1);
		request.setMovementType( PAN_TO_CONCENTRATOR_TOTAL_AMOUNT );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTA_DESTINO_NO_ES_CONCENTRADORA);
	}
	
	@Ignore
	@Test
	public void PanToConErrP_P(){
		
		TransferRequestDto request = new TransferRequestDto("98871988", "98871989");
		request.setSourceAmount(1);
		request.setMovementType( PAN_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), TRANSFERENCIA_EXITOSA);
	}
	
	/** De cuenta concentradora a Cuenta Normal Aprobada **/
	@Ignore
	@Test
	public void ConcentradoraToPanOK(){
		
		double sourceAmount      = 0;
		double destAmount        = 0;
		
		double transferAmount    = 5;
		
		/** Búscando Información de las cuentas a afectar **/
		CardholderDto sourceCardHolder      = cardHolderDao.getCardholderInfoByAccountTBLEmployee(CONCENTRADORA_BETA);
		CardholderDto destinationCardholder = cardHolderDao.getCardholderInfoByAccountTBLEmployee(ACCOUNT_GROOVY);
		
		if( sourceCardHolder!=null && destinationCardholder!=null ){
			sourceAmount      = sourceCardHolder.getAccount().getAvailableBalance();
			destAmount = destinationCardholder.getAccount().getAvailableBalance();
		}
		
		TransferRequestDto request = new TransferRequestDto(CONCENTRADORA_BETA, ACCOUNT_GROOVY);
		request.setSourceAmount( transferAmount );
		request.setMovementType( CONCENTRATOR_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);

		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), TRANSFERENCIA_EXITOSA);
		assertEquals( (sourceAmount-transferAmount), response.getSourceAvailableBalance(), 0);
		assertEquals( (destAmount+transferAmount), response.getDestinationAvailableBalance(), 0);
	}
	
	/** De cuenta concentradora a Cuenta Normal Aprobada **/
	@Ignore
	@Test
	public void fondosInsuficientes(){
		
		TransferRequestDto request = new TransferRequestDto(CONCENTRADORA_BETA, ACCOUNT_GROOVY);
		request.setSourceAmount( 100000 );
		request.setMovementType( CONCENTRATOR_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);
		
		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), FONDOS_INSUFICIENTES);
		
	}
	@Ignore	
	@Test
	public void validacionCuaebtasIguales(){
		
		double destAmount        = 0;
		
		double transferAmount    = 5;
		
		/** Búscando Información de las cuentas a afectar **/
		CardholderDto destinationCardholder = cardHolderDao.getCardholderInfoByAccountTBLEmployee(ACCOUNT_GROOVY);
		
		if( destinationCardholder!=null ){			
			destAmount = destinationCardholder.getAccount().getAvailableBalance();
		}
		
		TransferRequestDto request = new TransferRequestDto(ACCOUNT_GROOVY, ACCOUNT_GROOVY);
		request.setSourceAmount( transferAmount );
		request.setMovementType( CONCENTRATOR_TO_PAN );
		
		TransferResponseDto response = transfersService.onlineTransfers(request, clave_emisor);

		assertEquals(GeneralUtil.convertStringToInt(response.getCode()), CUENTA_ORIGEN_DESTINO_IGUALES);		
	}
	
	
}
