package com.syc.service.test;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.syc.dto.request.DemographicDataRequestDto;
import com.syc.dto.response.CardAssignmentResponseDto;
import com.syc.services.AditionalCardRequestDto;
import com.syc.services.CardIssuanceService;

//@ContextConfiguration(locations = {"/camel-routes-ctx.xml"})

public class CardIssuanceServiceTest extends BaseTest{
	
	@Autowired
	CardIssuanceService cardIssuanceService;
	
	/**
	 * Alta exito
	 */
	@Ignore
	@Test
	public void embosserCardAssignment001(){
		login("test", "test");
		
		DemographicDataRequestDto demographicRequestDto = new DemographicDataRequestDto();
		demographicRequestDto.setAddress("Pto Zihua 121");
		demographicRequestDto.setCellPhone("5540697777");
		demographicRequestDto.setCity("DF");
		demographicRequestDto.setColony("Casas Aleman");
		demographicRequestDto.setHomePhone("75877587");
		demographicRequestDto.setName("Angel Contreras Torres");
		demographicRequestDto.setShortName("Angel Contreras.");
		demographicRequestDto.setZipCode("07580");
		demographicRequestDto.setMunicipality("Gustavo A. Madero");
		demographicRequestDto.setRfc("COTA821218");
		demographicRequestDto.setState("MEX");
		demographicRequestDto.setWorkPhone("35293529");
		
		
		CardAssignmentResponseDto response = cardIssuanceService.embosserCardAssignment("460068","00", "7099999980", demographicRequestDto);
		
		//Log.debug(response.toString());
		
		Assert.assertEquals(response.getCode(), 1);		
	}
	
	
	/**
	 * La cuenta ya fue asignada
	 */
	@Ignore
	@Test
	public void embosserCardAssignment002(){
		login("test", "test");
		
		DemographicDataRequestDto demographicRequestDto = new DemographicDataRequestDto();
		demographicRequestDto.setAddress("Pto Zihua 121");
		demographicRequestDto.setCellPhone("5540697777");
		demographicRequestDto.setCity("DF");
		demographicRequestDto.setColony("Casas Aleman");
		demographicRequestDto.setHomePhone("75877587");
		demographicRequestDto.setName("Angel Contreras Torres");
		demographicRequestDto.setShortName("Angel Contreras.");
		demographicRequestDto.setZipCode("07580");
		demographicRequestDto.setMunicipality("Gustavo A. Madero");
		demographicRequestDto.setRfc("COTA821218");
		demographicRequestDto.setState("MEX");
		demographicRequestDto.setWorkPhone("35293529");
				
		CardAssignmentResponseDto response = cardIssuanceService.embosserCardAssignment("460068","00", "40999999888", demographicRequestDto); 
		
		Assert.assertEquals(response.getCode(), 4);		
	}
	
	/**
	 * Invalid field length
	 */
	@Ignore
	@Test
	public void embosserCardAssignment003(){
		login("test", "test");
		
		DemographicDataRequestDto demographicRequestDto = new DemographicDataRequestDto();
		demographicRequestDto.setAddress("Pto Zihua 121");
		demographicRequestDto.setCellPhone("5540697777");
		demographicRequestDto.setCity("DF");
		demographicRequestDto.setColony("Casas Aleman");
		demographicRequestDto.setHomePhone("75877587");
		demographicRequestDto.setName("Angel Contreras Torres                           Invalid Lenht");
		demographicRequestDto.setShortName("Angel Contreras.");
		demographicRequestDto.setZipCode("07580");
		demographicRequestDto.setMunicipality("Gustavo A. Madero");
		demographicRequestDto.setRfc("COTA821218");
		demographicRequestDto.setState("MEX");
		demographicRequestDto.setWorkPhone("35293529");
				
		CardAssignmentResponseDto response = cardIssuanceService.embosserCardAssignment("460068","00", "40999999888", demographicRequestDto); 
		
		Assert.assertEquals(response.getCode(), 10);		
	}
	
	/**
	 * La tarjeta enviada no es titular
	 */
	@Ignore
	@Test
	public void aditionalCardAssignment001(){
		login("test", "test");			
		
		AditionalCardRequestDto aditionalCardRequestDto = new AditionalCardRequestDto();
		//aditionalCardRequestDto.setAccount(account);
		aditionalCardRequestDto.setAdditionalMail("francisco.resendiz@syc.com.mx");
		aditionalCardRequestDto.setAdditionalName("Francisco Resendiz");
		aditionalCardRequestDto.setAdditionalRFC("recf820720");
		aditionalCardRequestDto.setAdditionalShortName("Francisco Resendiz");
		aditionalCardRequestDto.setCardholder("4600680000000021");
						
		CardAssignmentResponseDto response = cardIssuanceService.aditionalCardAssignment(aditionalCardRequestDto);
		
		Assert.assertEquals(response.getCode(), 5);	
	}
	
	/**
	 * Alta adicional exitosa
	 */
	@Ignore
	@Test
	public void aditionalCardAssignment002(){
		login("test", "test");			
		
		AditionalCardRequestDto aditionalCardRequestDto = new AditionalCardRequestDto();
		//aditionalCardRequestDto.setAccount(account);
		aditionalCardRequestDto.setAdditionalMail("francisco.resendiz@syc.com.mx");
		aditionalCardRequestDto.setAdditionalName("Francisco Resendiz");
		aditionalCardRequestDto.setAdditionalRFC("recf820720");
		aditionalCardRequestDto.setAdditionalShortName("Francisco Resendiz");
		aditionalCardRequestDto.setCardholder("4600680000004429");
						
		CardAssignmentResponseDto response = cardIssuanceService.aditionalCardAssignment(aditionalCardRequestDto);
		
		//Log.debug(response.toString());
		
		Assert.assertEquals(response.getCode(), 1);	
	}
	
	/** Reemplazo exitoso */
	@Ignore
	@Test
	public void replaceCard001(){
		login("test", "test");
		CardAssignmentResponseDto response = cardIssuanceService.replaceCard("4600680000004429");
		//Log.debug(response.toString());
		Assert.assertEquals(response.getCode(), 1);
	}
	@Ignore
	@Test
	public void tarjetastock(){
		//login("test", "test");
		
		DemographicDataRequestDto demographicRequestDto = new DemographicDataRequestDto();
		demographicRequestDto.setAddress("Pto Zihua 121");
		demographicRequestDto.setCellPhone("5540697777");
		demographicRequestDto.setCity("DF");
		demographicRequestDto.setColony("Casas Aleman");
		demographicRequestDto.setHomePhone("75877587");
		demographicRequestDto.setName("Angel Contreras Torres");
		demographicRequestDto.setShortName("Angel Contreras.");
		demographicRequestDto.setZipCode("07580");
		demographicRequestDto.setMunicipality("Gustavo A. Madero");
		demographicRequestDto.setRfc("COTA821218");
		demographicRequestDto.setState("MEX");
		demographicRequestDto.setWorkPhone("35293529");
				
		//CardAssignmentResponseDto response = cardIssuanceService.stockCardAssignment("0000011000100000001", "5887711000119566243", demographicRequestDto); 
		
		//Assert.assertEquals(response.getCode(), 1);
	}
	
	@Test
	public void stockReplacement() throws Exception{
		login("test", "test");
		DemographicDataRequestDto demographicDataRequestDto = new DemographicDataRequestDto();
		
		demographicDataRequestDto.setName("pruebacibanco");
		demographicDataRequestDto.setShortName("cibanco");
		demographicDataRequestDto.setAddress("pajares 215");
		demographicDataRequestDto.setCellPhone("55304054");
		demographicDataRequestDto.setCity("mexico");
		demographicDataRequestDto.setColony("valle del sur");
		demographicDataRequestDto.setEmail("cibanco@cibanco");
		demographicDataRequestDto.setHomePhone("57357486");
		demographicDataRequestDto.setMunicipality("Iztacalco");
		demographicDataRequestDto.setRfc("baso901029");
		demographicDataRequestDto.setState("mexico");
		demographicDataRequestDto.setWorkPhone("15581863");
		demographicDataRequestDto.setZipCode("00000");
		
		CardAssignmentResponseDto response = cardIssuanceService.stockCardReplacement("00000715", "5061990500000200", "5061990500000192", demographicDataRequestDto, true, true);
		
		Assert.assertEquals(response.getCode(), 0);
	}
	

}
