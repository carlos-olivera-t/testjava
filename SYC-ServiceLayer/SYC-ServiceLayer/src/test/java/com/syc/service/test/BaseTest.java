package com.syc.service.test;

import org.junit.runner.RunWith;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
		"/persistence-app-TEST-ctx.xml", 
		"/services-app-ctx.xml"
		})
@TransactionConfiguration(transactionManager="transactionManager",defaultRollback = false)
@Transactional
public abstract class BaseTest extends AbstractTransactionalJUnit4SpringContextTests{

	 /**
	  * Setea un usuario para poder utilizar getPrincipal en los test de servicios	
	  * @param username
	  * @param password
	  */
	 protected void login(String username, String password) {
	        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(username, password));
	 
	        logger.debug("User:" + username + " logged in");
	    }
	 
	 protected String getLoginDetails() {
	        Object principal = SecurityContextHolder.getContext()
	                .getAuthentication().getPrincipal();
	 
	        if (principal instanceof UserDetails) {
	            return ((UserDetails)principal).getUsername();
	        }
	        else {
	            return principal.toString();
	        }
	 
	    }

}
