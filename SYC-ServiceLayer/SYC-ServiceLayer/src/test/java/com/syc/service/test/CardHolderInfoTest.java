package com.syc.service.test;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.syc.dto.request.BitMatchInfoClientRequestDto;
import com.syc.dto.request.CardholderInfoRequestDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.services.CardHolderService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
									"/persistence-app-TEST-ctx.xml", 
									"/services-app-ctx.xml",
									"/transaction-app-ctx.xml",
									"/camel-routes-ctx.xml"})
public class CardHolderInfoTest {
	
	private final static transient Log log = LogFactory.getLog(CardHolderInfoTest.class);
	
	@Autowired
	private CardHolderService cardHolderService;
	@Ignore
	@Test
	public void isServiceNull(){
		Assert.notNull(cardHolderService);
	}
	
	@Ignore
	@Test
	public void actualizaInfoCliente(){
		
		BitMatchInfoClientRequestDto request = new BitMatchInfoClientRequestDto();
		request.setCodigoPostal("07580");
		request.setColonia("CASAS ALEMAN");
		request.setCuenta("00000140022");//00000140007
		request.setDireccion("CALLE SIEMPRE");
		request.setEntidadFederativa("GUS");
		request.setNombreCorto("ANGEL CONTRERAS");
		request.setNombreLargo("LQS");
		request.setRfc("COTA");
		request.setTelefono("53289");
		request.setTelefono2("0000");
		
		BasicAuthRespDto response = cardHolderService.saveInformationClientBitMatch(request, "SNICOLAS");
		log.debug("===================================");
		log.debug( ReflectionToStringBuilder.toString(response) );
		log.debug("===================================");
		
	}
	
	@Ignore
	@Test
	public void asignaCuenta(){
		
		String account = "10100004521";
		String pan     = "5062810100000567";
		
		BitMatchInfoClientRequestDto request = new BitMatchInfoClientRequestDto();
		request.setCodigoPostal("07580");
		request.setColonia("CASAS ALEMAN");
		request.setCuenta(account);//00000140007
		request.setDireccion("CALLE SIEMPRE");
		request.setEntidadFederativa("DISTRITO FEDERAL");
		request.setNombreCorto("ANGELO");
		request.setNombreLargo("ANGEL CONTRERAS T");
		request.setRfc("COTA");
		request.setTelefono("53289");
		request.setTelefono2("0000");
		
		BasicAuthRespDto response = cardHolderService.assignmentAccount(pan, request);
		
		log.debug("===================================");
		log.debug( response.toString() );
		log.debug("===================================");
		
	}
	
	@Ignore
	@Test
	public void asignaCuentaProdic(){
		
		CardholderInfoRequestDto request = new CardholderInfoRequestDto();
		request.setPan("5339870900000065");
		request.setName("NUM 6 TARJETA PRUEBAS");
		request.setAddress("AV. PATRIOTISMO 229");
		request.setAddress2("PISO 8");
		request.setColony("SAN PEDRO DE LOS PINOS");
		request.setDelegation("BENITO JUAREZ");
		request.setFederalEntity("D.F.");
		request.setState("D.F.");
		request.setZipCode("03800");
		request.setHomePhone("28810220");
		request.setCellPhone("0445512");
		request.setWorkPhone("28810220");
		request.setRfc("TAPN890102");
		
		BasicAuthRespDto response = cardHolderService.cardholderRegistration(request);
		log.debug("===================================");
		log.debug( ReflectionToStringBuilder.toString(response) );
		log.debug("===================================");
		
	}

}
