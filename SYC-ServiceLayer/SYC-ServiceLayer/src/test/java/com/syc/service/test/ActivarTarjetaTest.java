package com.syc.service.test;

import static org.junit.Assert.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.syc.dto.response.BasicAuthRespDto;
import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dto.CardDto;
import com.syc.services.CardService;
import com.syc.utils.SystemConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
									"/persistence-app-TEST-ctx.xml", 
									"/services-app-ctx.xml",
									"/transaction-app-ctx.xml",
									"/camel-routes-ctx.xml"})
public class ActivarTarjetaTest {
	
	private static final transient Log log = LogFactory.getLog(ActivarTarjetaTest.class);
	
	@Autowired
	CardService modifyCardService;
	
	@Autowired
	CardDao cardDao;
	
	String pan  = "5887711000110889735";
	String user = "angelctmex";

	@Ignore
	@Test
	public void isNull(){
		assertTrue( modifyCardService!=null );
	}
	
	@Ignore
//	@Test
	public void inactivarTarjeta() throws Exception{
		
		/***************************
		 *   Inactivando Tarjeta
		 ***************************/
		CardDto cardDto = cardDao.findByNumberCard(pan);
		cardDto.setStatus( SystemConstants.STATUS_INACTIVE_CARD );
		
		cardDao.sqlUpdate( cardDto );
		
	}
	@Ignore
	@Test
	public void activarTarjeta() throws Exception{
		
		/**********************************************
		 * Invocando el servicio para Activar Tarjeta
		 **********************************************/
		BasicAuthRespDto response = modifyCardService.activationCard( pan, user);
		
		log.debug("====================================================");
		log.debug( response.toString() );
		log.debug("====================================================");
		
//		assertEquals(response.getCode(), 1);
//		assertTrue  ( !response.getAuthorization().equals("0")  );
		
	}
	

}
