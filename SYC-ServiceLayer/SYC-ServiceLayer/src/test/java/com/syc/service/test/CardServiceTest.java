package com.syc.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.syc.dto.response.BalanceQueryResponseDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.BasicResponseDto;
import com.syc.persistence.dao.CardDao;
import com.syc.persistence.dto.CardDto;
import com.syc.persistence.dto.SHCLogDto;
import com.syc.services.CardService;
import com.syc.utils.SystemConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
									"/persistence-app-TEST-ctx.xml", 
									"/services-app-ctx.xml",
									"/transaction-app-ctx.xml",
									"/camel-routes-ctx.xml"})
public class CardServiceTest {
	
	
	private static final transient Log log = LogFactory.getLog(CardServiceTest.class);
	
	@Autowired
	CardService cardService;
	
	@Autowired
	CardDao     cardDao;
	
	String pan  = "5887711000110889735";
	String user = "angelctmex";
	@Ignore
	@Test
	public void isNull(){
		assertTrue( cardService!=null );
	}
	
	@Ignore
	@Test
	public void bloqueaTarjeta() throws Exception{
		
		/**************************************
		 *  Realizando una consulta de Saldo
		 **************************************/
		BasicAuthRespDto  response = cardService.cardLock(pan, user);
		System.out.println(response.toString());
		
	}
	
	@Ignore
	@Test	
	public void consultaSaldo() throws Exception{
		
		/**************************************
		 *  Realizando una consulta de Saldo
		 **************************************/
		BalanceQueryResponseDto  response = cardService.balanceQuery(pan, "ws_test");
		System.out.println(response.toString());
		
	}
	@Ignore
	@Test	
	public void consultaSaldoM() throws Exception{
		
		/**************************************
		 *  Realizando una consulta de Saldo
		 **************************************/
		BalanceQueryResponseDto  response = cardService.balanceQueryWhitCommision(pan, 1);
		System.out.println(response.toString());
		
	}
	
	@Ignore
	@Test
	public void consultaEstatus() throws Exception{
		int status = 0;
		/** Obteniendo Información de la Tarjeta **/
		CardDto cardDto = cardDao.findByNumberCard(pan);
		
		if( cardDto != null ){
			status = cardDto.getStatus();
			
			/** Bloqueando la Tarjeta con Código 52[Extraviada] **/
			cardDto.setStatus(52);
			cardDao.sqlUpdate( cardDto );
			
			/** Realizando una nueva búsqueda en la Base de Datos**/
			BasicResponseDto response = cardService.cardStatus(pan, "ws_test");
			
			/** Comparando el Estatus 52 **/
			assertEquals(response.getCodigo(), 52);
			
			/** Devolviendo al estatus Original **/
			cardDto.setStatus(status);
			cardDao.sqlUpdate( cardDto );
		}
	}
	
	@Ignore
	@Test
	public void bloqueoTemporal() throws Exception{
		final int BLOQUEA_TARJETA    = 1;
		final int DESBLOQUEA_TARJETA = 2;
		final int OPERACION_ERRONEA  = 3;
		
		
		int status = 0;
		/** Obteniendo Información de la Tarjeta **/
		CardDto cardDto = cardDao.findByNumberCard(pan);
		
		if( cardDto != null ){
			status = cardDto.getStatus();
			
			/** Activando Tarjeta **/
			cardDto.setStatus(SystemConstants.STATUS_ACTIVE_CARD);
			cardDao.sqlUpdate( cardDto );
			
			/** Invocando un Bloqueo Temporal**/
			BasicAuthRespDto response = cardService.temporaryLock( pan, BLOQUEA_TARJETA );
			/** Valida que la operación sea exitosa **/
			assertEquals(response.getCode(), 1);
			/** Valida que el estatus de la tarjeta tenga Bloqueo Temporal **/
			cardDto = cardDao.findByNumberCard(pan);
			assertEquals(cardDto.getStatus(), SystemConstants.STATUS_TEMPORARY_LOCK);
			
			
			/** Invocando una Operación Erronea **/
			response = cardService.temporaryLock( pan, OPERACION_ERRONEA );
			/** Valida que la respuesta sea rechazada por Código 4[Tipo de Operación Inválida] **/
			assertEquals(response.getCode(), 4);
			
			/** Invocando una Reactivación **/
			response = cardService.temporaryLock( pan, DESBLOQUEA_TARJETA );
			/** Valida que la operación sea éxitosa **/
			assertEquals(response.getCode(), 1);
			
			/** Valida que el estatus de la tarjeta sea Activada **/
			cardDto = cardDao.findByNumberCard(pan);
			assertEquals(cardDto.getStatus(), SystemConstants.STATUS_ACTIVE_CARD);
			
			/** Devolviendo al estatus Original **/
			cardDto.setStatus(status);
			cardDao.sqlUpdate( cardDto );
		}
		
		

	}
	@Ignore
	@Test
	public void  getLogByCardNumberDetails() throws Exception{
		
		SHCLogDto shclogDto = cardService.getLogDetails("5453250100087008", "29-09-2011", 210, "118423");
		if( shclogDto != null ){
			log.debug( shclogDto.toString() );	
		}else{
			log.debug("Informacion No encontrada\n");
		}
	
		
		
	}
	
//	@Test	
//	public void consultaSaldoAkala() throws Exception{
//		
//		pan = "5061990000000023";
//		
//		/**************************************
//		 *  Realizando una consulta de Saldo
//		 **************************************/
//		BalanceQueryResponseDto  response = cardService.akalaBalanceQuery(pan);
//		System.out.println(response.toString());
//		
//	}


}
