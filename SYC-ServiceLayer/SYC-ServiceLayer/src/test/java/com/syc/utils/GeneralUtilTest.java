package com.syc.utils;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;


import com.syc.service.test.BaseTest;

@ContextConfiguration(locations = { 
"/camel-routes-ctx.xml"})
public class GeneralUtilTest extends BaseTest{
	@Ignore
	@Test
	public void buildNumberCard001(){
		String tarjeta = GeneralUtil.buildNumberCard("426808","02", 40009);
		
		Assert.assertTrue("Digito verificador debe ser 2",tarjeta.equals("4268080200400092"));
	}
	@Ignore
	@Test
	public void buildNumberCard002(){
		String tarjeta = GeneralUtil.buildNumberCard("426808","02", 17268);
		
		Assert.assertTrue("Digito verificador debe ser 3",tarjeta.equals("4268080200172683"));
	}
	@Ignore
	@Test
	public void buildNumberCard003(){
		String tarjeta = GeneralUtil.buildNumberCard("426808", "02",16854);
		
		Assert.assertTrue("Digito verificador debe ser 1",tarjeta.equals("4268080200168541"));
	}
	@Ignore
	@Test
	public void buildNumberCard004(){
		String tarjeta = GeneralUtil.buildNumberCard("533987","03", 4704);
		
		Assert.assertTrue("Digito verificador debe ser 7",tarjeta.equals("5339870300047047"));
	}
	@Ignore
	@Test
	public void buildNumberCard005(){
		String tarjeta = GeneralUtil.buildNumberCard("529506","00", 176);
		
		Assert.assertTrue("Digito verificador debe ser 5",tarjeta.equals("5295060000001765"));
	}
	@Ignore
	@Test
	public void buildNumberCard006(){
		String tarjeta = GeneralUtil.buildNumberCard("525678","05", 9559679);
		
		Assert.assertTrue("Digito verificador debe ser 9",tarjeta.equals("5256780595596799"));
	}
	
	
	
	

}
