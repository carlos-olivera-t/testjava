package org.tempuri.dscardsop;  
  
import javax.xml.bind.annotation.XmlRegistry;  
  
  
/**  
 	8	+ * This object contains factory methods for each   
 	9	+ * Java content interface and Java element interface   
 	10	+ * generated in the org.tempuri.dscardsop package.   
 	11	+ * <p>An ObjectFactory allows you to programatically   
 	12	+ * construct new instances of the Java representation   
 	13	+ * for XML content. The Java representation of XML   
 	14	+ * content can consist of schema derived interfaces   
 	15	+ * and classes representing the binding of schema   
 	16	+ * type definitions, element declarations and model   
 	17	+ * groups.  Factory methods for each of these are   
 	18	+ * provided in this class.  
 	19	+ *   
 	20	+ */  
@XmlRegistry  
public class ObjectFactory {  
  
  
    /**  
 	26	+     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri.dscardsop  
 	27	+     *   
 	28	+     */  
    public ObjectFactory() {  
    }  
  
    /**  
 	33	+     * Create an instance of {@link DsCardsOpDef }  
 	34	+     *   
 	35	+     */  
    public DsCardsOpDef createDsCardsOpDef() {  
        return new DsCardsOpDef();  
    }  
  
    /**  
 	41	+     * Create an instance of {@link DsCardsOpDef.Cards }  
 	42	+     *   
 	43	+     */  
    public DsCardsOpDef.Cards createDsCardsOpDefCards() {  
        return new DsCardsOpDef.Cards();  
    }  
 
}  