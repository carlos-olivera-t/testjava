package org.tempuri.dscardsop;  
  
import java.util.ArrayList;  
import java.util.List;  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  
import javax.xml.bind.annotation.XmlType;  
  
/**  
 	14	+ * <p>Java class for anonymous complex type.  
 	15	+ *   
 	16	+ * <p>The following schema fragment specifies the expected content contained within this class.  
 	17	+ *   
 	18	+ * <pre>  
 	19	+ * &lt;complexType>  
 	20	+ *   &lt;complexContent>  
 	21	+ *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	22	+ *       &lt;choice maxOccurs="unbounded" minOccurs="0">  
 	23	+ *         &lt;element name="Cards">  
 	24	+ *           &lt;complexType>  
 	25	+ *             &lt;complexContent>  
 	26	+ *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	27	+ *                 &lt;sequence>  
 	28	+ *                   &lt;element name="Cardnumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>  
 	29	+ *                   &lt;element name="Amount" minOccurs="0">  
 	30	+ *                     &lt;simpleType>  
 	31	+ *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">  
 	32	+ *                         &lt;maxLength value="12"/>  
 	33	+ *                       &lt;/restriction>  
 	34	+ *                     &lt;/simpleType>  
 	35	+ *                   &lt;/element>  
 	36	+ *                   &lt;element name="Sequence" minOccurs="0">  
 	37	+ *                     &lt;simpleType>  
 	38	+ *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">  
 	39	+ *                         &lt;maxLength value="4"/>  
 	40	+ *                       &lt;/restriction>  
 	41	+ *                     &lt;/simpleType>  
 	42	+ *                   &lt;/element>  
 	43	+ *                 &lt;/sequence>  
 	44	+ *               &lt;/restriction>  
 	45	+ *             &lt;/complexContent>  
 	46	+ *           &lt;/complexType>  
 	47	+ *         &lt;/element>  
 	48	+ *       &lt;/choice>  
 	49	+ *     &lt;/restriction>  
 	50	+ *   &lt;/complexContent>  
 	51	+ * &lt;/complexType>  
 	52	+ * </pre>  
 	53	+ *   
 	54	+ *   
 	55	+ */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "", propOrder = {  
    "cards"  
})  
@XmlRootElement(name = "dsCardsOpDef")  
public class DsCardsOpDef {  
  
    @XmlElement(name = "Cards")  
    protected List<DsCardsOpDef.Cards> cards;  
  
    /**  
 	67	+     * Gets the value of the cards property.  
 	68	+     *   
 	69	+     * <p>  
 	70	+     * This accessor method returns a reference to the live list,  
 	71	+     * not a snapshot. Therefore any modification you make to the  
 	72	+     * returned list will be present inside the JAXB object.  
 	73	+     * This is why there is not a <CODE>set</CODE> method for the cards property.  
 	74	+     *   
 	75	+     * <p>  
 	76	+     * For example, to add a new item, do as follows:  
 	77	+     * <pre>  
 	78	+     *    getCards().add(newItem);  
 	79	+     * </pre>  
 	80	+     *   
 	81	+     *   
 	82	+     * <p>  
 	83	+     * Objects of the following type(s) are allowed in the list  
 	84	+     * {@link DsCardsOpDef.Cards }  
 	85	+     *   
 	86	+     *   
 	87	+     */  
    public List<DsCardsOpDef.Cards> getCards() {  
        if (cards == null) {  
            cards = new ArrayList<DsCardsOpDef.Cards>();  
        }  
        return this.cards;  
    }  
  
  
    /**  
 	97	+     * <p>Java class for anonymous complex type.  
 	98	+     *   
 	99	+     * <p>The following schema fragment specifies the expected content contained within this class.  
 	100	+     *   
 	101	+     * <pre>  
 	102	+     * &lt;complexType>  
 	103	+     *   &lt;complexContent>  
 	104	+     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	105	+     *       &lt;sequence>  
 	106	+     *         &lt;element name="Cardnumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>  
 	107	+     *         &lt;element name="Amount" minOccurs="0">  
 	108	+     *           &lt;simpleType>  
 	109	+     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">  
 	110	+     *               &lt;maxLength value="12"/>  
 	111	+     *             &lt;/restriction>  
 	112	+     *           &lt;/simpleType>  
 	113	+     *         &lt;/element>  
 	114	+     *         &lt;element name="Sequence" minOccurs="0">  
 	115	+     *           &lt;simpleType>  
 	116	+     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">  
 	117	+     *               &lt;maxLength value="4"/>  
 	118	+     *             &lt;/restriction>  
 	119	+     *           &lt;/simpleType>  
 	120	+     *         &lt;/element>  
 	121	+     *       &lt;/sequence>  
 	122	+     *     &lt;/restriction>  
 	123	+     *   &lt;/complexContent>  
 	124	+     * &lt;/complexType>  
 	125	+     * </pre>  
 	126	+     *   
 	127	+     *   
 	128	+     */  
    @XmlAccessorType(XmlAccessType.FIELD)  
    @XmlType(name = "", propOrder = {  
        "cardnumber",  
        "amount",  
        "sequence"  
    })  
    public static class Cards {  
  
        @XmlElement(name = "Cardnumber")  
        protected String cardnumber;  
        @XmlElement(name = "Amount")  
       protected String amount;  
        @XmlElement(name = "Sequence")  
        protected String sequence;  
 
        /**  
 	145	+         * Gets the value of the cardnumber property.  
 	146	+         *   
 	147	+         * @return  
 	148	+         *     possible object is  
 	149	+         *     {@link String }  
 	150	+         *       
 	151	+         */  
        public String getCardnumber() {  
            return cardnumber;  
        }  
  
        /**  
 	157	+         * Sets the value of the cardnumber property.  
 	158	+         *   
 	159	+         * @param value  
 	160	+         *     allowed object is  
 	161	+         *     {@link String }  
 	162	+         *       
 	163	+         */  
        public void setCardnumber(String value) {  
            this.cardnumber = value;  
        }  
 
       /**  
 	169	+         * Gets the value of the amount property.  
 	170	+         *   
 	171	+         * @return  
 	172	+         *     possible object is  
 	173	+         *     {@link String }  
 	174	+         *       
 	175	+         */  
        public String getAmount() {  
            return amount;  
        }  
  
        /**  
 	181	+         * Sets the value of the amount property.  
 	182	+         *   
 	183	+         * @param value  
 	184	+         *     allowed object is  
 	185	+         *     {@link String }  
 	186	+         *       
 	187	+         */  
        public void setAmount(String value) {  
            this.amount = value;  
        }  

        /**  
 	193	+         * Gets the value of the sequence property.  
 	194	+         *   
 	195	+         * @return  
 	196	+         *     possible object is  
 	197	+         *     {@link String }  
 	198	+         *       
 	199	+         */  
        public String getSequence() {  
            return sequence;  
        }  
  
        /**  
 	205	+         * Sets the value of the sequence property.  
 	206	+         *   
 	207	+         * @param value  
 	208	+         *     allowed object is  
 	209	+         *     {@link String }  
 	210	+         *       
 	211	+         */  
        public void setSequence(String value) {  
            this.sequence = value;  
        }  
  
    }  
  
} 
