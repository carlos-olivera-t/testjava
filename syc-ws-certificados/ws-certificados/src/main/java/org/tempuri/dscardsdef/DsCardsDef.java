package org.tempuri.dscardsdef; 
  
import java.util.ArrayList;  
import java.util.List;  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  
import javax.xml.bind.annotation.XmlType;  
  
  
/**  
 	14	+ * <p>Java class for anonymous complex type.  
 	15	+ *   
 	16	+ * <p>The following schema fragment specifies the expected content contained within this class.  
 	17	+ *   
 	18	+ * <pre>  
 	19	+ * &lt;complexType>  
 	20	+ *   &lt;complexContent>  
 	21	+ *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	22	+ *       &lt;choice maxOccurs="unbounded" minOccurs="0">  
 	23	+ *         &lt;element name="Cards">  
 	24	+ *           &lt;complexType>  
 	25	+ *             &lt;complexContent>  
 	26	+ *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	27	+ *                 &lt;sequence>  
 	28	+ *                   &lt;element name="CardNumber" minOccurs="0">  
 	29	+ *                     &lt;simpleType>  
 	30	+ *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">  
 	31	+ *                         &lt;maxLength value="16"/>  
 	32	+ *                       &lt;/restriction>  
 	33	+ *                     &lt;/simpleType>  
 	34	+ *                   &lt;/element>  
 	35	+ *                 &lt;/sequence>  
 	36	+ *               &lt;/restriction>  
 	37	+ *             &lt;/complexContent>  
 	38	+ *           &lt;/complexType>  
 	39	+ *         &lt;/element>  
 	40	+ *       &lt;/choice>  
 	41	+ *     &lt;/restriction>  
 	42	+ *   &lt;/complexContent>  
 	43	+ * &lt;/complexType>  
 	44	+ * </pre>  
 	45	+ *   
 	46	+ *   
 	47	+ */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "", propOrder = {  
    "cards"  
})  
@XmlRootElement(name = "dsCardsDef")  
public class DsCardsDef {  
  
    @XmlElement(name = "Cards")  
    protected List<DsCardsDef.Cards> cards;  
  
    /**  
 	59	+     * Gets the value of the cards property.  
 	60	+     *   
 	61	+     * <p>  
 	62	+     * This accessor method returns a reference to the live list,  
 	63	+     * not a snapshot. Therefore any modification you make to the  
 	64	+     * returned list will be present inside the JAXB object.  
 	65	+     * This is why there is not a <CODE>set</CODE> method for the cards property.  
 	66	+     *   
 	67	+     * <p>  
 	68	+     * For example, to add a new item, do as follows:  
 	69	+     * <pre>  
 	70	+     *    getCards().add(newItem);  
 	71	+     * </pre>  
 	72	+     *   
 	73	+     *   
 	74	+     * <p>  
 	75	+     * Objects of the following type(s) are allowed in the list  
 	76	+     * {@link DsCardsDef.Cards }  
 	77	+     *   
 	78	+     *   
 	79	+     */  
   public List<DsCardsDef.Cards> getCards() {  
        if (cards == null) {  
            cards = new ArrayList<DsCardsDef.Cards>();  
        }  
        return this.cards;  
    }  
  
  
    /**  
 	89	+     * <p>Java class for anonymous complex type.  
 	90	+     *   
 	91	+     * <p>The following schema fragment specifies the expected content contained within this class.  
 	92	+     *   
 	93	+     * <pre>  
 	94	+     * &lt;complexType>  
 	95	+     *   &lt;complexContent>  
 	96	+     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	97	+     *       &lt;sequence>  
 	98	+     *         &lt;element name="CardNumber" minOccurs="0">  
 	99	+     *           &lt;simpleType>  
 	100	+     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">  
 	101	+     *               &lt;maxLength value="16"/>  
 	102	+     *             &lt;/restriction>  
 	103	+     *           &lt;/simpleType>  
 	104	+     *         &lt;/element>  
 	105	+     *       &lt;/sequence>  
 	106	+     *     &lt;/restriction>  
 	107	+     *   &lt;/complexContent>  
 	108	+     * &lt;/complexType>  
 	109	+     * </pre>  
 	110	+     *   
 	111	+     *   
 	112	+     */  
    @XmlAccessorType(XmlAccessType.FIELD)  
    @XmlType(name = "", propOrder = {  
        "cardNumber"  
    })  
    public static class Cards {  
  
       @XmlElement(name = "CardNumber")  
        protected String cardNumber;  
  
        /**  
 	123	+         * Gets the value of the cardNumber property.  
 	124	+         *   
 	125	+         * @return  
 	126	+         *     possible object is  
 	127	+         *     {@link String }  
 	128	+         *       
 	129	+         */  
        public String getCardNumber() {  
            return cardNumber;  
        }  
        /**  
 	135	+         * Sets the value of the cardNumber property.  
 	136	+         *   
 	137	+         * @param value  
 	138	+         *     allowed object is  
 	139	+         *     {@link String }  
 	140	+         *       
 	141	+         */  
        public void setCardNumber(String value) {  
            this.cardNumber = value;  
        }  
  
    }  
  
}