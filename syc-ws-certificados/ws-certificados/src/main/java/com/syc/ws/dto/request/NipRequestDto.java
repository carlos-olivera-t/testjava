package com.syc.ws.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nipRequest", propOrder = {
    "numeroTarjeta",
    "nip"
})
public class NipRequestDto {
	
	private String numeroTarjeta;
	private String nip;
	
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	
	public String toString() {
		String buffer;
		buffer = "numeroTarjeta[ " + numeroTarjeta + " ]  ";
		buffer += "nip[ " + nip + " ]  ";
		return buffer;
	}
	
}
