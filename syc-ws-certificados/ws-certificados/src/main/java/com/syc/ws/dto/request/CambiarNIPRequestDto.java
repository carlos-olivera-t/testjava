package com.syc.ws.dto.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cambiaNIPRequest", propOrder = {
    "numeroTarjeta",
    "nipActual",
    "nipNuevo"
})
public class CambiarNIPRequestDto {
	
	private String numeroTarjeta;
	private String nipActual;
	private String nipNuevo;
	
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}
	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	
	public String getNipActual() {
		return nipActual;
	}
	public void setNipActual(String nipActual) {
		this.nipActual = nipActual;
	}
	
	public String getNipNuevo() {
		return nipNuevo;
	}
	public void setNipNuevo(String nipNuevo) {
		this.nipNuevo = nipNuevo;
	}
	
	public String toString() {
		String buffer;
		buffer = "numeroTarjeta[ " + numeroTarjeta + " ]  ";
		buffer += "nipActual[ " + nipActual + " ]  ";
		buffer += "nipNuevo[ " + nipNuevo + " ]  ";
		return buffer;
	}

}