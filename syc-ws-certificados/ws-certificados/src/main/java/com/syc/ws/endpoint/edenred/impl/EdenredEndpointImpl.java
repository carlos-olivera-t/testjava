package com.syc.ws.endpoint.edenred.impl;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syc.annotation.CheckPAN;
import com.syc.dto.request.DemographicDataRequestDto;
import com.syc.dto.request.LoadBalanceAccountRequest;
import com.syc.dto.request.NewEmployeeRequestDto;
import com.syc.dto.request.TransferRequestDto;
import com.syc.dto.response.BalanceMovementAcctRespDto;
import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.dto.response.BalanceQueryResponseDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.BasicDateResponseDto;
import com.syc.dto.response.BasicResponseDto;
import com.syc.dto.response.CardAssignmentResponseDto;
import com.syc.dto.response.StatusResponseDto;
import com.syc.dto.response.TransactionsListResponseDto;
import com.syc.dto.response.TransferResponseDto;
import com.syc.dto.response.UserQueryResponseDto;
import com.syc.services.AccountWithdrawalService;
import com.syc.services.CardHolderService;
import com.syc.services.CardIssuanceService;
import com.syc.services.CardService;
import com.syc.services.DepositAccountService;
import com.syc.services.NIPService;
import com.syc.services.monex.TransfersService;
import com.syc.utils.GeneralUtil;
import com.syc.utils.SystemConstants;
import com.syc.ws.dto.request.NipRequestDto;
import com.syc.ws.endpoint.edenred.EdenredEndpoint;

@Service("edenredEndpoint")
@WebService(endpointInterface="com.syc.ws.endpoint.edenred.EdenredEndpoint")
public class EdenredEndpointImpl implements EdenredEndpoint{
	
	private static final transient Log log = LogFactory.getLog(EdenredEndpointImpl.class);
	
	private static final String EDENRED_USER = "ws_edenred";
	/*********************************
	 * cambiar el valor por 2 cuando se pase a produccion
	 *********************************/
	private static final int CLAVE_EDENRED = 2;
	
	/** Inyectando las Dependencias **/
	@Autowired
	CardService cardService;
	
	@Autowired
	AccountWithdrawalService accountWithdrawalService;
	
	@Autowired
	CardIssuanceService cardIssuanceService;
	
	@Autowired
	CardHolderService cardholderService;
	
	@Autowired
	DepositAccountService depositService;
	
	@Autowired
	NIPService nipService;
	
	@Autowired
	TransfersService transferService;
	
	boolean          validationFlag = true;
	
	@CheckPAN(idParameter = 1, type = String.class)
	public BalanceQueryResponseDto getBalanceQuery( String cardNumber ) throws Exception{
		return cardService.balanceQuery(cardNumber, true, true, EDENRED_USER );
	}
	
	@CheckPAN(idParameter = 1, type = String.class)
	public StatusResponseDto getCardStatus( String cardNumber ) throws Exception{
		return cardService.cardStatusDate( cardNumber, EDENRED_USER );
	}
	
	@CheckPAN(idParameter = 1, type = String.class)
	public BasicAuthRespDto cardLock( String pan ) throws Exception{
		return cardService.cardLock(pan, EDENRED_USER);
	}
	
	@CheckPAN(idParameter = 1, type = String.class)
	public BasicAuthRespDto temporaryLock( String cardNumber, int operationType ) throws Exception{
		return cardService.temporaryLock( cardNumber, operationType );
	}
	
	@CheckPAN(idParameter = 1, type = String.class)
	public BalanceMovementRespDto loadBalance( String cardNumber, double amount){
		try {
			return depositService.doDepositAccount(cardNumber, amount, true, EDENRED_USER);
		} catch (Exception e) {
			BalanceMovementRespDto response = new BalanceMovementRespDto();
			response.setAuthorization("0");
			response.setBalance(0.0);
			response.setCode(50);
			response.setCurrentBalance(0.0);
			response.setDescription("no fue posible realizar transaccion, intentelo de nuevo!!");
			
			return response;
		}
	}
	
	
	public TransferResponseDto onlineTransfers( TransferRequestDto request ){
		
		return transferService.onlineTransfers(request, CLAVE_EDENRED);
	}
	
	@CheckPAN(idParameter = 1, type = String.class)
	public BasicDateResponseDto updateDemographicData(String pan, DemographicDataRequestDto demographicDataRequestDto){
		
		return cardholderService.updateDemographicData(pan, demographicDataRequestDto);
	}

	@CheckPAN(idParameter = 1, type = String.class)
	public CardAssignmentResponseDto stockCardReplacement(String pan, String account, String lockedCard) throws Exception {
		return cardIssuanceService.stockCardReplacement(account, pan, lockedCard, false, true, CLAVE_EDENRED, true);
	}
	
	@CheckPAN(idParameter = 1, type = NipRequestDto.class)
	public BasicResponseDto asignaNIP( NipRequestDto validarNipRequestDto){
		BasicResponseDto response;
		
		boolean validationFlag = true;
		
		/** Validando parámetros de Entrada **/
		if( GeneralUtil.isEmtyString(validarNipRequestDto.getNip()) )           validationFlag=false;
		if( GeneralUtil.isEmtyString(validarNipRequestDto.getNumeroTarjeta()) ) validationFlag=false;
		
		
		/** Generando Respuesta **/
		if( validationFlag ){
			response = nipService.pinAssignment( validarNipRequestDto.getNumeroTarjeta().trim(), 
												 validarNipRequestDto.getNip().trim(), EDENRED_USER);
			
		}	
		else{
			log.debug("Los parámetros de entrada son inválidos[ "+validarNipRequestDto.toString()+" ]");
			response = new BasicResponseDto( 5, "Verificar parámetros de entrada." );
		}
		return response;
		
	}
	
	
	public List<BalanceMovementAcctRespDto> depositAccount( List<LoadBalanceAccountRequest> request){
		
		try {
			return depositService.depositAccount(request, EDENRED_USER, CLAVE_EDENRED, true);
		} catch (Exception e) {
			List<BalanceMovementAcctRespDto> resp = new ArrayList<BalanceMovementAcctRespDto>();
			BalanceMovementAcctRespDto response = new BalanceMovementAcctRespDto();
			response.setAuthorization("0");
			response.setBalance(0.0);
			response.setCode(50);
			response.setCurrentBalance(0.0);
			response.setDescription("no fue posible realizar transaccion, intentelo de nuevo!!");
			resp.add(response);
			return resp;
		}
	}
	
	public List<BalanceMovementAcctRespDto> withdrawalAccount( List<LoadBalanceAccountRequest> request) throws Exception{
		try{
			return accountWithdrawalService.withdrawalAccount(request, EDENRED_USER, CLAVE_EDENRED);
		} catch (Exception e) {
			List<BalanceMovementAcctRespDto> resp = new ArrayList<BalanceMovementAcctRespDto>();
			BalanceMovementAcctRespDto response = new BalanceMovementAcctRespDto();
			response.setAuthorization("0");
			response.setBalance(0.0);
			response.setCode(50);
			response.setCurrentBalance(0.0);
			response.setDescription("no fue posible realizar transaccion, intentelo de nuevo!!");
			resp.add(response);
			return resp;
		}
	}
	@CheckPAN(idParameter = 1, type = String.class)
	public UserQueryResponseDto searchClient(String pan, String cliente, String bin ) throws Exception{
		List<String> bines = new ArrayList<String>();
		bines.add("50630301");
		bines.add("50630302");
		bines.add("50630303");
		bines.add("63631811");
		bines.add("63631803");
		if(bines.contains(bin)){
			return cardService.findByUserOrPan(pan, cliente, EDENRED_USER, bin);
		}else
			throw new org.springframework.security.access.AccessDeniedException(
					"operacion no permitida sobre el BIN [ " + bin + " ] ");
	}
	
	
	@CheckPAN(idParameter = 1, type = String.class)
	public CardAssignmentResponseDto stockCardHolderAssignment(String pan, String account, String employer, String employeeCode,
			String product, DemographicDataRequestDto demographicRequestDto) {	NewEmployeeRequestDto employee = new NewEmployeeRequestDto();
		employee.setEmployee(employeeCode);
		employee.setEmployer(employer);
		employee.setIssCode(CLAVE_EDENRED);
		employee.setProduct(product);
			return cardIssuanceService.stockCardAssignment(null, pan, demographicRequestDto, false, false, true, employee,false);
	
	}
	
	
	
	
	@Override
	@CheckPAN(idParameter = 1, type = String.class)
	public TransactionsListResponseDto getLastestTransactions(String pan, String initialDay, String finalDay,  int numMaxOfRows, int operationType) {
		
		return cardService.lastestTransactions(pan, initialDay, finalDay, operationType, numMaxOfRows, true, SystemConstants.CODIGO_GENERICO );
	}

	@Override
	@CheckPAN(idParameter = 1, type = String.class)
	public BasicAuthRespDto unLockCardByAttemptsToNIP(String pan) throws Exception {
		return cardService.unLockNIP(pan, EDENRED_USER);
	}
}
