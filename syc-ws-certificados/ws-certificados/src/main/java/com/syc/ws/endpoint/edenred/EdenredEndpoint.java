package com.syc.ws.endpoint.edenred;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.syc.annotation.CheckPAN;
import com.syc.dto.request.DemographicDataRequestDto;
import com.syc.dto.request.LoadBalanceAccountRequest;
import com.syc.dto.request.TransferRequestDto;
import com.syc.dto.response.BalanceMovementAcctRespDto;
import com.syc.dto.response.BalanceMovementRespDto;
import com.syc.dto.response.BalanceQueryResponseDto;
import com.syc.dto.response.BasicAuthRespDto;
import com.syc.dto.response.BasicDateResponseDto;
import com.syc.dto.response.BasicResponseDto;
import com.syc.dto.response.CardAssignmentResponseDto;
import com.syc.dto.response.StatusResponseDto;
import com.syc.dto.response.TransactionsListResponseDto;
import com.syc.dto.response.TransferResponseDto;
import com.syc.dto.response.UserQueryResponseDto;
import com.syc.ws.dto.request.NipRequestDto;

/********************************************
 * Interfaz para los servicios Web de 
 *        ACCOR - EDENRED
 * 
 * Autor: Angel Contreras Torres.
 * Fecha: 13/06/2011
 ********************************************/

@WebService
public interface EdenredEndpoint {
	
	/** Consulta de Saldo **/
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	public BalanceQueryResponseDto getBalanceQuery( @WebParam(name="pan") String pan ) throws Exception;
	
	/** Consulta de Estatus **/
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	public StatusResponseDto getCardStatus( @WebParam(name="pan") String pan ) throws Exception;
	
	/** Bloqueo Definitivo de Tarjeta **/
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	public BasicAuthRespDto cardLock( @WebParam(name="pan") String pan ) throws Exception;
	
	/**************************************** 
	 * Bloqueo y Desbloqueo Temporal 
	 * 		Operation Type
	 * 			==> [1] Bloquea Tarjeta
	 * 			==> [2] Desbloquea Tarjeta 
	 ****************************************/
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	public BasicAuthRespDto temporaryLock( @WebParam(name="pan")String pan, @WebParam(name="operationType")int operationType ) throws Exception;
	
	/** Dispersión de Saldo **/
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	public BalanceMovementRespDto loadBalance(@WebParam(name="pan")String pan, @WebParam(name="amount")double amount) throws Exception;
	
	@WebMethod
	public TransferResponseDto onlineTransfers( @WebParam(name= "request") TransferRequestDto request );
	
	/** Asignacion de Tarjetas de Stock**/
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	public BasicDateResponseDto updateDemographicData(@WebParam(name = "pan") String pan, @WebParam(name = "cardinfo") DemographicDataRequestDto cardInfo);

	/** Cambio de NIP **/
	@WebMethod()
	@CheckPAN(idParameter = 1, type = NipRequestDto.class)
	public BasicResponseDto asignaNIP( NipRequestDto validarNipRequestDto);
	
	/**
	 * Servicio de proposito General que se encarga de Reemplzar una tarjeta de STOCK
	 * @param account
	 * @param pan
	 * @param lockedCard
	 * @param demographicRequestDto
	 * @return
	 * @throws Exception 
	 */
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	CardAssignmentResponseDto stockCardReplacement(@WebParam(name="card") String pan, @WebParam(name="account") String account,@WebParam(name="lockedCard") String lockedCard ) throws Exception;

	/**
	 * Servicio que tiene como proposito el realizar depositos masivos por cuenta
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	@WebMethod
	List<BalanceMovementAcctRespDto> depositAccount( @WebParam(name = "deposito") List<LoadBalanceAccountRequest> request) throws Exception;
	
	/**
	 * Servicio que tiene como proposito el realizar retiros masivos por cuenta
	 * @param request
	 * @return
	 * @throws Exception 
	 */
	@WebMethod
	List<BalanceMovementAcctRespDto> withdrawalAccount( @WebParam(name = "retiro") List<LoadBalanceAccountRequest> request) throws Exception;
	
	/**
	 * Servicio que tiene como objetivo realizar la busqueda de un cliente ya sea por nombre o 
	 * por numero de tarjeta
	 * @param pan
	 * @param cliente
	 * @return
	 * @throws Exception
	 */
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	UserQueryResponseDto searchClient( @WebParam(name="card") String pan, @WebParam(name="nombreCliente") String cliente, @WebParam(name="bin") String bin ) throws Exception;

	/**
	 * Obtiene un listado de movimientos
	 * @param pan
	 * @param numMaxOfRows
	 * @param operationType
	 * @return TransactionsListResponseDto
	 */
	
//	@WebMethod
//	@CheckPAN(idParameter = 1, type = String.class)
//	public TransactionsListResponseDto getLastestTransactions1( @WebParam(name="pan") String pan, 
//															   @WebParam(name="numMaxOfRows") int numMaxOfRows, 
//															   @WebParam(name="operationType") int operationType) throws Exception;
//	
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	CardAssignmentResponseDto stockCardHolderAssignment( @WebParam(name="card") String pan,@WebParam(name="account") String account, 
														 @WebParam(name="employer") String employer, @WebParam(name="employeeCode") String employeeCode, 
														 @WebParam(name="product") String product,  @WebParam(name="cardInfo") DemographicDataRequestDto demographicRequestDto);
	
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	public TransactionsListResponseDto getLastestTransactions( @WebParam(name="pan") String pan, 
															   @WebParam(name="initialDay") String initialDay, 
															   @WebParam(name="finalDay") String finalDay,
															   @WebParam(name="numMaxOfRows") int numMaxOfRows,
															   @WebParam(name="operationType") int operationType);

	
	@WebMethod
	@CheckPAN(idParameter = 1, type = String.class)
	public BasicAuthRespDto unLockCardByAttemptsToNIP(@WebParam(name="pan") String pan) throws Exception;
	
	
}
