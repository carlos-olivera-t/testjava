package com.syc.ws.secure;

import java.io.IOException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.ws.security.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerPasswordCallback implements CallbackHandler {

    static Logger logger = LoggerFactory
            .getLogger(ServerPasswordCallback.class);

    public void handle(Callback[] callbacks) throws IOException,
            UnsupportedCallbackException {

        logger.debug("Validando certificado");

        for (int i = 0; i < callbacks.length; i++) {

            WSPasswordCallback pc = (WSPasswordCallback) callbacks[i];

            if (pc.getUsage() == WSPasswordCallback.SIGNATURE
                    || pc.getUsage() == WSPasswordCallback.DECRYPT) {

                logger.debug("El usuario proporcionado es: {}",  pc.getIdentifier());
                System.out.println( "El usuario proporcionado es: {}" + pc.getIdentifier());	
                if (pc.getIdentifier().equals("syc")) {
                    logger.debug("Password para server");
                    System.out.println("Password para server");
                    pc.setPassword("12345678");
                }
                if (pc.getIdentifier().equals("edenred")) {
                    logger.debug("Password para Edenred");
                    System.out.println("Password para Edenred");
                    pc.setPassword("12345678");
                }

            }

        }

    }
}