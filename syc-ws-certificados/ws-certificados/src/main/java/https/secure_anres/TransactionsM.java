package https.secure_anres;  
  
import java.util.HashMap;  
import java.util.Map;  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlAnyAttribute;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlType;  
import javax.xml.namespace.QName;  
  
  
/**  
 	15	+ * <p>Java class for TransactionsM complex type.  
 	16	+ *   
 	17	+ * <p>The following schema fragment specifies the expected content contained within this class.  
 	18	+ *   
 	19	+ * <pre>  
 	20	+ * &lt;complexType name="TransactionsM">  
 	21	+ *   &lt;complexContent>  
 	22	+ *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	23	+ *       &lt;sequence>  
 	24	+ *         &lt;element name="TransactionsM" type="{https://secure.anres.net/}ArrayOfTransactionM" minOccurs="0"/>  
 	25	+ *       &lt;/sequence>  
 	26	+ *       &lt;anyAttribute/>  
 	27	+ *     &lt;/restriction>  
 	28	+ *   &lt;/complexContent>  
 	29	+ * &lt;/complexType>  
 	30	+ * </pre>  
 	31	+ *   
 	32	+ *   
 	33	+ */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "TransactionsM", propOrder = {  
    "transactionsM"  
})  
public class TransactionsM {  
  
    @XmlElement(name = "TransactionsM")  
    protected ArrayOfTransactionM transactionsM;  
    @XmlAnyAttribute  
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();  
  
    /**  
 	46	+     * Gets the value of the transactionsM property.  
 	47	+     *   
 	48	+     * @return  
 	49	+     *     possible object is  
 	50	+     *     {@link ArrayOfTransactionM }  
 	51	+     *       
 	52	+     */  
    public ArrayOfTransactionM getTransactionsM() {  
        return transactionsM;  
    }  
  
    /**  
 	58	+     * Sets the value of the transactionsM property.  
 	59	+     *   
 	60	+     * @param value  
 	61	+     *     allowed object is  
 	62	+     *     {@link ArrayOfTransactionM }  
 	63	+     *       
 	64	+     */  
    public void setTransactionsM(ArrayOfTransactionM value) {  
        this.transactionsM = value;  
    }  
  
    /**  
 	70	+     * Gets a map that contains attributes that aren't bound to any typed property on this class.  
 	71	+     *   
 	72	+     * <p>  
 	73	+     * the map is keyed by the name of the attribute and   
 	74	+     * the value is the string value of the attribute.  
 	75	+     *   
 	76	+     * the map returned by this method is live, and you can add new attribute  
 	77	+     * by updating the map directly. Because of this design, there's no setter.  
 	78	+     *   
 	79	+     *   
 	80	+     * @return  
 	81	+     *     always non-null  
 	82	+     */  
    public Map<QName, String> getOtherAttributes() {  
        return otherAttributes;  
    }  
  
}