package https.secure_anres;  
  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlAnyElement;  
import javax.xml.bind.annotation.XmlRootElement;  
import javax.xml.bind.annotation.XmlType;  
 
  
/**  
 * <p>Java class for anonymous complex type.  
 *   
 * <p>The following schema fragment specifies the expected content contained within this class.  
 *   
 * <pre>  
 * &lt;complexType>  
 *   &lt;complexContent>  
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 *       &lt;sequence>  
 *         &lt;element name="dsCards" minOccurs="0">  
 *           &lt;complexType>  
 *             &lt;complexContent>  
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 *                 &lt;sequence>  
 *                   &lt;any namespace='http://tempuri.org/dsCardsDef.xsd'/>  
 *                 &lt;/sequence>  
 *               &lt;/restriction>  
 *             &lt;/complexContent>  
 *           &lt;/complexType>  
 *         &lt;/element>  
 *       &lt;/sequence>  
 *     &lt;/restriction>  
 *   &lt;/complexContent>  
 * &lt;/complexType>  
 * </pre>  
 *   
 *   
 */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "", propOrder = {  
    "dsCards"  
})  
@XmlRootElement(name = "getBalanceV")  
public class GetBalanceV {  
  
    protected GetBalanceV.DsCards dsCards;  
  
    /**  
     * Gets the value of the dsCards property.  
     *   
     * @return  
     *     possible object is  
     *     {@link GetBalanceV.DsCards }  
     *       
    */  
    public GetBalanceV.DsCards getDsCards() {  
        return dsCards;  
    }  
  
    /**  
     * Sets the value of the dsCards property.  
     *   
     * @param value  
     *     allowed object is  
     *     {@link GetBalanceV.DsCards }  
     *       
     */  
    public void setDsCards(GetBalanceV.DsCards value) {  
        this.dsCards = value;  
    }  
  
  
    /**  
     * <p>Java class for anonymous complex type.  
     *   
     * <p>The following schema fragment specifies the expected content contained within this class.  
     *   
     * <pre>  
     * &lt;complexType>  
     *   &lt;complexContent>  
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
     *       &lt;sequence>  
     *         &lt;any namespace='http://tempuri.org/dsCardsDef.xsd'/>  
     *       &lt;/sequence>  
     *     &lt;/restriction>  
     *   &lt;/complexContent>  
     * &lt;/complexType>  
     * </pre>  
     *   
     *   
     */  
    @XmlAccessorType(XmlAccessType.FIELD)  
    @XmlType(name = "", propOrder = {  
        "any"  
    })  
    public static class DsCards {  
  
        @XmlAnyElement(lax = true)  
        protected Object any;  
  
        /**  
         * Gets the value of the any property.  
         *   
        * @return  
         *     possible object is  
         *     {@link Object }  
         *       
         */  
       public Object getAny() {  
           return any;  
        }  
  
        /**  
        * Sets the value of the any property.  
         *   
        * @param value  
         *     allowed object is  
         *     {@link Object }  
         *       
        */  
        public void setAny(Object value) {  
            this.any = value;  
        }  
  
    }  
  
}  