package https.secure_anres;  
  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlType;  
  
  
/**  
 	11	+ * <p>Java class for TransactionM complex type.  
 	12	+ *   
 	13	+ * <p>The following schema fragment specifies the expected content contained within this class.  
 	14	+ *   
 	15	+ * <pre>  
 	16	+ * &lt;complexType name="TransactionM">  
 	17	+ *   &lt;complexContent>  
 	18	+ *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	19	+ *       &lt;sequence>  
 	20	+ *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>  
 	21	+ *         &lt;element name="MerchantName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>  
 	22	+ *         &lt;element name="AmountCompleted" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	23	+ *         &lt;element name="Fee" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	24	+ *         &lt;element name="LocalDate" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	25	+ *         &lt;element name="LocalTime" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	26	+ *         &lt;element name="ProcessingCode" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	27	+ *         &lt;element name="Matched" type="{http://www.w3.org/2001/XMLSchema}boolean"/>  
 	28	+ *       &lt;/sequence>  
 	29	+ *     &lt;/restriction>  
 	30	+ *   &lt;/complexContent>  
 	31	+ * &lt;/complexType>  
 	32	+ * </pre>  
 	33	+ *   
 	34	+ *   
 	35	+ */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "TransactionM", propOrder = {  
    "address",  
    "merchantName",  
    "amountCompleted",  
    "fee",  
    "localDate",  
    "localTime",  
   "processingCode",  
    "matched"  
})  
public class TransactionM {  
  
    @XmlElement(name = "Address")  
    protected String address;  
    @XmlElement(name = "MerchantName")  
    protected String merchantName;  
    @XmlElement(name = "AmountCompleted")  
    protected int amountCompleted;  
    @XmlElement(name = "Fee")  
    protected int fee;  
    @XmlElement(name = "LocalDate")  
    protected int localDate;  
    @XmlElement(name = "LocalTime")  
    protected int localTime;  
    @XmlElement(name = "ProcessingCode")  
    protected int processingCode;  
    @XmlElement(name = "Matched")  
    protected boolean matched;  
  
    /**  
 	67	+     * Gets the value of the address property.  
 	68	+     *   
 	69	+     * @return  
 	70	+     *     possible object is  
 	71	+     *     {@link String }  
 	72	+     *       
 	73	+     */  
    public String getAddress() {  
        return address;  
    }  
  
    /**  
 	79	+     * Sets the value of the address property.  
 	80	+     *   
 	81	+     * @param value  
 	82	+     *     allowed object is  
 	83	+     *     {@link String }  
 	84	+     *       
 	85	+     */  
    public void setAddress(String value) {  
        this.address = value;  
    }  
  
    /**  
 	91	+     * Gets the value of the merchantName property.  
 	92	+     *   
 	93	+     * @return  
 	94	+     *     possible object is  
 	95	+     *     {@link String }  
 	96	+     *       
 	97	+     */  
    public String getMerchantName() {  
        return merchantName;  
    }  
  
    /**  
 	103	+     * Sets the value of the merchantName property.  
 	104	+     *   
 	105	+     * @param value  
 	106	+     *     allowed object is  
 	107	+     *     {@link String }  
 	108	+     *       
 	109	+     */  
    public void setMerchantName(String value) {  
        this.merchantName = value;  
   }  
  
    /**  
 	115	+     * Gets the value of the amountCompleted property.  
 	116	+     *   
 	117	+     */  
    public int getAmountCompleted() {  
        return amountCompleted;  
    }  
  
    /**  
 	123	+     * Sets the value of the amountCompleted property.  
 	124	+     *   
 	125	+     */  
    public void setAmountCompleted(int value) {  
        this.amountCompleted = value;  
    }  
  
    /**  
 	131	+     * Gets the value of the fee property.  
 	132	+     *   
 	133	+     */  
    public int getFee() {  
        return fee;  
    }  
  
    /**  
 	139	+     * Sets the value of the fee property.  
 	140	+     *   
 	141	+     */  
    public void setFee(int value) {  
        this.fee = value;  
    }  
  
    /**  
 	147	+     * Gets the value of the localDate property.  
 	148	+     *   
 	149	+     */  
    public int getLocalDate() {  
        return localDate;  
    }  
  
    /**  
 	155	+     * Sets the value of the localDate property.  
 	156	+     *   
 	157	+     */  
    public void setLocalDate(int value) {  
        this.localDate = value;  
    }  
  
    /**  
 	163	+     * Gets the value of the localTime property.  
 	164	+     *   
 	165	+     */  
    public int getLocalTime() {  
        return localTime;  
    }  
  
    /**  
 	171	+     * Sets the value of the localTime property.  
 	172	+     *   
 	173	+     */  
    public void setLocalTime(int value) {  
        this.localTime = value;  
    }  
  
    /**  
 	179	+     * Gets the value of the processingCode property.  
 	180	+     *   
 	181	+     */  
    public int getProcessingCode() {  
        return processingCode;  
    }  
 
    /**  
 	187	+     * Sets the value of the processingCode property.  
 	188	+     *   
 	189	+     */  
    public void setProcessingCode(int value) {  
        this.processingCode = value;  
   }  
  
    /**  
 	195	+     * Gets the value of the matched property.  
 	196	+     *   
 	197	+     */  
    public boolean isMatched() {  
        return matched;  
    }  
 
    /**  
 	203	+     * Sets the value of the matched property.  
 	204	+     *   
 	205	+     */  
    public void setMatched(boolean value) {  
        this.matched = value;  
    }  
  
}  