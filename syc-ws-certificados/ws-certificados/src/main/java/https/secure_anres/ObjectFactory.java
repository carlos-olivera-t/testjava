package https.secure_anres;  
  
import javax.xml.bind.JAXBElement;  
import javax.xml.bind.annotation.XmlElementDecl;  
import javax.xml.bind.annotation.XmlRegistry;  
import javax.xml.namespace.QName;  
  
  
/**  
 	11	+ * This object contains factory methods for each   
 	12	+ * Java content interface and Java element interface   
 	13	+ * generated in the https.secure_anres package.   
 	14	+ * <p>An ObjectFactory allows you to programatically   
 	15	+ * construct new instances of the Java representation   
 	16	+ * for XML content. The Java representation of XML   
 	17	+ * content can consist of schema derived interfaces   
 	18	+ * and classes representing the binding of schema   
 	19	+ * type definitions, element declarations and model   
 	20	+ * groups.  Factory methods for each of these are   
 	21	+ * provided in this class.  
 	22	+ *   
 	23	+ */  
@XmlRegistry  
public class ObjectFactory {  
  
    private final static QName _Transactions_QNAME = new QName("https://secure.anres.net/", "Transactions");  
    private final static QName _TransactionsM_QNAME = new QName("https://secure.anres.net/", "TransactionsM");  
    private final static QName _Response_QNAME = new QName("https://secure.anres.net/", "Response");  
    private final static QName _MaintResponse_QNAME = new QName("https://secure.anres.net/", "MaintResponse");  
    private final static QName _ClientCredentials_QNAME = new QName("https://secure.anres.net/", "ClientCredentials");  
    private final static QName _BalanceResponse_QNAME = new QName("https://secure.anres.net/", "BalanceResponse");  
  
    /**  
 	35	+     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: https.secure_anres  
 	36	+     *   
 	37	+     */  
    public ObjectFactory() {  
    }  
  
    /**  
 	42	+     * Create an instance of {@link GetBalanceVResponse }  
 	43	+     *   
 	44	+     */  
    public GetBalanceVResponse createGetBalanceVResponse() {  
        return new GetBalanceVResponse();  
    }  
  
    /**  
 	50	+     * Create an instance of {@link GetBalanceV }  
 	51	+     *   
 	52	+     */  
    public GetBalanceV createGetBalanceV() {  
        return new GetBalanceV();  
    }  
  
    /**  
 	58	+     * Create an instance of {@link DepositVResponse }  
 	59	+     *   
 	60	+     */  
    public DepositVResponse createDepositVResponse() {  
       return new DepositVResponse();  
    }  
  
    /**  
 	66	+     * Create an instance of {@link DepositV }  
 	67	+     *   
 	68	+     */  
    public DepositV createDepositV() {  
       return new DepositV();  
   }  
 
    /**  
 	74	+     * Create an instance of {@link BalanceResponse }  
 	75	+     *   
 	76	+     */  
   public BalanceResponse createBalanceResponse() {  
       return new BalanceResponse();  
    }  
  
    /**  
 	82	+     * Create an instance of {@link ClientCredentials }  
 	83	+     *   
 	84	+     */  
    public ClientCredentials createClientCredentials() {  
        return new ClientCredentials();  
    }  
  
    /**  
 	90	+     * Create an instance of {@link CardSaleResponse }  
 	91	+     *   
 	92	+     */  
    public CardSaleResponse createCardSaleResponse() {  
        return new CardSaleResponse();  
    }  
  
    /**  
 	98	+     * Create an instance of {@link Response }  
 	99	+     *   
 	100	+     */  
    public Response createResponse() {  
        return new Response();  
    }  
  
    /**  
 	106	+     * Create an instance of {@link GetBalanceVResponse.GetBalanceVResult }  
 	107	+     *   
 	108	+     */  
    public GetBalanceVResponse.GetBalanceVResult createGetBalanceVResponseGetBalanceVResult() {  
        return new GetBalanceVResponse.GetBalanceVResult();  
    }  
  
   /**  
 	114	+     * Create an instance of {@link MaintResponse }  
 	115	+     *   
 	116	+     */  
    public MaintResponse createMaintResponse() {  
        return new MaintResponse();  
    }  
  
    /**  
 	122	+     * Create an instance of {@link CardSale }  
 	123	+     *   
 	124	+     */  
    public CardSale createCardSale() {  
        return new CardSale();  
   }  
  
    /**  
 	130	+     * Create an instance of {@link TransactionsM }  
 	131	+     *   
 	132	+     */  
    public TransactionsM createTransactionsM() {  
        return new TransactionsM();  
    }  
  
   /**  
 	138	+     * Create an instance of {@link Transactions }  
 	139	+     *   
 	140	+     */  
    public Transactions createTransactions() {  
        return new Transactions();  
    }  
  
    /**  
 	146	+     * Create an instance of {@link GetBalanceV.DsCards }  
 	147	+     *   
 	148	+     */  
    public GetBalanceV.DsCards createGetBalanceVDsCards() {  
        return new GetBalanceV.DsCards();  
    }  
  
    /**  
 	154	+     * Create an instance of {@link DepositVResponse.DepositVResult }  
 	155	+     *   
 	156	+     */  
    public DepositVResponse.DepositVResult createDepositVResponseDepositVResult() {  
       return new DepositVResponse.DepositVResult();  
    }  
  
   /**  
 	162	+     * Create an instance of {@link DepositV.DsCardsOp }  
 	163	+     *   
 	164	+     */  
    public DepositV.DsCardsOp createDepositVDsCardsOp() {  
        return new DepositV.DsCardsOp();  
    }  
  
    /**  
 	170	+     * Create an instance of {@link Transaction }  
 	171	+     *   
 	172	+     */  
    public Transaction createTransaction() {  
        return new Transaction();  
    }  
  
    /**  
 	178	+     * Create an instance of {@link TransactionM }  
 	179	+     *   
 	180	+     */  
    public TransactionM createTransactionM() {  
        return new TransactionM();  
    }  
  
    /**  
 	186	+     * Create an instance of {@link ArrayOfTransaction }  
 	187	+     *   
 	188	+     */  
    public ArrayOfTransaction createArrayOfTransaction() {  
        return new ArrayOfTransaction();  
   }  
  
    /**  
 	194	+     * Create an instance of {@link ArrayOfTransactionM }  
 	195	+     *   
 	196	+     */  
    public ArrayOfTransactionM createArrayOfTransactionM() {  
        return new ArrayOfTransactionM();  
    }  
  
    /**  
 	202	+     * Create an instance of {@link JAXBElement }{@code <}{@link Transactions }{@code >}}  
 	203	+     *   
 	204	+     */  
    @XmlElementDecl(namespace = "https://secure.anres.net/", name = "Transactions")  
   public JAXBElement<Transactions> createTransactions(Transactions value) {  
        return new JAXBElement<Transactions>(_Transactions_QNAME, Transactions.class, null, value);  
    }  
  
    /**  
 	211	+     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionsM }{@code >}}  
 	212	+     *   
 	213	+     */  
    @XmlElementDecl(namespace = "https://secure.anres.net/", name = "TransactionsM")  
   public JAXBElement<TransactionsM> createTransactionsM(TransactionsM value) {  
        return new JAXBElement<TransactionsM>(_TransactionsM_QNAME, TransactionsM.class, null, value);  
    }  
  
    /**  
 	220	+     * Create an instance of {@link JAXBElement }{@code <}{@link Response }{@code >}}  
 	221	+     *   
 	222	+     */  
    @XmlElementDecl(namespace = "https://secure.anres.net/", name = "Response")  
    public JAXBElement<Response> createResponse(Response value) {  
        return new JAXBElement<Response>(_Response_QNAME, Response.class, null, value);  
    }  
  
    /**  
 	229	+     * Create an instance of {@link JAXBElement }{@code <}{@link MaintResponse }{@code >}}  
 	230	+     *   
 	231	+     */  
    @XmlElementDecl(namespace = "https://secure.anres.net/", name = "MaintResponse")  
    public JAXBElement<MaintResponse> createMaintResponse(MaintResponse value) {  
        return new JAXBElement<MaintResponse>(_MaintResponse_QNAME, MaintResponse.class, null, value);  
    }  
  
    /**  
 	238	+     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}  
 	239	+     *   
 	240	+     */  
    @XmlElementDecl(namespace = "https://secure.anres.net/", name = "ClientCredentials")  
    public JAXBElement<ClientCredentials> createClientCredentials(ClientCredentials value) {  
        return new JAXBElement<ClientCredentials>(_ClientCredentials_QNAME, ClientCredentials.class, null, value);  
    }  
  
    /**  
 	247	+     * Create an instance of {@link JAXBElement }{@code <}{@link BalanceResponse }{@code >}}  
 	248	+     *   
 	249	+     */  
    @XmlElementDecl(namespace = "https://secure.anres.net/", name = "BalanceResponse")  
    public JAXBElement<BalanceResponse> createBalanceResponse(BalanceResponse value) {  
        return new JAXBElement<BalanceResponse>(_BalanceResponse_QNAME, BalanceResponse.class, null, value);  
    }  
  
}  