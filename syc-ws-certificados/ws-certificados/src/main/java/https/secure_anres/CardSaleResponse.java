package https.secure_anres;  
  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  
import javax.xml.bind.annotation.XmlType;  
  
  
/**  
 * <p>Java class for anonymous complex type.  
 *   
 * <p>The following schema fragment specifies the expected content contained within this class.  
 *   
 * <pre>  
 * &lt;complexType>  
 *   &lt;complexContent>  
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 *       &lt;sequence>  
 *         &lt;element name="CardSaleResult" type="{https://secure.anres.net/}Response" minOccurs="0"/>  
 *       &lt;/sequence>  
 *     &lt;/restriction>  
 *   &lt;/complexContent>  
 * &lt;/complexType>  
* </pre>  
 *   
 *   
 */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "", propOrder = {  
    "cardSaleResult"  
})  
@XmlRootElement(name = "CardSaleResponse")  
public class CardSaleResponse {  
  
    @XmlElement(name = "CardSaleResult")  
    protected Response cardSaleResult;  
  
    /**  
     * Gets the value of the cardSaleResult property.  
     *   
     * @return  
     *     possible object is  
     *     {@link Response }  
     *       
     */  
    public Response getCardSaleResult() {  
        return cardSaleResult;  
    }  
  
    /**  
    * Sets the value of the cardSaleResult property.  
     *   
     * @param value  
     *     allowed object is  
     *     {@link Response }  
     *       
     */  
    public void setCardSaleResult(Response value) {  
        this.cardSaleResult = value;  
    }  
  
}  