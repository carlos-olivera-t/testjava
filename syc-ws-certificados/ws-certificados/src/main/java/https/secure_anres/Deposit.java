package https.secure_anres;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cards"
})
@XmlRootElement(name = "Tarjetas")
public class Deposit {
	
	 @XmlElement(name = "Cards")
	 protected List<Deposit.Cards2> cards;
	
	 


	public List<Deposit.Cards2> getCards() {
		return cards;
	}




	public void setCards(List<Deposit.Cards2> cards) {
		this.cards = cards;
	}




	@XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "Cardnumber",
        "Amount",
        "sequence"    		
    })
    public static class Cards2 {
		
		private String Cardnumber;
		private String Amount;
		private String sequence;
		public String getCardnumber() {
			return Cardnumber;
		}
		public void setCardnumber(String cardnumber) {
			Cardnumber = cardnumber;
		}
		public String getAmount() {
			return Amount;
		}
		public void setAmount(String amount) {
			Amount = amount;
		}
		public String getSequence() {
			return sequence;
		}
		public void setSequence(String sequence) {
			this.sequence = sequence;
		}
		
		
	}

}