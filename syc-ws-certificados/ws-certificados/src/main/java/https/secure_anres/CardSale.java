package https.secure_anres;  
 
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlRootElement;  
import javax.xml.bind.annotation.XmlType;  
  
  
/**  
 * <p>Java class for anonymous complex type.  
 *   
 * <p>The following schema fragment specifies the expected content contained within this class.  
 *   
 * <pre>  
 * &lt;complexType>  
 *   &lt;complexContent>  
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 *       &lt;sequence>  
 *         &lt;element name="fromcardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>  
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 *       &lt;/sequence>  
 *     &lt;/restriction>  
 *   &lt;/complexContent>  
 * &lt;/complexType>  
 * </pre>  
 *   
 *   
 */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "", propOrder = {  
    "fromcardNumber",  
    "amount"  
})  
@XmlRootElement(name = "CardSale")  
public class CardSale {  
  
    protected String fromcardNumber;  
    protected String amount;  
  
    /**  
     * Gets the value of the fromcardNumber property.  
     *   
     * @return  
     *     possible object is  
     *     {@link String }  
     *       
     */  
    public String getFromcardNumber() {  
        return fromcardNumber;  
    }  
  
    /**  
     * Sets the value of the fromcardNumber property.  
     *   
     * @param value  
     *     allowed object is  
     *     {@link String }  
     *       
     */  
    public void setFromcardNumber(String value) {  
        this.fromcardNumber = value;  
     }  
  
    /**  
     * Gets the value of the amount property.  
     *   
     */  
    public String getAmount() {  
        return amount;  
    }  
  
    /**  
     * Sets the value of the amount property.  
     *   
     */  
    public void setAmount(String value) {  
        this.amount = value;  
    }  
  
} 