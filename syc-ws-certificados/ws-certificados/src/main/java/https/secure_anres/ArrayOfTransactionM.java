package https.secure_anres;  
  
import java.util.ArrayList;  
import java.util.List;  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlType;  
  
  
/**  
 * <p>Java class for ArrayOfTransactionM complex type.  
 *   
 * <p>The following schema fragment specifies the expected content contained within this class.  
 *   
 * <pre>  
 * &lt;complexType name="ArrayOfTransactionM">  
 *   &lt;complexContent>  
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 *       &lt;sequence>  
 *         &lt;element name="TransactionM" type="{https://secure.anres.net/}TransactionM" maxOccurs="unbounded" minOccurs="0"/>  
 *       &lt;/sequence>  
 *     &lt;/restriction>  
 *   &lt;/complexContent>  
 * &lt;/complexType>  
 * </pre>  
 *   
 *   
 */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "ArrayOfTransactionM", propOrder = {  
    "transactionM"  
})  
public class ArrayOfTransactionM {  
  
    @XmlElement(name = "TransactionM", nillable = true)  
    protected List<TransactionM> transactionM;  
 
    /**  
     * Gets the value of the transactionM property.  
     *   
      * <p>  
     * This accessor method returns a reference to the live list,  
     * not a snapshot. Therefore any modification you make to the  
     * returned list will be present inside the JAXB object.  
     * This is why there is not a <CODE>set</CODE> method for the transactionM property.  
     *   
     * <p>  
     * For example, to add a new item, do as follows:  
     * <pre>  
     *    getTransactionM().add(newItem);  
     * </pre>  
     *   
     *   
     * <p>  
     * Objects of the following type(s) are allowed in the list  
     * {@link TransactionM }  
     *   
     *   
     */  
    public List<TransactionM> getTransactionM() {  
        if (transactionM == null) {  
            transactionM = new ArrayList<TransactionM>();  
        }  
       return this.transactionM;  
    }  
  
 }  