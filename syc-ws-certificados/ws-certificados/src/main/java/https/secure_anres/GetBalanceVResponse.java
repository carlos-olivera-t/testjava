package https.secure_anres;  
  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlAnyElement;  
import javax.xml.bind.annotation.XmlRootElement;  
import javax.xml.bind.annotation.XmlType;  
  
  
/**  
 * <p>Java class for anonymous complex type.  
 *   
* <p>The following schema fragment specifies the expected content contained within this class.  
*   
* <pre>  
* &lt;complexType>  
 *   &lt;complexContent>  
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 *       &lt;sequence>  
 *         &lt;element name="getBalanceVResult" minOccurs="0">  
 *           &lt;complexType>  
 *             &lt;complexContent>  
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 *                 &lt;sequence>  
 *                   &lt;any namespace='http://tempuri.org/RBalances.xsd'/>  
 *                 &lt;/sequence>  
 *               &lt;/restriction>  
 *             &lt;/complexContent>  
 *           &lt;/complexType>  
 *         &lt;/element>  
 *       &lt;/sequence>  
 *     &lt;/restriction>  
 *   &lt;/complexContent>  
 * &lt;/complexType>  
 * </pre>  
 *   
 *   
 */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "", propOrder = {  
    "getBalanceVResult"  
})  
@XmlRootElement(name = "getBalanceVResponse")  
public class GetBalanceVResponse {  
  
    protected GetBalanceVResponse.GetBalanceVResult getBalanceVResult;  
  
    /**  
     * Gets the value of the getBalanceVResult property.  
     *   
     * @return  
     *     possible object is  
     *     {@link GetBalanceVResponse.GetBalanceVResult }  
    *       
     */  
    public GetBalanceVResponse.GetBalanceVResult getGetBalanceVResult() {  
        return getBalanceVResult;  
    }  
  
    /**  
     * Sets the value of the getBalanceVResult property.  
     *   
    * @param value  
     *     allowed object is  
     *     {@link GetBalanceVResponse.GetBalanceVResult }  
     *       
    */  
    public void setGetBalanceVResult(GetBalanceVResponse.GetBalanceVResult value) {  
        this.getBalanceVResult = value;  
    }  
  
  
    /**  
 	75	+     * <p>Java class for anonymous complex type.  
 	76	+     *   
 	77	+     * <p>The following schema fragment specifies the expected content contained within this class.  
 	78	+     *   
 	79	+     * <pre>  
 	80	+     * &lt;complexType>  
 	81	+     *   &lt;complexContent>  
 	82	+     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	83	+     *       &lt;sequence>  
 	84	+     *         &lt;any namespace='http://tempuri.org/RBalances.xsd'/>  
 	85	+     *       &lt;/sequence>  
 	86	+     *     &lt;/restriction>  
 	87	+     *   &lt;/complexContent>  
 	88	+     * &lt;/complexType>  
 	89	+     * </pre>  
 	90	+     *   
 	91	+     *   
 	92	+     */  
    @XmlAccessorType(XmlAccessType.FIELD)  
    @XmlType(name = "", propOrder = {  
        "any"  
    })  
    public static class GetBalanceVResult {  
  
        @XmlAnyElement(lax = true)  
        protected Object any;  
  
       /**  
 	103	+         * Gets the value of the any property.  
 	104	+         *   
 	105	+         * @return  
 	106	+         *     possible object is  
 	107	+         *     {@link Object }  
 	108	+         *       
 	109	+         */  
        public Object getAny() {  
            return any;  
        }  
  
        /**  
 	115	+         * Sets the value of the any property.  
 	116	+         *   
 	117	+         * @param value  
 	118	+         *     allowed object is  
 	119	+         *     {@link Object }  
 	120	+         *       
 	121	+         */  
        public void setAny(Object value) {  
            this.any = value;  
        }  
  
    }  
  
}  