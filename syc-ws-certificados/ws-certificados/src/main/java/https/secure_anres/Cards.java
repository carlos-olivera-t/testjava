package https.secure_anres;  
   
import java.util.List;  
  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  
import javax.xml.bind.annotation.XmlType;  
  
  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "", propOrder = {  
    "cards"  
})  
@XmlRootElement(name = "Tarjetas")  
public class Cards {  
	  
	 @XmlElement(name = "Cards")  
	 protected List<Cards.Cards2> cards;  
	  
	   
  
  
	public List<Cards.Cards2> getCards() {  
		return cards;  
	}  
  
  
  
 
	public void setCards(List<Cards.Cards2> cards) {  
		this.cards = cards;  
	}  
  
  
  
  
	@XmlAccessorType(XmlAccessType.FIELD)  
    @XmlType(name = "", propOrder = {  
         "CardNumber",  
        "Code"  
   		  
    })  
    public static class Cards2 {  
		  
		private String CardNumber;  
		private String Code;  
  
 		public String getCardNumber() {  
			return CardNumber;  
		}  
  
		public void setCardNumber(String cardNumber) {  
			CardNumber = cardNumber;  
		}  
  
		public String getCode() {  
			return Code;  
		}  
  
		public void setCode(String code) {  
			Code = code;  
		}  
		  
		  
		  
	}  
  
} 
