package https.secure_anres;  
  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlType;  
  
  
/**  
 	10	+ * <p>Java class for Response complex type.  
 	11	+ *   
 	12	+ * <p>The following schema fragment specifies the expected content contained within this class.  
 	13	+ *   
 	14	+ * <pre>  
 	15	+ * &lt;complexType name="Response">  
 	16	+ *   &lt;complexContent>  
 	17	+ *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	18	+ *       &lt;sequence>  
 	19	+ *         &lt;element name="responseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>  
 	20	+ *       &lt;/sequence>  
 	21	+ *     &lt;/restriction>  
 	22	+ *   &lt;/complexContent>  
 	23	+ * &lt;/complexType>  
 	24	+ * </pre>  
 	25	+ *   
 	26	+ *   
 	27	+ */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "Response", propOrder = {  
    "responseCode"  
})  
public class Response {  
  
    protected String responseCode;  
  
    /**  
 	37	+     * Gets the value of the responseCode property.  
 	38	+     *   
 	39	+     * @return  
 	40	+     *     possible object is  
 	41	+     *     {@link String }  
 	42	+     *       
 	43	+     */  
    public String getResponseCode() {  
        return responseCode;  
    }  
  
    /**  
 	49	+     * Sets the value of the responseCode property.  
 	50	+     *   
 	51	+     * @param value  
 	52	+     *     allowed object is  
 	53	+     *     {@link String }  
 	54	+     *       
 	55	+     */  
    public void setResponseCode(String value) {  
        this.responseCode = value;  
    }  
  
}  