package https.secure_anres;  
  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlType;  
  
  
/**  
 	11	+ * <p>Java class for Transaction complex type.  
 	12	+ *   
 	13	+ * <p>The following schema fragment specifies the expected content contained within this class.  
 	14	+ *   
 	15	+ * <pre>  
 	16	+ * &lt;complexType name="Transaction">  
 	17	+ *   &lt;complexContent>  
 	18	+ *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">  
 	19	+ *       &lt;sequence>  
 	20	+ *         &lt;element name="Address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>  
 	21	+ *         &lt;element name="MerchantName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>  
 	22	+ *         &lt;element name="AmountCompleted" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	23	+ *         &lt;element name="Fee" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	24	+ *         &lt;element name="LocalDate" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	25	+ *         &lt;element name="LocalTime" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	26	+ *         &lt;element name="ProcessingCode" type="{http://www.w3.org/2001/XMLSchema}int"/>  
 	27	+ *       &lt;/sequence>  
 	28	+ *     &lt;/restriction>  
 	29	+ *   &lt;/complexContent>  
 	30	+ * &lt;/complexType>  
 	31	+ * </pre>  
 	32	+ *   
 	33	+ *   
 	34	+ */  
@XmlAccessorType(XmlAccessType.FIELD)  
@XmlType(name = "Transaction", propOrder = {  
    "address",  
    "merchantName",  
    "amountCompleted",  
    "fee",  
    "localDate",  
    "localTime",  
    "processingCode"  
})  
public class Transaction {  
  
    @XmlElement(name = "Address")  
    protected String address;  
    @XmlElement(name = "MerchantName")  
    protected String merchantName;  
    @XmlElement(name = "AmountCompleted")  
    protected int amountCompleted;  
    @XmlElement(name = "Fee")  
    protected int fee;  
    @XmlElement(name = "LocalDate")  
    protected int localDate;  
    @XmlElement(name = "LocalTime")  
    protected int localTime;  
    @XmlElement(name = "ProcessingCode")  
    protected int processingCode;  
  
    /**  
 	63	+     * Gets the value of the address property.  
 	64	+     *   
 	65	+     * @return  
 	66	+     *     possible object is  
 	67	+     *     {@link String }  
 	68	+     *       
 	69	+     */  
    public String getAddress() {  
        return address;  
    }  
  
    /**  
 	75	+     * Sets the value of the address property.  
 	76	+     *   
 	77	+     * @param value  
 	78	+     *     allowed object is  
 	79	+     *     {@link String }  
 	80	+     *       
 	81	+     */  
    public void setAddress(String value) {  
        this.address = value;  
    }  
  
    /**  
 	87	+     * Gets the value of the merchantName property.  
 	88	+     *   
 	89	+     * @return  
 	90	+     *     possible object is  
 	91	+     *     {@link String }  
 	92	+     *       
 	93	+     */  
    public String getMerchantName() {  
        return merchantName;  
    }  
  
    /**  
 	99	+     * Sets the value of the merchantName property.  
 	100	+     *   
 	101	+     * @param value  
 	102	+     *     allowed object is  
 	103	+     *     {@link String }  
 	104	+     *       
 	105	+     */  
    public void setMerchantName(String value) {  
        this.merchantName = value;  
    }  
  
    /**  
 	111	+     * Gets the value of the amountCompleted property.  
 	112	+     *   
 	113	+     */  
   public int getAmountCompleted() {  
        return amountCompleted;  
    }  
  
    /**  
 	119	+     * Sets the value of the amountCompleted property.  
 	120	+     *   
 	121	+     */  
    public void setAmountCompleted(int value) {  
        this.amountCompleted = value;  
    }  
  
    /**  
 	127	+     * Gets the value of the fee property.  
 	128	+     *   
 	129	+     */  
    public int getFee() {  
        return fee;  
    }  
  
    /**  
 	135	+     * Sets the value of the fee property.  
 	136	+     *   
 	137	+     */  
    public void setFee(int value) {  
       this.fee = value;  
    }  
  
    /**  
 	143	+     * Gets the value of the localDate property.  
 	144	+     *   
 	145	+     */  
    public int getLocalDate() {  
        return localDate;  
    }  
 
    /**  
 	151	+     * Sets the value of the localDate property.  
 	152	+     *   
 	153	+     */  
    public void setLocalDate(int value) {  
        this.localDate = value;  
    }  
  
    /**  
 	159	+     * Gets the value of the localTime property.  
 	160	+     *   
 	161	+     */  
    public int getLocalTime() {  
        return localTime;  
    }  
  
    /**  
 	167	+     * Sets the value of the localTime property.  
 	168	+     *   
 	169	+     */  
    public void setLocalTime(int value) {  
        this.localTime = value;  
    }  
  
    /**  
 	175	+     * Gets the value of the processingCode property.  
 	176	+     *   
 	177	+     */  
    public int getProcessingCode() {  
        return processingCode;  
    }  
  
    /**  
 	183	+     * Sets the value of the processingCode property.  
 	184	+     *   
 	185	+     */  
    public void setProcessingCode(int value) {  
        this.processingCode = value;  
   }  
  
} 